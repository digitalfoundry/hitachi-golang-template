// this script adds an admin user

// useful links:
// https://www.arangodb.com/docs/stable/administration-managing-users.html
// https://www.arangodb.com/docs/stable/administration-managing-users-in-arangosh.html
// https://www.arangodb.com/docs/stable/data-modeling-databases-working-with.html#list-databases

// NOTE you can access environment variables, ex:
// print(process.env.arangodb_port)

let  userName = ARGUMENTS[0]
let password = ARGUMENTS[1]
if (ARGUMENTS.length < 2) {
    print("usage: setupAdminUser <userName> <password>")
    process.exit(1)
}
const seed = () => {
    const fs = require('fs');
    var versionRaw = fs.readFileSync('/version/db-version.json', 'utf8');
    const { version } = JSON.parse(versionRaw);

    let versionCollection = db._collection("version");
    print("checking database version", version);
    if(versionCollection){
        const exists = versionCollection.exists({_key: version});
        if(exists) {
            print("database version", version, "exists. Exiting.");
            return;
        }
    } else {
        db._create("version");
        versionCollection = db._collection("version");
    }
    print("database version", version, "does not exists. Seeding databases.");

    var dbsRaw = fs.readFileSync('/dbs/dbs.json', 'utf8');
    const databases = JSON.parse(dbsRaw);

    databases.forEach(function (dbMeta) {
        const checkDb = dbMeta.database;

        const dbs = db._databases();
        var dbExists = dbs.find(function (db) {
            return db === checkDb;
        })
        if(!dbExists){
            print("database", checkDb, "does not exist. Creating.");
            db._createDatabase(checkDb, {}, [{username: userName, passwd: password}]);
            print(checkDb, "created");
        }

        db._useDatabase(checkDb)
        const collections = dbMeta.collections
        collections.forEach(function (col) {
            const checkCollection = col
            var collection = db._collection(checkCollection);
            if(!collection) {
                print("collection", checkCollection, "does not exist. Creating.");
                collection = db._create(checkCollection);
                print(checkCollection, "created");
            }

            var dataRaw = fs.readFileSync('/seeds/' + checkDb + "/" + checkCollection + '_seed.json', 'utf8')
            print("seeding", checkCollection)
            const data = JSON.parse(dataRaw)
            data.forEach(function (elem) {
                collection.insert(elem, {overwrite: true})
            })
        })

        db._useDatabase("_system")
    })

    versionCollection.insert({_key: version})
}

seed()
