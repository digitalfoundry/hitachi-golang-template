#!/bin/sh

# fail if commands fail
set -ex

echo "Waiting for arangodb server to start -> https://${ARANGODB_SERVER}:${ARANGODB_PORT}/ping?verbose=true"
  until curl --fail -s -X GET http://root:${ARANGODB_PWD}@${ARANGODB_SERVER}:${ARANGODB_PORT}/_db/_system/_api/version; do
    echo -ne ".."
    sleep ${ARANGODB_WAIT_AVAIL_SECS}
  done

arangosh --server.endpoint http://${ARANGODB_SERVER}:${ARANGODB_PORT} --server.username ${ARANGODB_ADMIN_USER} --server.password ${ARANGODB_PWD} -javascript.execute /scripts/arangoSeeder.js ${ARANGODB_ADMIN_USER} ${ARANGODB_ADMIN_USER_PWD}
