package main

import (
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/assetsservice"
)

//main has comment
func main() {
	service := assetsservice.NewService()
	service.Init()
	service.Start()
}
