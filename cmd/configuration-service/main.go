package main

import "bitbucket.org/digitalfoundry/hitachi-golang-template/insights/configurationservice"

//main has comment
func main() {
	service := configurationservice.NewService()
	service.Init()
	service.Start()
}
