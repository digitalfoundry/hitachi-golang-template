package main

import (
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/cronservice"
)

func main() {
	service := cronservice.NewService()
	service.Init()
	service.Start()
}
