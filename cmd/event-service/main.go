package main

import "bitbucket.org/digitalfoundry/hitachi-golang-template/insights/eventservice"

// run the service
func main() {
	service := eventservice.NewService()
	service.Start()
}
