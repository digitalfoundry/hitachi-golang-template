package main

import "bitbucket.org/digitalfoundry/hitachi-golang-template/insights/kpiservice"

//main has comment
func main() {
	service := kpiservice.NewService()
	service.Init()
	service.Start()
}
