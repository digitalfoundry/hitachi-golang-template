package main

import "bitbucket.org/digitalfoundry/hitachi-golang-template/insights/service"

func main() {
	s := service.New()

	s.Start()
}
