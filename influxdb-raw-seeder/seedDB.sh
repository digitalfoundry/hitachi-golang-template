#!/bin/sh

set -ex

until curl  --fail -s -sl -I http://${INFLUXDB_RAW_HOST}:${INFLUXDB_RAW_PORT}/ping?verbose=true; do
  echo -ne ".."
  sleep 2
done
echo "Seeding influxDB-raw"
SEEDMODDATE=$(date -r /seed/rawInflux_seed.txt --rfc-2822)
curl -i -XPOST "http://${INFLUXDB_RAW_HOST}:${INFLUXDB_RAW_PORT}/write?db=KPI&u=${INFLUXDB_USERNAME}&p=${INFLUXDB_PASSWORD}" --data-binary @/seed/rawInflux_seed.txt
curl -i -XPOST "http://${INFLUXDB_RAW_HOST}:${INFLUXDB_RAW_PORT}/write?db=KPI&u=${INFLUXDB_USERNAME}&p=${INFLUXDB_PASSWORD}" --data-binary "db_version modDate=\"$SEEDMODDATE\" 1465839830100400201"

