package insights

// OIDCToken represents an open id connect token
type OIDCToken struct {
	AccessToken      string
	ExpiresIn        float64 // Access Token Lifespan
	RefreshExpiresIn float64 // SSO Session Idle in seconds
	RefreshToken     string
	TokenType        string
	TimeRetrieved    float64
}
