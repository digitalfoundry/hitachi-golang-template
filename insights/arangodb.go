package insights

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	driver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
)

const (
	retryAttempts = 10
	sleepTimer    = 5
)

// Binder represents values that need to be bound to a query string
type Binder struct {
	Name  string      // Name is the name of the binder in the query string
	Value interface{} // Value is the value of the binder
}

// Version is the version of the arangodb
type Version struct {
	Version string `json:"version"`
}

// ArangoDber represents an interface for interacting with a remote arangodb instance
type ArangoDBer interface {
	Connect(username string, password string) error
	GetCollection(dbName string, collectionName string) (ArangoCollection, error)
	Query(dbName string, query ArangoDBQuery, params ...Binder) (ArangoCursor, error)
}

// NewArangoDBer returns an arangodb interface
func NewArangoDBer(dbAddress string) ArangoDBer {
	return &arangoDB{
		dbAddress: dbAddress,
	}
}

// ArangoCollection is an interface to the arango go-driver Collection interface
// this is used to reduce the number of methods required to be mocked during testing
type ArangoCollection interface {
	CreateDocument(ctx context.Context, document interface{}) (driver.DocumentMeta, error)
	ReadDocument(ctx context.Context, key string, result interface{}) (driver.DocumentMeta, error)
	UpdateDocument(ctx context.Context, key string, update interface{}) (driver.DocumentMeta, error)
}

// ArangoCursor is an interface to the arango go-driver Collection interface
// this is used to reduce the number of methods required to be mocked during testing
type ArangoCursor interface {
	ReadDocument(ctx context.Context, result interface{}) (driver.DocumentMeta, error)
	Close() error
}

//ArangoDBQuery represents a raw arango sql query
type ArangoDBQuery struct {
	Query string
}

// PrepareQuery prepares the query templating in any parameters
func (q *ArangoDBQuery) PrepareQuery() string {
	return fmt.Sprintf(q.Query)
}

type arangoDB struct {
	dbAddress string
	client    driver.Client
}

// CheckDatabaseRunning ensures the database is in a valid state to be used by a service, if the database is not in a valid
// state, an error will be returned
func CheckDatabaseRunning(db ArangoDBer, versionFilePath string) error {
	versionFile, err := os.Open(versionFilePath)
	if err != nil {
		return fmt.Errorf("unable to open database version from file %v", err)
	}

	byteVersion, err := ioutil.ReadAll(versionFile)
	if err != nil {
		return fmt.Errorf("unable to convert database version file to byte array %v", err)
	}

	var version Version
	unmarshallErr := json.Unmarshal(byteVersion, &version)
	if unmarshallErr != nil {
		return unmarshallErr
	}

	fmt.Println("Checking database version")
	for i := 1; i <= retryAttempts; i++ {
		col, err := db.GetCollection("_system", "version")
		// if an error occurs, the database may not be up, set sleep timer and then continue
		if err != nil {
			fmt.Printf("error: %v\n", err.Error())
			time.Sleep(time.Second * time.Duration(sleepTimer))
			fmt.Printf("retry attempt %d/%d\n", i, retryAttempts)
			continue
		}

		tempVersion := Version{}
		_, err = col.ReadDocument(context.Background(), version.Version, &tempVersion)
		// if an error occurs, the database version may not have been set, set sleep timer and then continue
		if err != nil {
			fmt.Printf("error: %v\n", err.Error())
			time.Sleep(time.Second * time.Duration(sleepTimer))
			fmt.Printf("retry attempt %d/%d\n", i, retryAttempts)
			continue
		}
		// if the current version of the database is found, the service is clear to run the database
		fmt.Println("Database version found")
		return nil
	}
	return fmt.Errorf("unable to discover latest database version")
}

// Connect connects to a remote arango instance given a username and password
func (a *arangoDB) Connect(username string, password string) error {
	conn, connErr := http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{a.dbAddress},
	})
	if connErr != nil {
		return connErr
	}
	client, clientErr := driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication(username, password),
	})
	if clientErr != nil {
		return clientErr
	}

	a.client = client
	return nil
}

// GetCollection returns a arangodb  driver collection
func (a *arangoDB) GetCollection(dbName string, collectionName string) (ArangoCollection, error) {
	ctx := context.Background()
	db, dbErr := a.client.Database(ctx, dbName)
	if dbErr != nil {
		return nil, dbErr
	}
	return db.Collection(ctx, collectionName)
}

// Query returns arangodb cursor given a certain query
func (a *arangoDB) Query(dbName string, query ArangoDBQuery, params ...Binder) (ArangoCursor, error) {
	ctx1 := context.Background()
	db, dbErr := a.client.Database(ctx1, dbName)
	if dbErr != nil {
		return nil, dbErr
	}
	bindings := make(map[string]interface{})
	for _, param := range params {
		bindings[param.Name] = param.Value
	}
	return db.Query(ctx1, query.PrepareQuery(), bindings)
}
