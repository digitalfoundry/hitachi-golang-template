package assetsservice

import (
	"context"
	"fmt"

	"github.com/graph-gophers/graphql-go"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
	"github.com/arangodb/go-driver"
)

// Resolver is the root resolver for the configuration service. It contains all mutation and query resolver functions
type Resolver struct {
	db insights.ArangoDBer
}

type assetByNameArgs struct {
	Name string
}

type assetByIDArg struct {
	ID string
}

type assetsByIDsArg struct {
	AssetIds []string
}

type assetByTypeArg struct {
	Type string
}

type assetsByLikeArg struct {
	AssetNameFilter string
}

type assetsByDisplayNameLikeArg struct {
	AssetDisplayNameFilter string
}

// GetAllAssets returns all instances of assets from arango, returns nil asset and error on fail
func (r *Resolver) GetAllAssets(ctx context.Context) ([]*structs.AssetEntityResolver, error) {
	fmt.Println("Graphql endpoint getAllAssets accessed")
	query := insights.ArangoDBQuery{Query: "FOR d in assetEntity return d"}
	resolverArr, err := r.getMultipleAssetsQuery(ctx, query)
	if err != nil {
		return nil, err
	}

	fmt.Println("Successfully returned Graphql Resolvers for getAllAssets endpoint")
	return resolverArr, nil
}

// GetAssetByName returns an instances of an asset from arango that matches the given name in the argument,
// returns nil asset and error on fail
func (r *Resolver) GetAssetByName(ctx context.Context, args assetByNameArgs) (*structs.AssetEntityResolver, error) {
	fmt.Println("Graphql endpoint getAssetByName accessed")
	query := insights.ArangoDBQuery{Query: "FOR d IN assetEntity FILTER d.name == @name RETURN d"}
	param := insights.Binder{
		Name:  "name",
		Value: args.Name,
	}

	asset, err := r.getSingleAssetQuery(ctx, query, param)
	if err != nil {
		return nil, err
	}

	resolver := structs.NewAssetEntityResolver(asset, r.db)

	fmt.Println("Successfully returned Graphql Resolver for getAssetByName endpoint")
	return resolver, nil
}

// GetAssetByName returns an instances of an asset from arango that matches the given ID in the argument,
// returns nil asset and error on fail
func (r *Resolver) GetAssetByID(ctx context.Context, arg assetByIDArg) (*structs.AssetEntityResolver, error) {
	fmt.Println("Graphql endpoint getAssetById accessed")

	col, err := r.db.GetCollection("spring-demo", "assetEntity")
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to get assetEntity collection")
		return nil, err
	}

	asset, err := getAssetByID(ctx, col, arg.ID)
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to getAssetByID")
		return nil, err
	}

	resolver := structs.NewAssetEntityResolver(asset, r.db)

	fmt.Println("Successfully returned Graphql Resolver for getAssetById endpoint")
	return resolver, nil
}

// GetAssetsByType returns all assets with template ID's that match the type given, returns nil asset and error on fail
func (r *Resolver) GetAssetsByType(ctx context.Context, arg assetByTypeArg) ([]*structs.AssetEntityResolver, error) {
	fmt.Println("Graphql endpoint getAssetsByType accessed")
	query := insights.ArangoDBQuery{Query: "FOR d IN assetEntity FILTER d.templateId == @type return d"}
	param := insights.Binder{
		Name:  "type",
		Value: arg.Type,
	}

	resolverArr, err := r.getMultipleAssetsQuery(ctx, query, param)
	if err != nil {
		return nil, err
	}

	fmt.Println("Successfully returned Graphql Resolver for getAssetsByType endpoint")
	return resolverArr, nil
}

// GetAssetsByRelationType returns a list of assets that are source or destination entities in the specified assetRelationshipEntity type
func (r *Resolver) GetAssetsByRelationType(ctx context.Context, args assetByTypeArg) ([]*structs.AssetEntityResolver, error) {
	fmt.Println("Graphql endpoint getAssetsByRelationType accessed")
	query := insights.ArangoDBQuery{Query: "For a IN assetEntity FOR r in assetRelationshipEntity FILTER a._ID == r._TO " +
		"OR  a._ID == r._FROM AND r.relationshipType = @type RETURN a"}
	param := insights.Binder{
		Name:  "type",
		Value: args.Type,
	}

	resolverArr, err := r.getMultipleAssetsQuery(ctx, query, param)
	if err != nil {
		return nil, err
	}

	fmt.Println("Successfully returned Graphql Resolver for getAssetsByRelationType endpoint")
	return resolverArr, nil
}

// GetAssetsByIDs returns all assets with ID's that match the IDs given, returns nil asset and error on fail
func (r *Resolver) GetAssetsByIDs(ctx context.Context, args assetsByIDsArg) ([]*structs.AssetEntityResolver, error) {
	fmt.Println("Graphql endpoint getAssetsByIds accessed")
	col, err := r.db.GetCollection("spring-demo", "assetEntity")
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to create document in assetEntity collection")
		return nil, err
	}
	var resolverArr []*structs.AssetEntityResolver

	for _, id := range args.AssetIds {
		var asset structs.AssetEntity
		meta, err := col.ReadDocument(ctx, id, &asset)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			insights.LogErrorWithInfo(err, "", "Failed to read document on arango")
			return nil, err
		}
		asset.ID = graphql.ID(meta.Key)
		resolverArr = append(resolverArr, structs.NewAssetEntityResolver(&asset, r.db))
	}

	fmt.Println("Successfully returned Graphql Resolver for getAssetsByIds endpoint")
	return resolverArr, nil
}

// GetAssetsByNameLike returns all assets that have names that match with the given regex,
// returns nil asset and error on fail
func (r *Resolver) GetAssetsByNameLike(ctx context.Context, args assetsByLikeArg) ([]*structs.AssetEntityResolver, error) {
	fmt.Println("Graphql endpoint getAssetsByNameLike accessed")
	query := insights.ArangoDBQuery{Query: "FOR d IN assetEntity FILTER d.name LIKE @regex RETURN d"}
	param := insights.Binder{
		Name:  "regex",
		Value: args.AssetNameFilter,
	}

	resolverArr, err := r.getMultipleAssetsQuery(ctx, query, param)
	if err != nil {
		return nil, err
	}

	fmt.Println("Successfully returned Graphql Resolver for getAssetsByNameLike endpoint")
	return resolverArr, nil
}

// GetAssetsByDisplayNameLike returns all assets that have display names that match with the given regex,
// returns nil asset and error on fail
func (r *Resolver) GetAssetsByDisplayNameLike(ctx context.Context, args assetsByDisplayNameLikeArg) ([]*structs.AssetEntityResolver, error) {
	fmt.Println("Graphql endpoint getAssetsByDisplayNameLike accessed")
	query := insights.ArangoDBQuery{Query: "FOR d IN assetEntity FILTER d.displayName LIKE @regex RETURN d"}
	param := insights.Binder{
		Name:  "regex",
		Value: args.AssetDisplayNameFilter,
	}

	resolverArr, err := r.getMultipleAssetsQuery(ctx, query, param)
	if err != nil {
		return nil, err
	}

	fmt.Println("Successfully returned Graphql Resolver for getAssetsByDisplayNameLike endpoint")
	return resolverArr, nil
}

// getSingleAssetQuery is an abstraction for other single query item functions to call,
// returns asset entity on success, else returns error
func (r *Resolver) getSingleAssetQuery(ctx context.Context,
	query insights.ArangoDBQuery,
	params ...insights.Binder) (*structs.AssetEntity, error) {
	cursor, queryErr := r.db.Query("spring-demo", query, params...)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query spring-demo on arango")
		return nil, queryErr
	}
	defer insights.DeferHandleError(cursor.Close)

	var asset structs.AssetEntity
	meta, readErr := cursor.ReadDocument(ctx, &asset)
	if readErr != nil {
		if driver.IsNoMoreDocuments(readErr) {
			insights.LogErrorWithInfo(queryErr, "", "Asset not found")
		} else {
			insights.LogErrorWithInfo(queryErr, "", "Failed to read document on arango")
		}
		return nil, readErr
	}
	asset.ID = graphql.ID(meta.Key)

	return &asset, nil
}

// getMultipleAssetsQuery is an abstraction for other queries to call to return multiple assets
// returns asset entity array on success, else returns error
func (r *Resolver) getMultipleAssetsQuery(ctx context.Context,
	query insights.ArangoDBQuery,
	params ...insights.Binder) ([]*structs.AssetEntityResolver, error) {

	cursor, queryErr := r.db.Query("spring-demo", query, params...)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query spring-demo on arango")
		return nil, queryErr
	}
	defer insights.DeferHandleError(cursor.Close)

	var resolvers []*structs.AssetEntityResolver
	for {
		var asset structs.AssetEntity
		meta, readErr := cursor.ReadDocument(ctx, &asset)
		if driver.IsNoMoreDocuments(readErr) {
			break
		} else if readErr != nil {
			insights.LogErrorWithInfo(queryErr, "", "Failed to read document on arango")
			return nil, readErr
		}

		asset.ID = graphql.ID(meta.Key)
		resolvers = append(resolvers, structs.NewAssetEntityResolver(&asset, r.db))
	}
	return resolvers, nil
}

// getAssetByID retrieves a sing asset by id given id, return asset entity on success, else return error
func getAssetByID(ctx context.Context, col insights.ArangoCollection, id string) (*structs.AssetEntity, error) {
	var asset structs.AssetEntity
	meta, err := col.ReadDocument(ctx, id, &asset)
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to read document on arango")
		return nil, err
	}
	asset.ID = graphql.ID(meta.Key)

	return &asset, nil
}

// Todo add comments and tests to mutations once they are needed
// The below mutators and arguments do not appear to be used client side or for service to service calls
//type locationMutationArgs struct {
//	Description *string
//	DisplayName *string
//	LastUpdated *structs.Date
//	Latitude    float64
//	Longitude   float64
//	Name        string
//}
//
//type assetsMutationArgs struct {
//	Description       *string                           `json:"description"`
//	DisplayName       *string                           `json:"displayName"`
//	Image             *string                           `json:"image"`
//	LastUpdated       *structs.Date                     `json:"lastUpdated"`
//	Location          *structs.LocationIDReferenceInput `json:"location"`
//	Manufacturer      *string                           `json:"manufacturer"`
//	ManufacturingDate *structs.Date                     `json:"manufacturingDate"`
//	Model             *string                           `json:"model"`
//	Name              string                            `json:"name"`
//	Properties        *structs.PropertyIDReferenceInput `json:"properties"`
//	SerialNumber      *string                           `json:"serialNumber"`
//	TemplateID        string                            `json:"templateId"`
//}
//
//type assetRelationshipMutationArgs struct {
//	Description       *string                       `json:"description,omitempty"`
//	DestinationEntity structs.AssetIDReferenceInput `json:"destinationEntity"`
//	Direction         string                        `json:"direction"`
//	DisplayName       *string                       `json:"displayName,omitempty"`
//	LastUpdated       *structs.Date                 `json:"lastUpdated,omitempty"`
//	Name              *string                       `json:"name"`
//	RelationshipType  string                        `json:"relationshipType"`
//	SourceEntity      structs.AssetIDReferenceInput `json:"sourceEntity"`
//}
//
//type simplePropertyEntityCollectionMutationArgs struct {
//	Properties *[]*simplePropertyEntityMutationArgs
//}
//
//type simplePropertyEntityMutationArgs struct {
//	DataType    *string
//	Description *string
//	DisplayName *string
//	LastUpdated *structs.Date
//	Name        string
//	Value       *string
//}
//
//type simplePropertyEntityUpdateArgs struct {
//	AssetID  string
//	Property simplePropertyEntityMutationArgs
//}

//func (r *Resolver) CreateAssets(ctx context.Context, args struct{ Assets []*assetsMutationArgs }) ([]*structs.AssetEntityResolver, error) {
//	fmt.Println("Graphql endpoint createAssets accessed")
//	col, colErr := r.db.GetCollection("spring-demo", "assetEntity")
//	if colErr != nil {
//		insights.LogErrorWithInfo(colErr, "", "Failed to get assetEntity collection from arango")
//		return nil, colErr
//	}
//
//	tempTimestamp := structs.Date{Time: time.Now()}
//	var resolvers []*structs.AssetEntityResolver
//	for _, input := range args.Assets {
//		asset := &structs.AssetEntity{
//			Name:        input.Name,
//			TemplateID:  input.TemplateID,
//			CreatedDate: &tempTimestamp,
//			LastUpdated: &tempTimestamp,
//		}
//
//		setOptionalAssetFields(input, asset)
//
//		meta, createErr := col.CreateDocument(ctx, asset)
//		if createErr != nil {
//			insights.LogErrorWithInfo(colErr, "", "Failed to create document in assetEntity collection")
//			return nil, createErr
//		}
//		fmt.Printf("Document created with id (%s)\n", meta.Key)
//		asset.ID = graphql.ID(meta.Key)
//
//		resolvers = append(resolvers, structs.NewAssetEntityResolver(asset, r.db))
//	}
//
//	return resolvers, nil
//}
//
//func (r *Resolver) CreateRelations(ctx context.Context,
//	args struct {
//		Relations *[]*assetRelationshipMutationArgs
//	}) ([]*structs.AssetRelationshipEntityResolver, error) {
//	fmt.Println("Graphql endpoint createRelations accessed")
//	relationshipCol, colErr := r.db.GetCollection("spring-demo", "assetRelationships")
//	if colErr != nil {
//		insights.LogErrorWithInfo(colErr, "", "Failed to get assetRelationships collection from arango")
//		return nil, colErr
//	}
//	assetCol, colErr := r.db.GetCollection("spring-demo", "assetEntity")
//	if colErr != nil {
//		insights.LogErrorWithInfo(colErr, "", "Failed to get assetEntity collection from arango")
//		return nil, colErr
//	}
//
//	var resolvers []*structs.AssetRelationshipEntityResolver
//
//	tempTimestamp := structs.Date{time.Now()}
//	for _, relation := range *args.Relations {
//		destAsset, destErr := getAssetByID(ctx, assetCol, relation.DestinationEntity.ID)
//		if destErr != nil {
//			insights.LogErrorWithInfo(destErr, "", "Failed to getAssetByID")
//			return nil, destErr
//		}
//		srcAsset, srcErr := getAssetByID(ctx, assetCol, relation.SourceEntity.ID)
//		if srcErr != nil {
//			insights.LogErrorWithInfo(srcErr, "", "Failed to getAssetByID")
//			return nil, srcErr
//		}
//
//		destAsset.RelatedAssets = append(destAsset.RelatedAssets, srcAsset)
//		_, updateErr := assetCol.UpdateDocument(ctx, string(destAsset.ID), destAsset)
//		if updateErr != nil {
//			insights.LogErrorWithInfo(updateErr, "", "Failed to update relationship for assetEntity collection from arango")
//			return nil, updateErr
//		}
//
//		assetRelationship := structs.AssetRelationshipEntity{
//			DestinationEntity: *destAsset,
//			Direction:         relation.Direction,
//			LastUpdated:       &tempTimestamp,
//			RelationshipType:  relation.RelationshipType,
//			SourceEntity:      *srcAsset,
//		}
//
//		setOptionalAssetRelationshipEntityFields(*relation, &assetRelationship)
//
//		relMeta, createErr := relationshipCol.CreateDocument(ctx, assetRelationship)
//		if createErr != nil {
//			insights.LogErrorWithInfo(colErr, "", "Failed to create document in assetRelationships collection")
//			return nil, createErr
//		}
//		tempID := graphql.ID(relMeta.Key)
//		assetRelationship.ID = &tempID
//
//		resolvers = append(resolvers, structs.NewAssetRelationshipEntityResolver(&assetRelationship, r.db))
//	}
//
//	return resolvers, nil
//}
//
//func (r *Resolver) CreateProperties(ctx context.Context, args struct {
//	Properties simplePropertyEntityCollectionMutationArgs
//}) (*structs.SimplePropertyCollectionEntityResolver, error) {
//	fmt.Println("Graphql endpoint createProperties accessed")
//	col, colErr := r.db.GetCollection("spring-demo", "properties")
//	if colErr != nil {
//		insights.LogErrorWithInfo(colErr, "", "Failed to get properties collection from arango")
//		return nil, colErr
//	}
//
//	tempTimestamp := structs.Date{Time: time.Now()}
//
//	var props []*structs.SimplePropertyEntity
//	if args.Properties.Properties != nil {
//		for _, sp := range *args.Properties.Properties {
//			propEntity := structs.SimplePropertyEntity{
//				LastUpdated: tempTimestamp,
//				Name:        sp.Name,
//			}
//
//			setOptionalSimplePropertyEntityFields(*sp, &propEntity)
//			props = append(props, &propEntity)
//		}
//	}
//
//	spc := structs.SimplePropertyCollectionEntity{
//		LastUpdated: tempTimestamp,
//	}
//
//	if props != nil {
//		spc.Properties = props
//	}
//
//	meta, createErr := col.CreateDocument(ctx, spc)
//	if createErr != nil {
//		insights.LogErrorWithInfo(colErr, "", "Failed to create document in properties collection")
//		return nil, createErr
//	}
//	fmt.Printf("Document created with id (%s)\n", meta.Key)
//	spc.ID = graphql.ID(meta.Key)
//
//	resolver := structs.NewSimplePropertyCollectionEntityResolver(&spc)
//
//	return resolver, nil
//}
//
//func (r *Resolver) CreateLocation(ctx context.Context, args struct{ Location locationMutationArgs }) (*structs.LocationEntityResolver, error) {
//	fmt.Println("Graphql endpoint createLocation accessed")
//	col, colErr := r.db.GetCollection("spring-demo", "location")
//	if colErr != nil {
//		insights.LogErrorWithInfo(colErr, "", "Failed to get createLocation for location collection from arango")
//		return nil, colErr
//	}
//
//	tempTimestamp := structs.Date{Time: time.Now()}
//	location := structs.LocationEntity{
//		LastUpdated: &tempTimestamp,
//		Latitude:    args.Location.Latitude,
//		Longitude:   args.Location.Longitude,
//		Name:        args.Location.Name,
//	}
//
//	setOptionalLocationFields(args.Location, &location)
//
//	meta, createErr := col.CreateDocument(ctx, location)
//	if createErr != nil {
//		insights.LogErrorWithInfo(colErr, "", "Failed to create document in location collection")
//		return nil, createErr
//	}
//	fmt.Printf("Document created with id (%s)\n", meta.Key)
//	location.ID = graphql.ID(meta.Key)
//
//	resolver := structs.NewLocationEntityResolver(&location)
//
//	return resolver, nil
//}
//
//func (r *Resolver) UpdateAssets(ctx context.Context, args struct{ Assets []*assetsMutationArgs }) ([]*structs.AssetEntityResolver, error) {
//	fmt.Println("Graphql endpoint createAssets accessed")
//	col, colErr := r.db.GetCollection("spring-demo", "assetEntity")
//	if colErr != nil {
//		insights.LogErrorWithInfo(colErr, "", "Failed to get assetEntity collection from arango")
//		return nil, colErr
//	}
//
//	var resolvers []*structs.AssetEntityResolver
//	for _, input := range args.Assets {
//		var asset structs.AssetEntity
//		meta, readErr := col.ReadDocument(ctx, input.TemplateID, &asset)
//		if readErr != nil {
//			insights.LogErrorWithInfo(readErr, "", "Failed to read document from assetEntity collection")
//			return nil, readErr
//		}
//
//		asset.Name = input.Name
//		asset.TemplateID = input.TemplateID
//		tempTimestamp := structs.Date{Time: time.Now()}
//		asset.LastUpdated = &tempTimestamp
//		asset.ID = graphql.ID(meta.Key)
//
//		setOptionalAssetFields(input, &asset)
//
//		meta, updateErr := col.UpdateDocument(ctx, asset.TemplateID, asset)
//		if updateErr != nil {
//			insights.LogErrorWithInfo(colErr, "", "Failed to update document from assetEntity collection")
//			return nil, updateErr
//		}
//		fmt.Printf("Document updated with id (%s)\n", meta.Key)
//
//		resolvers = append(resolvers, structs.NewAssetEntityResolver(&asset, r.db))
//	}
//
//	return resolvers, nil
//}
//
//func (r *Resolver) UpdateProperty(ctx context.Context, args simplePropertyEntityUpdateArgs) (*structs.SimplePropertyEntityResolver, error) {
//	fmt.Println("Graphql endpoint updateProperty accessed")
//	assetCol, err := r.db.GetCollection("spring-demo", "assetEntity")
//	if err != nil {
//		insights.LogErrorWithInfo(err, "", "Failed to create document in assetEntity collection")
//		return nil, err
//	}
//
//	// get asset
//	asset, assetErr := getAssetByID(ctx, assetCol, args.AssetID)
//	if assetErr != nil {
//		insights.LogErrorWithInfo(assetErr, "", "Failed to getAssetByID")
//		return nil, assetErr
//	}
//
//	// read property object
//	propertiesCol, err := r.db.GetCollection("spring-demo", "properties")
//	if err != nil {
//		insights.LogErrorWithInfo(err, "", "Failed to create document in assetEntity collection")
//		return nil, err
//	}
//
//	// inflate new property with args
//	tempTimestamp := structs.Date{Time: time.Now()}
//	propEntity := structs.SimplePropertyEntity{
//		LastUpdated: tempTimestamp,
//		Name:        args.Property.Name,
//	}
//
//	setOptionalSimplePropertyEntityFields(args.Property, &propEntity)
//
//	// check asset has properties foreign key
//	collection := asset.Properties
//	if collection != "" {
//		var spc structs.SimplePropertyCollectionEntity
//		meta, err := propertiesCol.ReadDocument(ctx, collection, &spc)
//		if err != nil {
//			insights.LogErrorWithInfo(err, "", "Failed to read document on arango")
//			return nil, err
//		}
//		spc.ID = graphql.ID(meta.Key)
//
//		// append new property to properties collection
//		spc.Properties = append(spc.Properties, &propEntity)
//
//		// update collection with new property
//		updateMeta, updateErr := propertiesCol.UpdateDocument(ctx, meta.Key, spc)
//		if updateErr != nil {
//			return nil, updateErr
//		}
//
//		propEntity.ID = graphql.ID(updateMeta.Key)
//	} else {
//		tempTimestamp := structs.Date{Time: time.Now()}
//
//		var props []*structs.SimplePropertyEntity
//		props = append(props, &propEntity)
//
//		spc := structs.SimplePropertyCollectionEntity{
//			LastUpdated: tempTimestamp,
//			Properties:  props,
//		}
//
//		createMeta, createErr := propertiesCol.CreateDocument(ctx, &spc)
//		if createErr != nil {
//			return nil, createErr
//		}
//
//		asset.Properties = createMeta.Key
//		asset.LastUpdated = &tempTimestamp
//		propEntity.ID = graphql.ID(createMeta.Key)
//
//		_, updateErr := assetCol.UpdateDocument(ctx, string(asset.ID), &asset)
//		if updateErr != nil {
//			return nil, updateErr
//		}
//	}
//
//	return structs.NewSimplePropertyEntityResolver(&propEntity), nil
//}
//
//func setOptionalAssetFields(input *assetsMutationArgs, asset *structs.AssetEntity) {
//	if input.Location != nil {
//		// todo check location is valid
//		asset.Location = input.Location.ID
//	}
//
//	if input.Properties != nil {
//		// todo check properties valid
//		asset.Properties = input.Properties.ID
//	}
//
//	if input.SerialNumber != nil {
//		asset.SerialNumber = *input.SerialNumber
//	}
//
//	if input.Model != nil {
//		asset.Model = *input.Model
//	}
//
//	if input.ManufacturingDate != nil {
//		asset.ManufacturingDate = input.ManufacturingDate
//	}
//
//	if input.Manufacturer != nil {
//		asset.Manufacturer = *input.Manufacturer
//	}
//
//	if input.Image != nil {
//		asset.Image = *input.Image
//	}
//
//	if input.DisplayName != nil {
//		asset.DisplayName = *input.DisplayName
//	}
//
//	if input.Description != nil {
//		asset.Description = *input.Description
//	}
//}
//
//func setOptionalLocationFields(input locationMutationArgs, location *structs.LocationEntity) {
//	if input.Description != nil {
//		location.Description = *input.Description
//	}
//
//	if input.DisplayName != nil {
//		location.DisplayName = *input.DisplayName
//	}
//}
//
//func setOptionalSimplePropertyEntityFields(input simplePropertyEntityMutationArgs, sp *structs.SimplePropertyEntity) {
//	if input.DataType != nil {
//		sp.DataType = *input.DataType
//	}
//
//	if input.Description != nil {
//		sp.Description = *input.Description
//	}
//
//	if input.DisplayName != nil {
//		sp.DisplayName = *input.DisplayName
//	}
//
//	if input.Value != nil {
//		sp.Value = *input.Value
//	}
//}
//
//func setOptionalAssetRelationshipEntityFields(input assetRelationshipMutationArgs, ar *structs.AssetRelationshipEntity) {
//	if input.Description != nil {
//		ar.Description = input.Description
//	}
//
//	if input.DisplayName != nil {
//		ar.DisplayName = input.DisplayName
//	}
//
//	if input.Name != nil {
//		ar.Name = input.Name
//	}
//}
