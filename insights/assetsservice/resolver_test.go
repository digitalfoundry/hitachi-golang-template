package assetsservice

import (
	"context"
	"fmt"
	"testing"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
	"github.com/arangodb/go-driver"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestResolver_GetAllAssets(t *testing.T) {
	asset1 := &structs.AssetEntity{
		Name: "Asset1",
		ID:   "Asset1-id",
	}

	asset2 := &structs.AssetEntity{
		Name: "Asset2",
		ID:   "Asset2-id",
	}

	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorAssetEntity{}
	mockDb.On("Query", "spring-demo", mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset2).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	mockDbNoAssets := structs.NewMockArangoDBer()
	mockCursorNoAssets := &structs.MockArangoCursorAssetEntity{}
	mockDbNoAssets.On("Query", "spring-demo", mock.Anything).Return(mockCursorNoAssets, nil).Once()
	mockCursorNoAssets.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorNoAssets.On("Close").Return(nil).Once()

	mockDbReadFail := structs.NewMockArangoDBer()
	mockCursorReadFail := &structs.MockArangoCursorAssetEntity{}
	mockDbReadFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorReadFail, nil).Once()
	mockCursorReadFail.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()
	mockCursorReadFail.On("Close").Return(nil).Once()

	mockDbQueryFail := structs.NewMockArangoDBer()
	mockCursorQueryFail := &structs.MockArangoCursorAssetEntity{}
	mockDbQueryFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorQueryFail, fmt.Errorf("query error"))

	cases := map[string]struct {
		db             *structs.MockArangoDB
		cursor         *structs.MockArangoCursorAssetEntity
		errMsg         string
		expectedResult []*structs.AssetEntity
		queryCount     int
		readDocCount   int
		closeCount     int
	}{
		"success": {
			db:     mockDb,
			cursor: mockCursor,
			errMsg: "",
			expectedResult: []*structs.AssetEntity{
				asset1,
				asset2,
			},
			queryCount:   1,
			readDocCount: 3,
			closeCount:   1,
		},
		"query_fail": {
			db:             mockDbQueryFail,
			cursor:         mockCursorQueryFail,
			errMsg:         "query error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   0,
			closeCount:     0,
		},
		"no_assets": {
			db:             mockDbNoAssets,
			cursor:         mockCursorNoAssets,
			errMsg:         "",
			expectedResult: []*structs.AssetEntity{},
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
		"read_fail": {
			db:             mockDbReadFail,
			cursor:         mockCursorReadFail,
			errMsg:         "read error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := &Resolver{db: c.db}
			acResolver, getAllErr := resolver.GetAllAssets(context.Background())
			c.db.AssertNumberOfCalls(t, "Query", c.queryCount)
			c.cursor.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)
			c.cursor.AssertNumberOfCalls(t, "Close", c.closeCount)

			if getAllErr != nil {
				assert.Equal(t, c.errMsg, getAllErr.Error())
				assert.Nil(t, acResolver)
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.Equal(t, len(c.expectedResult), len(acResolver))
				if tc == "success" {
					firstACResolver := acResolver[0]
					secondACResolver := acResolver[1]
					assert.Equal(t, *firstACResolver.Name(), asset1.Name)
					assert.Equal(t, *secondACResolver.Name(), asset2.Name)
				}
			}
		})
	}
}

func TestResolver_GetAssetByName(t *testing.T) {
	asset := &structs.AssetEntity{
		Name: "Asset",
		ID:   "Asset-id",
	}

	mockDbSuccess := structs.NewMockArangoDBer()
	mockCursorSuccess := &structs.MockArangoCursorAssetEntity{}
	mockDbSuccess.On("Query", "spring-demo", mock.Anything).Return(mockCursorSuccess, nil).Once()
	mockCursorSuccess.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset).Once()
	mockCursorSuccess.On("Close").Return(nil).Once()

	mockDbNotFound := structs.NewMockArangoDBer()
	mockCursorNotFound := &structs.MockArangoCursorAssetEntity{}
	mockDbNotFound.On("Query", "spring-demo", mock.Anything).Return(mockCursorNotFound, nil).Once()
	mockCursorNotFound.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorNotFound.On("Close").Return(nil).Once()

	mockDbReadFail := structs.NewMockArangoDBer()
	mockCursorReadFail := &structs.MockArangoCursorAssetEntity{}
	mockDbReadFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorReadFail, nil).Once()
	mockCursorReadFail.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()
	mockCursorReadFail.On("Close").Return(nil).Once()

	mockDbQueryFail := structs.NewMockArangoDBer()
	mockCursorQueryFail := &structs.MockArangoCursorAssetEntity{}
	mockDbQueryFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorQueryFail, fmt.Errorf("query error"))

	cases := map[string]struct {
		db             *structs.MockArangoDB
		cursor         *structs.MockArangoCursorAssetEntity
		errMsg         string
		expectedResult *structs.AssetEntity
		queryCount     int
		readDocCount   int
		closeCount     int
	}{
		"success": {
			db:             mockDbSuccess,
			cursor:         mockCursorSuccess,
			errMsg:         "",
			expectedResult: asset,
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
		"query_fail": {
			db:             mockDbQueryFail,
			cursor:         mockCursorQueryFail,
			errMsg:         "query error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   0,
			closeCount:     0,
		},
		"not_found": {
			db:             mockDbNotFound,
			cursor:         mockCursorNotFound,
			errMsg:         "no more documents",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
		"read_fail": {
			db:             mockDbReadFail,
			cursor:         mockCursorReadFail,
			errMsg:         "read error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := &Resolver{db: c.db}
			assetResolver, err := resolver.GetAssetByName(context.Background(), assetByNameArgs{Name: asset.Name})

			c.db.AssertNumberOfCalls(t, "Query", c.queryCount)
			c.cursor.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)
			c.cursor.AssertNumberOfCalls(t, "Close", c.closeCount)

			if err != nil {
				assert.Equal(t, c.errMsg, err.Error())
				assert.Nil(t, assetResolver)
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.NotNil(t, assetResolver)
				assert.Equal(t, c.expectedResult.Name, *assetResolver.Name())
			}
		})
	}
}

func TestResolver_GetAssetByID(t *testing.T) {
	asset := &structs.AssetEntity{
		Name: "Asset",
		ID:   "Asset-id",
	}

	mockDbSuccess := structs.NewMockArangoDBer()
	mockCollectionSuccess := &structs.MockArangoCollectionAssetEntity{}
	mockDbSuccess.On("GetCollection", "spring-demo", mock.Anything).Return(mockCollectionSuccess, nil).Once()
	mockCollectionSuccess.On("ReadDocument", mock.Anything, mock.Anything, mock.Anything).Return(nil, asset).Once()

	mockDbNotFound := structs.NewMockArangoDBer()
	mockCollectionNotFound := &structs.MockArangoCollectionAssetEntity{}
	mockDbNotFound.On("GetCollection", "spring-demo", mock.Anything).Return(mockCollectionNotFound, nil).Once()
	mockCollectionNotFound.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()

	mockDbReadFail := structs.NewMockArangoDBer()
	mockCollectionReadFail := &structs.MockArangoCollectionAssetEntity{}
	mockDbReadFail.On("GetCollection", "spring-demo", mock.Anything).Return(mockCollectionReadFail, nil).Once()
	mockCollectionReadFail.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()

	mockDbCollectionFail := structs.NewMockArangoDBer()
	mockCollectionGetCollectionFail := &structs.MockArangoCollectionAssetEntity{}
	mockDbCollectionFail.On("GetCollection", "spring-demo", mock.Anything).Return(mockCollectionGetCollectionFail, fmt.Errorf("get collection error"))

	cases := map[string]struct {
		db                 *structs.MockArangoDB
		collection         *structs.MockArangoCollectionAssetEntity
		errMsg             string
		expectedResult     *structs.AssetEntity
		getCollectionCount int
		readDocCount       int
	}{
		"success": {
			db:                 mockDbSuccess,
			collection:         mockCollectionSuccess,
			errMsg:             "",
			expectedResult:     asset,
			getCollectionCount: 1,
			readDocCount:       1,
		},
		"get_collection_fail": {
			db:                 mockDbCollectionFail,
			collection:         mockCollectionGetCollectionFail,
			errMsg:             "get collection error",
			expectedResult:     nil,
			getCollectionCount: 1,
			readDocCount:       0,
		},
		"not_found": {
			db:                 mockDbNotFound,
			collection:         mockCollectionNotFound,
			errMsg:             "no more documents",
			expectedResult:     nil,
			getCollectionCount: 1,
			readDocCount:       1,
		},
		"read_fail": {
			db:                 mockDbReadFail,
			collection:         mockCollectionReadFail,
			errMsg:             "read error",
			expectedResult:     nil,
			getCollectionCount: 1,
			readDocCount:       1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := &Resolver{db: c.db}
			assetResolver, err := resolver.GetAssetByID(context.Background(), assetByIDArg{ID: "Asset-id"})
			c.db.AssertNumberOfCalls(t, "GetCollection", c.getCollectionCount)
			c.collection.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)

			if err != nil {
				assert.Equal(t, c.errMsg, err.Error())
				assert.Nil(t, assetResolver)
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.NotNil(t, assetResolver)
				assert.Equal(t, c.expectedResult.ID, *assetResolver.ID(context.Background()))
			}
		})
	}
}

func TestResolver_GetAssetsByType(t *testing.T) {
	asset1 := &structs.AssetEntity{
		Name:       "Asset1",
		ID:         "Asset1-id",
		TemplateID: "1",
	}

	asset2 := &structs.AssetEntity{
		Name:       "Asset2",
		ID:         "Asset2-id",
		TemplateID: "1",
	}

	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorAssetEntity{}
	mockDb.On("Query", "spring-demo", mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset2).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	mockDbNoAssets := structs.NewMockArangoDBer()
	mockCursorNoAssets := &structs.MockArangoCursorAssetEntity{}
	mockDbNoAssets.On("Query", "spring-demo", mock.Anything).Return(mockCursorNoAssets, nil).Once()
	mockCursorNoAssets.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorNoAssets.On("Close").Return(nil).Once()

	mockDbReadFail := structs.NewMockArangoDBer()
	mockCursorReadFail := &structs.MockArangoCursorAssetEntity{}
	mockDbReadFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorReadFail, nil).Once()
	mockCursorReadFail.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()
	mockCursorReadFail.On("Close").Return(nil).Once()

	mockDbQueryFail := structs.NewMockArangoDBer()
	mockCursorQueryFail := &structs.MockArangoCursorAssetEntity{}
	mockDbQueryFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorQueryFail, fmt.Errorf("query error"))

	cases := map[string]struct {
		db             *structs.MockArangoDB
		cursor         *structs.MockArangoCursorAssetEntity
		errMsg         string
		assetType      string
		expectedResult []*structs.AssetEntity
		queryCount     int
		readDocCount   int
		closeCount     int
	}{
		"success": {
			db:        mockDb,
			cursor:    mockCursor,
			errMsg:    "",
			assetType: "1",
			expectedResult: []*structs.AssetEntity{
				asset1,
				asset2,
			},
			queryCount:   1,
			readDocCount: 3,
			closeCount:   1,
		},
		"query_fail": {
			db:             mockDbQueryFail,
			cursor:         mockCursorQueryFail,
			errMsg:         "query error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   0,
			closeCount:     0,
		},
		"no_assets": {
			db:             mockDbNoAssets,
			cursor:         mockCursorNoAssets,
			errMsg:         "",
			expectedResult: []*structs.AssetEntity{},
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
		"read_fail": {
			db:             mockDbReadFail,
			cursor:         mockCursorReadFail,
			errMsg:         "read error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := &Resolver{db: c.db}
			acResolver, getAllErr := resolver.GetAssetsByType(context.Background(), assetByTypeArg{Type: c.assetType})
			c.db.AssertNumberOfCalls(t, "Query", c.queryCount)
			c.cursor.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)
			c.cursor.AssertNumberOfCalls(t, "Close", c.closeCount)

			if getAllErr != nil {
				assert.Equal(t, c.errMsg, getAllErr.Error())
				assert.Nil(t, acResolver)
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.Equal(t, len(c.expectedResult), len(acResolver))
				if tc == "success" {
					firstACResolver := acResolver[0]
					secondACResolver := acResolver[1]
					assert.Equal(t, *firstACResolver.Name(), asset1.Name)
					assert.Equal(t, *secondACResolver.Name(), asset2.Name)
				}
			}
		})
	}
}

func TestResolver_GetAssetsByIDs(t *testing.T) {
	asset1 := &structs.AssetEntity{
		Name:       "Asset1",
		ID:         "Asset1-id",
		TemplateID: "1",
	}

	asset2 := &structs.AssetEntity{
		Name:       "Asset2",
		ID:         "Asset2-id",
		TemplateID: "1",
	}

	ids := assetsByIDsArg{AssetIds: []string{
		"Asset1-id",
		"Asset2-id",
	}}

	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionAssetEntity{}
	mockDb.On("GetCollection", "spring-demo", "assetEntity").Return(mockCollection, nil).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything, mock.Anything).Return(nil, asset1).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything, mock.Anything).Return(nil, asset2).Once()

	mockDbNoAssets := structs.NewMockArangoDBer()
	mockCollectionNoAssets := &structs.MockArangoCollectionAssetEntity{}
	mockDbNoAssets.On("GetCollection", "spring-demo", "assetEntity").Return(mockCollectionNoAssets, nil).Once()
	mockCollectionNoAssets.On("ReadDocument", mock.Anything, mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()

	mockDbReadFail := structs.NewMockArangoDBer()
	mockCollectionReadFail := &structs.MockArangoCollectionAssetEntity{}
	mockDbReadFail.On("GetCollection", "spring-demo", "assetEntity").Return(mockCollectionReadFail, nil).Once()
	mockCollectionReadFail.On("ReadDocument", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()

	mockDbGetCollectionFail := structs.NewMockArangoDBer()
	mockCollectionQueryFail := &structs.MockArangoCollectionAssetEntity{}
	mockDbGetCollectionFail.On("GetCollection", "spring-demo", "assetEntity").Return(mockCollectionQueryFail, fmt.Errorf("query error"))

	cases := map[string]struct {
		db                 *structs.MockArangoDB
		collection         *structs.MockArangoCollectionAssetEntity
		errMsg             string
		assetType          string
		expectedResult     []*structs.AssetEntity
		getCollectionCount int
		readDocCount       int
	}{
		"success": {
			db:         mockDb,
			collection: mockCollection,
			errMsg:     "",
			assetType:  "1",
			expectedResult: []*structs.AssetEntity{
				asset1,
				asset2,
			},
			getCollectionCount: 1,
			readDocCount:       2,
		},
		"get_collection_fail": {
			db:                 mockDbGetCollectionFail,
			collection:         mockCollectionQueryFail,
			errMsg:             "query error",
			expectedResult:     nil,
			getCollectionCount: 1,
			readDocCount:       0,
		},
		"no_assets": {
			db:                 mockDbNoAssets,
			collection:         mockCollectionNoAssets,
			errMsg:             "",
			expectedResult:     []*structs.AssetEntity{},
			getCollectionCount: 1,
			readDocCount:       1,
		},
		"read_fail": {
			db:                 mockDbReadFail,
			collection:         mockCollectionReadFail,
			errMsg:             "read error",
			expectedResult:     nil,
			getCollectionCount: 1,
			readDocCount:       1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := &Resolver{db: c.db}
			acResolver, getAllErr := resolver.GetAssetsByIDs(context.Background(), ids)

			c.db.AssertNumberOfCalls(t, "GetCollection", c.getCollectionCount)
			c.collection.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)

			if getAllErr != nil {
				assert.Equal(t, c.errMsg, getAllErr.Error())
				assert.Nil(t, acResolver)
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.Equal(t, len(c.expectedResult), len(acResolver))
				if tc == "success" {
					firstACResolver := acResolver[0]
					secondACResolver := acResolver[1]
					assert.Equal(t, *firstACResolver.Name(), asset1.Name)
					assert.Equal(t, *secondACResolver.Name(), asset2.Name)
				}
			}
		})
	}
}

func TestResolver_GetAssetsByNameLike(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorAssetEntity{}

	asset1 := &structs.AssetEntity{
		Name:       "Asset1",
		ID:         "Asset1-id",
		TemplateID: "1",
	}

	asset2 := &structs.AssetEntity{
		Name:       "Asset2",
		ID:         "Asset2-id",
		TemplateID: "1",
	}

	mockDb.On("Query", "spring-demo", mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset2).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	mockDbNoAssets := structs.NewMockArangoDBer()
	mockCursorNoAssets := &structs.MockArangoCursorAssetEntity{}
	mockDbNoAssets.On("Query", "spring-demo", mock.Anything).Return(mockCursorNoAssets, nil).Once()
	mockCursorNoAssets.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorNoAssets.On("Close").Return(nil).Once()

	mockDbReadFail := structs.NewMockArangoDBer()
	mockCursorReadFail := &structs.MockArangoCursorAssetEntity{}
	mockDbReadFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorReadFail, nil).Once()
	mockCursorReadFail.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()
	mockCursorReadFail.On("Close").Return(nil).Once()

	mockDbQueryFail := structs.NewMockArangoDBer()
	mockCursorQueryFail := &structs.MockArangoCursorAssetEntity{}
	mockDbQueryFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorQueryFail, fmt.Errorf("query error"))

	cases := map[string]struct {
		db             *structs.MockArangoDB
		cursor         *structs.MockArangoCursorAssetEntity
		errMsg         string
		assetType      string
		expectedResult []*structs.AssetEntity
		queryCount     int
		readDocCount   int
		closeCount     int
	}{
		"success": {
			db:        mockDb,
			cursor:    mockCursor,
			errMsg:    "",
			assetType: "1",
			expectedResult: []*structs.AssetEntity{
				asset1,
				asset2,
			},
			queryCount:   1,
			readDocCount: 3,
			closeCount:   1,
		},
		"query_fail": {
			db:             mockDbQueryFail,
			cursor:         mockCursorQueryFail,
			errMsg:         "query error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   0,
			closeCount:     0,
		},
		"no_assets": {
			db:             mockDbNoAssets,
			cursor:         mockCursorNoAssets,
			errMsg:         "",
			expectedResult: []*structs.AssetEntity{},
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
		"read_fail": {
			db:             mockDbReadFail,
			cursor:         mockCursorReadFail,
			errMsg:         "read error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := &Resolver{db: c.db}
			acResolver, getAllErr := resolver.GetAssetsByNameLike(context.Background(), assetsByLikeArg{AssetNameFilter: "Asset_"})

			c.db.AssertNumberOfCalls(t, "Query", c.queryCount)
			c.cursor.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)
			c.cursor.AssertNumberOfCalls(t, "Close", c.closeCount)

			if getAllErr != nil {
				assert.Equal(t, c.errMsg, getAllErr.Error())
				assert.Nil(t, acResolver)
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.Equal(t, len(c.expectedResult), len(acResolver))
				if tc == "success" {
					firstACResolver := acResolver[0]
					secondACResolver := acResolver[1]
					assert.Equal(t, *firstACResolver.Name(), asset1.Name)
					assert.Equal(t, *secondACResolver.Name(), asset2.Name)
				}
			}
		})
	}
}

func TestResolver_GetAssetsByDisplayNameLike(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorAssetEntity{}

	asset1 := &structs.AssetEntity{
		Name:        "Asset1",
		DisplayName: "Display Name 1",
		ID:          "Asset1-id",
		TemplateID:  "1",
	}

	asset2 := &structs.AssetEntity{
		Name:        "Asset2",
		DisplayName: "Display Name 2",
		ID:          "Asset2-id",
		TemplateID:  "1",
	}

	mockDb.On("Query", "spring-demo", mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset2).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	mockDbNoAssets := structs.NewMockArangoDBer()
	mockCursorNoAssets := &structs.MockArangoCursorAssetEntity{}
	mockDbNoAssets.On("Query", "spring-demo", mock.Anything).Return(mockCursorNoAssets, nil).Once()
	mockCursorNoAssets.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorNoAssets.On("Close").Return(nil).Once()

	mockDbReadFail := structs.NewMockArangoDBer()
	mockCursorReadFail := &structs.MockArangoCursorAssetEntity{}
	mockDbReadFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorReadFail, nil).Once()
	mockCursorReadFail.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()
	mockCursorReadFail.On("Close").Return(nil).Once()

	mockDbQueryFail := structs.NewMockArangoDBer()
	mockCursorQueryFail := &structs.MockArangoCursorAssetEntity{}
	mockDbQueryFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorQueryFail, fmt.Errorf("query error"))

	cases := map[string]struct {
		db             *structs.MockArangoDB
		cursor         *structs.MockArangoCursorAssetEntity
		errMsg         string
		assetType      string
		expectedResult []*structs.AssetEntity
		queryCount     int
		readDocCount   int
		closeCount     int
	}{
		"success": {
			db:        mockDb,
			cursor:    mockCursor,
			errMsg:    "",
			assetType: "1",
			expectedResult: []*structs.AssetEntity{
				asset1,
				asset2,
			},
			queryCount:   1,
			readDocCount: 3,
			closeCount:   1,
		},
		"query_fail": {
			db:             mockDbQueryFail,
			cursor:         mockCursorQueryFail,
			errMsg:         "query error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   0,
			closeCount:     0,
		},
		"no_assets": {
			db:             mockDbNoAssets,
			cursor:         mockCursorNoAssets,
			errMsg:         "",
			expectedResult: []*structs.AssetEntity{},
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
		"read_fail": {
			db:             mockDbReadFail,
			cursor:         mockCursorReadFail,
			errMsg:         "read error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := &Resolver{db: c.db}
			acResolver, getAllErr := resolver.GetAssetsByDisplayNameLike(context.Background(), assetsByDisplayNameLikeArg{AssetDisplayNameFilter: "^[a-zA-Z0-9_]$"})

			c.db.AssertNumberOfCalls(t, "Query", c.queryCount)
			c.cursor.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)
			c.cursor.AssertNumberOfCalls(t, "Close", c.closeCount)

			if getAllErr != nil {
				assert.Equal(t, c.errMsg, getAllErr.Error())
				assert.Nil(t, acResolver)
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.Equal(t, len(c.expectedResult), len(acResolver))
				if tc == "success" {
					firstACResolver := acResolver[0]
					secondACResolver := acResolver[1]
					assert.Equal(t, *firstACResolver.Name(), asset1.Name)
					assert.Equal(t, *secondACResolver.Name(), asset2.Name)
				}
			}
		})
	}
}

func TestResolver_GetAssetsByRelationType(t *testing.T) {
	asset := &structs.AssetEntity{
		Name: "Asset",
		ID:   "Asset-id",
	}

	mockDbSuccess := structs.NewMockArangoDBer()
	mockCursorSuccess := &structs.MockArangoCursorAssetEntity{}
	mockDbSuccess.On("Query", "spring-demo", mock.Anything).Return(mockCursorSuccess, nil).Once()
	mockCursorSuccess.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, asset).Once()
	mockCursorSuccess.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorSuccess.On("Close").Return(nil).Once()

	mockDbReadFail := structs.NewMockArangoDBer()
	mockCursorReadFail := &structs.MockArangoCursorAssetEntity{}
	mockDbReadFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorReadFail, nil).Once()
	mockCursorReadFail.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()
	mockCursorReadFail.On("Close").Return(nil).Once()

	mockDbQueryFail := structs.NewMockArangoDBer()
	mockCursorQueryFail := &structs.MockArangoCursorAssetEntity{}
	mockDbQueryFail.On("Query", "spring-demo", mock.Anything).Return(mockCursorQueryFail, fmt.Errorf("query error"))

	cases := map[string]struct {
		db             *structs.MockArangoDB
		cursor         *structs.MockArangoCursorAssetEntity
		errMsg         string
		expectedResult *structs.AssetEntity
		queryCount     int
		readDocCount   int
		closeCount     int
	}{
		"success": {
			db:             mockDbSuccess,
			cursor:         mockCursorSuccess,
			errMsg:         "",
			expectedResult: asset,
			queryCount:     1,
			readDocCount:   2,
			closeCount:     1,
		},
		"query_fail": {
			db:             mockDbQueryFail,
			cursor:         mockCursorQueryFail,
			errMsg:         "query error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   0,
			closeCount:     0,
		},
		"read_fail": {
			db:             mockDbReadFail,
			cursor:         mockCursorReadFail,
			errMsg:         "read error",
			expectedResult: nil,
			queryCount:     1,
			readDocCount:   1,
			closeCount:     1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := &Resolver{db: c.db}
			assetResolver, err := resolver.GetAssetsByRelationType(context.Background(), assetByTypeArg{Type: "healthy"})

			c.db.AssertNumberOfCalls(t, "Query", c.queryCount)
			c.cursor.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)
			c.cursor.AssertNumberOfCalls(t, "Close", c.closeCount)

			if err != nil {
				assert.Equal(t, c.errMsg, err.Error())
				assert.Nil(t, assetResolver)
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.NotNil(t, assetResolver)
			}
		})
	}
}
