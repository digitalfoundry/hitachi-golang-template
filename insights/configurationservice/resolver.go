package configurationservice

import (
	"context"
	"fmt"
	"time"

	"github.com/graph-gophers/graphql-go"

	"github.com/arangodb/go-driver"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
)

const globalConfigID = "__global"

// Resolver is the root resolver for the configuration service. It contains all mutation and query resolver functions
type Resolver struct {
	db insights.ArangoDBer
}

// GetAllEventTypes is the graphql endpoint for returning all event types
func (r *Resolver) GetAllEventTypes(ctx context.Context) ([]structs.EventTypeInfoResolver, error) {
	fmt.Println("Graphql endpoint getAllEventTypes accessed")

	query := insights.ArangoDBQuery{Query: "FOR e IN eventConfiguration RETURN e"}

	cursor, err := r.db.Query("config-db", query)
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to query config-db on arango")
		return nil, err
	}
	defer insights.DeferHandleError(cursor.Close)
	var eTypes []structs.EventTypeInfoResolver
	for {
		var eConf structs.EventConfiguration
		_, readErr := cursor.ReadDocument(ctx, &eConf)
		if readErr != nil {
			if driver.IsNoMoreDocuments(readErr) {
				break
			}
			insights.LogErrorWithInfo(readErr, "", "Failed to read event configuration from arango")
			return nil, readErr
		}
		eventID := graphql.ID(eConf.EventTypeID)
		eventTypeInfo := structs.EventTypeInfo{
			EventTypeID:          &eventID,
			EventTypeDisplayName: eConf.EventTypeDisplayName,
		}

		eTypes = append(eTypes, *structs.NewEventTypeInfoResolver(&eventTypeInfo))
	}

	return eTypes, nil
}

// GetGlobalEventConfiguration is the graphql endpoint for getting or creating the global event configuration
func (r *Resolver) GetGlobalEventConfiguration(ctx context.Context) (*structs.EventConfigurationResolver, error) {
	fmt.Println("Graphql endpoint getGlobalEventConfiguration accessed")

	col, err := r.db.GetCollection("config-db", "eventConfiguration")
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to get eventConfiguration collection")
		return nil, err
	}

	eventConfiguration, getConfigErr := getEventConfigurationByID(ctx, col, globalConfigID)
	if getConfigErr != nil {
		if driver.IsNotFound(getConfigErr) {
			return createGlobalEventConfiguration(ctx, col)
		}
		insights.LogErrorWithInfo(getConfigErr, "", "Failed to get global event configuration")
		return nil, getConfigErr
	}

	resolver := structs.NewEventConfigurationResolver(eventConfiguration)

	fmt.Println("Successfully returned graphql resolver for the getGlobalEventConfiguration endpoint")
	return resolver, nil
}

// createGlobalEventConfiguration is a helper function to create the global event configuration if it does not exist
func createGlobalEventConfiguration(ctx context.Context, col insights.ArangoCollection) (*structs.EventConfigurationResolver, error) {
	graphID := graphql.ID(globalConfigID)
	event := &structs.EventConfiguration{
		Key: globalConfigID,
		ID:  &graphID,
		Email: &structs.EmailConfiguration{
			Enabled:  true,
			Triggers: nil,
			Template: structs.EmailObject{},
		},
	}

	_, createErr := col.CreateDocument(ctx, &event)
	if createErr != nil {
		return nil, createErr
	}

	resolver := structs.NewEventConfigurationResolver(event)
	return resolver, nil
}

// GetAllEventConfigurations is a graphql endpoint for returning all event configurations
func (r *Resolver) GetAllEventConfigurations(ctx context.Context) ([]*structs.EventConfigurationResolver, error) {
	fmt.Println("Graphql endpoint getAllEventConfigurations accessed")
	query := insights.ArangoDBQuery{Query: "FOR d in eventConfiguration return d"}

	cursor, queryErr := r.db.Query("config-db", query)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query config-db on arango")
		return nil, queryErr
	}
	defer insights.DeferHandleError(cursor.Close)
	resolverArr := []*structs.EventConfigurationResolver{}

	for {
		var conf structs.EventConfiguration
		meta, readErr := cursor.ReadDocument(ctx, &conf)
		if driver.IsNoMoreDocuments(readErr) {
			break
		} else if readErr != nil {
			insights.LogErrorWithInfo(readErr, "", "Failed to read document on arango")
			return nil, readErr
		}
		graphID := graphql.ID(meta.Key)
		conf.ID = &graphID

		resolverArr = append(resolverArr, structs.NewEventConfigurationResolver(&conf))
	}

	fmt.Println("Successfully returned Graphql Resolvers for getAllEventConfigurations endpoint")
	return resolverArr, nil
}

func getEventConfigurationByID(ctx context.Context, col insights.ArangoCollection, id string) (*structs.EventConfiguration, error) {
	var eventConfiguration structs.EventConfiguration
	meta, err := col.ReadDocument(ctx, id, &eventConfiguration)
	if err != nil {
		return nil, err
	}

	graphID := graphql.ID(meta.Key)
	eventConfiguration.ID = &graphID

	return &eventConfiguration, nil
}

type assetTypesAndEventTypeIDQueryArgs struct {
	AssetType   string
	EventTypeID string
}

type templateQueryArgs struct {
	TemplateType string
}

// GetByAssetTypeAndEventTypeID is the query resolver function for the grapqhl getByAssetTypeAndEventTypeId graphql function
func (r *Resolver) GetByAssetTypeAndEventTypeID(ctx context.Context, args assetTypesAndEventTypeIDQueryArgs) (*structs.EventConfigurationResolver, error) {
	fmt.Println("Graphql endpoint getByAssetTypeAndEventTypeId accessed")

	returnConfig, err := getByAssetTypeAndEventTypeID(ctx, r.db, args.AssetType, args.EventTypeID)
	if err != nil {
		if driver.IsNoMoreDocuments(err) {
			return nil, fmt.Errorf("no event configuration found with asset type %s and event type id %s", args.AssetType, args.EventTypeID)
		}

		insights.LogErrorWithInfo(err, "", "Failed to getByAssetTypeAndEventTypeID")
		return nil, err
	}

	resolver := structs.NewEventConfigurationResolver(returnConfig)
	fmt.Println("Successfully returned Graphql Resolvers for getByAssetTypeAndEventTypeId endpoint")
	return resolver, nil
}

func getByAssetTypeAndEventTypeID(ctx context.Context, db insights.ArangoDBer, assetType string, eventTypeID string) (*structs.EventConfiguration, error) {
	query := insights.ArangoDBQuery{Query: "FOR d in eventConfiguration return d"}

	cursor, queryErr := db.Query("config-db", query)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query config-db on arango")
		return nil, queryErr
	}
	defer insights.DeferHandleError(cursor.Close)
	var returnConfig *structs.EventConfiguration

	for {
		var conf structs.EventConfiguration
		meta, readErr := cursor.ReadDocument(ctx, &conf)
		if driver.IsNoMoreDocuments(readErr) {
			return nil, readErr
		} else if readErr != nil {
			insights.LogErrorWithInfo(queryErr, "", "Failed to read document on arango")
			return nil, readErr
		} else if conf.AssetType == assetType && conf.EventTypeID == eventTypeID {
			graphID := graphql.ID(meta.Key)
			conf.ID = &graphID
			returnConfig = &conf
			break
		}
	}

	return returnConfig, nil
}

// GetTemplate is the query resolver function for the getTemplate graphql endpoint
func (r *Resolver) GetTemplate(ctx context.Context, args templateQueryArgs) (*structs.TemplateClassResolver, error) {
	fmt.Println("Graphql endpoint getTemplate accessed")

	template, err := getTemplateClassByType(ctx, r.db, args.TemplateType)
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to retrieve template by type from arango")
		return nil, err
	}

	fmt.Println("Successfully returned Graphql Resolvers for getTemplate endpoint")
	return structs.NewTemplateClassResolver(template), nil
}

// Todo unused on front end
//func (r *Resolver) GetRelatedTemplates(ctx context.Context, args templateQueryArgs) *structs.TemplateClassResolver {
//	template := getTemplateClassByType(ctx, r.db, args.TemplateType)
//	query := insights.ArangoDBQuery{Query: "FOR d in template LIMIT 100 return d"}
//
//	cursor, queryErr := r.db.Query("config-db", query)
//	if queryErr != nil {
//		insights.PanicErrorIfExists(queryErr)
//	}
//	defer insights.DeferHandleError(cursor.Close)
//	var returnTemp *structs.TemplateClassResolver
//
//	for {
//		var temp structs.TemplateClass
//		_, readErr := cursor.ReadDocument(ctx, &temp)
//		if driver.IsNoMoreDocuments(readErr) {
//			break
//		} else if readErr != nil {
//			insights.PanicErrorIfExists(readErr)
//		} else if temp.Type == args.TemplateType {
//			returnTemp = structs.NewTemplateClassResolver(&temp)
//			break
//		}
//	}
//
//	return returnTemp
//}

// getTemplateClassByType is a helper function to get the template class by template type
func getTemplateClassByType(ctx context.Context, db insights.ArangoDBer, templateType string) (*structs.TemplateClass, error) {
	query := insights.ArangoDBQuery{Query: "FOR d in template LIMIT 100 return d"}

	cursor, queryErr := db.Query("config-db", query)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query config-db on arango")
		return nil, queryErr
	}
	defer insights.DeferHandleError(cursor.Close)
	var returnTemp *structs.TemplateClass
	for {
		var temp structs.TemplateClass
		_, readErr := cursor.ReadDocument(ctx, &temp)
		if driver.IsNoMoreDocuments(readErr) {
			return nil, fmt.Errorf("template not found")
		} else if readErr != nil {
			insights.LogErrorWithInfo(queryErr, "", "Failed to read document on arango")
			return nil, readErr
		} else if temp.Type == templateType {
			returnTemp = &temp
			break
		}
	}

	return returnTemp, nil
}

type createThresholdQueryArgs struct {
	Name        string
	DisplayName *string
	Description *string
	AssetType   string
	EventTypeID string
	Severities  []*structs.SeverityThreshold
}

type updateThresholdMutationArgs struct {
	ID              string
	Name            *string
	DisplayName     *string
	Description     *string
	Severities      *[]*structs.SeverityThreshold
	Email           *structs.EmailConfigurationInput
	SuppressionTime *float64
}

// CreateThreshold is the mutation resolver function for the CreateThreshold graphql endpoint
func (r *Resolver) CreateThreshold(ctx context.Context, args struct{ Threshold createThresholdQueryArgs }) (*structs.EventConfigurationResolver, error) {
	fmt.Println("Graphql endpoint createThreshold accessed")
	col, colErr := r.db.GetCollection("config-db", "eventConfiguration")
	if colErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to get eventConfiguration collection from arango")
		return nil, colErr
	}
	conf := &structs.EventConfiguration{
		Name:        &args.Threshold.Name,
		LastUpdated: structs.Date{Time: time.Now()},
		Severities:  args.Threshold.Severities,
		AssetType:   args.Threshold.AssetType,
		EventTypeID: args.Threshold.EventTypeID,
	}

	// Handle possible null values
	if args.Threshold.DisplayName != nil {
		conf.DisplayName = args.Threshold.DisplayName
	}

	if args.Threshold.Description != nil {
		conf.Description = *args.Threshold.Description
	}

	meta, createErr := col.CreateDocument(ctx, conf)
	if createErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to create document in eventConfiguration collection")
		return nil, createErr
	}
	fmt.Printf("Document created with id (%s)\n", meta.Key)

	var loadedConf structs.EventConfiguration
	meta, readErr := col.ReadDocument(ctx, meta.Key, &loadedConf)
	if readErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to read document from eventConfiguration collection")
		return nil, readErr
	}

	graphID := graphql.ID(meta.Key)
	loadedConf.ID = &graphID

	fmt.Println("Successfully returned Graphql Resolvers for createThreshold endpoint")
	return structs.NewEventConfigurationResolver(&loadedConf), nil
}

// UpdateThreshold is the mutation resolver function for the updateThreshold graphql endpoint
func (r *Resolver) UpdateThreshold(ctx context.Context, args struct{ Threshold updateThresholdMutationArgs }) (*structs.EventConfigurationResolver, error) {
	fmt.Println("Graphql endpoint updateThreshold accessed")
	col, colErr := r.db.GetCollection("config-db", "eventConfiguration")
	if colErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to get eventConfiguration collection from arango")
		return nil, colErr
	}

	var conf structs.EventConfiguration

	meta, readErr := col.ReadDocument(ctx, args.Threshold.ID, &conf)
	if readErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to read document from eventConfiguration collection")
		return nil, readErr
	}

	graphID := graphql.ID(meta.Key)
	conf.ID = &graphID

	conf.LastUpdated = structs.Date{Time: time.Now()}

	if args.Threshold.Severities != nil {
		conf.Severities = *args.Threshold.Severities
	}
	// Handle possible null values
	if args.Threshold.Name != nil {
		conf.Name = args.Threshold.Name
	}
	// Handle possible null values
	if args.Threshold.DisplayName != nil {
		conf.DisplayName = args.Threshold.DisplayName
	}

	if args.Threshold.Description != nil {
		conf.Description = *args.Threshold.Description
	}

	if args.Threshold.SuppressionTime != nil {
		conf.SuppressionTime = args.Threshold.SuppressionTime
	}

	meta, updateErr := col.UpdateDocument(ctx, args.Threshold.ID, conf)
	if updateErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to update document from eventConfiguration collection")
		return nil, updateErr
	}

	fmt.Println("Successfully returned Graphql Resolvers for updateThreshold endpoint")
	return structs.NewEventConfigurationResolver(&conf), nil
}

type eventConfigMutationArgs struct {
	Name        string
	DisplayName *string
	Description *string
	AssetType   string
	EventTypeID string
	Severities  []structs.SeverityThresholdInput
}

// CreateEventConfiguration is the mutation resolver function or the createEventConfiguration graphql endpoint
func (r *Resolver) CreateEventConfiguration(ctx context.Context, args struct{ Config eventConfigMutationArgs }) (*structs.EventConfigurationResolver, error) {
	fmt.Println("Graphql endpoint createEventConfiguration accessed")

	returnConfig, err := getByAssetTypeAndEventTypeID(ctx, r.db, args.Config.AssetType, args.Config.EventTypeID)
	if err == nil {
		return structs.NewEventConfigurationResolver(returnConfig), nil
	} else if !driver.IsNoMoreDocuments(err) {
		insights.LogErrorWithInfo(err, "", "Failed to getByAssetTypeAndEventTypeID")
		return nil, err
	}

	severities := parseSeverityThresholdInput(args.Config.Severities)
	eventConfig := &structs.EventConfiguration{
		Name:        &args.Config.Name,
		LastUpdated: structs.Date{Time: time.Now()},
		AssetType:   args.Config.AssetType,
		EventTypeID: args.Config.EventTypeID,
		Severities:  severities,
	}

	setOptionalCreateEventConfigurationFields(args.Config, eventConfig)

	col, colErr := r.db.GetCollection("config-db", "eventConfiguration")
	if colErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to get eventConfiguration error")
		return nil, colErr
	}

	meta, createErr := col.CreateDocument(ctx, eventConfig)
	if createErr != nil {
		insights.LogErrorWithInfo(createErr, "", "Failed to create document in eventConfiguration collection")
		return nil, createErr
	}
	fmt.Printf("Document created with id (%s)\n", meta.Key)
	graphID := graphql.ID(meta.Key)
	eventConfig.ID = &graphID

	return structs.NewEventConfigurationResolver(eventConfig), nil
}

type eventConfigUpdateArgs struct {
	ID              graphql.ID
	Name            *string
	DisplayName     *string
	Description     *string
	Severities      *[]structs.SeverityThresholdInput
	Email           *structs.EmailConfigurationInput
	SuppressionTime *float64
}

// UpdateEventConfiguration is the mutation resolver function or the updateEventConfiguration graphql endpoint
func (r *Resolver) UpdateEventConfiguration(ctx context.Context, args struct{ Config eventConfigUpdateArgs }) (*structs.EventConfigurationResolver, error) {
	fmt.Println("Graphql endpoint createEventConfiguration accessed")

	col, colErr := r.db.GetCollection("config-db", "eventConfiguration")
	if colErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to get eventConfiguration error")
		return nil, colErr
	}

	id := string(args.Config.ID)

	eventConfiguration, err := getEventConfigurationByID(ctx, col, id)
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to find event configuration id (%s)", id)
		return nil, err
	}

	updateOptionalEventConfigurationFields(args.Config, eventConfiguration)
	eventConfiguration.LastUpdated = structs.Date{Time: time.Now()}

	_, updateErr := col.UpdateDocument(ctx, id, eventConfiguration)
	if updateErr != nil {
		insights.LogErrorWithInfo(err, "", "Failed to update event configuration id (%s)", id)
		return nil, updateErr
	}

	return structs.NewEventConfigurationResolver(eventConfiguration), nil
}

func parseSeverityThresholdInput(input []structs.SeverityThresholdInput) []*structs.SeverityThreshold {
	var severities []*structs.SeverityThreshold
	for _, severity := range input {
		threshold := &structs.Threshold{}

		if severity.Threshold != nil {
			threshold = &structs.Threshold{
				MinValue:   severity.Threshold.MinValue,
				MaxValue:   severity.Threshold.MaxValue,
				Hysteresis: severity.Threshold.Hysteresis,
			}
		}

		severities = append(severities, &structs.SeverityThreshold{
			Severity:    severity.Severity,
			DisplayName: severity.DisplayName,
			Threshold:   threshold,
		})
	}

	return severities
}

func parseEmailInput(input structs.EmailConfigurationInput) *structs.EmailConfiguration {
	emailObject := structs.EmailObject{}

	if input.Template != nil {
		emailObject = structs.EmailObject{
			From: input.Template.From,
		}

		if input.Template.To != nil {
			emailObject.To = *input.Template.To
		}

		if input.Template.Subject != nil {
			emailObject.Subject = *input.Template.Subject
		}

		if input.Template.Message != nil {
			emailObject.Message = *input.Template.Message
		}
	}

	emailConfiguration := &structs.EmailConfiguration{
		Template: emailObject,
	}

	if input.Enabled != nil {
		emailConfiguration.Enabled = *input.Enabled
	}

	if input.Triggers != nil {
		emailConfiguration.Triggers = *input.Triggers
	}

	return emailConfiguration
}

func setOptionalCreateEventConfigurationFields(input eventConfigMutationArgs, eventConfig *structs.EventConfiguration) {
	if input.DisplayName != nil {
		eventConfig.DisplayName = input.DisplayName
	}

	if input.Description != nil {
		eventConfig.Description = *input.Description
	}
}

func updateOptionalEventConfigurationFields(input eventConfigUpdateArgs, eventConfig *structs.EventConfiguration) {
	if input.Name != nil {
		eventConfig.Name = input.Name
	}

	if input.DisplayName != nil {
		eventConfig.DisplayName = input.DisplayName
	}

	if input.Description != nil {
		eventConfig.Description = *input.Description
	}

	if input.Severities != nil {
		eventConfig.Severities = parseSeverityThresholdInput(*input.Severities)
	}

	if input.Email != nil {
		eventConfig.Email = parseEmailInput(*input.Email)
	}

	if input.SuppressionTime != nil {
		eventConfig.SuppressionTime = input.SuppressionTime
	}
}
