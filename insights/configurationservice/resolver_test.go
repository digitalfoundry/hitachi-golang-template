package configurationservice

import (
	"context"
	"fmt"
	"testing"

	"github.com/graph-gophers/graphql-go"

	"github.com/arangodb/go-driver"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestResolver_GetAllEventTypes(t *testing.T) {
	tempName1 := "Name 1"
	ec1 := &structs.EventConfiguration{
		EventTypeID:          "ID 1",
		EventTypeDisplayName: &tempName1,
	}

	tempName2 := "Name 2"
	ec2 := &structs.EventConfiguration{
		EventTypeID:          "ID 2",
		EventTypeDisplayName: &tempName2,
	}
	mockDBSuccess := structs.NewMockArangoDBer()
	mockCursorSuccess := &structs.MockArangoCursorEventConfiguration{}
	mockDBSuccess.On("Query", "config-db", mock.Anything).Return(mockCursorSuccess, nil).Once()
	mockCursorSuccess.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, ec1).Once()
	mockCursorSuccess.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, ec2).Once()
	mockCursorSuccess.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorSuccess.On("Close").Return(nil).Once()

	mockDBReadFail := structs.NewMockArangoDBer()
	mockCursorReadFail := &structs.MockArangoCursorEventConfiguration{}
	mockDBReadFail.On("Query", "config-db", mock.Anything).Return(mockCursorReadFail, nil).Once()
	mockCursorReadFail.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()
	mockCursorReadFail.On("Close").Return(nil).Once()

	mockDBQueryFail := structs.NewMockArangoDBer()
	mockCursorQueryFail := &structs.MockArangoCursorEventConfiguration{}
	mockDBQueryFail.On("Query", "config-db", mock.Anything).Return(mockCursorQueryFail, fmt.Errorf("query error")).Once()

	cases := map[string]struct {
		db           *structs.MockArangoDB
		cursor       *structs.MockArangoCursorEventConfiguration
		errMsg       string
		arrayLen     int
		queryCount   int
		readDocCount int
		closeCount   int
	}{
		"success": {
			db:           mockDBSuccess,
			cursor:       mockCursorSuccess,
			arrayLen:     2,
			queryCount:   1,
			readDocCount: 3,
			closeCount:   1,
		},
		"query_fail": {
			db:           mockDBQueryFail,
			cursor:       mockCursorQueryFail,
			errMsg:       "query error",
			arrayLen:     0,
			queryCount:   1,
			readDocCount: 0,
			closeCount:   0,
		},
		"read_fail": {
			db:           mockDBReadFail,
			cursor:       mockCursorReadFail,
			errMsg:       "read error",
			arrayLen:     0,
			queryCount:   1,
			readDocCount: 1,
			closeCount:   1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := Resolver{db: c.db}
			eTypesResolvers, err := resolver.GetAllEventTypes(context.Background())
			assert.Len(t, eTypesResolvers, c.arrayLen)
			c.db.AssertNumberOfCalls(t, "Query", c.queryCount)
			c.cursor.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)
			c.cursor.AssertNumberOfCalls(t, "Close", c.closeCount)

			if err != nil {
				assert.Equal(t, c.errMsg, err.Error())
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.Equal(t, graphql.ID(ec1.EventTypeID), *eTypesResolvers[0].EventTypeID(context.Background()))
				assert.Equal(t, ec1.EventTypeDisplayName, eTypesResolvers[0].EventTypeDisplayName())
				assert.Equal(t, graphql.ID(ec2.EventTypeID), *eTypesResolvers[1].EventTypeID(context.Background()))
				assert.Equal(t, ec2.EventTypeDisplayName, eTypesResolvers[1].EventTypeDisplayName())

			}
		})
	}
}

func TestResolver_GetGlobalEventConfiguration_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionAssetEntity{}
	resolver := &Resolver{db: mockDb}

	display := "randy"
	ec1 := &structs.EventConfiguration{
		DisplayName: &display,
	}

	mockDb.On("GetCollection", "config-db", mock.Anything).Return(mockCollection, nil).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, ec1).Once()
	mockCollection.On("Close").Return(nil).Once()

	ecResolver, err := resolver.GetGlobalEventConfiguration(context.Background())
	assert.NoError(t, err)
	assert.NotNil(t, ecResolver)

	mockDbCreate := structs.NewMockArangoDBer()
	mockCollectionCreate := &structs.MockArangoCollectionAssetEntity{}
	resolverCreate := &Resolver{db: mockDbCreate}
	notFound := driver.ArangoError{Code: 404}

	mockDbCreate.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionCreate, nil).Once()
	mockCollectionCreate.On("ReadDocument", mock.Anything, mock.Anything).Return(notFound, ec1).Once()
	mockCollectionCreate.On("CreateDocument", mock.Anything, mock.Anything).Return(nil, nil).Once()

	createResolver, createErr := resolverCreate.GetGlobalEventConfiguration(context.Background())
	assert.NoError(t, createErr)
	assert.NotNil(t, createResolver)
}

func TestResolver_GetGlobalEventConfiguration_Failure(t *testing.T) {
	ec1 := &structs.EventConfiguration{}

	mockDbGetColErr := structs.NewMockArangoDBer()
	mockCollectionGetColErr := &structs.MockArangoCollectionAssetEntity{}
	resolverGetColErr := &Resolver{db: mockDbGetColErr}

	mockDbGetColErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionGetColErr, fmt.Errorf("get col err")).Once()

	ecResolver, err := resolverGetColErr.GetGlobalEventConfiguration(context.Background())
	assert.Errorf(t, err, "get col err")
	assert.Nil(t, ecResolver)

	mockDbReadErr := structs.NewMockArangoDBer()
	mockCollectionReadErr := &structs.MockArangoCollectionAssetEntity{}
	resolverReadErr := &Resolver{db: mockDbReadErr}

	mockDbReadErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionReadErr, nil).Once()
	mockCollectionReadErr.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read err"), ec1).Once()
	mockCollectionReadErr.On("Close").Return(nil).Once()

	ecResolverReadErr, err := resolverReadErr.GetGlobalEventConfiguration(context.Background())
	assert.Errorf(t, err, "read err")
	assert.Nil(t, ecResolverReadErr)

}

func TestResolver_GetAllEventConfigurations_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := structs.MockArangoCursorEventConfiguration{}
	resolver := &Resolver{db: mockDb}
	s := "bee"
	graphID := graphql.ID("foo")
	ac1 := &structs.EventConfiguration{
		Name: &s,
		ID:   &graphID,
	}
	ac2 := &structs.EventConfiguration{
		Name: &s,
		ID:   &graphID,
	}

	mockDb.On("Query", "config-db", mock.Anything).Return(&mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, ac1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, ac2).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()
	acResolver, getAllErr := resolver.GetAllEventConfigurations(context.Background())
	assert.NoError(t, getAllErr)
	assert.NotNil(t, acResolver)
	assert.Len(t, acResolver, 2)
	firstACResolver := acResolver[0]
	secondACResolver := acResolver[1]

	assert.Equal(t, firstACResolver.Name(), ac1.Name)
	assert.Equal(t, secondACResolver.Name(), ac2.Name)
}

func TestResolver_GetAllEventConfigurations_Failure(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := structs.MockArangoCursorEventConfiguration{}
	resolver := &Resolver{db: mockDb}

	name := "ac1-name"
	id := graphql.ID("ac1-id")
	ac1 := &structs.EventConfiguration{
		Name: &name,
		ID:   &id,
	}

	mockDb.On("Query", "config-db", mock.Anything).Return(&mockCursor, fmt.Errorf("query err")).Once()

	acResolver, getAllErr := resolver.GetAllEventConfigurations(context.Background())
	assert.Nil(t, acResolver)
	assert.NotNil(t, getAllErr)

	mockDbRead := structs.NewMockArangoDBer()
	mockCursorRead := structs.MockArangoCursorEventConfiguration{}
	resolverRead := &Resolver{db: mockDbRead}

	mockDbRead.On("Query", "config-db", mock.Anything).Return(&mockCursorRead, nil).Once()
	mockCursorRead.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read err"), ac1).Once()
	mockCursorRead.On("Close").Return(nil).Once()

	readResolver, readErr := resolverRead.GetAllEventConfigurations(context.Background())
	assert.NotNil(t, readErr)
	assert.Nil(t, readResolver)
}

func TestResolver_GetByAssetTypeAndEventTypeID_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := structs.MockArangoCursorEventConfiguration{}
	resolver := &Resolver{db: mockDb}

	assetType := "asset-type-1"
	eventTypeID := "event-type-1"
	id := graphql.ID("ac1-id")
	name := "ac1-name"
	ac1 := &structs.EventConfiguration{
		Name:        &name,
		ID:          &id,
		AssetType:   assetType,
		EventTypeID: eventTypeID,
	}

	args := assetTypesAndEventTypeIDQueryArgs{
		AssetType:   assetType,
		EventTypeID: eventTypeID,
	}

	mockDb.On("Query", "config-db", mock.Anything).Return(&mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, ac1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()
	acResolver, getAllErr := resolver.GetByAssetTypeAndEventTypeID(context.Background(), args)
	assert.NoError(t, getAllErr)
	assert.NotNil(t, acResolver)

	assert.Equal(t, acResolver.Name(), ac1.Name)
}

func TestResolver_GetByAssetTypeAndEventTypeID_Failure(t *testing.T) {
	mockDbQuery := structs.NewMockArangoDBer()
	mockCursorQuery := structs.MockArangoCursorEventConfiguration{}
	resolverQuery := &Resolver{db: mockDbQuery}

	mockDbQuery.On("Query", "config-db", mock.Anything).Return(&mockCursorQuery, fmt.Errorf("query err")).Once()

	acResolverQuery, queryErr := resolverQuery.GetByAssetTypeAndEventTypeID(context.Background(), assetTypesAndEventTypeIDQueryArgs{})
	assert.Errorf(t, queryErr, "query err")
	assert.Nil(t, acResolverQuery)

	mockDbRead := structs.NewMockArangoDBer()
	mockCursorRead := structs.MockArangoCursorEventConfiguration{}
	resolverRead := &Resolver{db: mockDbRead}

	mockDbRead.On("Query", "config-db", mock.Anything).Return(&mockCursorRead, nil).Once()
	mockCursorRead.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read err"), nil).Once()
	mockCursorRead.On("Close").Return(nil).Once()

	acResolverRead, readErr := resolverRead.GetByAssetTypeAndEventTypeID(context.Background(), assetTypesAndEventTypeIDQueryArgs{})
	assert.Errorf(t, readErr, "read err")
	assert.Nil(t, acResolverRead)

	mockDbNo := structs.NewMockArangoDBer()
	mockCursorNo := structs.MockArangoCursorEventConfiguration{}
	resolverNo := &Resolver{db: mockDbNo}

	mockDbNo.On("Query", "config-db", mock.Anything).Return(&mockCursorNo, nil).Once()
	mockCursorNo.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorNo.On("Close").Return(nil).Once()

	acResolverNo, NoErr := resolverNo.GetByAssetTypeAndEventTypeID(context.Background(), assetTypesAndEventTypeIDQueryArgs{})
	assert.Errorf(t, NoErr, "no more documents")
	assert.Nil(t, acResolverNo)
}

func TestResolver_GetTemplate_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := structs.MockArangoCursorTemplateClass{}
	resolver := &Resolver{db: mockDb}

	templateType := "template-type-1"
	tc1 := &structs.TemplateClass{
		Name:     "rand",
		Type:     templateType,
		BaseType: "o",
	}
	args := templateQueryArgs{
		TemplateType: templateType,
	}

	mockDb.On("Query", "config-db", mock.Anything).Return(&mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, tc1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()
	templateResolver, getAllErr := resolver.GetTemplate(context.Background(), args)
	assert.NoError(t, getAllErr)
	assert.NotNil(t, templateResolver)
}

func TestResolver_GetTemplate_Failure(t *testing.T) {
	templateType := "template-type-1"
	args := templateQueryArgs{
		TemplateType: templateType,
	}

	mockDbQueryErr := structs.NewMockArangoDBer()
	mockCursorQueryErr := structs.MockArangoCursorTemplateClass{}
	resolverQueryErr := &Resolver{db: mockDbQueryErr}

	mockDbQueryErr.On("Query", "config-db", mock.Anything).Return(&mockCursorQueryErr, fmt.Errorf("query err")).Once()
	templateResolverQueryErr, queryErr := resolverQueryErr.GetTemplate(context.Background(), args)
	assert.Errorf(t, queryErr, "query err")
	assert.Nil(t, templateResolverQueryErr)

	mockDbNoDoc := structs.NewMockArangoDBer()
	mockCursorNoDoc := structs.MockArangoCursorTemplateClass{}
	resolverNoDoc := &Resolver{db: mockDbNoDoc}

	mockDbNoDoc.On("Query", "config-db", mock.Anything).Return(&mockCursorNoDoc, nil).Once()
	mockCursorNoDoc.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorNoDoc.On("Close").Return(nil).Once()

	templateResolverNoDoc, noDoc := resolverNoDoc.GetTemplate(context.Background(), args)
	assert.Errorf(t, noDoc, "template not found")
	assert.Nil(t, templateResolverNoDoc)

	mockDbReadErr := structs.NewMockArangoDBer()
	mockCursorReadErr := structs.MockArangoCursorTemplateClass{}
	resolverReadErr := &Resolver{db: mockDbReadErr}

	mockDbReadErr.On("Query", "config-db", mock.Anything).Return(&mockCursorReadErr, nil).Once()
	mockCursorReadErr.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read err"), nil).Once()
	mockCursorReadErr.On("Close").Return(nil).Once()

	templateResolverReadErr, readErr := resolverReadErr.GetTemplate(context.Background(), args)
	assert.Errorf(t, readErr, "read err")
	assert.Nil(t, templateResolverReadErr)
}

func TestResolver_CreateThreshold_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionAssetEntity{}
	resolver := &Resolver{db: mockDb}
	displayName := "thhresh1"
	description := "eat more chikin"
	thresh := &structs.EventConfiguration{}

	a := struct{ Threshold createThresholdQueryArgs }{createThresholdQueryArgs{
		Name:        "asd",
		DisplayName: &displayName,
		Description: &description,
		Severities:  nil,
	},
	}

	mockDb.On("GetCollection", "config-db", mock.Anything).Return(mockCollection, nil).Once()
	mockCollection.On("CreateDocument", mock.Anything, mock.Anything).Return(nil, thresh).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, thresh).Once()
	mockCollection.On("Close").Return(nil).Once()
	templateResolver, getAllErr := resolver.CreateThreshold(context.Background(), a)
	assert.NoError(t, getAllErr)
	assert.NotNil(t, templateResolver)
	assert.Equal(t, thresh.Description, *templateResolver.Description())
	assert.Equal(t, thresh.Name, templateResolver.Name())
}

func TestResolver_CreateThreshold_Failure(t *testing.T) {
	displayName := "thhresh1"
	description := "eat more chikin"
	thresh := &structs.EventConfiguration{}
	a := struct{ Threshold createThresholdQueryArgs }{createThresholdQueryArgs{
		Name:        "asd",
		DisplayName: &displayName,
		Description: &description,
		Severities:  nil,
	},
	}

	mockDbColErr := structs.NewMockArangoDBer()
	mockCollectionErr := &structs.MockArangoCollectionAssetEntity{}
	resolverColErr := &Resolver{db: mockDbColErr}

	mockDbColErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionErr, fmt.Errorf("get err")).Once()
	colResolver, getColErr := resolverColErr.CreateThreshold(context.Background(), a)
	assert.Errorf(t, getColErr, "get err")
	assert.Nil(t, colResolver)

	mockDbCreateErr := structs.NewMockArangoDBer()
	mockCollectionCreateErr := &structs.MockArangoCollectionAssetEntity{}
	resolverCreateErr := &Resolver{db: mockDbCreateErr}

	mockDbCreateErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionCreateErr, nil).Once()
	mockCollectionCreateErr.On("CreateDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("create err"), thresh).Once()
	createResolver, createErr := resolverCreateErr.CreateThreshold(context.Background(), a)
	assert.Errorf(t, createErr, "create err")
	assert.Nil(t, createResolver)

	mockDbReadErr := structs.NewMockArangoDBer()
	mockCollectionReadErr := &structs.MockArangoCollectionAssetEntity{}
	resolverReadErr := &Resolver{db: mockDbReadErr}

	mockDbReadErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionReadErr, nil).Once()
	mockCollectionReadErr.On("CreateDocument", mock.Anything, mock.Anything).Return(nil, thresh).Once()
	mockCollectionReadErr.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read err"), thresh).Once()
	mockCollectionReadErr.On("Close").Return(nil).Once()
	readResolver, readErr := resolverReadErr.CreateThreshold(context.Background(), a)
	assert.Errorf(t, readErr, "read err")
	assert.Nil(t, readResolver)
}

func TestResolver_UpdateThreshold_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionAssetEntity{}
	resolver := &Resolver{db: mockDb}
	displayName := "thhresh1"
	name := "num"
	description := "eat more chikin"
	thresh := &structs.EventConfiguration{}

	a := struct{ Threshold updateThresholdMutationArgs }{updateThresholdMutationArgs{
		Name:        &name,
		DisplayName: &displayName,
		Description: &description,
		Severities:  nil,
	},
	}

	mockDb.On("GetCollection", "config-db", mock.Anything).Return(mockCollection, nil).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, thresh).Once()
	mockCollection.On("UpdateDocument", mock.Anything, mock.Anything, mock.Anything).Return(nil, thresh).Once()
	templateResolver, getAllErr := resolver.UpdateThreshold(context.Background(), a)
	assert.NoError(t, getAllErr)
	assert.NotNil(t, templateResolver)
	assert.Equal(t, &description, templateResolver.Description())
	assert.Equal(t, &name, templateResolver.Name())
}

func TestResolver_UpdateThreshold_Failure(t *testing.T) {
	displayName := "thhresh1"
	name := "num"
	description := "eat more chikin"
	thresh := &structs.EventConfiguration{}

	a := struct{ Threshold updateThresholdMutationArgs }{updateThresholdMutationArgs{
		Name:        &name,
		DisplayName: &displayName,
		Description: &description,
		Severities:  nil,
	},
	}

	mockDbColErr := structs.NewMockArangoDBer()
	mockCollectionColErr := &structs.MockArangoCollectionAssetEntity{}
	resolverColErr := &Resolver{db: mockDbColErr}

	mockDbColErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionColErr, fmt.Errorf("col err")).Once()
	updateResolverColErr, colErr := resolverColErr.UpdateThreshold(context.Background(), a)
	assert.Errorf(t, colErr, "col err")
	assert.Nil(t, updateResolverColErr)

	mockDbReadErr := structs.NewMockArangoDBer()
	mockCollectionReadErr := &structs.MockArangoCollectionAssetEntity{}
	resolverReadErr := &Resolver{db: mockDbReadErr}

	mockDbReadErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionReadErr, nil).Once()
	mockCollectionReadErr.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read err"), thresh).Once()
	updateResolverReadErr, readErr := resolverReadErr.UpdateThreshold(context.Background(), a)
	assert.Errorf(t, readErr, "read err")
	assert.Nil(t, updateResolverReadErr)

	mockDbUpdateErr := structs.NewMockArangoDBer()
	mockCollectionUpdateErr := &structs.MockArangoCollectionAssetEntity{}
	resolverUpdateErr := &Resolver{db: mockDbUpdateErr}

	mockDbUpdateErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionUpdateErr, nil).Once()
	mockCollectionUpdateErr.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, thresh).Once()
	mockCollectionUpdateErr.On("UpdateDocument", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("update err"), thresh).Once()

	updateResolverUpdateErr, updateErr := resolverUpdateErr.UpdateThreshold(context.Background(), a)
	assert.Errorf(t, updateErr, "update err")
	assert.Nil(t, updateResolverUpdateErr)
}

func TestResolver_CreateEventConfiguration_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionAssetEntity{}
	mockCursor := structs.MockArangoCursorTemplateClass{}
	resolver := &Resolver{db: mockDb}

	displayName := "display"
	description := "description"
	severity := structs.Catastrophic
	hysteresis := 4.44
	severities := []structs.SeverityThresholdInput{
		{
			Severity:    &severity,
			DisplayName: &displayName,
			Threshold: &structs.ThresholdInput{
				MinValue:   0,
				MaxValue:   0,
				Hysteresis: &hysteresis,
			},
		},
	}

	eventConfig := struct{ Config eventConfigMutationArgs }{eventConfigMutationArgs{
		Name:        "name-1",
		DisplayName: &displayName,
		Description: &description,
		AssetType:   "asset-1",
		EventTypeID: "evt-1",
		Severities:  severities,
	},
	}

	mockDb.On("Query", mock.Anything, mock.Anything).Return(&mockCursor, nil)
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	mockDb.On("GetCollection", "config-db", mock.Anything).Return(mockCollection, nil).Once()
	mockCollection.On("CreateDocument", mock.Anything, mock.Anything).Return(nil, nil).Once()
	mockCollection.On("Close").Return(nil).Once()
	templateResolver, getAllErr := resolver.CreateEventConfiguration(context.Background(), eventConfig)
	assert.NoError(t, getAllErr)
	assert.Equal(t, *templateResolver.DisplayName(), displayName)
	assert.Equal(t, templateResolver.Severities()[0].Threshold(context.Background()).Hysteresis(context.Background()), severities[0].Threshold.Hysteresis)
}

func TestResolver_CreateEventConfiguration_Failure(t *testing.T) {
	displayName := "display"
	description := "description"
	eventConfig := struct{ Config eventConfigMutationArgs }{eventConfigMutationArgs{
		Name:        "name-1",
		DisplayName: &displayName,
		Description: &description,
		AssetType:   "asset-1",
		EventTypeID: "evt-1",
	},
	}

	mockDbReadErr := structs.NewMockArangoDBer()
	mockCursorReadErr := structs.MockArangoCursorTemplateClass{}
	resolverReadErr := &Resolver{db: mockDbReadErr}

	mockDbReadErr.On("Query", mock.Anything, mock.Anything).Return(&mockCursorReadErr, nil)
	mockCursorReadErr.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read err"), nil).Once()
	mockCursorReadErr.On("Close").Return(nil).Once()

	templateResolverReadErr, ReadErr := resolverReadErr.CreateEventConfiguration(context.Background(), eventConfig)
	assert.Errorf(t, ReadErr, "read err")
	assert.Nil(t, templateResolverReadErr)

	mockDbGetErr := structs.NewMockArangoDBer()
	mockCursorGetErr := structs.MockArangoCursorTemplateClass{}
	mockCollectionGetErr := structs.MockArangoCollectionEventConfiguration{}
	resolverGetErr := &Resolver{db: mockDbGetErr}

	mockDbGetErr.On("Query", mock.Anything, mock.Anything).Return(&mockCursorGetErr, nil)
	mockCursorGetErr.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursorGetErr.On("Close").Return(nil).Once()

	mockDbGetErr.On("GetCollection", "config-db", mock.Anything).Return(&mockCollectionGetErr, fmt.Errorf("get col err")).Once()

	templateResolverGetErr, GetErr := resolverGetErr.CreateEventConfiguration(context.Background(), eventConfig)
	assert.Errorf(t, GetErr, "get col err")
	assert.Nil(t, templateResolverGetErr)

}

func TestResolver_UpdateEventConfiguration_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionAssetEntity{}
	resolver := &Resolver{db: mockDb}

	ec := &structs.EventConfiguration{}
	displayName := "display"
	severity := structs.Catastrophic
	hysteresis := 4.44
	severities := []structs.SeverityThresholdInput{
		{
			Severity:    &severity,
			DisplayName: &displayName,
			Threshold: &structs.ThresholdInput{
				MinValue:   0,
				MaxValue:   0,
				Hysteresis: &hysteresis,
			},
		},
	}
	email := structs.EmailConfigurationInput{
		Enabled: new(bool),
		Triggers: &map[string]interface{}{
			"test": "t",
		},
		Template: &structs.EmailObjectInput{
			From:    new(string),
			To:      new([]string),
			Subject: new(string),
			Message: new(string),
		},
	}
	updateArgs := struct{ Config eventConfigUpdateArgs }{eventConfigUpdateArgs{
		ID:          "id-1",
		Name:        new(string),
		DisplayName: new(string),
		Description: new(string),
		Severities:  &severities,
		Email:       &email,
	}}

	mockDb.On("GetCollection", "config-db", mock.Anything).Return(mockCollection, nil).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, ec).Once()
	mockCollection.On("UpdateDocument", mock.Anything, mock.Anything, mock.Anything).Return(nil, ec).Once()

	ecResolver, err := resolver.UpdateEventConfiguration(context.Background(), updateArgs)
	assert.NoError(t, err)
	assert.NotNil(t, ecResolver)
	assert.Equal(t, *ecResolver.ID(context.Background()), updateArgs.Config.ID)
}

func TestResolver_UpdateEventConfiguration_Failure(t *testing.T) {
	mockDbGetColErr := structs.NewMockArangoDBer()
	mockCollectionGetColErr := &structs.MockArangoCollectionAssetEntity{}
	resolverGetColErr := &Resolver{db: mockDbGetColErr}

	mockDbGetColErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionGetColErr, fmt.Errorf("get col err")).Once()

	ecResolver, err := resolverGetColErr.UpdateEventConfiguration(context.Background(), struct{ Config eventConfigUpdateArgs }{})
	assert.Errorf(t, err, "get col err")
	assert.Nil(t, ecResolver)

	mockDbReadErr := structs.NewMockArangoDBer()
	mockCollectionReadErr := &structs.MockArangoCollectionAssetEntity{}
	resolverReadErr := &Resolver{db: mockDbReadErr}

	mockDbReadErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionReadErr, nil).Once()
	mockCollectionReadErr.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read err"), nil).Once()

	ecResolver2, err := resolverReadErr.UpdateEventConfiguration(context.Background(), struct{ Config eventConfigUpdateArgs }{})
	assert.Errorf(t, err, "read err")
	assert.Nil(t, ecResolver2)

	mockDbUpdateErr := structs.NewMockArangoDBer()
	mockCollectionUpdateErr := &structs.MockArangoCollectionAssetEntity{}
	resolverUpdateErr := &Resolver{db: mockDbUpdateErr}

	mockDbUpdateErr.On("GetCollection", "config-db", mock.Anything).Return(mockCollectionUpdateErr, nil).Once()
	mockCollectionUpdateErr.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, nil).Once()
	mockCollectionUpdateErr.On("UpdateDocument", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("update err"), nil).Once()

	ecResolver3, err := resolverUpdateErr.UpdateEventConfiguration(context.Background(), struct{ Config eventConfigUpdateArgs }{})
	assert.Errorf(t, err, "update err")
	assert.Nil(t, ecResolver3)
}
