package cronservice

import (
	"fmt"
	"os"
)

type config struct {
	CronSample           string
	HostPort             string
	GraphQLSchemaFile    string
	InfluxRawDBAddress   string
	InfluxRawUsername    string
	InfluxRawPassword    string
	InfluxDBAddress      string
	InfluxUsername       string
	InfluxPassword       string
	ArangoDBAddress      string
	ArangoDBUsername     string
	ArangoDBPassword     string
	ArangoDBVersionFile  string
	RabbitMQAddress      string
	RabbitMQUser         string
	RabbitMQPassword     string
	RabbitMQPort         string
	KeycloakAddress      string
	KeycloakRealm        string
	KeycloakClientID     string
	KeycloakClientSecret string
	InfluxRawSeedFile    string
}

func getConfig() *config {
	getEnvString := func(key string, optional bool) string {
		environmentVar := os.Getenv(key)
		if environmentVar == "" && !optional {
			panic(fmt.Sprintf("$%s must be set. please make sure the environment values are set correctly", key))
		}
		return environmentVar
	}

	return &config{
		CronSample:           getEnvString("CRON_SAMPLE_ROUTINE", false),
		HostPort:             getEnvString("HOST_PORT", false),
		GraphQLSchemaFile:    getEnvString("GRAPHQL_SCHEMA_FILE", false),
		InfluxRawDBAddress:   getEnvString("INFLUX_RAW_DB_ADDRESS", false),
		InfluxRawUsername:    getEnvString("INFLUX_RAW_DB_USERNAME", false),
		InfluxRawPassword:    getEnvString("INFLUX_RAW_DB_PASSWORD", false),
		InfluxDBAddress:      getEnvString("INFLUX_DB_ADDRESS", false),
		InfluxUsername:       getEnvString("INFLUX_DB_USERNAME", false),
		InfluxPassword:       getEnvString("INFLUX_DB_PASSWORD", false),
		ArangoDBAddress:      getEnvString("ARANGO_DB_ADDRESS", false),
		ArangoDBUsername:     getEnvString("ARANGO_DB_USERNAME", false),
		ArangoDBPassword:     getEnvString("ARANGO_DB_PASSWORD", false),
		ArangoDBVersionFile:  getEnvString("ARANGO_DB_VERSION_FILE", false),
		RabbitMQAddress:      getEnvString("RABBIT_MQ_ADDRESS", false),
		RabbitMQUser:         getEnvString("RABBIT_MQ_USER", false),
		RabbitMQPassword:     getEnvString("RABBIT_MQ_PASSWORD", false),
		RabbitMQPort:         getEnvString("RABBIT_MQ_PORT", false),
		KeycloakRealm:        getEnvString("KEYCLOAK_REALM", false),
		KeycloakAddress:      getEnvString("KEYCLOAK_ADDRESS", false),
		KeycloakClientID:     getEnvString("KEYCLOAK_CLIENT_ID", false),
		KeycloakClientSecret: getEnvString("KEYCLOAK_CLIENT_SECRET", false),
		InfluxRawSeedFile:    getEnvString("INFLUX_RAW_SEED_FILE", false),
	}
}
