package cronservice

import (
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
	"testing"
	"time"
)

func TestEvaluator_Init(t *testing.T) {
	e := newEvaluator("ABSOLUTE_DEVIATION")

	e.Init()
}

func TestEvaluator_CalculateSeverity(t *testing.T) {
	event := newEvent()
	e := newEvaluator("ABSOLUTE_DEVIATION")

	e.CalculateSeverity(event)
}

func TestEvaluator_CalculateSeverity2(t *testing.T) {
	event := newEvent()
	e := newEvaluator("ABSOLUTE_PERCENTAGE_DEVIATION")

	e.CalculateSeverity(event)
}

func TestEvaluator_CalculateSeverity3(t *testing.T) {
	event := newEvent()
	e := newEvaluator("DEVIATION")

	e.CalculateSeverity(event)
}

func newEvent() structs.Event {
	riskScore := 4.4

	return structs.Event{
		ID:           "",
		Name:         nil,
		DisplayName:  nil,
		Description:  nil,
		LastUpdated:  time.Time{},
		CreatedDate:  time.Time{},
		EventType:    "",
		EventTypeID:  "",
		RiskScore:    &riskScore,
		Status:       structs.StatusWrapper{},
		Priority:     nil,
		Severity:     structs.SeverityWrapper{},
		AssetID:      "",
		ResolvedDate: time.Time{},
		Properties:   map[string]interface{}{
			"field1": 5.55,
		},
		Thresholds:   nil,
	}

}

func newEvaluator(calculationType string) *Evaluator {
	critical := structs.Critical
	hysteresis := 4.4
	calculationBaseField := "field1"

	return &Evaluator{
		eventConfiguration: structs.EventConfiguration{
			Severities:                   []*structs.SeverityThreshold{
				{
					Severity: &critical,
					Threshold: &structs.Threshold{
						MinValue:   0.3,
						MaxValue:   0.5,
						Hysteresis: &hysteresis,
					},
				},
				{
					Severity: &critical,
					Threshold: &structs.Threshold{
						MinValue:   0.2,
						MaxValue:   0.6,
						Hysteresis: &hysteresis,
					},
				},
			},
			CalculationType:              &calculationType,
			CalculationBaseField: &calculationBaseField,
		},
		hysteresis:         map[structs.Severity]structs.Hysteresis{},
		thresholds: []ThresholdSeverity{
			{
				Severity: critical,
			},
			{
				Severity: critical,
			},
		},
	}
}
