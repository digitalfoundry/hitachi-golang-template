package cronservice

import "net/http"

func (s *Service) routeHandleFunc(route string, function func(w http.ResponseWriter, r *http.Request)) {
	s.serveMux.HandleFunc(route, function)
}

func (s *Service) setupRouteHandlers() {
	s.routeHandleFunc(homeRoute+healthRoute, s.handleHealthCheck)
	s.routeHandleFunc(homeRoute+installKPIsRoute, s.installKPIs)
}
