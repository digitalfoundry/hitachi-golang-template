package cronservice

const (
	homeRoute        = "/"
	healthRoute      = "health"
	installKPIsRoute = "installKPIs"
)
