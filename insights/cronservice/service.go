package cronservice

import (
	"encoding/json"
	"fmt"
	"github.com/graph-gophers/graphql-go"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
	"github.com/robfig/cron"
)

var newArangoDB = insights.NewArangoDBer
var newInfluxDB = insights.NewInfluxDBer

const (
	retryAttempts = 10
	sleepTime     = 2
)

// Service represents the cron service
type Service struct {
	serveMux            *http.ServeMux
	server              *http.Server
	config              *config
	gqlSchema  *graphql.Schema
	influxRawDB         insights.InfluxDBer
	measurementDB       insights.InfluxDBer
	arangoDB            insights.ArangoDBer
	rabbit              *insights.RabbitMQ
	keycloaker          insights.Keycloaker
	configServiceClient insights.Clienter
	KPICalculators      []structs.KPICalculatorInterface
}

// NewService returns a new instance of the service
func NewService() *Service {
	return &Service{}
}

// Init initializes the service
func (s *Service) Init() {
	s.config = getConfig()
	s.gqlSchema = s.getGraphqlFromFile()

	s.influxRawDB = newInfluxDB()
	err := s.influxRawDB.Connect(s.config.InfluxRawDBAddress, s.config.InfluxRawUsername, s.config.InfluxRawPassword)
	if err != nil {
		insights.PanicErrorIfExists(err)
	}

	// check that the raw database is current
	err = s.checkInfluxDBRawVersion()
	if err != nil {
		insights.PanicErrorIfExists(err)
	}

	s.measurementDB = newInfluxDB()
	err = s.measurementDB.Connect(s.config.InfluxDBAddress, s.config.InfluxUsername, s.config.InfluxPassword)
	if err != nil {
		insights.PanicErrorIfExists(err)
	}

	s.arangoDB = newArangoDB(s.config.ArangoDBAddress)
	err = s.arangoDB.Connect(s.config.ArangoDBUsername, s.config.ArangoDBPassword)
	if err != nil {
		insights.PanicErrorIfExists(err)
	}

	versionErr := insights.CheckDatabaseRunning(s.arangoDB, s.config.ArangoDBVersionFile)
	if versionErr != nil {
		insights.PanicErrorIfExists(versionErr)
	}

	s.serveMux = http.NewServeMux()
	s.server = &http.Server{
		Handler: s.serveMux,
		Addr:    fmt.Sprintf(":%s", s.config.HostPort),
	}
}

// checkInfluxDBRawVersion validates that the raw influx database version matches the expected version of the database
func (s *Service) checkInfluxDBRawVersion() error {
	// grab the file info for the seed file
	info, err := os.Stat(s.config.InfluxRawSeedFile)
	if err != nil {
		insights.LogErrorWithInfo(err, "ensure influx seed file exists", "unable to connect to latest version of raw influx database")
		return err
	}

	// get the last mod date for the seed file in a string form matching the date string in the influx database
	dateString := info.ModTime().Format(time.RFC1123Z)

	query := "SELECT modDate AS modDate FROM \"KPI\".\"autogen\".\"db_version\""
	fmt.Println("Attempting to check raw influx database version")
	// attempt to connect to the raw influx database and check the seed file date against the last date stored in the database
	for i := 1; i <= retryAttempts; i++ {
		resp, err := s.influxRawDB.Query(query, "KPI")
		if err != nil {
			time.Sleep(time.Second * time.Duration(sleepTime))
			fmt.Printf("Retry attempt %d/%d\n", i, retryAttempts)
			continue
		}

		// if there is no values or the dates don't match, the database is not up-to-date so retry, else exit successfully
		if len(resp.Results[0].Series) < 1 || resp.Results[0].Series[0].Values[0][1] != dateString {
			time.Sleep(time.Second * time.Duration(sleepTime))
			fmt.Printf("Retry attempt %d/%d\n", i, retryAttempts)
			continue
		}
		return nil
	}

	// unable to connect to newest version of the database, return error
	return fmt.Errorf("unable to connect to latest version of raw influx database")
}

func (s *Service) loadCalculators() []structs.KPICalculatorInterface {
	//var availabilityCalculator calculators.KPICalculatorInterface = calculators.NewAvailability(s.influxRawDB)
	seamType := structs.NewSeamType(s.influxRawDB, s.measurementDB, s.arangoDB)
	//throughput := calculators.NewThroughput(s.influxRawDB)
	return []structs.KPICalculatorInterface{
		//structs.NewSetPoints(s.influxRawDB, s.arangoDB),
		//structs.NewAvailability("availability", s.influxRawDB, s.arangoDB),
		structs.NewFailurePrediction("failure-prediction-tailings", "Tailings", s.influxRawDB, s.arangoDB, s.configServiceClient, *seamType),
		structs.NewFailurePrediction("failure-prediction-second_stage", "SecondStage", s.influxRawDB, s.arangoDB, s.configServiceClient, *seamType),
	}
}

// Start begins the service, returns error if a cron function cannot be created
func (s *Service) Start() {
	fmt.Println("Starting Cron Service")

	c := cron.New()
	err := c.AddFunc(s.config.CronSample, func() {
		//fmt.Println(s.config.CronSample)
	})
	if err != nil {
		insights.PanicErrorIfExists(err)
	}

	s.keycloaker = insights.NewKeycloaker(s.config.KeycloakAddress, s.config.KeycloakRealm, s.config.KeycloakClientID, s.config.KeycloakClientSecret)
	keyErr := s.keycloaker.RetrievePublicKey()
	if keyErr != nil && keyErr != http.ErrServerClosed {
		insights.PanicErrorIfExists(keyErr)
	}

	s.configServiceClient = insights.NewGraphqlClienter(&s.keycloaker, "http://config-service:8080/graphql") // TODO: make this an env var
	s.rabbit = insights.ConnectToBroker(fmt.Sprintf("amqp://%s:%s@%s:%s/", s.config.RabbitMQUser,
		s.config.RabbitMQPassword, s.config.RabbitMQAddress, s.config.RabbitMQPort))
	s.KPICalculators = s.loadCalculators()
	s.setupRouteHandlers()

	c.Start()
	defer c.Stop()

	err = s.server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		insights.PanicErrorIfExists(err)
	}

	fmt.Println("Cron Service Started")
}

func (s *Service) runCalculators() {
	timestamp := time.Now()
	for _, calc := range s.KPICalculators {
		events := calc.GenerateEvents(timestamp)
		err := s.sendEvents(events)
		if err != nil {
			insights.LogErrorWithInfo(err, "", "error publishing message to RabbitMQ for calculator: %s", calc.GetID())
		}
	}
	// TODO: Insert calculated data back into measurementDB
}

func (s *Service) sendEvents(events []structs.Event) error {
	for i := range events {
		event := events[i]
		bytes, marshalErr := json.Marshal(event)
		if marshalErr != nil {
			return marshalErr
		}
		fmt.Println(event)
		err := s.rabbit.Publish(bytes, "EventEntity", "fanout")
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) getGraphqlFromFile() *graphql.Schema {
	schemaBytes, loadErr := ioutil.ReadFile(s.config.GraphQLSchemaFile)
	if loadErr != nil {
		insights.PanicErrorIfExists(loadErr)
	}

	resolver := &Resolver{
	}

	schema, parseErr := graphql.ParseSchema(string(schemaBytes), resolver, graphql.UseStringDescriptions())
	if parseErr != nil {
		insights.LogErrorWithInfo(parseErr, "Check graphql resolvers and schema", "Failed to parse graphql schema into resolvers")
		insights.PanicErrorIfExists(parseErr)
	}
	return schema
}

// handleHealthCheck provides an endpoint to check the health of the cron service
func (s *Service) handleHealthCheck(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint /health Accessed")
	s.runCalculators()
	w.WriteHeader(http.StatusOK)
}

func (s *Service) installKPIs(w http.ResponseWriter, r *http.Request) {
	respVal, err := json.Marshal(s.KPICalculators)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_, writeErr := w.Write(respVal)
	if writeErr != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
