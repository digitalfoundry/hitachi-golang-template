package cronservice

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"testing"
	"time"

	"github.com/influxdata/influxdb1-client/models"
	client "github.com/influxdata/influxdb1-client/v2"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
	"github.com/stretchr/testify/mock"

	"github.com/stretchr/testify/assert"
)

func TestNewService(t *testing.T) {
	newService := NewService()
	assert.Nil(t, newService.config)
}

//nolint:gocyclo
func TestService_Init(t *testing.T) {
	tmpDir, err := ioutil.TempDir("", "temp")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := os.RemoveAll(tmpDir)
		if err != nil {
			t.Fatal(err)
		}
	}()

	tempFilePath := path.Join(tmpDir, "version.json")
	type Version struct {
		Version string `json:"version"`
	}

	value := Version{Version: "1"}
	versionFile, fileErr := json.MarshalIndent(value, "", "")
	if fileErr != nil {
		t.Fatal(fileErr)
	}

	writeErr := ioutil.WriteFile(tempFilePath, versionFile, 0644)
	if writeErr != nil {
		t.Fatal(writeErr)
	}

	fileInfo, infoErr := os.Stat(tempFilePath)
	if infoErr != nil {
		t.Fatal(infoErr)
	}

	tempGraphSchemaPath := path.Join(tmpDir, "cron-service.graphql")

	writeGraphErr := ioutil.WriteFile(tempGraphSchemaPath, []byte{}, 0644)
	if writeGraphErr != nil {
		t.Fatal(writeGraphErr)
	}

	fileInfo, infoErr = os.Stat(tempGraphSchemaPath)
	if infoErr != nil {
		t.Fatal(infoErr)
	}

	modDate := fileInfo.ModTime().Format(time.RFC1123Z)

	versionDB := structs.NewMockArangoDBer()
	versionCollection := &structs.MockArangoCollection{}
	versionDB.On("Connect", mock.Anything, mock.Anything).Return(nil)
	versionDB.On("GetCollection", mock.Anything, mock.Anything).Return(versionCollection, nil).Once()
	versionCollection.On("ReadDocument", mock.Anything, mock.Anything, mock.Anything).Return(nil, value.Version).Once()

	service := &Service{}
	newArangoDB = func(dbAddress string) insights.ArangoDBer {
		return versionDB
	}

	resp := client.Response{
		Results: []client.Result{
			{
				Series: []models.Row{
					{
						Name:    "",
						Tags:    nil,
						Columns: nil,
						Values: [][]interface{}{
							{
								"value",
								modDate,
							},
						},
						Partial: false,
					},
				},
				Messages: nil,
				Err:      "",
			},
		},
		Err: "",
	}

	influxVersionDB := structs.NewMockInfluxDBer()
	influxVersionDB.On("Connect", mock.Anything, mock.Anything, mock.Anything).Return(nil).Twice()
	influxVersionDB.On("Query", mock.Anything, mock.Anything, mock.Anything).Return(&resp, nil).Once()
	newInfluxDB = func() insights.InfluxDBer {
		return influxVersionDB
	}

	envErr := os.Setenv("CRON_SAMPLE_ROUTINE", "@every 1s")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("ARANGO_DB_VERSION_FILE", tempFilePath)
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("HOST_PORT", "8080")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("GRAPHQL_SCHEMA_FILE", tempGraphSchemaPath)
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("INFLUX_RAW_DB_ADDRESS", "http://influxdb-raw:8086")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("INFLUX_RAW_DB_USERNAME", "user")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("INFLUX_RAW_DB_PASSWORD", "password")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("INFLUX_DB_ADDRESS", "http://influxdb:8086")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("INFLUX_DB_USERNAME", "user")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("INFLUX_DB_PASSWORD", "password")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("INFLUX_RAW_SEED_FILE", tempFilePath)
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("ARANGO_DB_ADDRESS", "http://arangodb:8529")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("ARANGO_DB_USERNAME", "user")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("ARANGO_DB_PASSWORD", "password")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("RABBIT_MQ_ADDRESS", "rabbitmq")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("RABBIT_MQ_USER", "rabbitmq")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("RABBIT_MQ_PASSWORD", "rabbitmq")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("RABBIT_MQ_PORT", "5672")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("KEYCLOAK_REALM", "mipoc4")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("KEYCLOAK_ADDRESS", "http://keycloak:8080/auth")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("KEYCLOAK_CLIENT_ID", "mipoc4webapp")
	if envErr != nil {
		t.Fatal(envErr)
	}

	envErr = os.Setenv("KEYCLOAK_CLIENT_SECRET", "b45e55ce-ecf5-45ca-b58b-f683fa11d6a3")
	if envErr != nil {
		t.Fatal(envErr)
	}
	service.Init()
	assert.Equal(t, "@every 1s", service.config.CronSample)
	assert.Equal(t, "8080", service.config.HostPort)
	assert.NotNil(t, service.serveMux)
	assert.NotNil(t, service.server)
}

func TestCronService_handleHealthCheck(t *testing.T) {
	mockService := Service{}
	req, err := http.NewRequest(http.MethodGet, "/health", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mockService.handleHealthCheck)

	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
}
