package insights

import "fmt"

// PanicErrorIfExists panics on the parameterized error object
func PanicErrorIfExists(err error) {
	if err != nil {
		panic(err)
	}
}

// LogErrorWithInfo logs out the error with an extra message and action provided through parameters
func LogErrorWithInfo(err error, action string, message string, msgParams ...interface{}) {
	msg := fmt.Sprintf(message, msgParams...)
	fmt.Println(fmt.Errorf("Error Encountered: \nMessage: (%s)\nAction: (%s)\n Error: (%v)", msg, action, err))
}

// DeferHandleError panics if an error occurs during a deferred function
func DeferHandleError(errorFunc func() error) {
	err := errorFunc()
	if err != nil {
		PanicErrorIfExists(err)
	}
}
