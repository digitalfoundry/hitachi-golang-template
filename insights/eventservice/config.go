package eventservice

import (
	"fmt"
	"os"
)

type config struct {
	HostPort             string
	ArangoDBAddress      string
	ArangoDBUsername     string
	ArangoDBPassword     string
	ArangoDBVersionFile  string
	KeycloakAddress      string
	KeycloakRealm        string
	GraphQLSchemaFile    string
	KeycloakClientID     string
	KeycloakClientSecret string
}

func getConfig() *config {
	getEnvString := func(key string, optional bool) string {
		environmentVar := os.Getenv(key)
		if environmentVar == "" {
			panic(fmt.Sprintf("$%s must be set. please make sure the environment values are set correctly", key))
		}
		return environmentVar
	}

	return &config{
		ArangoDBAddress:      getEnvString("ARANGO_DB_ADDRESS", false),
		ArangoDBUsername:     getEnvString("ARANGO_DB_USERNAME", false),
		ArangoDBPassword:     getEnvString("ARANGO_DB_PASSWORD", false),
		ArangoDBVersionFile:  getEnvString("ARANGO_DB_VERSION_FILE", false),
		HostPort:             getEnvString("HOST_PORT", false),
		GraphQLSchemaFile:    getEnvString("GRAPHQL_SCHEMA_FILE", false),
		KeycloakRealm:        getEnvString("KEYCLOAK_REALM", false),
		KeycloakAddress:      getEnvString("KEYCLOAK_ADDRESS", false),
		KeycloakClientID:     getEnvString("KEYCLOAK_CLIENT_ID", false),
		KeycloakClientSecret: getEnvString("KEYCLOAK_CLIENT_SECRET", false),
	}
}
