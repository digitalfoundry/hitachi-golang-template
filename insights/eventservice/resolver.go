package eventservice

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"time"

	driver "github.com/arangodb/go-driver"
	"github.com/graph-gophers/graphql-go"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
)

// Resolver is the root resolver for the configuration service. It contains all mutation and query resolver functions
type Resolver struct {
	db                  insights.ArangoDBer
	keycloaker          insights.Keycloaker
	assetServiceClient  insights.Clienter
	configServiceClient insights.Clienter
}

type eventByIDQueryArgs struct {
	ID string
}

// EventByID is the query resolver function for eventById graphql function
func (r *Resolver) EventByID(ctx context.Context, args eventByIDQueryArgs) (*structs.EventResolver, error) {
	fmt.Println("Graphql endpoint eventByID accessed")

	event, err := r.getEventByID(ctx, args.ID)
	if err != nil {
		return nil, err
	}

	fmt.Println("Successfully returned Graphql Resolvers for eventById endpoint")
	return structs.NewEventResolver(event), nil
}

func (r *Resolver) getEventByID(ctx context.Context, id string) (*structs.Event, error) {
	col, colErr := r.db.GetCollection("spring-demo", "Events")
	if colErr != nil {
		return nil, colErr
	}

	var evt structs.Event

	meta, readErr := col.ReadDocument(ctx, id, &evt)
	if readErr != nil {
		insights.LogErrorWithInfo(readErr, "", "Failed to read document on arango")
		return nil, readErr
	}

	evt.ID = graphql.ID(meta.Key)

	return &evt, nil
}

type eventsByAssetIDQueryArgs struct {
	AssetID string
}

func (r *Resolver) getEventsByQuery(ctx context.Context, query insights.ArangoDBQuery) ([]*structs.EventResolver, error) {
	cursor, queryErr := r.db.Query("spring-demo", query)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query spring-demo on arango")
		return nil, queryErr
	}
	defer insights.DeferHandleError(cursor.Close)

	var resolverArr []*structs.EventResolver

	for {
		var evt structs.Event
		meta, readErr := cursor.ReadDocument(ctx, &evt)
		if driver.IsNoMoreDocuments(readErr) {
			break
		} else if readErr != nil {
			insights.LogErrorWithInfo(queryErr, "", "Failed to read document on arango")
			return nil, readErr
		}

		evt.ID = graphql.ID(meta.Key)
		resolverArr = append(resolverArr, structs.NewEventResolver(&evt))
	}

	return resolverArr, nil
}

// EventsByAssetID is the query resolver function for eventsByAssetID graphql function
func (r *Resolver) EventsByAssetID(ctx context.Context, args eventsByAssetIDQueryArgs) ([]*structs.EventResolver, error) {
	fmt.Println("Graphql endpoint EventsByAssetID accessed")

	q := fmt.Sprintf("FOR e in Events FILTER e.assetId == %q return e", args.AssetID)
	query := insights.ArangoDBQuery{Query: q}

	resolverArr, err := r.getEventsByQuery(ctx, query)
	if err != nil {
		return nil, err
	}

	fmt.Println("Successfully returned Graphql Resolvers for eventsByAssetId endpoint")
	return resolverArr, nil
}

func (r *Resolver) getAssetsByNameLikeFromAssetService(ctx context.Context, assetNameFilter string) (*[]string, error) {
	graphqlRequester := insights.NewRequester(`
		query ($assetNameFilter: String!) {
			getAssetsByNameLike(assetNameFilter: $assetNameFilter) {
				id
			}
		}
	`)

	graphqlRequester.Var("assetNameFilter", assetNameFilter)

	getAssetsByNameLikeResponse := structs.GetAssetsByNameLikeResponse{}

	err := r.assetServiceClient.Run(ctx, graphqlRequester, &getAssetsByNameLikeResponse)
	if err != nil {
		return nil, err
	}

	assetIds := &[]string{}
	for _, asset := range *getAssetsByNameLikeResponse.GetAssetsByName {
		*assetIds = append(*assetIds, string(asset.ID))
	}

	return assetIds, nil
}

type eventsQueryArgs struct {
	First           *int32
	After           *string
	Sort            *structs.SortOptions
	AssetNameFilter *string
	Filter          *structs.EventFilterOptions
}

//nolint:gocyclo // Events is the query resolver function for events graphql function
func (r *Resolver) Events(ctx context.Context, args eventsQueryArgs) (*structs.EventResultResolver, error) {
	if args.AssetNameFilter != nil {
		assetIds, err := r.getAssetsByNameLikeFromAssetService(ctx, *args.AssetNameFilter)
		if err != nil {
			return nil, err
		}

		if len(*assetIds) == 0 {
			return nil, nil
		}

		filter := structs.EventFilterOptions{}
		a := "assetId"
		filter.Field = &a
		filter.String_in = assetIds

		if args.Filter != nil {
			args.Filter.AND = &[]structs.EventFilterOptions{filter}
		} else {
			args.Filter = &filter
		}
	}

	filterQuery := ""

	if args.Filter != nil {
		whereStr, err := args.Filter.GetAQLWhereClause("e")
		if err != nil {
			return nil, err
		}

		filterQuery = fmt.Sprintf("FILTER %s", whereStr)
	}

	sortStr := ""

	if args.Sort != nil {
		if args.Sort.Order != nil && *args.Sort.Order != structs.Unsorted {
			order := ""

			if *args.Sort.Order == structs.Descending {
				order = "DESC"
			}

			sortStr = fmt.Sprintf(" SORT e.%s %s", *args.Sort.Field, order)
		}
	}

	eventQuery := fmt.Sprintf(`LET result = (FOR e IN Events %s %s RETURN e)`, filterQuery, sortStr)

	pageable := ""

	if args.After != nil && args.First == nil {
		err := errors.New("malformed query")
		insights.LogErrorWithInfo(err, "", "must include first with after parameter")

		return nil, err
	} else if args.After != nil {
		pageable = fmt.Sprintf("LIMIT %s, %d", *args.After, *args.First)
	} else if args.First != nil {
		pageable = fmt.Sprintf("LIMIT %d", *args.First)
	}

	eventResultQuery := fmt.Sprintf(`LET edges = (FOR node IN result %s RETURN { cursor: NULL, node: node }) RETURN { totalCount: LENGTH(result), edges: edges }`, pageable)

	q := fmt.Sprintf("%s %s", eventQuery, eventResultQuery)

	//Example
	//LET result = (FOR e IN Events FILTER e.eventType == "Failure Prediction"  SORT e.totalCount  RETURN e)
	//LET edges = (FOR node IN result  RETURN { cursor: NULL, node: node })
	//RETURN { totalCount: LENGTH(result), edges: edges }

	query := insights.ArangoDBQuery{Query: q}

	cursor, queryErr := r.db.Query("spring-demo", query)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query spring-demo on arango")
		return nil, queryErr
	}
	defer insights.DeferHandleError(cursor.Close)

	var evtResult structs.EventResult
	// TODO: reading the result directly into the eventResult will drop ids
	// TODO: PageInfo has not been implemented
	_, readErr := cursor.ReadDocument(ctx, &evtResult)

	if readErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to read document on arango")
		return nil, readErr
	}

	return structs.NewEventResultResolver(&evtResult), nil
}

type getEventsForEachAssetIDQueryArgs struct {
	AssetsIds []string
	First     *int32
	Sort      *structs.SortOptions
	Filter    *structs.EventFilterOptions
}

// GetEventsForEachAssetID is the query resolver function for getEventsForEachAssetId graphql function
func (r *Resolver) GetEventsForEachAssetID(ctx context.Context, args getEventsForEachAssetIDQueryArgs) ([][]*structs.EventResolver, error) {
	fmt.Println("Graphql endpoint getEventsForEachAssetId accessed")

	var returnArr [][]*structs.EventResolver

	for i := range args.AssetsIds {
		// prepare LIMIT
		pageable := ""
		if args.First != nil {
			pageable = fmt.Sprintf("LIMIT %d", *args.First)
		}

		// prepare FILTER
		filter := structs.EventFilterOptions{}
		tmp := "assetId"
		filter.Field = &tmp
		filter.String_is = &args.AssetsIds[i]

		if args.Filter != nil {
			filter.AND = &[]structs.EventFilterOptions{*args.Filter}
		}

		filterQuery, err := filter.GetAQLWhereClause("e")
		if err != nil {
			return nil, err
		}

		// prepare SORT
		sortQuery := ""
		if args.Sort != nil {
			if *args.Sort.Order != structs.Unsorted {
				order := ""

				if *args.Sort.Order == structs.Descending {
					order = "DESC"
				}

				sortQuery = fmt.Sprintf("SORT e.%s %s\n", *args.Sort.Field, order)
			}
		}

		q := fmt.Sprintf("FOR e IN Events FILTER %s %s %s RETURN e", filterQuery, pageable, sortQuery)

		query := insights.ArangoDBQuery{Query: q}

		resolverArr, err := r.getEventsByQuery(ctx, query)
		if err != nil {
			return nil, err
		}

		returnArr = append(returnArr, resolverArr)
	}

	return returnArr, nil
}

type getMostSevereOpenEventsQueryArgs struct {
	AssetsIds []string
	Start     structs.Date
	End       structs.Date
	First     *int32
}

func getMostSevereOpenEventsFilter(args getMostSevereOpenEventsQueryArgs, i int) structs.EventFilterOptions {
	assetFilter := structs.EventFilterOptions{}
	a := "assetId"
	assetFilter.Field = &a
	assetFilter.String_is = &args.AssetsIds[i]

	startFilter := structs.EventFilterOptions{}
	b := "createdDate"
	startFilter.Field = &b
	startFilter.Date_gte = &args.Start

	endFilter := structs.EventFilterOptions{}
	endFilter.Field = &b
	endFilter.Date_lt = &args.End

	statusWrapper := structs.StatusWrapper{Status: structs.Open}
	statusFilter := structs.EventFilterOptions{}
	c := "status"
	statusFilter.Field = &c
	statusFilter.Status_is = &statusWrapper.Status

	assetFilter.AND = &[]structs.EventFilterOptions{startFilter, endFilter, statusFilter}

	return assetFilter
}

// getMostSevereOpenEventsByCreatedDateBetweenForEachAssetId graphql function
func (r *Resolver) GetMostSevereOpenEventsByCreatedDateBetweenForEachAssetID(ctx context.Context, args getMostSevereOpenEventsQueryArgs) ([][]*structs.EventResolver, error) {
	fmt.Println("Graphql endpoint getMostSevereOpenEventsByCreatedDateBetweenForEachAssetId accessed")

	var returnArr [][]*structs.EventResolver

	for i := range args.AssetsIds {
		// prepare LIMIT
		pageable := ""
		if args.First != nil {
			pageable = fmt.Sprintf("LIMIT %d", *args.First)
		}

		filter := getMostSevereOpenEventsFilter(args, i)

		filterQuery, err := filter.GetAQLWhereClause("e")
		if err != nil {
			return nil, err
		}

		sortQuery := "SORT e.severity DESC, e.createdDate DESC"

		q := fmt.Sprintf("FOR e IN Events FILTER %s %s %s RETURN e", filterQuery, pageable, sortQuery)

		query := insights.ArangoDBQuery{Query: q}

		resolverArr, err := r.getEventsByQuery(ctx, query)
		if err != nil {
			return nil, err
		}

		returnArr = append(returnArr, resolverArr)
	}

	return returnArr, nil
}

type getEventsByIdsQueryArgs struct {
	EventIds []string
}

// GetEventsByIds is the query resolver function for getEventsByIds graphql function
func (r *Resolver) GetEventsByIds(ctx context.Context, args getEventsByIdsQueryArgs) ([]*structs.EventResolver, error) {
	fmt.Println("Graphql endpoint getEventsByIds accessed")

	var returnArr []*structs.EventResolver

	for _, eventID := range args.EventIds {
		eventResolver, err := r.getEventByID(ctx, eventID)
		if err != nil {
			return nil, err
		}

		returnArr = append(returnArr, structs.NewEventResolver(eventResolver))
	}

	return returnArr, nil
}

func (r *Resolver) getAssetByIDFromAssetService(ctx context.Context, assetID string) (*structs.AssetEntity, error) {
	graphqlRequester := insights.NewRequester(`
		query ($id: String!) {
			getAssetById(id: $id) {
				templateId
			}
		}
	`)

	graphqlRequester.Var("id", assetID)

	getAssetByIDResponse := structs.GetAssetByIDResponse{}

	err := r.assetServiceClient.Run(ctx, graphqlRequester, &getAssetByIDResponse)
	if err != nil {
		return nil, err
	}

	return &getAssetByIDResponse.GetAssetByID, nil
}

func getEventTraceFilter(eventTypeID string, args GetEventTraceQueryArgs) structs.EventFilterOptions {
	evtFilter := structs.EventFilterOptions{}
	a := "eventTypeID"
	evtFilter.Field = &a
	evtFilter.String_is = &eventTypeID

	assetIDFilter := structs.EventFilterOptions{}
	b := "assetId"
	assetIDFilter.Field = &b
	assetIDFilter.String_is = &args.AssetID

	startFilter := structs.EventFilterOptions{}
	c := "createdDate"
	startFilter.Field = &c
	startFilter.Date_gte = &args.StartTime

	endFilter := structs.EventFilterOptions{}
	endFilter.Field = &c
	endFilter.Date_lt = &args.EndTime

	severityFilter := structs.EventFilterOptions{}
	d := "severity"
	severityFilter.Field = &d
	severityFilter.Severity_in = args.Severities

	evtFilter.AND = &[]structs.EventFilterOptions{assetIDFilter, startFilter, endFilter, severityFilter}

	return evtFilter
}

func (r *Resolver) getByAssetTypeAndEventTypeIDFromConfigService(ctx context.Context, assetTemplateID string, eventTypeID string) (*structs.EventConfiguration, error) {
	graphqlRequester := insights.NewRequester(`
			query ($assetType: String!, $eventTypeId: String!) {
				getByAssetTypeAndEventTypeId(assetType: $assetType, eventTypeId: $eventTypeId) {
					calculationField
				}
			}
		`)

	graphqlRequester.Var("assetType", assetTemplateID) // assetType maps to assetEntity.Template in the java code
	graphqlRequester.Var("eventTypeId", eventTypeID)

	getByAssetTypeAndEventTypeIDResponse := structs.GetByAssetTypeAndEventTypeIDResponse{}

	err := r.configServiceClient.Run(ctx, graphqlRequester, &getByAssetTypeAndEventTypeIDResponse)
	if err != nil {
		return nil, err
	}

	return &getByAssetTypeAndEventTypeIDResponse.GetByAssetTypeAndEventTypeID, nil
}

func (r *Resolver) getEventsByEventTypeID(ctx context.Context, args GetEventTraceQueryArgs, eventTypeID string) ([]structs.Event, error) {
	filter := getEventTraceFilter(eventTypeID, args)

	filterQuery, err := filter.GetAQLWhereClause("e")
	if err != nil {
		return nil, err
	}

	sortQuery := "SORT e.createdDate ASC"

	query := fmt.Sprintf("FOR e in Events FILTER %s %s RETURN e", filterQuery, sortQuery)

	q := insights.ArangoDBQuery{Query: query}

	cursor, queryErr := r.db.Query("spring-demo", q)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query spring-demo on arango")
		return nil, queryErr
	}

	defer insights.DeferHandleError(cursor.Close)

	eventsByEventTypeID := []structs.Event{}

	for {
		var evt structs.Event
		meta, readErr := cursor.ReadDocument(ctx, &evt)
		if driver.IsNoMoreDocuments(readErr) {
			break
		} else if readErr != nil {
			insights.LogErrorWithInfo(queryErr, "", "Failed to read document on arango")
			return nil, readErr
		} else {
			evt.ID = graphql.ID(meta.Key)
			eventsByEventTypeID = append(eventsByEventTypeID, evt)
		}
	}

	return eventsByEventTypeID, nil
}

// query parameter struct for query resolver function GetEventTrace
type GetEventTraceQueryArgs struct {
	EventTypeIds []string
	AssetID      string
	StartTime    structs.Date
	EndTime      structs.Date
	Severities   *[]structs.Severity
}

//nolint:gocyclo // GetEventTrace is the query resolver function for getEventTrace graphql function
func (r *Resolver) GetEventTrace(ctx context.Context, args GetEventTraceQueryArgs) ([]*structs.EventTraceTimeSeriesResolver, error) {
	fmt.Println("Graphql endpoint getEventTrace accessed")

	if args.Severities == nil {
		args.Severities = &[]structs.Severity{structs.Negligible, structs.Marginal, structs.Critical, structs.Catastrophic}
	}

	assetEntity, err := r.getAssetByIDFromAssetService(ctx, args.AssetID)
	if err != nil {
		return nil, err
	}

	eventsByEventTypeID := make(map[string][]structs.Event)
	// build up map of events keyed by eventTypeID
	for _, eventTypeID := range args.EventTypeIds {
		events, err := r.getEventsByEventTypeID(ctx, args, eventTypeID)
		if err != nil {
			return nil, err
		}
		if len(events) > 0 {
			eventsByEventTypeID[eventTypeID] = events
		}
	}

	eventTraceTimeSeriesResolver := []*structs.EventTraceTimeSeriesResolver{}
	for eventTypeID, events := range eventsByEventTypeID {

		config, err := r.getByAssetTypeAndEventTypeIDFromConfigService(ctx, assetEntity.TemplateID, eventTypeID)
		if err != nil {
			return nil, err
		}

		// this be the problem
		trace := structs.EventTraceTimeSeries{}
		trace.ID = graphql.ID(eventTypeID)

		eventValues := structs.TimeSeries{}
		eventValues.ID = graphql.ID(eventTypeID)

		var timeStamps []structs.Date

		values := &[]float64{}
		eventValues.Values = values
		thresholdSeries := &[]structs.TimeSeries{}

		trace.Values = &eventValues

		for _, event := range events {
			timeStamps = append(timeStamps, structs.Date{Time: event.CreatedDate})
			calculationFieldKey := config.CalculationField
			calculationFieldValue := event.Properties[*calculationFieldKey]
			if calculationFieldValue == nil {
				insights.LogErrorWithInfo(errors.New("unable to process event"), "", "calculationField is not found in properties")
				continue
			}

			thresholdMap := make(map[string]float64)

			*values = append(*values, calculationFieldValue.(float64))
			for severity, value := range event.Thresholds {
				valueType := fmt.Sprintf("%T", value)
				threshKey := severity

				if valueType == "float64" {
					thresholdMap[threshKey] = value.(float64)
				} else if valueType == "map[string]interface {}" {
					tMap := value.(map[string]interface{})
					min := tMap["left"].(float64)
					max := tMap["right"].(float64)
					if min == max {
						continue
					} else if min < max {
						thresholdMap["MIN_"+threshKey] = min
						thresholdMap["MAX_"+threshKey] = max
					}
				}
			}

			init := false
			if len(*thresholdSeries) == 0 {
				for key := range thresholdMap {
					series := structs.TimeSeries{}
					series.ID = graphql.ID(key)
					series.Timestamps = timeStamps
					series.Values = &[]float64{}
					*thresholdSeries = append(*thresholdSeries, series)
				}
				init = true
			}

			for i, series := range *thresholdSeries {
				*series.Values = append(*series.Values, thresholdMap[string(series.ID)])
				(*thresholdSeries)[i].Timestamps = timeStamps // maybe pull this outside for loops
			}

			if init {
				sort.Slice(*thresholdSeries, func(i, j int) bool {
					t1 := (*thresholdSeries)[i]
					t2 := (*thresholdSeries)[j]
					return (*t1.Values)[0] < (*t2.Values)[0]
				})
			}
		}

		trace.Threshold = *thresholdSeries

		trace.Values.Timestamps = timeStamps

		eventTraceTimeSeriesResolver = append(eventTraceTimeSeriesResolver, structs.NewEventTraceTimeSeriesResolver(&trace))
	}

	return eventTraceTimeSeriesResolver, nil
}

// query parameter struct for query resolver function Dismiss
type DismissQueryArgs struct {
	EventID string
}

// Dismiss is the mutator resolver function for dismiss graphql function
func (r *Resolver) Dismiss(ctx context.Context, args DismissQueryArgs) (*structs.EventResolver, error) {
	fmt.Println("Graphql endpoint dismiss accessed")

	event, err := r.getEventByID(ctx, args.EventID)
	if err != nil {
		return nil, err
	}

	if event.Status.Status == structs.Dismissed {
		return structs.NewEventResolver(event), nil
	}

	updatedEvent, err := r.updateEventStatus(ctx, *event, structs.Dismissed, true)
	if err != nil {
		return nil, err
	}

	return structs.NewEventResolver(updatedEvent), nil
}

func (r *Resolver) updateEventStatus(ctx context.Context, event structs.Event, status structs.Status, resolved bool) (*structs.Event, error) {
	statusWrapper, err := structs.NewStatusWrapper(status)
	if err != nil {
		return nil, err
	}
	event.Status = *statusWrapper
	resolvedDate := time.Time{}
	lastUpdated := time.Now()

	if resolved {
		resolvedDate = lastUpdated
	}

	event.ResolvedDate, event.LastUpdated = resolvedDate, resolvedDate

	col, colErr := r.db.GetCollection("spring-demo", "Events")
	if colErr != nil {
		insights.PanicErrorIfExists(colErr)
	}

	_, err = col.UpdateDocument(ctx, string(event.ID), event)
	if err != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to update document from Events collection")
		return nil, err
	}

	// TODO: sendNotification - rabbitmq
	fmt.Println("Successfully returned Graphql Resolvers for updateThreshold endpoint")
	return &event, nil
}

// query parameter struct for query resolver function Reopen
type ReopenQueryArgs struct {
	EventID string
}

// Reopen is the mutator resolver function for reopen graphql function
func (r *Resolver) Reopen(ctx context.Context, args ReopenQueryArgs) (*structs.EventResolver, error) {
	fmt.Println("Graphql endpoint reopen accessed")

	event, err := r.getEventByID(ctx, args.EventID)
	if err != nil {
		return nil, err
	}

	if event.Status.Status != structs.Dismissed {
		reopenErr := fmt.Errorf("cannot reopen event %s: status is %s", args.EventID, event.Status.Status)
		insights.LogErrorWithInfo(reopenErr, "", "cannot reopen an event that hasn't been dismissed")
		return nil, reopenErr
	}

	updatedEvent, err := r.updateEventStatus(ctx, *event, structs.Open, false)
	if err != nil {
		return nil, err
	}

	return structs.NewEventResolver(updatedEvent), nil
}

type closeQueryArgs struct {
	EventID string
}

func (r *Resolver) close(ctx context.Context, args closeQueryArgs) (*structs.EventResolver, error) {
	event, err := r.getEventByID(ctx, args.EventID)
	if err != nil {
		return nil, err
	}

	if event.Status.Status == structs.Closed {
		return structs.NewEventResolver(event), nil
	}

	updatedEvent, err := r.updateEventStatus(ctx, *event, structs.Closed, true)
	if err != nil {
		return nil, err
	}

	return structs.NewEventResolver(updatedEvent), nil
}

type setToPendingQueryArgs struct {
	EventID string
}

func (r *Resolver) setToPending(ctx context.Context, args setToPendingQueryArgs) (*structs.EventResolver, error) {
	event, err := r.getEventByID(ctx, args.EventID)
	if err != nil {
		return nil, err
	}

	switch event.Status.Status {
	case structs.Open:
		updatedEvent, err := r.updateEventStatus(ctx, *event, structs.Pending, false)
		if err != nil {
			return nil, err
		}

		return structs.NewEventResolver(updatedEvent), nil
	case structs.Pending:
		return structs.NewEventResolver(event), nil
	default:
		statusErr := fmt.Errorf("invalid status for setToPending: %s", event.Status.Status)
		insights.LogErrorWithInfo(statusErr, "", "cannot set event %s to pending", event.ID)
		return nil, statusErr
	}
}

// query parameter struct for query resolver function UpdateEventStatus
type UpdateEventStatusQueryArgs struct {
	EventID *string
	Status  *structs.Status
}

// UpdateEventStatus is the mutator resolver function for updateEventStatus graphql function
func (r *Resolver) UpdateEventStatus(ctx context.Context, args UpdateEventStatusQueryArgs) (*structs.EventResolver, error) {
	fmt.Println("Graphql endpoint reopen accessed")

	switch *args.Status {
	case structs.Open:
		return r.Reopen(ctx, ReopenQueryArgs{*args.EventID})
	case structs.Dismissed:
		return r.Dismiss(ctx, DismissQueryArgs{*args.EventID})
	case structs.Closed:
		return r.close(ctx, closeQueryArgs{*args.EventID})
	case structs.Pending:
		return r.setToPending(ctx, setToPendingQueryArgs{*args.EventID})
	default:
		return nil, fmt.Errorf("unsupported status: %v", args.Status)
	}
}

func getEventsByIdsInAndStatusFilter(eventIds []string, status structs.Status) structs.EventFilterOptions {
	eventFilter := structs.EventFilterOptions{}
	id := "id"
	eventFilter.Field = &id
	eventFilter.String_in = &eventIds

	statusWrapper := structs.StatusWrapper{Status: status}
	statusFilter := structs.EventFilterOptions{}
	a := "status"
	statusFilter.Field = &a
	statusFilter.Status_is = &statusWrapper.Status

	eventFilter.AND = &[]structs.EventFilterOptions{statusFilter}

	return eventFilter
}

func (r *Resolver) getEventsByIdsInAndStatus(ctx context.Context, eventIds []string, status structs.Status) ([]*structs.Event, error) {
	filter := getEventsByIdsInAndStatusFilter(eventIds, status)
	filterQuery, err := filter.GetAQLWhereClause("e")
	if err != nil {
		return nil, err
	}

	query := fmt.Sprintf("FOR e in Events FILTER %s RETURN e", filterQuery)

	q := insights.ArangoDBQuery{Query: query}

	cursor, queryErr := r.db.Query("spring-demo", q) // TODO: db name should be env var
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query spring-demo on arango")
		return nil, queryErr
	}
	defer insights.DeferHandleError(cursor.Close)

	var events []*structs.Event
	for {
		var event structs.Event
		meta, readErr := cursor.ReadDocument(ctx, &event)
		if driver.IsNoMoreDocuments(readErr) {
			break
		} else if readErr != nil {
			insights.LogErrorWithInfo(queryErr, "", "Failed to read document on arango")
			return nil, readErr
		} else {
			event.ID = graphql.ID(meta.Key)
			events = append(events, &event)
		}
	}

	return events, nil
}

// query parameter struct for query resolver function CloseEvents
type CloseEventsQueryArgs struct {
	EventIds []string
}

// CloseEvents is the mutator resolver function for closeEvents graphql function
func (r *Resolver) CloseEvents(ctx context.Context, args CloseEventsQueryArgs) ([]*structs.EventResolver, error) {
	fmt.Println("Graphql endpoint closeEvent accessed")
	events, err := r.getEventsByIdsInAndStatus(ctx, args.EventIds, structs.Pending)
	if err != nil {
		return nil, err
	}

	col, colErr := r.db.GetCollection("spring-demo", "Events") // TODO: collection name env var
	if colErr != nil {
		insights.PanicErrorIfExists(colErr)
	}

	var updatedEventsResolver []*structs.EventResolver

	for _, event := range events {
		lastUpdated := time.Now()
		event.LastUpdated = lastUpdated
		event.ResolvedDate = lastUpdated
		event.Status.Status = structs.Closed

		_, err := col.UpdateDocument(ctx, string(event.ID), event)
		if err != nil {
			insights.LogErrorWithInfo(colErr, "", "Failed to update document from Events collection")
			return nil, err
		}

		updatedEventsResolver = append(updatedEventsResolver, structs.NewEventResolver(event))
		// TODO: sendNotification - rabbitmq
	}

	return updatedEventsResolver, nil
}

// query parameter struct for query resolver function CleanEventServiceConfigCache
type CleanEventServiceConfigCacheQueryArgs struct {
	Keys *[]string
}

// CleanEventServiceConfigCache is the mutator resolver function for cleanEventServiceConfigCache graphql function
func (r *Resolver) CleanEventServiceConfigCache(ctx context.Context, args CleanEventServiceConfigCacheQueryArgs) (*bool, error) {
	// there were no instances of this being called on the front end
	ret := false
	return &ret, nil
}
