package eventservice

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/arangodb/go-driver"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestResolver_EventByID_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionEvent{}

	resolver := &Resolver{db: mockDb}
	e := &structs.Event{
		ID: "4444",
	}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollection, nil).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e).Once()

	args := eventByIDQueryArgs{
		ID: "4444",
	}
	eventResolver, err := resolver.EventByID(context.Background(), args)
	assert.NoError(t, err)
	assert.NotNil(t, eventResolver)

	assert.Equal(t, *eventResolver.ID(), e.ID)
}

func TestResolver_EventByID_Failure(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollectionErr := &structs.MockArangoCollectionEvent{}
	resolverColErr := &Resolver{db: mockDb}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollectionErr, fmt.Errorf("GetCollection error")).Once()

	args := eventByIDQueryArgs{
		ID: "4444",
	}
	eventResolverColEr, colErr := resolverColErr.EventByID(context.Background(), args)
	assert.Error(t, colErr, "GetCollection error")
	assert.Nil(t, eventResolverColEr)

	mockDbReadErr := structs.NewMockArangoDBer()
	mockCollectionReadErr := &structs.MockArangoCollectionEvent{}
	resolverReadErr := &Resolver{db: mockDbReadErr}

	mockCollectionReadErr.On("ReadDocument", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("ReadDocument error"), nil).Once()
	mockDbReadErr.On("GetCollection", "spring-demo", "Events").Return(mockCollectionReadErr, nil).Once()

	eventResolverReadErr, readErr := resolverReadErr.EventByID(context.Background(), args)
	assert.Error(t, readErr)
	assert.Nil(t, eventResolverReadErr)
}

func TestResolver_EventsByAssetId_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEvent{}
	resolver := &Resolver{db: mockDb}

	e1 := &structs.Event{AssetID: "assetId"}
	e2 := &structs.Event{AssetID: "assetId"}

	mockDb.On("Query", mock.Anything, mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e2).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	args := eventsByAssetIDQueryArgs{
		AssetID: "assetId",
	}

	eventResolver, err := resolver.EventsByAssetID(context.Background(), args)
	assert.NoError(t, err)
	assert.NotNil(t, eventResolver)
	assert.Len(t, eventResolver, 2)

	assert.Equal(t, eventResolver[0].AssetID(), e1.AssetID)
	assert.Equal(t, eventResolver[1].AssetID(), e2.AssetID)
}

func TestResolver_EventsByAssetId_Failure(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEvent{}
	resolver := &Resolver{db: mockDb}

	mockDb.On("Query", mock.Anything, mock.Anything).Return(mockCursor, fmt.Errorf("Query error")).Once()

	args := eventsByAssetIDQueryArgs{
		AssetID: "assetId",
	}

	eventResolver, queryErr := resolver.EventsByAssetID(context.Background(), args)
	assert.Nil(t, eventResolver)
	assert.NotNil(t, queryErr)

	mockDbRead := structs.NewMockArangoDBer()
	mockCursorRead := &structs.MockArangoCursorEvent{}
	resolverRead := &Resolver{db: mockDbRead}

	mockDbRead.On("Query", "spring-demo", mock.Anything).Return(mockCursorRead, nil).Once()
	mockCursorRead.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("ReadDocument error"), nil).Once()
	mockCursorRead.On("Close").Return(nil).Once()

	eventResolver, err := resolverRead.EventsByAssetID(context.Background(), args)
	assert.Nil(t, eventResolver)
	assert.NotNil(t, err)
}

func TestResolver_Events_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEventResult{}
	mockASClient := structs.NewMockClienter()
	resolver := &Resolver{db: mockDb, assetServiceClient: mockASClient}

	mockResp := &structs.GetAssetsByNameLikeResponse{
		GetAssetsByName: &[]structs.AssetEntity{{ID: "assetId"}},
	}

	mockASClient.On("Run", mock.Anything, mock.Anything, mock.Anything).Return(nil, mockResp)

	eventResult := &structs.EventResult{
		Edges: []structs.EventEdge{
			{Cursor: "", Node: structs.Event{ID: "4444"}},
			{Cursor: "", Node: structs.Event{ID: "5555"}},
		},
		PageInfo:   &structs.PageInfo{},
		TotalCount: 2,
	}

	mockDb.On("Query", "spring-demo", mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, eventResult).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	first := int32(10)
	sortField := "assetId"
	filterField := "eventType"
	filterString := "Failure_Prediction"
	assetNameFilter := "assetNameFilter"
	args := eventsQueryArgs{
		First: &first,
		Sort: &structs.SortOptions{
			Field: &sortField,
		},
		Filter: &structs.EventFilterOptions{
			Field:     &filterField,
			String_is: &filterString,
		},
		AssetNameFilter: &assetNameFilter,
	}

	eventsResultResolver, err := resolver.Events(context.Background(), args)
	assert.NoError(t, err)
	assert.NotNil(t, eventsResultResolver)

	assert.Equal(t, eventResult.Edges[0].Node.ID, *eventsResultResolver.Edges()[0].Node().ID())
	assert.Equal(t, eventResult.Edges[1].Node.ID, *eventsResultResolver.Edges()[1].Node().ID())

	assert.Equal(t, eventResult.TotalCount, eventsResultResolver.TotalCount())
}

func TestResolver_Events_Failure(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEventResult{}
	mockASClient := structs.NewMockClienter()
	resolver := &Resolver{db: mockDb, assetServiceClient: mockASClient}

	mockResp := &structs.GetAssetsByNameLikeResponse{
		GetAssetsByName: &[]structs.AssetEntity{{ID: "assetId"}},
	}

	mockASClient.On("Run", mock.Anything, mock.Anything, mock.Anything).Return(nil, mockResp)

	mockDb.On("Query", "spring-demo", mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("ReadDocument error"), nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	first := int32(10)

	args := eventsQueryArgs{
		First: &first,
	}

	eventsResultResolver, err := resolver.Events(context.Background(), args)
	assert.Nil(t, eventsResultResolver)
	assert.NotNil(t, err)

	after := "5"

	args = eventsQueryArgs{
		After: &after,
	}

	resolverArgErr, err := resolver.Events(context.Background(), args)
	assert.Nil(t, resolverArgErr)
	assert.NotNil(t, err)
}

func TestResolver_GetEventsForEachAssetId_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEvent{}

	resolver := &Resolver{db: mockDb}

	e1 := &structs.Event{AssetID: "assetIdAlpha"}
	e2 := &structs.Event{AssetID: "assetIdOmega"}

	mockDb.On("Query", mock.Anything, mock.Anything).Return(mockCursor, nil).Twice()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e2).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Twice()

	args := getEventsForEachAssetIDQueryArgs{
		AssetsIds: []string{"CHPP", "Failure_Prediction"},
	}

	eventResolver, err := resolver.GetEventsForEachAssetID(context.Background(), args)
	assert.NoError(t, err)
	assert.NotNil(t, eventResolver)
	assert.Len(t, eventResolver, 2)

	assert.Equal(t, e1.AssetID, eventResolver[0][0].AssetID())
	assert.Equal(t, e2.AssetID, eventResolver[1][0].AssetID())
}

func TestResolver_GetEventsForEachAssetId_Failure(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEvent{}

	resolver := &Resolver{db: mockDb}

	mockDb.On("Query", mock.Anything, mock.Anything).Return(mockCursor, nil).Twice()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("ReadDocument error"), nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	args := getEventsForEachAssetIDQueryArgs{
		AssetsIds: []string{"assetIdAlpha", "assetIdOmega"},
	}

	eventResolver, err := resolver.GetEventsForEachAssetID(context.Background(), args)
	assert.Nil(t, eventResolver)
	assert.NotNil(t, err)
}

func TestResolver_GetMostSevereOpenEventsByCreatedDateBetweenForEachAssetId_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEvent{}

	resolver := &Resolver{db: mockDb}

	e1 := &structs.Event{AssetID: "assetIdAlpha"}
	e2 := &structs.Event{AssetID: "assetIdOmega"}

	mockDb.On("Query", mock.Anything, mock.Anything).Return(mockCursor, nil).Twice()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e2).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Twice()

	args := getMostSevereOpenEventsQueryArgs{
		AssetsIds: []string{"CHPP", "Failure_Prediction"},
	}

	eventResolver, err := resolver.GetMostSevereOpenEventsByCreatedDateBetweenForEachAssetID(context.Background(), args)
	assert.NoError(t, err)
	assert.NotNil(t, eventResolver)
	assert.Len(t, eventResolver, 2)

	assert.Equal(t, e1.AssetID, eventResolver[0][0].AssetID())
	assert.Equal(t, e2.AssetID, eventResolver[1][0].AssetID())
}

func TestResolver_GetMostSevereOpenEventsByCreatedDateBetweenForEachAssetId_Failure(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEvent{}

	resolver := &Resolver{db: mockDb}

	mockDb.On("Query", mock.Anything, mock.Anything).Return(mockCursor, nil).Twice()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("ReadDocument error"), nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	args := getMostSevereOpenEventsQueryArgs{
		AssetsIds: []string{"assetIdAlpha", "assetIdOmega"},
	}

	eventResolver, err := resolver.GetMostSevereOpenEventsByCreatedDateBetweenForEachAssetID(context.Background(), args)
	assert.Nil(t, eventResolver)
	assert.NotNil(t, err)
}

func TestResolver_GetEventsByIds_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionEvent{}

	resolver := &Resolver{db: mockDb}
	e1 := &structs.Event{ID: "4444"}
	e2 := &structs.Event{ID: "5555"}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollection, nil).Twice()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e1).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e2).Once()

	args := getEventsByIdsQueryArgs{EventIds: []string{"4444", "5555"}}
	eventResolver, err := resolver.GetEventsByIds(context.Background(), args)
	assert.NoError(t, err)
	assert.NotNil(t, eventResolver)

	assert.Equal(t, e1.ID, *eventResolver[0].ID())
	assert.Equal(t, e2.ID, *eventResolver[1].ID())
}

func TestResolver_GetEventsByIds_Failure(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollectionErr := &structs.MockArangoCollectionEvent{}

	resolver := &Resolver{db: mockDb}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollectionErr, fmt.Errorf("GetCollection error")).Once()

	args := getEventsByIdsQueryArgs{EventIds: []string{"4444", "5555"}}
	eventResolver, err := resolver.GetEventsByIds(context.Background(), args)
	assert.Nil(t, eventResolver)
	assert.NotNil(t, err)

	mockDbReadErr := structs.NewMockArangoDBer()
	mockCollectionReadErr := &structs.MockArangoCollectionEvent{}
	resolverReadErr := &Resolver{db: mockDbReadErr}

	mockDbReadErr.On("GetCollection", "spring-demo", "Events").Return(mockCollectionReadErr, nil).Once()
	mockCollectionReadErr.On("ReadDocument", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("ReadDocument error"), nil).Once()

	eventResolverReadErr, readErr := resolverReadErr.GetEventsByIds(context.Background(), args)
	assert.NotNil(t, readErr)
	assert.Nil(t, eventResolverReadErr)
}

func TestResolver_GetEventTrace_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEvent{}
	mockASClient := structs.NewMockClienter()
	mockCSClient := structs.NewMockClienter()

	resolver := &Resolver{db: mockDb, assetServiceClient: mockASClient, configServiceClient: mockCSClient}

	mockASResp := &structs.GetAssetByIDResponse{GetAssetByID: structs.AssetEntity{TemplateID: "Tailings"}}

	mockASClient.On("Run", mock.Anything, mock.Anything, mock.Anything).Return(nil, mockASResp)

	eventProps := make(map[string]interface{})
	eventProps["throughput"] = 1234.1234
	eventThresh := make(map[string]interface{})
	eventThresh[string(structs.Critical)] = 54

	e1 := &structs.Event{
		EventTypeID: "Failure_Prediction",
		CreatedDate: time.Now(),
		Properties:  eventProps,
		Thresholds:  eventThresh,
	}

	mockDb.On("Query", mock.Anything, mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()
	throughput := "throughput"

	mockCSResp := &structs.GetByAssetTypeAndEventTypeIDResponse{
		GetByAssetTypeAndEventTypeID: structs.EventConfiguration{CalculationField: &throughput},
	}

	mockCSClient.On("Run", mock.Anything, mock.Anything, mock.Anything).Return(nil, mockCSResp)

	args := GetEventTraceQueryArgs{
		EventTypeIds: []string{"Failure_Prediction"},
		AssetID:      "Tailings",
	}

	eventResolver, err := resolver.GetEventTrace(context.Background(), args)

	assert.NoError(t, err)
	assert.NotNil(t, eventResolver)
}

func TestResolver_GetEventTrace_Failure(t *testing.T) {
	// asset-service error
	mockDb := structs.NewMockArangoDBer()
	mockASClient := structs.NewMockClienter()

	resolver := &Resolver{db: mockDb, assetServiceClient: mockASClient}

	mockASClient.On("Run", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("asset-service error"), nil)

	args := GetEventTraceQueryArgs{
		EventTypeIds: []string{"Failure_Prediction"},
		AssetID:      "Tailings",
	}

	eventResolver, err := resolver.GetEventTrace(context.Background(), args)

	assert.Error(t, err, "asset-service error")
	assert.Nil(t, eventResolver)

	// read document error
	mockDb = structs.NewMockArangoDBer()
	mockCursor := &structs.MockArangoCursorEvent{}
	mockASClient = structs.NewMockClienter()
	mockCSClient := structs.NewMockClienter()

	resolver = &Resolver{db: mockDb, assetServiceClient: mockASClient, configServiceClient: mockCSClient}

	mockASResp := &structs.GetAssetByIDResponse{GetAssetByID: structs.AssetEntity{TemplateID: "Tailings"}}

	mockASClient.On("Run", mock.Anything, mock.Anything, mock.Anything).Return(nil, mockASResp)

	mockDb.On("Query", mock.Anything, mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("ReadDocument error"), nil).Once()
	mockCursor.On("Close").Return(nil).Once()
	throughput := "throughput"

	mockCSResp := &structs.GetByAssetTypeAndEventTypeIDResponse{
		GetByAssetTypeAndEventTypeID: structs.EventConfiguration{CalculationField: &throughput},
	}

	mockCSClient.On("Run", mock.Anything, mock.Anything, mock.Anything).Return(nil, mockCSResp)

	args = GetEventTraceQueryArgs{
		EventTypeIds: []string{"Failure_Prediction"},
		AssetID:      "Tailings",
	}

	eventResolver, err = resolver.GetEventTrace(context.Background(), args)

	assert.Error(t, err, "ReadDocument error")
	assert.Nil(t, eventResolver)

	// config-service error
	mockDb = structs.NewMockArangoDBer()
	mockCursor = &structs.MockArangoCursorEvent{}
	mockASClient = structs.NewMockClienter()
	mockCSClient = structs.NewMockClienter()

	resolver = &Resolver{db: mockDb, assetServiceClient: mockASClient, configServiceClient: mockCSClient}

	mockASResp = &structs.GetAssetByIDResponse{GetAssetByID: structs.AssetEntity{TemplateID: "Tailings"}}

	mockASClient.On("Run", mock.Anything, mock.Anything, mock.Anything).Return(nil, mockASResp)

	eventProps := make(map[string]interface{})
	eventProps["throughput"] = 1234.1234
	eventThresh := make(map[string]interface{})
	eventThresh[string(structs.Critical)] = 54

	e1 := &structs.Event{
		EventTypeID: "Failure_Prediction",
		CreatedDate: time.Now(),
		Properties:  eventProps,
		Thresholds:  eventThresh,
	}

	mockDb.On("Query", mock.Anything, mock.Anything).Return(mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, e1).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()
	mockCursor.On("Close").Return(nil).Once()

	mockCSClient.On("Run", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("config-service error"), nil)

	args = GetEventTraceQueryArgs{
		EventTypeIds: []string{"Failure_Prediction"},
		AssetID:      "Tailings",
	}

	eventResolver, err = resolver.GetEventTrace(context.Background(), args)

	assert.Error(t, err, "config-service error")
	assert.Nil(t, eventResolver)
}

func TestResolver_Dismiss_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionEvent{}

	resolver := &Resolver{db: mockDb}
	evt := &structs.Event{
		ID: "4444",
	}
	updatedEvt := &structs.Event{
		ID:     "4444",
		Status: structs.StatusWrapper{Status: structs.Dismissed},
	}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollection, nil).Twice()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, evt).Once()
	mockCollection.On("UpdateDocument", mock.Anything, mock.Anything, mock.Anything).Return(nil, updatedEvt).Once()

	args := DismissQueryArgs{
		EventID: "4444",
	}

	eventResolver, err := resolver.Dismiss(context.Background(), args)
	assert.NoError(t, err)
	assert.NotNil(t, eventResolver)

	assert.Equal(t, *eventResolver.ID(), evt.ID)
	updatedStatus, statusErr := eventResolver.Status()
	assert.Equal(t, structs.Dismissed, updatedStatus)
	assert.Nil(t, statusErr)
}

func TestResolver_Dismiss_Failure(t *testing.T) {
	// ReadDocument error
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionEvent{}

	resolver := &Resolver{db: mockDb}
	evt := &structs.Event{
		ID: "4444",
	}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollection, nil).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("ReadDocument error"), nil).Once()

	args := DismissQueryArgs{
		EventID: "4444",
	}

	eventResolver, err := resolver.Dismiss(context.Background(), args)
	assert.Error(t, err, "ReadDocument error")
	assert.Nil(t, eventResolver)

	// UpdateDocument error
	mockDb = structs.NewMockArangoDBer()
	mockCollection = &structs.MockArangoCollectionEvent{}

	resolver = &Resolver{db: mockDb}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollection, nil).Twice()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, evt).Once()
	mockCollection.On("UpdateDocument", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("UpdateDocument error"), nil).Once()

	eventResolver, err = resolver.Dismiss(context.Background(), args)
	assert.Error(t, err, "UpdateDocument error")
	assert.Nil(t, eventResolver)
}

func TestResolver_Reopen_Success(t *testing.T) {
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionEvent{}

	resolver := &Resolver{db: mockDb}
	evt := &structs.Event{
		ID:     "4444",
		Status: structs.StatusWrapper{Status: structs.Dismissed},
	}

	updatedEvt := &structs.Event{
		ID:     "4444",
		Status: structs.StatusWrapper{Status: structs.Open},
	}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollection, nil).Twice()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, evt).Once()
	mockCollection.On("UpdateDocument", mock.Anything, mock.Anything, mock.Anything).Return(nil, updatedEvt).Once()

	args := ReopenQueryArgs{
		EventID: "4444",
	}

	eventResolver, err := resolver.Reopen(context.Background(), args)
	assert.NoError(t, err)
	assert.NotNil(t, eventResolver)

	assert.Equal(t, *eventResolver.ID(), evt.ID)
	updatedStatus, statusErr := eventResolver.Status()
	assert.Equal(t, structs.Open, updatedStatus)
	assert.Nil(t, statusErr)
}

func TestResolver_Reopen_Failure(t *testing.T) {
	// ReadDocument error
	mockDb := structs.NewMockArangoDBer()
	mockCollection := &structs.MockArangoCollectionEvent{}

	resolver := &Resolver{db: mockDb}
	evt := &structs.Event{
		ID:     "4444",
		Status: structs.StatusWrapper{Status: structs.Dismissed},
	}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollection, nil).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("ReadDocument error"), nil).Once()

	args := ReopenQueryArgs{
		EventID: "4444",
	}

	eventResolver, err := resolver.Reopen(context.Background(), args)
	assert.Error(t, err, "ReadDocument error")
	assert.Nil(t, eventResolver)

	// UpdateDocument error
	mockDb = structs.NewMockArangoDBer()
	mockCollection = &structs.MockArangoCollectionEvent{}

	resolver = &Resolver{db: mockDb}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollection, nil).Twice()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, evt).Once()
	mockCollection.On("UpdateDocument", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("UpdateDocument error"), nil).Once()

	eventResolver, err = resolver.Reopen(context.Background(), args)
	assert.Error(t, err, "UpdateDocument error")
	assert.Nil(t, eventResolver)

	// cannot reopen event error
	mockDb = structs.NewMockArangoDBer()
	mockCollection = &structs.MockArangoCollectionEvent{}

	resolver = &Resolver{db: mockDb}

	openEvt := &structs.Event{
		ID:     "4444",
		Status: structs.StatusWrapper{Status: structs.Open},
	}

	mockDb.On("GetCollection", "spring-demo", "Events").Return(mockCollection, nil).Once()
	mockCollection.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, openEvt).Once()

	eventResolver, err = resolver.Reopen(context.Background(), args)
	assert.Error(t, err)
	assert.Nil(t, eventResolver)
}

// TODO: test remaining mutators
