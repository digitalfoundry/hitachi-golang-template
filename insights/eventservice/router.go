package eventservice

import (
	"fmt"
	"net/http"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"

	"github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
)

// SetupGraphqlEndpoint initializes up the graphql query route and authentication handler
func (s *Service) SetupGraphqlEndpoint(schema *graphql.Schema) {
	handler := &relay.Handler{Schema: schema}

	s.serveMux.HandleFunc("/graphql", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("handling qraphql request")

		isOk, err := s.keycloaker.VerifyRequest(r)

		insights.PanicErrorIfExists(err)

		if isOk {
			handler.ServeHTTP(w, r)
		} else {
			http.Error(w, "Not authorized", 401)
		}
	})
}
func (s *Service) routeHandleFunc(route string, function func(w http.ResponseWriter, r *http.Request)) {
	s.serveMux.HandleFunc(route, function)
}

func (s *Service) setUpRouteHandlers() {
	s.routeHandleFunc(homeRoute, s.handleHomeRoute)
}
