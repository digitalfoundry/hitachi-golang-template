package eventservice

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"github.com/graph-gophers/graphql-go"
)

// new returns a new instance of the service
func NewService() *Service {
	return &Service{}
}

// Service is the primary struct for the service
type Service struct {
	serveMux            *http.ServeMux
	server              *http.Server
	config              *config
	gqlSchema           *graphql.Schema
	arangoDB            insights.ArangoDBer
	keycloaker          insights.Keycloaker
	assetServiceClient  insights.Clienter
	configServiceClient insights.Clienter
}

// Start starts up the service
func (s *Service) Start() {
	fmt.Println("Starting Event Service")
	s.config = getConfig()
	s.arangoDB = insights.NewArangoDBer(s.config.ArangoDBAddress)

	connectErr := s.arangoDB.Connect(s.config.ArangoDBUsername, s.config.ArangoDBPassword)
	if connectErr != nil {
		insights.LogErrorWithInfo(connectErr, "Check Username, Password and DB Address Configurations", "Connection to remote Arango instance failed")
		insights.PanicErrorIfExists(connectErr)
	}

	s.keycloaker = insights.NewKeycloaker(s.config.KeycloakAddress, s.config.KeycloakRealm, s.config.KeycloakClientID, s.config.KeycloakClientSecret)

	keyErr := s.keycloaker.RetrievePublicKey()

	if keyErr != nil && keyErr != http.ErrServerClosed {
		insights.PanicErrorIfExists(keyErr)
	}
	fmt.Println(s.keycloaker.GetAccessTokenString(false))

	s.assetServiceClient = insights.NewGraphqlClienter(&s.keycloaker, "http://asset-service:8080/graphql")   // TODO: make this an env var
	s.configServiceClient = insights.NewGraphqlClienter(&s.keycloaker, "http://config-service:8080/graphql") // TODO: make this an env var

	s.gqlSchema = s.getGraphqlFromFile()

	s.serveMux = http.NewServeMux()
	s.server = &http.Server{
		Handler: s.serveMux,
		Addr:    fmt.Sprintf(":%s", s.config.HostPort),
	}

	s.setUpRouteHandlers()

	s.SetupGraphqlEndpoint(s.gqlSchema)

	listenErr := s.server.ListenAndServe()

	if listenErr != nil && listenErr != http.ErrServerClosed {
		insights.PanicErrorIfExists(listenErr)
	}
}

func (s *Service) handleHomeRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint / Accessed")
	arango := insights.NewArangoDBer(s.config.ArangoDBAddress)
	connectErr := arango.Connect(s.config.ArangoDBUsername, s.config.ArangoDBPassword)
	if connectErr != nil {
		insights.PanicErrorIfExists(connectErr)
	}

	_, collErr := arango.GetCollection("spring-demo", "Events")
	if collErr != nil {
		insights.PanicErrorIfExists(collErr)
	}
	fmt.Println("Successfully connected to Arango")
}

func (s *Service) getGraphqlFromFile() *graphql.Schema {
	schemaBytes, loadErr := ioutil.ReadFile(s.config.GraphQLSchemaFile)
	if loadErr != nil {
		insights.PanicErrorIfExists(loadErr)
	}

	resolver := &Resolver{
		db:                  s.arangoDB,
		keycloaker:          s.keycloaker,
		assetServiceClient:  s.assetServiceClient,
		configServiceClient: s.configServiceClient,
	}

	schema, parseErr := graphql.ParseSchema(string(schemaBytes), resolver, graphql.UseStringDescriptions())
	if parseErr != nil {
		insights.LogErrorWithInfo(parseErr, "Check graphql resolvers and schema", "Failed to parse graphql schema into resolvers")
		insights.PanicErrorIfExists(parseErr)
	}
	return schema
}
