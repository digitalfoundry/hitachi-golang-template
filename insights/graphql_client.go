package insights

import (
	"context"
	"fmt"
	"net/http"

	"github.com/machinebox/graphql"
)

type transport struct {
	underlyingTransport http.RoundTripper
	keycloak            Keycloaker
}

// clienter defines functions that are called off of our Client struct
type Clienter interface {
	Run(ctx context.Context, req *Request, resp interface{}) error
}

// Client wraps the graphql client
type Client struct {
	endpoint string
	client   *graphql.Client
}

// NewGraphqlClienter returns a the wrapped Client struct for interacting with the graphql client
func NewGraphqlClienter(keycloak *Keycloaker, endpoint string) *Client {
	httpclient := http.Client{Transport: &transport{
		underlyingTransport: http.DefaultTransport,
		keycloak:            *keycloak,
	}}

	// returns our (must be ptr to pass in as interface) *client -> wraps
	client := graphql.NewClient(endpoint, graphql.WithHTTPClient(&httpclient))
	return &Client{
		endpoint: endpoint,
		client:   client,
	}
}

// Run wraps the graphql client Run function
func (c *Client) Run(ctx context.Context, req *Request, resp interface{}) error {
	err := c.client.Run(ctx, req.request, resp)
	if err != nil {
		return err
	}

	return nil
}

// Requester represents an interface for the graphql request
type Requester interface {
	Var(key string, value interface{})
}

// Request wraps the graphql client request
type Request struct {
	requestStr string
	request    *graphql.Request
}

// NewRequester returns the wrapped Request struct for interacting with the graphql client request
func NewRequester(requestStr string) *Request {
	request := graphql.NewRequest(requestStr)

	return &Request{
		requestStr: requestStr,
		request:    request,
	}
}

// Var wraps graphql client request function for building requests
func (r *Request) Var(key string, value interface{}) {
	r.request.Var(key, value)
}

func (t *transport) RoundTrip(req *http.Request) (*http.Response, error) {
	token, err := t.keycloak.GetAccessTokenString(false)

	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))

	response, tripErr := t.underlyingTransport.RoundTrip(req)

	if tripErr != nil {
		return nil, tripErr
	}

	if response.StatusCode == 401 || response.StatusCode == 400 {
		// Refresh the token and try one more time
		refreshedToken, refreshErr := t.keycloak.GetAccessTokenString(true)

		if refreshErr != nil {
			return nil, refreshErr
		}

		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", refreshedToken))

		return t.underlyingTransport.RoundTrip(req)
	}

	return response, nil
}
