package insights

import (
	"fmt"

	client "github.com/influxdata/influxdb1-client/v2"
)

// NewInfluxDber returns an InfluxDber struct
func NewInfluxDBer() InfluxDBer {
	return &influxDB{}
}

// InfluxDber is a an interface used for interacting with Influx DB
type InfluxDBer interface {
	Connect(addr string, username string, password string) error
	Close() error
	Query(query string, db string, parameters ...Binder) (*client.Response, error)
}

type influxDB struct {
	client client.Client
}

// Connect is used to connect to a remote influx instance given an address, username, and password
func (i *influxDB) Connect(addr string, username string, password string) error {
	c, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     addr,
		Username: username,
		Password: password,
	})
	if err != nil {
		return err
	}
	i.client = c
	return nil
}

// Query given a influxdb query it executes the query and returns a response
func (i *influxDB) Query(query string, db string, parameters ...Binder) (*client.Response, error) {
	q := client.NewQuery(query, db, "")
	params := make(map[string]interface{})
	for _, parameter := range parameters {
		params[parameter.Name] = parameter.Value
	}
	q.Parameters = params
	response, err := i.client.Query(q)
	if err != nil {
		return nil, err
	}

	if response.Error() != nil {
		return nil, response.Error()
	}

	return response, nil
}

// Close closes the influx connection
func (i *influxDB) Close() error {
	return i.client.Close()
}

// TemplateInitialParams performs sprint f on all non parameterized queries and sanitze all them for sql injection
func TemplateInitialParams(baseQueryString string, params ...interface{}) string {
	// FIXME actually sanitize inputs
	//for _, p := range params {
	//	// sanitize
	//}
	return fmt.Sprintf(baseQueryString, params...)
}
