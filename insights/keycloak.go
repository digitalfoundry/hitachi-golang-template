package insights

import (
	"bytes"
	"crypto/rsa"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gopkg.in/resty.v1"
)

// Keycloaker represents an interface for interacting with the keycloak service used for managing jwt
type Keycloaker interface {
	GetAccessTokenString(forceRefresh bool) (string, error)
	VerifyRequest(r *http.Request) (bool, error)
	RetrievePublicKey() error
	RefreshKeycloakToken(refreshToken string) error
}

// NewKeycloaker returns keycloak interface
func NewKeycloaker(keycloakServer string, realm string, clientID string, clientSecret string) Keycloaker {
	return &keycloak{
		keycloakServer: keycloakServer,
		realm:          realm,
		clientID:       clientID,
		clientSecret:   clientSecret,
	}
}

type keycloak struct {
	keycloakServer string
	realm          string
	clientID       string
	clientSecret   string
	publicKey      *rsa.PublicKey
	token          *OIDCToken
}

func directGrantAuthentication(keycloakServer string, realm string, clientID string, clientSecret string) (*OIDCToken, error) {
	fmt.Println("Retrieving openid token")

	response := &resty.Response{}

	retryErr := Retry(5, time.Millisecond*500, func() error {
		resp, err := resty.R().
			SetHeader("Content-Type", "application/x-www-form-urlencoded").
			SetFormData(map[string]string{
				"grant_type":    "client_credentials",
				"client_id":     clientID,
				"client_secret": clientSecret,
			}).Post(keycloakServer + "/realms/" + realm + "/protocol/openid-connect/token")

		if err != nil {
			return err
		}

		if resp.IsSuccess() {
			response = resp
		}

		return nil
	})

	if retryErr != nil {
		return nil, retryErr
	}

	return tokenFromResponse(response)
}

func tokenFromResponse(response *resty.Response) (*OIDCToken, error) {
	var result map[string]interface{}
	if err := json.Unmarshal(response.Body(), &result); err != nil {
		return nil, err
	}

	if _, ok := result["access_token"]; ok {
		return &OIDCToken{
			AccessToken:      result["access_token"].(string),
			ExpiresIn:        result["expires_in"].(float64),
			RefreshExpiresIn: result["refresh_expires_in"].(float64),
			RefreshToken:     result["refresh_token"].(string),
			TokenType:        result["token_type"].(string),
			TimeRetrieved:    float64(time.Now().Unix()),
		}, nil
	}

	return nil, errors.New("unable to process authentication response")
}

func refreshAuthentication(keycloakServer string, realm string, clientID string, clientSecret string, refreshToken string) (*OIDCToken, error) {
	fmt.Println("Retrieving openid token")

	response := &resty.Response{}

	retryErr := Retry(5, time.Millisecond*500, func() error {
		resp, err := resty.R().
			SetHeader("Content-Type", "application/x-www-form-urlencoded").
			SetFormData(map[string]string{
				"grant_type":    "refresh_token",
				"client_id":     clientID,
				"client_secret": clientSecret,
				"refresh_token": refreshToken,
			}).Post(keycloakServer + "/realms/" + realm + "/protocol/openid-connect/token")

		if err != nil {
			return err
		}

		if resp.IsSuccess() {
			response = resp
		}

		return nil
	})

	if retryErr != nil {
		return nil, retryErr
	}

	return tokenFromResponse(response)
}

func (k *keycloak) getKeycloakToken() (*OIDCToken, error) {
	if k.keycloakServer == "" || k.realm == "" {
		return nil, errors.New("keycloak server or realm is not set")
	}

	token, err := directGrantAuthentication(k.keycloakServer, k.realm, k.clientID, k.clientSecret)

	if err != nil {
		return nil, err
	}

	return token, nil
}

func (k *keycloak) RefreshKeycloakToken(refreshToken string) error {
	if k.token == nil || k.token.RefreshToken == "" {
		return errors.New("invalid refresh token")
	}

	token, err := refreshAuthentication(k.keycloakServer, k.realm, k.clientID, k.clientSecret, k.token.RefreshToken)

	if err != nil {
		return err
	}

	k.token = token
	return nil
}

// GetAccessTokenString gets a valid jwt token for an authorization header
func (k *keycloak) GetAccessTokenString(forceRefresh bool) (string, error) {
	token := k.token

	if token != nil && !forceRefresh {
		// If the token is still valid, use existing token
		now := float64(time.Now().Unix())
		timeFromRetrieval := now - token.TimeRetrieved

		// Times are in seconds. If the token has more than two minutes remaining, use existing token
		if token.ExpiresIn-timeFromRetrieval > 120 {
			return token.AccessToken, nil
		}

		if token.RefreshExpiresIn-timeFromRetrieval > 120 {
			// Refresh using token
			refreshErr := k.RefreshKeycloakToken(token.RefreshToken)

			if refreshErr != nil {
				return "", refreshErr
			}

			return k.token.AccessToken, nil
		}
	}

	// No token or refresh token is expired, we need a fresh one
	newToken, err := k.getKeycloakToken()

	k.token = newToken

	if err != nil {
		return "", err
	}

	return newToken.AccessToken, nil
}

type realmResponse struct {
	PublicKey string `json:"public_key"`
}

func (k *keycloak) insertNewlinesAtInterval(s string, n int) (string, error) {
	var buffer bytes.Buffer
	var n1 = n - 1
	var l1 = len(s) - 1
	for i, r := range s {
		_, err := buffer.WriteRune(r)

		if err != nil {
			return "", err
		}

		if i%n == n1 && i != l1 {
			_, runeErr := buffer.WriteRune('\n')

			if runeErr != nil {
				return "", runeErr
			}
		}
	}

	return buffer.String(), nil
}

// RetrievePublicKey gets the public key used to validate jwt access tokens
func (k *keycloak) RetrievePublicKey() error {
	response := &http.Response{}

	retryErr := Retry(10, time.Second, func() error {
		res, reqErr := http.Get(fmt.Sprintf("%s/realms/%s", k.keycloakServer, k.realm))

		if reqErr != nil {
			return reqErr
		}

		if res.StatusCode != 200 {
			fmt.Println(res.StatusCode)
			return errors.New("could not retrieve public key")
		}

		response = res
		return nil
	})

	if retryErr != nil {
		return retryErr
	}

	body, readErr := ioutil.ReadAll(response.Body)

	if readErr != nil {
		return readErr
	}

	respStruct := &realmResponse{}

	parseErr := json.Unmarshal(body, &respStruct)

	if parseErr != nil {
		return parseErr
	}

	separatedKey, insertErr := k.insertNewlinesAtInterval(respStruct.PublicKey, 64)

	if insertErr != nil {
		return insertErr
	}

	formattedKey := fmt.Sprintf("-----BEGIN PUBLIC KEY-----\n%s\n-----END PUBLIC KEY-----", separatedKey)

	pkey, keyErr := jwt.ParseRSAPublicKeyFromPEM([]byte(formattedKey))

	if keyErr != nil {
		return keyErr
	}

	k.publicKey = pkey
	fmt.Println("Retrieved keycloak public key")

	return nil
}

// VerifyRequest checks the authorization header of a request against the public key
func (k *keycloak) VerifyRequest(r *http.Request) (bool, error) {
	auth := r.Header.Get("Authorization")
	// Expected structure "Bearer laajhsdf90uaodsnfda0-saFDS"
	jwtStrs := strings.Split(auth, " ")
	if len(jwtStrs) != 2 {
		return false, fmt.Errorf("invalid authorization token structure")
	}
	token, err := jwt.Parse(jwtStrs[1], func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return k.publicKey, nil
	})

	if err != nil {
		return false, err
	}

	return token.Valid, nil
}
