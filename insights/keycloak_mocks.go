package insights

import "github.com/stretchr/testify/mock"

// NewMockKeycloaker returns a new mock keycloaker struct for usage in tests
func NewMockKeycloaker() *MockKeycloak {
	return &MockKeycloak{}
}

// MockKeycloak is the mock keycloak struct
type MockKeycloak struct {
	mock.Mock
}
