package kpiservice

import (
	"fmt"
	"os"
)

type config struct {
	HostPort                 string
	ArangoDBAddress          string
	ArangoDBUsername         string
	ArangoDBPassword         string
	ArangoDBVersionFile      string
	GraphQLSchemaFile        string
	SSLServerCertificatePath string
	SSLServerKeyPath         string
	KeycloakAddress          string
	KeycloakRealm            string
	KeycloakClientID         string
	KeycloakClientSecret     string
	InfluxDBAddress          string
	InfluxDBUsername         string
	InfluxDBPassword         string
}

func getConfig() *config {
	getEnvString := func(key string, optional bool) string {
		environmentVar := os.Getenv(key)
		if environmentVar == "" && !optional {
			panic(fmt.Sprintf("$%s must be set. please make sure the environment values are set correctly", key))
		}
		return environmentVar
	}

	return &config{
		HostPort:                 getEnvString("HOST_PORT", false),
		ArangoDBAddress:          getEnvString("ARANGO_DB_ADDRESS", false),
		ArangoDBUsername:         getEnvString("ARANGO_DB_USERNAME", false),
		ArangoDBPassword:         getEnvString("ARANGO_DB_PASSWORD", false),
		ArangoDBVersionFile:      getEnvString("ARANGO_DB_VERSION_FILE", false),
		GraphQLSchemaFile:        getEnvString("GRAPHQL_SCHEMA_FILE", false),
		SSLServerCertificatePath: getEnvString("SSL_SERVER_CERT_PATH", true),
		SSLServerKeyPath:         getEnvString("SSL_SERVER_KEY_PATH", true),
		KeycloakRealm:            getEnvString("KEYCLOAK_REALM", false),
		KeycloakAddress:          getEnvString("KEYCLOAK_ADDRESS", false),
		KeycloakClientID:         getEnvString("KEYCLOAK_CLIENT_ID", false),
		KeycloakClientSecret:     getEnvString("KEYCLOAK_CLIENT_SECRET", false),
		InfluxDBAddress:          getEnvString("INFLUX_DB_ADDRESS", false),
		InfluxDBUsername:         getEnvString("INFLUX_DB_USERNAME", false),
		InfluxDBPassword:         getEnvString("INFLUX_DB_PASSWORD", false),
	}
}
