package kpiservice

import (
	"context"
	"fmt"

	"github.com/emirpasic/gods/sets/treeset"

	"github.com/graph-gophers/graphql-go"

	"github.com/arangodb/go-driver"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
)

// Resolver is the root resolver for the graphql endpoint of the kpi service
type Resolver struct {
	arangoDb insights.ArangoDBer
	influxDb insights.InfluxDBer
}

// GetAvailableKPIs is the graphql resolver function to getAvailableKPIs from arango
func (r *Resolver) GetAvailableKPIs(ctx context.Context) ([]*structs.KPIResolver, error) {
	fmt.Println("Graphql endpoint getAvailableKPIs accessed")
	query := insights.ArangoDBQuery{Query: "FOR d in KPI_Metadata return d"}
	cursor, queryErr := r.arangoDb.Query("kpi-service", query)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query kpi-service on arango")
	}
	defer insights.DeferHandleError(cursor.Close)
	resolverArr := []*structs.KPIResolver{}

	for {
		kpi := structs.KPI{}
		meta, readErr := cursor.ReadDocument(ctx, &kpi)
		if driver.IsNoMoreDocuments(readErr) {
			break
		} else if readErr != nil {
			insights.LogErrorWithInfo(readErr, "", "Failed to read document on arango")
			return nil, readErr
		}
		id := graphql.ID(meta.Key)
		kpi.ID = id

		resolverArr = append(resolverArr, structs.NewKPIResolver(&kpi))
	}

	fmt.Println("Successfully returned Graphql Resolvers for getAvailableKPIs endpoint")
	return resolverArr, nil
}

type timeSeriesArgs struct {
	KpiIds             []graphql.ID
	StartTime          structs.Date
	EndTime            structs.Date
	EstimatedDataCount *int
}

// GetKPITimeSeries is the graphql resolver function for getting multiple time series objects given a kpi id, data will be smoothed to clean the data for graphing
func (r *Resolver) GetKPITimeSeries(ctx context.Context, args timeSeriesArgs) ([]*structs.TimeSeriesResolver, error) {
	fmt.Println("Graphql endpoint getKPITimeSeries accessed")
	var resolvers []*structs.TimeSeriesResolver
	var errors map[string]error
	duration := args.EndTime.Sub(args.StartTime.Time)
	if duration < 24 {
		resolvers, errors = r.getKPITimeSeriesMultiple(args.KpiIds, args.StartTime, args.EndTime)
	} else {
		estimatedDataCount := defaultEstimatedCountForSmoothing

		if args.EstimatedDataCount != nil && *args.EstimatedDataCount != 0 {
			estimatedDataCount = *args.EstimatedDataCount
		}
		smoothness := int(duration.Seconds()) / estimatedDataCount / 60
		resolvers, errors = r.getKPITimeSeriesSmoothMultiple(args.KpiIds, args.StartTime, args.EndTime, smoothness)
	}

	if len(errors) > 0 {
		errorString := "The following errors occurred:"
		for kpi, kpiErr := range errors {
			errorString += fmt.Sprintf(" Error with timeseries (%s), error:(%s);", kpi, kpiErr.Error())
		}
		return resolvers, fmt.Errorf(errorString)
	}

	return resolvers, nil
}

// GetUncompressedKPITimeSeries is the graphql resolver function for getting multiple time series objects given kpi ids without any smoothing
func (r *Resolver) GetUncompressedKPITimeSeries(ctx context.Context, args timeSeriesArgs) ([]*structs.TimeSeriesResolver, error) {
	fmt.Println("Graphql endpoint getUncompressedKPITimeSeries accessed")
	var resolvers []*structs.TimeSeriesResolver
	var errors map[string]error

	resolvers, errors = r.getKPITimeSeriesMultiple(args.KpiIds, args.StartTime, args.EndTime)

	if len(errors) > 0 {
		errorString := "The following errors occurred:"
		for kpi, kpiErr := range errors {
			errorString += fmt.Sprintf(" Error with timeseries (%s), error:(%s);", kpi, kpiErr.Error())
		}
		return resolvers, fmt.Errorf(errorString)
	}

	return resolvers, nil
}

type timeSeriesLastChangedArgs struct {
	KpiIds      []graphql.ID
	EndTime     structs.Date
	TargetValue float64
}

// GetLastChangedKPITimeSeries is the graphql resolver function for the query getLastChangedKPITimeSeries
func (r *Resolver) GetLastChangedKPITimeSeries(ctx context.Context, args timeSeriesLastChangedArgs) ([]*structs.TimeSeriesResolver, error) {
	resolvers := []*structs.TimeSeriesResolver{}
	errors := map[string]error{}
	for _, kpi := range args.KpiIds {
		queryString := insights.TemplateInitialParams(`SELECT value FROM "%s" WHERE time <= $endTime and value != $targetValue ORDER BY time DESC LIMIT 1`, kpi)
		binders := []insights.Binder{
			{
				Name: "endTime",
				Value: args.EndTime,
			},
			{
				Name: "targetValue",
				Value: args.TargetValue,
			},
		}
		response, queryErr := r.influxDb.Query(queryString, "KPI", binders...)
		if queryErr != nil {
			errors[string(kpi)] = queryErr
			continue
		}

		ts, tsErr := parseTimeSeriesFromInfluxResponse(string(kpi), response)
		if tsErr != nil {
			errors[string(kpi)] = tsErr
			continue
		}

		resolvers = append(resolvers, structs.NewTimeSeriesResolver(ts))

	}
	if len(errors) > 0 {
		errorString := "The following errors occurred:"
		for kpi, kpiErr := range errors {
			errorString += fmt.Sprintf(" Error with timeseries (%s), error:(%s);", kpi, kpiErr.Error())
		}
		return resolvers, fmt.Errorf(errorString)
	}

	return resolvers, nil
}

type setPointsArgs struct {
	KpiIds    []graphql.ID
	StartTime structs.Date
	EndTime   structs.Date
	TargetID  graphql.ID
}

// Comparator will make type assertion (see IntComparator for example),
// which will panic if a or b are not of the asserted type.
//
// Should return a number:
//    negative , if a < b
//    zero     , if a == b
//    positive , if a > b
type Comparator func(a, b interface{}) int

func customDateComparatorfunc(a, b interface{}) int {
	aDate := a.(structs.Date).Time
	bDate := b.(structs.Date).Time

	if aDate.Before(bDate) {
		return -1
	} else if aDate.Equal(bDate) {
		return 0
	}

	return 1
}

// GetSetPoints is the graphql query resolver for the getSetPoints endpoint
func (r *Resolver) GetSetPoints(ctx context.Context, args setPointsArgs) ([]*structs.TimeSeriesResolver, error) {
	diffTs, diffErr := r.getDifferenceMeasurements(args.KpiIds, args.StartTime, args.EndTime)
	if diffErr != nil {
		insights.LogErrorWithInfo(diffErr, "", "Failed to query difference in influx for time series data")
		return nil, diffErr
	}

	targetTs, targetErr := r.getKPITimeSeries(string(args.TargetID), args.StartTime, args.EndTime)
	if targetErr != nil {
		insights.LogErrorWithInfo(targetErr, "", "Failed to query target kpi in influx for time series data")
		return nil, targetErr
	}

	tree := treeset.NewWith(customDateComparatorfunc)

	for i, spTimeStamp := range diffTs.Timestamps {
		diff := (*diffTs.Values)[i]
		if diff != 0.0 {
			tree.Add(spTimeStamp)
		}
	}

	ts := structs.TimeSeries{
		ID:         "set-points",
		Timestamps: []structs.Date{},
		Values:     &[]float64{},
	}
	recentIndex := 0
	for _, val := range tree.Values() {
		i := recentIndex
		for i < len(targetTs.Timestamps) {
			targetTimeStamp := targetTs.Timestamps[i]
			var secondsDiff float64
			valTime := val.(structs.Date).Time
			tsTime := targetTimeStamp.Time
			if valTime.Before(tsTime) {
				secondsDiff = tsTime.Sub(valTime).Seconds()
			} else {
				secondsDiff = valTime.Sub(tsTime).Seconds()
			}

			if secondsDiff >= 0 && secondsDiff < 60 {
				recentIndex = i
				break
			}
			i++
		}
		ts.Timestamps = append(ts.Timestamps, targetTs.Timestamps[recentIndex])
		*ts.Values = append(*ts.Values, (*targetTs.Values)[recentIndex])
	}

	return []*structs.TimeSeriesResolver{
		structs.NewTimeSeriesResolver(&ts),
	}, nil
}

//type thresholdArgs struct {
//	KpiIds      []graphql.ID
//	AssetID     graphql.ID
//	eventTypeId graphql.ID
//	StartTime   structs.Date
//	EndTime     structs.Date
//}

//type shiftChangeArgs struct {
//	KpiID     graphql.ID
//	TargetID  graphql.ID
//	StartTime structs.Date
//	EndTime   structs.Date
//}

// GetShiftChangeBoundary returns the shift change boundary information
//func (r *Resolver) GetShiftChangeBoundary(ctx context.Context, args shiftChangeArgs) []*structs.ShiftChangeSeriesResolver {
//	res, _ := r.findDistinctShiftTimeInTimeRange(args.KpiID, args.StartTime, args.EndTime)
//	fmt.Print(res)
//	return nil
//}
