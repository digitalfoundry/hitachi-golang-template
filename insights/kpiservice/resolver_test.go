package kpiservice

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/influxdata/influxdb1-client/models"
	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"

	"github.com/arangodb/go-driver"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
	"github.com/graph-gophers/graphql-go"

	"github.com/stretchr/testify/mock"
)

func TestResolver_GetAvailableKPIs_Success(t *testing.T) {
	mockArango := structs.NewMockArangoDBer()
	resolver := &Resolver{
		arangoDb: mockArango,
		influxDb: nil,
	}
	mockCursor := structs.MockArangoCursorKPI{}
	id := graphql.ID("id1")
	unit := "test-unit"
	value := 99.0
	kpi := structs.KPI{
		ID: id,
		Indicator: &structs.MetricEntity{
			Value: value,
			Unit:  &unit,
		},
	}
	mockArango.On("Query", "kpi-service", mock.Anything).Return(&mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, &kpi).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil)
	mockCursor.On("Close").Return(nil).Once()

	resolvers, err := resolver.GetAvailableKPIs(context.Background())
	assert.NoError(t, err)
	assert.Len(t, resolvers, 1)
	assert.Equal(t, unit, *resolvers[0].Indicator().Unit())
	assert.Equal(t, value, resolvers[0].Indicator().Value())
}

func TestResolver_GetAvailableKPIs_Fail(t *testing.T) {
	mockArango := structs.NewMockArangoDBer()
	resolver := &Resolver{
		arangoDb: mockArango,
		influxDb: nil,
	}
	mockCursor := structs.MockArangoCursorKPI{}
	mockArango.On("Query", "kpi-service", mock.Anything).Return(&mockCursor, nil).Once()
	mockCursor.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("test-error"), nil)
	mockCursor.On("Close").Return(nil).Once()

	resolvers, err := resolver.GetAvailableKPIs(context.Background())
	assert.EqualError(t, err, "test-error")
	assert.Nil(t, resolvers)
}

func TestResolver_GetKPITimeSeries_Success(t *testing.T) {
	mockInflux := structs.NewMockInfluxDBer()

	resolver := &Resolver{
		arangoDb: nil,
		influxDb: mockInflux,
	}

	kpiIds := []graphql.ID{graphql.ID("hola")}

	resp := testSampleResponse()

	timeSeriesArgs1 := timeSeriesArgs{
		KpiIds:             kpiIds,
		StartTime:          structs.Date{},
		EndTime:            structs.Date{},
		EstimatedDataCount: nil,
	}

	mockInflux.On("Query", mock.Anything).Return(&resp, nil).Once()

	tsResolvers, err := resolver.GetKPITimeSeries(context.Background(), timeSeriesArgs1)
	assert.NoError(t, err)
	assert.NotNil(t, tsResolvers)
	assert.Equal(t, (*tsResolvers[0].Values())[0], float64(4))


	timeSeriesArgs2 := timeSeriesArgs{
		KpiIds:             kpiIds,
		StartTime:          structs.Date{Time: whatYearIsItAnyway()},
		EndTime:            structs.Date{Time: whatYearIsItAnyway().AddDate(0,0,2)},
		EstimatedDataCount: nil,
	}

	mockInflux.On("Query", mock.Anything).Return(&resp, nil).Once()

	tsResolvers2, err2 := resolver.GetKPITimeSeries(context.Background(), timeSeriesArgs2)
	assert.NoError(t, err2)
	assert.NotNil(t, tsResolvers2)
}

func TestResolver_GetKPITimeSeries_Fail(t *testing.T) {
	mockInflux := structs.NewMockInfluxDBer()
	resolver := &Resolver{
		arangoDb: nil,
		influxDb: mockInflux,
	}

	kpiIds := []graphql.ID{graphql.ID("hola")}

	resp := testSampleResponse()

	timeSeriesArgs1 := timeSeriesArgs{
		KpiIds:             kpiIds,
		StartTime:          structs.Date{},
		EndTime:            structs.Date{},
		EstimatedDataCount: nil,
	}

	mockInflux.On("Query", mock.Anything).Return(&resp, fmt.Errorf("query err")).Once()

	tsResolvers, errs := resolver.GetKPITimeSeries(context.Background(), timeSeriesArgs1)
	assert.Error(t, errs)
	assert.Len(t, tsResolvers, 0)
}

func TestResolver_GetUncompressedKPITimeSeries_Success(t *testing.T) {
	mockInflux := structs.NewMockInfluxDBer()
	resolver := &Resolver{
		arangoDb: nil,
		influxDb: mockInflux,
	}

	kpiIds := []graphql.ID{graphql.ID("hola")}

	resp := testSampleResponse()

	timeSeriesArgs1 := timeSeriesArgs{
		KpiIds:             kpiIds,
		StartTime:          structs.Date{},
		EndTime:            structs.Date{},
		EstimatedDataCount: nil,
	}

	mockInflux.On("Query", mock.Anything).Return(&resp, nil).Once()

	tsResolvers, err := resolver.GetUncompressedKPITimeSeries(context.Background(), timeSeriesArgs1)
	assert.NoError(t, err)
	assert.NotNil(t, tsResolvers)
	assert.Equal(t, (*tsResolvers[0].Values())[0], float64(4))
}

func TestResolver_GetUncompressedKPITimeSeries_Fail(t *testing.T) {
	mockInflux := structs.NewMockInfluxDBer()
	resolver := &Resolver{
		arangoDb: nil,
		influxDb: mockInflux,
	}

	kpiIds := []graphql.ID{graphql.ID("hola")}

	resp := testSampleResponse()

	timeSeriesArgs1 := timeSeriesArgs{
		KpiIds:             kpiIds,
		StartTime:          structs.Date{},
		EndTime:            structs.Date{},
		EstimatedDataCount: nil,
	}

	mockInflux.On("Query", mock.Anything).Return(&resp, fmt.Errorf("query err")).Once()

	tsResolvers, errs := resolver.GetUncompressedKPITimeSeries(context.Background(), timeSeriesArgs1)
	assert.Error(t, errs)
	assert.Len(t, tsResolvers, 0)
}

func TestResolver_GetLastChangedKPITimeSeries_Success(t *testing.T) {
	mockInflux := structs.NewMockInfluxDBer()
	resolver := &Resolver{
		arangoDb: nil,
		influxDb: mockInflux,
	}

	kpiIds := []graphql.ID{graphql.ID("hola")}

	resp := testSampleResponse()

	timeSeriesArgs1 := timeSeriesLastChangedArgs{
		KpiIds:      kpiIds,
		EndTime:     structs.Date{},
		TargetValue: 0,
	}

	mockInflux.On("Query", mock.Anything).Return(&resp, nil).Once()

	tsResolvers, err := resolver.GetLastChangedKPITimeSeries(context.Background(), timeSeriesArgs1)
	assert.NoError(t, err)
	assert.NotNil(t, tsResolvers)
	assert.Equal(t, (*tsResolvers[0].Values())[0], float64(4))
}

func TestResolver_GetLastChangedKPITimeSeries_Fail(t *testing.T) {
	mockInflux := structs.NewMockInfluxDBer()
	resolver := &Resolver{
		arangoDb: nil,
		influxDb: mockInflux,
	}

	kpiIds := []graphql.ID{graphql.ID("hola")}

	resp := testSampleResponse()

	timeSeriesArgs1 := timeSeriesLastChangedArgs{
		KpiIds:      kpiIds,
		EndTime:     structs.Date{},
		TargetValue: 0,
	}

	mockInflux.On("Query", mock.Anything).Return(&resp, fmt.Errorf("query err")).Once()

	tsResolvers, err := resolver.GetLastChangedKPITimeSeries(context.Background(), timeSeriesArgs1)
	assert.Error(t, err)
	assert.Len(t, tsResolvers, 0)
}

func TestResolver_GetSetPoints_Success(t *testing.T) {
	mockInflux := structs.NewMockInfluxDBer()
	resolver := &Resolver{
		arangoDb: nil,
		influxDb: mockInflux,
	}

	kpiIds := []graphql.ID{graphql.ID("hola")}

	resp := testSampleResponse()

	setPointsArgs1 := setPointsArgs{
		KpiIds:    kpiIds,
		StartTime: structs.Date{},
		EndTime:   structs.Date{},
		TargetID:  "",
	}

	mockInflux.On("Query", mock.Anything).Return(&resp, nil).Once()
	mockInflux.On("Query", mock.Anything).Return(&resp, nil).Once()

	tsResolvers, err := resolver.GetSetPoints(context.Background(), setPointsArgs1)
	assert.NoError(t, err)
	assert.NotNil(t, tsResolvers)
	assert.Equal(t, (*tsResolvers[0].Values())[0], float64(4))
}

func TestResolver_GetSetPoints_Fail(t *testing.T) {
	mockInflux := structs.NewMockInfluxDBer()
	resolver := &Resolver{
		arangoDb: nil,
		influxDb: mockInflux,
	}

	kpiIds := []graphql.ID{graphql.ID("hola")}

	resp := testSampleResponse()

	setPointsArgs1 := setPointsArgs{
		KpiIds:    kpiIds,
		StartTime: structs.Date{},
		EndTime:   structs.Date{},
		TargetID:  "",
	}

	mockInflux.On("Query", mock.Anything).Return(&resp, fmt.Errorf("query err")).Once()

	tsResolvers, err := resolver.GetSetPoints(context.Background(), setPointsArgs1)
	assert.Errorf(t, err, "Failed to query difference in influx for time series data")
	assert.Len(t, tsResolvers, 0)

	mockInflux.On("Query", mock.Anything).Return(&resp, nil).Once()
	mockInflux.On("Query", mock.Anything).Return(&resp, fmt.Errorf("query err")).Once()

	tsResolvers2, err2 := resolver.GetSetPoints(context.Background(), setPointsArgs1)
	assert.Errorf(t, err2, "Failed to query target kpi in influx for time series data")
	assert.Len(t, tsResolvers2, 0)
}

func testSampleResponse() client.Response {
	return client.Response{
		Results: []client.Result{
			{
				Series: []models.Row{
					{
						Values: [][]interface{}{
							{
								"2019-08-27T13:22:53.108Z",
								json.Number("4"),
							},
						},
					},
				},
			},
		},
		Err:     "",
	}
}
func whatYearIsItAnyway() time.Time {
	t, err := time.Parse(time.RFC3339, "2019-08-27T13:22:53.108Z")
	if err != nil {
		return time.Time{}
	}

	return t
}
