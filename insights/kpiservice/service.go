package kpiservice

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
)

// New
func NewService() *Service {
	return &Service{}
}

// Service primary struct for the service, contains the server handler
type Service struct {
	serveMux   *http.ServeMux
	server     *http.Server
	config     *config
	gqlSchema  *graphql.Schema
	arangoDB   insights.ArangoDBer
	influxDB   insights.InfluxDBer
	keycloaker insights.Keycloaker
}

// Init initializes the service with any properties that need to be set and retrieves the config from env
func (s *Service) Init() {
	s.config = getConfig()
	s.arangoDB = insights.NewArangoDBer(s.config.ArangoDBAddress)
	s.influxDB = insights.NewInfluxDBer()
	s.gqlSchema = s.getGraphqlFromFile()

	connectErr := s.arangoDB.Connect(s.config.ArangoDBUsername, s.config.ArangoDBPassword)
	if connectErr != nil {
		insights.LogErrorWithInfo(connectErr, "Check Username, Password and DB Address Configurations", "Connection to remote Arango instance failed")
		insights.PanicErrorIfExists(connectErr)
	}

	connectErr = s.influxDB.Connect(s.config.InfluxDBAddress, s.config.InfluxDBUsername, s.config.InfluxDBPassword)
	if connectErr != nil {
		insights.LogErrorWithInfo(connectErr, "Check Username, Password and DB Address Configurations", "Connection to remote Influx instance failed")
		insights.PanicErrorIfExists(connectErr)
	}

	s.serveMux = http.NewServeMux()
	s.server = &http.Server{
		Handler: s.serveMux,
		Addr:    fmt.Sprintf(":%s", s.config.HostPort),
	}
	s.setupGraphqlEndpoint()

	s.keycloaker = insights.NewKeycloaker(s.config.KeycloakAddress, s.config.KeycloakRealm, s.config.KeycloakClientID, s.config.KeycloakClientSecret)

	keyErr := s.keycloaker.RetrievePublicKey()

	if keyErr != nil {
		insights.PanicErrorIfExists(keyErr)
	}
}

// Start begins the servers listeners for request holding the service open. Function only exits when server exits
func (s *Service) Start() {
	fmt.Println("Successfully Completed Server Initialization. Starting Listener ")

	var listenErr error
	// if we havent been configured with an ssl certificate or an ssl key dont use ssl
	if s.config.SSLServerKeyPath == "" || s.config.SSLServerCertificatePath == "" {
		listenErr = s.server.ListenAndServe()
	} else {
		listenErr = s.server.ListenAndServeTLS(s.config.SSLServerCertificatePath, s.config.SSLServerKeyPath)
	}

	if listenErr != nil && listenErr != http.ErrServerClosed {
		insights.LogErrorWithInfo(listenErr, "Check that the configured port is available", "Failed to start server on configured port")
		insights.PanicErrorIfExists(listenErr)
	}
}

func (s *Service) getGraphqlFromFile() *graphql.Schema {
	schemaBytes, loadErr := ioutil.ReadFile(s.config.GraphQLSchemaFile)
	if loadErr != nil {
		insights.PanicErrorIfExists(loadErr)
	}

	resolver := Resolver{
		arangoDb: s.arangoDB,
		influxDb: s.influxDB,
	}

	schema, parseErr := graphql.ParseSchema(string(schemaBytes), &resolver, graphql.UseStringDescriptions())
	if parseErr != nil {
		insights.LogErrorWithInfo(parseErr, "Check graphql resolvers and schema", "Failed to parse graphql schema into resolvers")
		insights.PanicErrorIfExists(parseErr)
	}
	return schema
}

func (s *Service) setupGraphqlEndpoint() {
	handler := &relay.Handler{Schema: s.gqlSchema}

	s.serveMux.HandleFunc("/graphql", func(w http.ResponseWriter, r *http.Request) {
		isOk, err := s.keycloaker.VerifyRequest(r)

		if isOk || err != nil {
			handler.ServeHTTP(w, r)
		} else {
			insights.LogErrorWithInfo(err, "", "failed to authenticated")
			http.Error(w, "Not authorized", 401)
		}
	})
}
