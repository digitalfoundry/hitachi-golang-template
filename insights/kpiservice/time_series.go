package kpiservice

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
	"github.com/graph-gophers/graphql-go"
	client "github.com/influxdata/influxdb1-client/v2"
)

const defaultEstimatedCountForSmoothing = 1000

func parseTimeSeriesFromInfluxResponse(kpiID string, response *client.Response) (*structs.TimeSeries, error) {
	ts := structs.TimeSeries{}
	ts.Timestamps = []structs.Date{}
	ts.Values = &[]float64{}
	ts.ID = graphql.ID(kpiID)

	if response == nil {
		return nil, fmt.Errorf("invalid time series format")
	} else if response.Results == nil {
		return nil, fmt.Errorf("invalid time series format")
	} else if response.Results[0].Series == nil {
		return nil, fmt.Errorf("invalid time series format")
	}

	for _, vals := range response.Results[0].Series[0].Values {
		if len(vals) < 2 {
			return nil, fmt.Errorf("invalid time series format")
		}

		t, tErr := time.Parse(time.RFC3339, vals[0].(string))
		if tErr != nil {
			return nil, tErr
		}

		if vals[1] == nil {
			// ignore the case where no data is provided
			continue
		}

		jsonNum, ok := vals[1].(json.Number)
		if !ok {
			return nil, fmt.Errorf("value is not a json number")
		}
		flt, fltErr := jsonNum.Float64()
		if fltErr != nil {
			return nil, fltErr
		}

		// these assignments must be after all validation checks so that the arrays maintain equal length
		ts.Timestamps = append(ts.Timestamps, structs.Date{Time: t})
		*ts.Values = append(*ts.Values, flt)
	}
	return &ts, nil
}

func (r *Resolver) getKPITimeSeries(kpiID string, startTime structs.Date, endTime structs.Date) (*structs.TimeSeries, error) {
	queryString := insights.TemplateInitialParams(`SELECT value FROM "%s" WHERE TIME <= $endTime and time >=$startTime`, kpiID)
	binders := []insights.Binder{
		{
			Name: "startTime",
			Value: startTime,
		},
		{
			Name: "endTime",
			Value: endTime,
		},
	}
	response, queryErr := r.influxDb.Query(queryString, "KPI", binders...)
	if queryErr != nil {
		return nil, queryErr
	}

	ts, tsErr := parseTimeSeriesFromInfluxResponse(kpiID, response)
	if tsErr != nil {
		return nil, tsErr
	}

	return ts, nil
}

func (r *Resolver) getKPITimeSeriesResolver(kpiID string, startTime structs.Date, endTime structs.Date) (*structs.TimeSeriesResolver, error) {
	ts, tsErr := r.getKPITimeSeries(kpiID, startTime, endTime)
	if tsErr != nil {
		return nil, tsErr
	}

	return structs.NewTimeSeriesResolver(ts), nil
}

func (r *Resolver) getKPITimeSeriesMultiple(kpiIDs []graphql.ID, startTime structs.Date, endTime structs.Date) ([]*structs.TimeSeriesResolver, map[string]error) {
	resolvers := []*structs.TimeSeriesResolver{}
	errors := map[string]error{}
	for _, kpi := range kpiIDs {
		tsr, tsrErr := r.getKPITimeSeriesResolver(string(kpi), startTime, endTime)
		if tsrErr != nil {
			errors[string(kpi)] = tsrErr
		} else {
			resolvers = append(resolvers, tsr)
		}
	}
	return resolvers, errors
}

func (r *Resolver) getKPITimeSeriesSmooth(kpiID string, startTime structs.Date, endTime structs.Date, smoothness int) (*structs.TimeSeriesResolver, error) {
	queryString := insights.TemplateInitialParams(`SELECT mean(value) FROM "%s" WHERE TIME <= $endTime and time >=$startTime GROUP BY time(%dm) FILL(none)`, kpiID, smoothness)
	binders := []insights.Binder{
		{
			Name: "startTime",
			Value: startTime,
		},
		{
			Name: "endTime",
			Value: endTime,
		},
	}
	response, queryErr := r.influxDb.Query(queryString, "KPI", binders...)
	if queryErr != nil {
		return nil, queryErr
	}

	ts, tsErr := parseTimeSeriesFromInfluxResponse(kpiID, response)
	if tsErr != nil {
		return nil, tsErr
	}

	return structs.NewTimeSeriesResolver(ts), nil
}

func (r *Resolver) getKPITimeSeriesSmoothMultiple(kpiIDs []graphql.ID, startTime structs.Date, endTime structs.Date, smoothness int) ([]*structs.TimeSeriesResolver, map[string]error) {
	resolvers := []*structs.TimeSeriesResolver{}
	errors := map[string]error{}
	for _, kpi := range kpiIDs {
		tsr, tsrErr := r.getKPITimeSeriesSmooth(string(kpi), startTime, endTime, smoothness)
		if tsrErr != nil {
			errors[string(kpi)] = tsrErr
		} else {
			resolvers = append(resolvers, tsr)
		}
	}
	return resolvers, errors
}

func (r *Resolver) getDifferenceMeasurements(kpiIds []graphql.ID, startTime structs.Date, endTime structs.Date) (*structs.TimeSeries, error) {
	differenceQueryStrings := []string{}
	measurements := []string{}
	for _, kpi := range kpiIds {
		measurements = append(measurements, string(kpi))
		differenceQueryStrings = append(differenceQueryStrings, insights.TemplateInitialParams(`(SELECT difference(value) AS "diff" FROM "%s")`, kpi))
	}

	queryString := insights.TemplateInitialParams(`SELECT diff AS "%s" FROM %s WHERE time <= $endTime AND time >=$startTime ORDER BY time DESC`,
		strings.Join(measurements, `", "`), strings.Join(differenceQueryStrings, `, `))

	binders := []insights.Binder{
		{
			Name: "startTime",
			Value: startTime,
		},
		{
			Name: "endTime",
			Value: endTime,
		},
	}

	response, queryErr := r.influxDb.Query(queryString, "KPI", binders...)
	if queryErr != nil {
		return nil, queryErr
	}

	ts, tsErr := parseTimeSeriesFromInfluxResponse("", response)
	if tsErr != nil {
		return nil, tsErr
	}

	return ts, nil
}

func (r *Resolver) findDistinctShiftTimeInTimeRange(kpi graphql.ID, startTime structs.Date, endTime structs.Date) ([]*structs.ShiftChangeSeriesResolver, error) {
	subQuery := insights.TemplateInitialParams(`SELECT value, shiftTime FROM "%s" WHERE time <= $endTime AND time >= $startTime`, kpi)
	queryString := insights.TemplateInitialParams(`SELECT distinct(shiftTime) as shiftTime FROM (%s)`, subQuery)
	binders := []insights.Binder{
		{
			Name: "startTime",
			Value: startTime,
		},
		{
			Name: "endTime",
			Value: endTime,
		},
	}

	response, queryErr := r.influxDb.Query(queryString, "KPI", binders...)
	if queryErr != nil {
		return nil, queryErr
	}

	fmt.Print(response)
	return nil, nil
}
