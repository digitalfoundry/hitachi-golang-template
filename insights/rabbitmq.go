package insights

import (
	"errors"
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

// Defines our interface for connecting and consuming messages.
type IRabbitMQer interface {
	ConnectToBroker(connectionString string)
	Publish(msg []byte, exchangeName string, exchangeType string) error
	PublishOnQueue(msg []byte, queueName string) error
	Subscribe(exchangeName string, exchangeType string, consumerName string, handlerFunc func(amqp.Delivery)) error
	SubscribeToQueue(queueName string, consumerName string, handlerFunc func(amqp.Delivery)) error
	Close()
}

// Real implementation, encapsulates a pointer to an amqp.Connection
type RabbitMQ struct {
	conn *amqp.Connection
}

//ConnectToBroker connects to RabbitMQ instance
func ConnectToBroker(connectionString string) *RabbitMQ {
	fmt.Println("Setting up connection to RabbitMQ instance", connectionString)
	if connectionString == "" {
		PanicErrorIfExists(errors.New("cannot initialize connection to broker, connectionString not set"))
	}

	conn, err := amqp.Dial(fmt.Sprintf("%s/", connectionString))
	PanicErrorIfExists(err)

	return &RabbitMQ{conn: conn}
}

//Publish pushes message into RabbitMQ instance
func (m *RabbitMQ) Publish(body []byte, exchangeName string, exchangeType string) error {
	if m.conn == nil {
		PanicErrorIfExists(errors.New("tried to send message before connection was initialized"))
	}
	ch, err := m.conn.Channel() // Get a channel from the connection
	PanicErrorIfExists(err)

	defer func() {
		err = ch.Close()
		PanicErrorIfExists(err)
	}()

	err = ch.ExchangeDeclare(
		exchangeName, // name of the exchange
		exchangeType, // type
		true,         // durable
		false,        // delete when complete
		false,        // internal
		false,        // noWait
		nil,          // arguments
	)
	PanicErrorIfExists(err)

	queue, err := ch.QueueDeclare( // Declare a queue that will be created if not exists with some args
		"",    // our queue name
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	PanicErrorIfExists(err)

	err = ch.QueueBind(
		queue.Name,   // name of the queue
		exchangeName, // bindingKey
		exchangeName, // sourceExchange
		false,        // noWait
		nil,          // arguments
	)
	PanicErrorIfExists(err)

	err = ch.Publish( // Publishes a message onto the queue.
		exchangeName, // exchange
		exchangeName, // routing key      q.Name
		false,        // mandatory
		false,        // immediate
		amqp.Publishing{
			Body: body, // Our JSON body as []byte
		})
	PanicErrorIfExists(err)

	fmt.Printf("A message was sent: %v", body)
	return err
}

//PublishOnQueue pushes message into RabbitMQ instance
func (m *RabbitMQ) PublishOnQueue(body []byte, queueName string) error {
	if m.conn == nil {
		PanicErrorIfExists(errors.New("tried to send message before connection was initialized"))
	}
	ch, err := m.conn.Channel() // Get a channel from the connection
	PanicErrorIfExists(err)

	defer func() {
		err = ch.Close()
		PanicErrorIfExists(err)
	}()

	queue, err := ch.QueueDeclare( // Declare a queue that will be created if not exists with some args
		queueName, // our queue name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	PanicErrorIfExists(err)

	// Publishes a message onto the queue.
	err = ch.Publish(
		"",         // exchange
		queue.Name, // routing key
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        body, // Our JSON body as []byte
		})
	PanicErrorIfExists(err)

	fmt.Printf("A message was sent to queue %v: %v", queueName, body)
	return err
}

//Subscribe listens to RabbitMQ instance
func (m *RabbitMQ) Subscribe(exchangeName string, exchangeType string, consumerName string,
	handlerFunc func(amqp.Delivery)) error {
	ch, err := m.conn.Channel()
	PanicErrorIfExists(err)
	// defer ch.Close()

	err = ch.ExchangeDeclare(
		exchangeName, // name of the exchange
		exchangeType, // type
		true,         // durable
		false,        // delete when complete
		false,        // internal
		false,        // noWait
		nil,          // arguments
	)
	PanicErrorIfExists(err)

	queue, err := ch.QueueDeclare(
		"",    // name of the queue
		false, // durable
		false, // delete when usused
		false, // exclusive
		false, // noWait
		nil,   // arguments
	)
	PanicErrorIfExists(err)

	log.Printf("declared Queue (%d messages, %d consumers), binding to Exchange (key '%s')",
		queue.Messages, queue.Consumers, exchangeName)

	err = ch.QueueBind(
		queue.Name,   // name of the queue
		exchangeName, // bindingKey
		exchangeName, // sourceExchange
		false,        // noWait
		nil,          // arguments
	)
	PanicErrorIfExists(err)

	msgs, err := ch.Consume(
		queue.Name,   // queue
		consumerName, // consumer
		true,         // auto-ack
		false,        // exclusive
		false,        // no-local
		false,        // no-wait
		nil,          // args
	)
	PanicErrorIfExists(err)

	go consumeLoop(msgs, handlerFunc)
	return nil
}

//SubscribeToQueue listens to RabbitMQ instance
func (m *RabbitMQ) SubscribeToQueue(queueName string, consumerName string, handlerFunc func(amqp.Delivery)) error {
	ch, err := m.conn.Channel()
	PanicErrorIfExists(err)

	queue, err := ch.QueueDeclare(
		queueName, // name of the queue
		false,     // durable
		false,     // delete when usused
		false,     // exclusive
		false,     // noWait
		nil,       // arguments
	)
	PanicErrorIfExists(err)

	msgs, err := ch.Consume(
		queue.Name,   // queue
		consumerName, // consumer
		true,         // auto-ack
		false,        // exclusive
		false,        // no-local
		false,        // no-wait
		nil,          // args
	)
	PanicErrorIfExists(err)

	go consumeLoop(msgs, handlerFunc)
	return nil
}

//Close closes connection to RabbitMQ instance
func (m *RabbitMQ) Close() {
	if m.conn != nil {
		defer func() {
			err := m.conn.Close()
			PanicErrorIfExists(err)
		}()
	}
}

func consumeLoop(deliveries <-chan amqp.Delivery, handlerFunc func(d amqp.Delivery)) {
	for d := range deliveries {
		// Invoke the handlerFunc func we passed as parameter.
		handlerFunc(d)
	}
}
