package service

import (
	"fmt"
	"os"
)

type config struct {
	HostPort             string
	ArangoDBAddress      string
	ArangoDBUsername     string
	ArangoDBPassword     string
	KeycloakAddress      string
	KeycloakRealm        string
	GraphQLSchemaFile    string
	RabbitMQAddress      string
	RabbitMQUser         string
	RabbitMQPassword     string
	RabbitMQPort         string
	InfluxDBAddress      string
	InfluxDBUsername     string
	InfluxDBPassword     string
	KeycloakClientID     string
	KeycloakClientSecret string
}

func getConfig() *config {
	getEnvString := func(key string) string {
		environmentVar := os.Getenv(key)
		if environmentVar == "" {
			panic(fmt.Sprintf("$%s must be set. please make sure the environment values are set correctly", key))
		}
		return environmentVar
	}

	return &config{
		ArangoDBAddress:      getEnvString("ARANGO_DB_ADDRESS"),
		ArangoDBUsername:     getEnvString("ARANGO_DB_USERNAME"),
		ArangoDBPassword:     getEnvString("ARANGO_DB_PASSWORD"),
		HostPort:             getEnvString("HOST_PORT"),
		GraphQLSchemaFile:    getEnvString("GRAPHQL_SCHEMA_FILE"),
		RabbitMQAddress:      getEnvString("RABBIT_MQ_ADDRESS"),
		RabbitMQUser:         getEnvString("RABBIT_MQ_USER"),
		RabbitMQPassword:     getEnvString("RABBIT_MQ_PASSWORD"),
		RabbitMQPort:         getEnvString("RABBIT_MQ_PORT"),
		KeycloakRealm:        getEnvString("KEYCLOAK_REALM"),
		InfluxDBAddress:      getEnvString("INFLUXDB_ADDRESS"),
		InfluxDBUsername:     getEnvString("INFLUXDB_USERNAME"),
		InfluxDBPassword:     getEnvString("INFLUXDB_PASSWORD"),
		KeycloakAddress:      getEnvString("KEYCLOAK_ADDRESS"),
		KeycloakClientID:     getEnvString("KEYCLOAK_CLIENT_ID"),
		KeycloakClientSecret: getEnvString("KEYCLOAK_CLIENT_SECRET"),
	}
}
