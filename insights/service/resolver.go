package service

import (
	"context"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights/structs"
)

// ObjQueryArgs is the struct representing passed in arguments to the getObjByID graphql endpoint
type ObjQueryArgs struct {
	ID *string
}

// QueryResolver is the example root resolver for the services graphql schema
type QueryResolver struct{}

// GetObjByID is an example query resolver that currently returns a static object for a getObjById graphql endpoint
func (r *QueryResolver) GetObjByID(ctx context.Context, args ObjQueryArgs) (*structs.ObjResolver, error) {
	obj := structs.Obj{
		ID:          "id1",
		Name:        "Obj 1",
		Description: "This the first Obj",
		Status:      structs.Closed,
	}

	return &structs.ObjResolver{
		M: obj,
	}, nil
}
