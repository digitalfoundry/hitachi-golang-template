package service

const (
	homeRoute     = "/"
	rabbitMQRoute = "/rabbit"
	influxDBRoute = "/influx"
)
