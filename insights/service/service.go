package service

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"github.com/graph-gophers/graphql-go"
	"github.com/streadway/amqp"
)

// new returns a new instance of the service
func New() *Service {
	return &Service{}
}

// Service is the primary struct for the service
type Service struct {
	serveMux   *http.ServeMux
	server     *http.Server
	config     *config
	rabbit     *insights.RabbitMQ
	keycloaker insights.Keycloaker
	//schema    graphql.Schema
}

// Start starts up the service
func (s *Service) Start() {
	fmt.Println("Starting Template Service")
	s.config = getConfig()
	s.rabbit = insights.ConnectToBroker(fmt.Sprintf("amqp://%s:%s@%s:%s/", s.config.RabbitMQUser,
		s.config.RabbitMQPassword, s.config.RabbitMQAddress, s.config.RabbitMQPort))

	err := s.rabbit.SubscribeToQueue("hello", "app", func(delivery amqp.Delivery) {
		body := delivery.Body
		fmt.Printf("Received message from RabbitMQ: %s", body)
	})

	if err != nil {
		insights.PanicErrorIfExists(err)
	}

	s.serveMux = http.NewServeMux()
	s.server = &http.Server{
		Handler: s.serveMux,
		Addr:    fmt.Sprintf(":%s", s.config.HostPort),
	}

	s.setUpRouteHandlers()

	fmt.Println("Loading Graphql Schema from file")
	schema, schemaErr := s.getGraphqlSchema()
	if schemaErr != nil {
		insights.PanicErrorIfExists(schemaErr)
	}

	s.SetupGraphqlEndpoint(schema)

	s.keycloaker = insights.NewKeycloaker(s.config.KeycloakAddress, s.config.KeycloakRealm, s.config.KeycloakClientID, s.config.KeycloakClientSecret)

	keyErr := s.keycloaker.RetrievePublicKey()

	s.serveMux.HandleFunc("/testkey", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("testing key")
		t, terr := s.keycloaker.GetAccessTokenString(false)

		fmt.Println(t)
		fmt.Println(terr)

	})

	if keyErr != nil {
		insights.PanicErrorIfExists(keyErr)
	}
	listenErr := s.server.ListenAndServe()

	if listenErr != nil && listenErr != http.ErrServerClosed {
		insights.PanicErrorIfExists(listenErr)
	}
}

func (s *Service) handleHomeRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint / Accessed")
	arrango := insights.NewArangoDBer(s.config.ArangoDBAddress)
	connectErr := arrango.Connect(s.config.ArangoDBUsername, s.config.ArangoDBPassword)
	if connectErr != nil {
		insights.PanicErrorIfExists(connectErr)
	}

	coll, collErr := arrango.GetCollection("config-db", "eventConfiguration")
	if collErr != nil {
		insights.PanicErrorIfExists(collErr)
	}
	fmt.Print(coll)
	fmt.Println("Successfully connected to Arango")
}

func (s *Service) getGraphqlSchema() (*graphql.Schema, error) {
	schemaBytes, loadErr := ioutil.ReadFile(s.config.GraphQLSchemaFile)
	if loadErr != nil {
		insights.PanicErrorIfExists(loadErr)
	}

	return graphql.ParseSchema(string(schemaBytes), &QueryResolver{}, graphql.UseStringDescriptions())
}

func (s *Service) handleRabbitMQRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Println("RabbitMQ Endpoint / Accessed")

	switch r.Method {
	case http.MethodPost:
		if err := r.ParseForm(); err != nil {
			insights.PanicErrorIfExists(err)
			return
		}

		err := s.rabbit.PublishOnQueue([]byte("Hello World"), "hello")
		if err != nil {
			insights.PanicErrorIfExists(err)
			return
		}
	default:
		//Unknown method
	}

	fmt.Println("Successfully connected to send message to RabbitMQ")
}

func (s *Service) handleInfluxRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Accessed Endpoint /influx")
	ifux := insights.NewInfluxDBer()
	connectErr := ifux.Connect(s.config.InfluxDBAddress, s.config.InfluxDBUsername, s.config.InfluxDBPassword)
	defer insights.DeferHandleError(ifux.Close)
	if connectErr != nil {
		insights.PanicErrorIfExists(connectErr)
	}

	fmt.Println("Successfully Connected to influxDB")
}
