package structs

import (
	"context"

	"github.com/jinzhu/copier"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"github.com/arangodb/go-driver"
	"github.com/stretchr/testify/mock"
)

// NewMockArangoDBer returns a new mock arango db struct for usage in tests
func NewMockArangoDBer() *MockArangoDB {
	return &MockArangoDB{}
}

// MockArangoDB is the mock arango db struct
type MockArangoDB struct {
	mock.Mock
}

// Connect is the mock function for Connect in the Arango DB struct
func (a *MockArangoDB) Connect(username string, password string) error {
	args := a.Called(username, password)
	return args.Error(0)
}

// GetCollection is the mock function for GetCollection in the Arango DB struct
func (a *MockArangoDB) GetCollection(dbName string, collectionName string) (insights.ArangoCollection, error) {
	args := a.Called(dbName, collectionName)
	switch collection := args[0].(type) {
	case *MockArangoCollectionAssetEntity:
		return collection, args.Error(1)
	case *MockArangoCollectionEvent:
		return collection, args.Error(1)
	case *MockArangoCollection:
		return collection, args.Error(1)
	case *MockArangoCollectionEventConfiguration:
		return collection, args.Error(1)
	case *MockArangoCollectionKPICalculator:
		return collection, args.Error(1)
	default:
		panic("invalid mock collection class")
	}
}

// Query is the mock function for Query in the Arango DB struct
// first return args should be the mock cursor, second should be an error
func (a *MockArangoDB) Query(dbName string, query insights.ArangoDBQuery, params ...insights.Binder) (insights.ArangoCursor, error) {
	args := a.Called(dbName, query)
	switch cursor := args[0].(type) {
	case *MockArangoCursorEventConfiguration:
		return cursor, args.Error(1)
	case *MockArangoCursorAssetEntity:
		return cursor, args.Error(1)
	case *MockArangoCursorTemplateClass:
		return cursor, args.Error(1)
	case *MockArangoCursorEvent:
		return cursor, args.Error(1)
		case *MockArangoCursorEventResult:
		return cursor, args.Error(1)
	case *MockArangoCursorKPI:
		return cursor, args.Error(1)
	default:
		panic("invalid mock cursor class")
	}
}

// MockArangoCollection is the mock struct for the ArangoCollection interface
type MockArangoCollection struct {
	mock.Mock
}

// CreateDocument is the mock function for the Arango go driver Collection CreateDocument function
func (c *MockArangoCollection) CreateDocument(ctx context.Context, document interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, document)
	return driver.DocumentMeta{}, args.Error(0)
}

// ReadDocument is the mock function for the Arango go driver Collection ReadDocument function
func (c *MockArangoCollection) ReadDocument(ctx context.Context, key string, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, key, result)
	return driver.DocumentMeta{}, args.Error(0)
}

// UpdateDocument is the mock function for the Arango go driver Collection UpdateDocument function
func (c *MockArangoCollection) UpdateDocument(ctx context.Context, key string, update interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, key, update)
	return driver.DocumentMeta{}, args.Error(0)
}

//MockArangoCursor is the mock struct for the ArangoCursor interface
type MockArangoCursor struct {
	mock.Mock
}

// ReadDocument is the mock function for the Arango go driver Cursor ReadDocument function
func (c *MockArangoCursor) ReadDocument(ctx context.Context, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, result)
	if ac, ok := args[1].(*EventConfiguration); ok && ac != nil {
		copyErr := copier.Copy(result, ac)
		if copyErr != nil {
			return driver.DocumentMeta{}, copyErr
		}
	}
	return driver.DocumentMeta{}, args.Error(0)
}

// Close is the mock function for the Arango go driver Cursor Close function
func (c *MockArangoCursor) Close() error {
	args := c.Called()
	return args.Error(0)
}
