package structs

import (
	"context"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"github.com/graph-gophers/graphql-go"
)

// NewAssetEntityResolver returns a new instance of AssetEntityResolver
func NewAssetEntityResolver(ae *AssetEntity, db insights.ArangoDBer) *AssetEntityResolver {
	return &AssetEntityResolver{
		m:  *ae,
		db: db,
	}
}

// AssetEntityResolver is the resolver struct for AssetEntity
type AssetEntityResolver struct {
	m  AssetEntity
	db insights.ArangoDBer
}

// AssetEntity represents an asset
type AssetEntity struct {
	CreatedDate       Date           `json:"createdDate,omitempty"`
	Description       string         `json:"description,omitempty"`
	DisplayName       string         `json:"displayName,omitempty"`
	ID                graphql.ID     `json:"id,omitempty"`
	Image             string         `json:"image,omitempty"`
	LastUpdated       Date           `json:"lastUpdated,omitempty"`
	Location          string         `json:"location,omitempty"`
	Manufacturer      string         `json:"manufacturer,omitempty"`
	ManufacturingDate Date           `json:"manufacturingDate,omitempty"`
	Model             string         `json:",omitempty"`
	Name              string         `json:"name,omitempty"`
	Properties        string         `json:"properties,omitempty"`
	ReferenceID       string         `json:"referenceId,omitempty"`
	RelatedAssets     []*AssetEntity `json:"relatedAssets,omitempty"`
	SerialNumber      string         `json:"serialNumber,omitempty"`
	TemplateID        string         `json:"templateId,omitempty"`
	//Events []*Events
	//GetMostSevereOpenEventsByCreatedDateBetween interface{} // a function??
}

// AssetEntityInput represents the AssetEntity fields that can be mutated by a user
type AssetEntityInput struct {
	Description       string                   `json:"description"`
	DisplayName       string                   `json:"displayName"`
	Image             string                   `json:"image"`
	LastUpdated       Date                     `json:"lastUpdated"`
	Location          LocationIDReferenceInput `json:"location"`
	Manufacturer      string                   `json:"manufacturer"`
	ManufacturingDate Date                     `json:"manufacturingDate"`
	Model             string                   `json:"model"`
	Name              string                   `json:"name"`
	Properties        PropertyIDReferenceInput `json:"properties"`
	SerialNumber      string                   `json:"serialNumber"`
	TemplateID        string                   `json:"templateId"`
}

// AssetIDReferenceInput is a reference to an asset ID for input
type AssetIDReferenceInput struct {
	ID string
}

// LocationIDReferenceInput is a reference to a location ID for input
type LocationIDReferenceInput struct {
	ID string
}

// PropertyIDReferenceInput is a reference to a property ID for input
type PropertyIDReferenceInput struct {
	ID string
}

// CreatedDate returns a pointer to the created date
func (a *AssetEntityResolver) CreatedDate() *Date {
	return &a.m.CreatedDate
}

// Description returns a pointer to the description string
func (a *AssetEntityResolver) Description() *string {
	return &a.m.Description
}

// DisplayName returns a pointer to the display name string
func (a *AssetEntityResolver) DisplayName() *string {
	return &a.m.DisplayName
}

// ID returns a pointer to the ID graphql.ID
func (a *AssetEntityResolver) ID(ctx context.Context) *graphql.ID {
	return &a.m.ID
}

// Image returns a pointer to the image string
func (a *AssetEntityResolver) Image() *string {
	return &a.m.Image
}

// LastUpdated returns a pointer to the last updated date
func (a *AssetEntityResolver) LastUpdated() *Date {
	return &a.m.LastUpdated
}

// Location returns a pointer to a LocationEntityResolver instance
func (a *AssetEntityResolver) Location(ctx context.Context) *LocationEntityResolver {
	col, err := a.db.GetCollection("spring-demo", "location")
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to get assetEntity collection")
		return nil
	}

	var location LocationEntity
	meta, err := col.ReadDocument(ctx, a.m.Location, &location)
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to read document on arango")
		return nil
	}

	location.ID = graphql.ID(meta.Key)
	return NewLocationEntityResolver(&location)
}

// Image returns a pointer to the manufacturer string
func (a *AssetEntityResolver) Manufacturer() *string {
	return &a.m.Manufacturer
}

// ManufacturingDate returns a pointer to the manufacturing date
func (a *AssetEntityResolver) ManufacturingDate() *Date {
	return &a.m.ManufacturingDate
}

// Model returns a pointer to the model string
func (a *AssetEntityResolver) Model() *string {
	return &a.m.Model
}

// Name returns a pointer to the name string
func (a *AssetEntityResolver) Name() *string {
	return &a.m.Name
}

// Properties returns a pointer to a SimplePropertyCollectionEntityResolver instance
func (a *AssetEntityResolver) Properties() *SimplePropertyCollectionEntityResolver {
	// todo query for simple property collection (table not found)
	return &SimplePropertyCollectionEntityResolver{}
}

// ReferenceID returns a pointer to the reference ID string
func (a *AssetEntityResolver) ReferenceID() *string {
	return &a.m.ReferenceID
}

// RelatedAssets returns an array of AssetEntityResolver instances
func (a *AssetEntityResolver) RelatedAssets() *[]*AssetEntityResolver {
	resolvers := []*AssetEntityResolver{}
	for _, asset := range a.m.RelatedAssets {
		resolvers = append(resolvers, &AssetEntityResolver{m: *asset})
	}
	return &resolvers
}

// SerialNumber returns a pointer to the serial number string
func (a *AssetEntityResolver) SerialNumber() *string {
	return &a.m.SerialNumber
}

// TemplateID returns a pointer to the template ID string
func (a *AssetEntityResolver) TemplateID() *string {
	return &a.m.TemplateID
}
