package structs

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	"github.com/arangodb/go-driver"
	"github.com/stretchr/testify/assert"
)

func newMockAssetEntity() *AssetEntity {
	tempTime := Date{time.Now()}
	relatedAsset := &AssetEntity{
		ID: "Related ID",
	}
	return &AssetEntity{
		CreatedDate:       tempTime,
		Description:       "Description",
		DisplayName:       "DisplayName",
		ID:                "ID",
		Image:             "Image",
		LastUpdated:       tempTime,
		Location:          "Location",
		Manufacturer:      "Manufacturer",
		ManufacturingDate: tempTime,
		Model:             "Model",
		Name:              "Name",
		Properties:        "Properties",
		ReferenceID:       "ReferenceID",
		RelatedAssets:     []*AssetEntity{relatedAsset},
		SerialNumber:      "SerialNumber",
		TemplateID:        "TemplateID",
	}
}

func newMockAssetEntityResolver(ae *AssetEntity) *AssetEntityResolver {
	return &AssetEntityResolver{
		m: *ae,
	}
}

func TestNewAssetEntityResolver(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	db := NewMockArangoDBer()
	resolver := NewAssetEntityResolver(mockAssetEntity, db)

	assert.Equal(t, *mockAssetEntity, resolver.m)
	assert.Equal(t, db, resolver.db)
}

func TestAssetEntityResolver_CreatedDate(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.True(t, mockAssetEntity.CreatedDate.Time.Equal(mockResolver.CreatedDate().Time))
}

func TestAssetEntityResolver_Description(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.Equal(t, mockAssetEntity.Description, *mockResolver.Description())
}

func TestAssetEntityResolver_DisplayName(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.Equal(t, mockAssetEntity.DisplayName, *mockResolver.DisplayName())
}

func TestAssetEntityResolver_ID(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	ctx := context.Background()
	assert.Equal(t, mockAssetEntity.ID, *mockResolver.ID(ctx))
}

func TestAssetEntityResolver_Image(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.Equal(t, mockAssetEntity.Image, *mockResolver.Image())
}

func TestAssetEntityResolver_LastUpdated(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.True(t, mockAssetEntity.LastUpdated.Time.Equal(mockResolver.LastUpdated().Time))

}

func TestAssetEntityResolver_Location(t *testing.T) {
	meta := &driver.DocumentMeta{
		Key: "loc-1",
		ID:  "id-1",
	}

	mockDbSuccess := NewMockArangoDBer()
	mockCollectionSuccess := &MockArangoCollectionAssetEntity{}
	mockDbSuccess.On("GetCollection", "spring-demo", mock.Anything).Return(mockCollectionSuccess, nil).Once()
	mockCollectionSuccess.On("ReadDocument", mock.Anything, mock.Anything).Return(nil, meta).Once()

	mockDbNotFound := NewMockArangoDBer()
	mockCollectionNotFound := &MockArangoCollectionAssetEntity{}
	mockDbNotFound.On("GetCollection", "spring-demo", mock.Anything).Return(mockCollectionNotFound, nil).Once()
	mockCollectionNotFound.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()

	mockDbReadFail := NewMockArangoDBer()
	mockCollectionReadFail := &MockArangoCollectionAssetEntity{}
	mockDbReadFail.On("GetCollection", "spring-demo", mock.Anything).Return(mockCollectionReadFail, nil).Once()
	mockCollectionReadFail.On("ReadDocument", mock.Anything, mock.Anything).Return(fmt.Errorf("read error"), nil).Once()

	mockDbCollectionFail := NewMockArangoDBer()
	mockCollectionGetCollectionFail := &MockArangoCollectionAssetEntity{}
	mockDbCollectionFail.On("GetCollection", "spring-demo", mock.Anything).Return(mockCollectionGetCollectionFail, fmt.Errorf("get collection error")).Once()

	ae := &AssetEntity{
		CreatedDate:       Date{},
		Description:       "",
		DisplayName:       "",
		ID:                "",
		Image:             "",
		LastUpdated:       Date{},
		Location:          "loc-1",
		Manufacturer:      "",
		ManufacturingDate: Date{},
		Model:             "",
		Name:              "loc-1",
		Properties:        "",
		ReferenceID:       "",
		RelatedAssets:     nil,
		SerialNumber:      "",
		TemplateID:        "",
	}
	cases := map[string]struct {
		db                 *MockArangoDB
		collection         *MockArangoCollectionAssetEntity
		errMsg             string
		expectedResult     *driver.DocumentMeta
		asset              *AssetEntity
		getCollectionCount int
		readDocCount       int
	}{
		"success": {
			db:                 mockDbSuccess,
			collection:         mockCollectionSuccess,
			errMsg:             "",
			asset:              ae,
			expectedResult:     meta,
			getCollectionCount: 1,
			readDocCount:       1,
		},
		"get_collection_fail": {
			db:                 mockDbCollectionFail,
			collection:         mockCollectionGetCollectionFail,
			errMsg:             "",
			asset:              ae,
			expectedResult:     meta,
			getCollectionCount: 1,
			readDocCount:       0,
		},
		"not_found": {
			db:                 mockDbNotFound,
			collection:         mockCollectionNotFound,
			errMsg:             "",
			asset:              ae,
			expectedResult:     meta,
			getCollectionCount: 1,
			readDocCount:       1,
		},
		"read_fail": {
			db:                 mockDbReadFail,
			collection:         mockCollectionReadFail,
			errMsg:             "",
			asset:              ae,
			expectedResult:     meta,
			getCollectionCount: 1,
			readDocCount:       1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			resolver := NewAssetEntityResolver(ae, c.db)
			acResolver := resolver.Location(context.Background())

			c.db.AssertNumberOfCalls(t, "GetCollection", c.getCollectionCount)
			c.collection.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)

			assert.Equal(t, c.errMsg, "")
			if tc == "success" {
				assert.Equal(t, meta.Key, string(*acResolver.ID(context.Background())))
			}
		})
	}
}

func TestAssetEntityResolver_Manufacturer(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.Equal(t, mockAssetEntity.Manufacturer, *mockResolver.Manufacturer())
}

func TestAssetEntityResolver_ManufacturingDate(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.True(t, mockAssetEntity.ManufacturingDate.Time.Equal(mockResolver.ManufacturingDate().Time))
	assert.Equal(t, mockAssetEntity.Description, *mockResolver.Description())
}

func TestAssetEntityResolver_Model(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.Equal(t, mockAssetEntity.Model, *mockResolver.Model())
}

func TestAssetEntityResolver_Name(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.Equal(t, mockAssetEntity.Name, *mockResolver.Name())
}

func TestAssetEntityResolver_ReferenceID(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.Equal(t, mockAssetEntity.ReferenceID, *mockResolver.ReferenceID())
}

func TestAssetEntityResolver_RelatedAssets(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	tempRelatedAssets := mockResolver.RelatedAssets()
	relatedAssets := *tempRelatedAssets
	ctx := context.Background()
	assert.Equal(t, mockAssetEntity.RelatedAssets[0].ID, *relatedAssets[0].ID(ctx))
}

func TestAssetEntityResolver_SerialNumber(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.Equal(t, mockAssetEntity.SerialNumber, *mockResolver.SerialNumber())
}

func TestAssetEntityResolver_TemplateID(t *testing.T) {
	mockAssetEntity := newMockAssetEntity()
	mockResolver := newMockAssetEntityResolver(mockAssetEntity)
	assert.Equal(t, mockAssetEntity.TemplateID, *mockResolver.TemplateID())
}
