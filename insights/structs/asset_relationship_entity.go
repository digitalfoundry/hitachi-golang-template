package structs

import (
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"github.com/graph-gophers/graphql-go"
)

// NewAssetRelationshipEntityResolver returns a pointer to an resolver for the asset relationship entity provided
func NewAssetRelationshipEntityResolver(ar *AssetRelationshipEntity, db insights.ArangoDBer) *AssetRelationshipEntityResolver {
	return &AssetRelationshipEntityResolver{
		m:  *ar,
		db: db,
	}
}

// AssetRelationshipEntityResolver is the graphql resolver struct for the AssetRelationshipEntity model
type AssetRelationshipEntityResolver struct {
	m  AssetRelationshipEntity
	db insights.ArangoDBer
}

// AssetRelationshipEntity struct represents the AssetRelationshipEntity object
type AssetRelationshipEntity struct {
	Description       *string     `json:"description,omitempty"`
	DestinationEntity AssetEntity `json:"destinationEntity"`
	DisplayName       *string     `json:"displayName,omitempty"`
	Direction         string      `json:"direction"`
	ID                *graphql.ID `json:"id,omitempty"`
	LastUpdated       *Date       `json:"lastUpdated,omitempty"`
	Name              *string     `json:"name,omitempty"`
	RelationshipType  string      `json:"relationshipType"`
	SourceEntity      AssetEntity `json:"sourceEntity"`
}

// AssetRelationshipEntityInput is the input for mutations on AssetRelationshipEntity
type AssetRelationshipEntityInput struct {
	Description       string                `json:"description"`
	DestinationEntity AssetIDReferenceInput `json:"destinationEntity"`
	Direction         string                `json:"direction"`
	DisplayName       string                `json:"displayName"`
	LastUpdated       Date                  `json:"lastUpdated"`
	Name              string                `json:"name"`
	RelationshipType  string                `json:"relationshipType"`
	SourceEntity      AssetIDReferenceInput `json:"sourceEntity"`
}

// Description is the graphql resolver function for the description object on the AssetRelationshipEntity struct
func (ar *AssetRelationshipEntityResolver) Description() *string {
	return ar.m.Description
}

// DestinationEntity is the graphql resolver function for the destination entity object on the AssetRelationshipEntity struct
func (ar *AssetRelationshipEntityResolver) DestinationEntity() *AssetEntityResolver {
	return &AssetEntityResolver{
		m:  ar.m.DestinationEntity,
		db: ar.db,
	}
}

// DisplayName is the graphql resolver function for the display name object on the AssetRelationshipEntity struct
func (ar *AssetRelationshipEntityResolver) DisplayName() *string {
	return ar.m.DisplayName
}

// Direction is the graphql resolver function for the direction object on the AssetRelationshipEntity struct
func (ar *AssetRelationshipEntityResolver) Direction() string {
	return ar.m.Direction
}

// ID is the graphql resolver function for the id object on the AssetRelationshipEntity struct
func (ar *AssetRelationshipEntityResolver) ID() *graphql.ID {
	return ar.m.ID
}

// LastUpdated is the graphql resolver function for the last updated object on the AssetRelationshipEntity struct
func (ar *AssetRelationshipEntityResolver) LastUpdated() *Date {
	return ar.m.LastUpdated
}

// Name is the graphql resolver function for the name object on the AssetRelationshipEntity struct
func (ar *AssetRelationshipEntityResolver) Name() *string {
	return ar.m.Name
}

// RelationshipType is the graphql resolver function for the relationship type object on the AssetRelationshipEntity struct
func (ar *AssetRelationshipEntityResolver) RelationshipType() string {
	return ar.m.RelationshipType
}

// SourceEntity is the graphql resolver function for the source entity object on the AssetRelationshipEntity struct
func (ar *AssetRelationshipEntityResolver) SourceEntity() *AssetEntityResolver {
	return &AssetEntityResolver{
		m:  ar.m.SourceEntity,
		db: ar.db,
	}
}
