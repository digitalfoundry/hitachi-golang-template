package structs

import (
	"testing"
	"time"

	"github.com/graph-gophers/graphql-go"
	"github.com/stretchr/testify/assert"
)

func newMockAssetRelationshipEntity() *AssetRelationshipEntity {
	t, err := time.Parse(time.RFC3339, "2019-08-27T13:22:53.108Z")
	if err != nil {
		return nil
	}

	asset1 := &AssetEntity{
		DisplayName: "asset1",
		ID:          "id-1",
	}

	asset2 := &AssetEntity{
		DisplayName: "asset2",
		ID:          "id-2",
	}

	desc := "help"
	graph := graphql.ID(desc)
	return &AssetRelationshipEntity{
		Description:       &desc,
		DestinationEntity: *asset1,
		DisplayName:       &desc,
		Direction:         "take",
		ID:                &graph,
		LastUpdated:       &Date{Time: t},
		Name:              &desc,
		RelationshipType:  "the pizza",
		SourceEntity:      *asset2,
	}
}

func TestNewAssetRelationshipEntityResolver(t *testing.T) {
	db := NewMockArangoDBer()
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, db)
	assert.NotNil(t, resolver)
}

func TestAssetRelationshipEntityResolver_Description(t *testing.T) {
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, nil)
	assert.Equal(t, ar.Description, resolver.Description())
}

func TestAssetRelationshipEntityResolver_DestinationEntity(t *testing.T) {
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, nil)
	assert.Equal(t, ar.DestinationEntity, resolver.DestinationEntity().m)
}
func TestAssetRelationshipEntityResolver_DisplayName(t *testing.T) {
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, nil)
	assert.Equal(t, ar.DisplayName, resolver.DisplayName())
}

func TestAssetRelationshipEntityResolver_Direction(t *testing.T) {
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, nil)
	assert.Equal(t, ar.Direction, resolver.Direction())
}

func TestAssetRelationshipEntityResolver_ID(t *testing.T) {
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, nil)
	assert.Equal(t, ar.ID, resolver.ID())
}

func TestAssetRelationshipEntityResolver_LastUpdated(t *testing.T) {
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, nil)
	assert.Equal(t, ar.LastUpdated.Time, resolver.LastUpdated().Time)
}

func TestAssetRelationshipEntityResolver_Name(t *testing.T) {
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, nil)
	assert.Equal(t, ar.Name, resolver.Name())
}

func TestAssetRelationshipEntityResolver_RelationshipType(t *testing.T) {
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, nil)
	assert.Equal(t, ar.RelationshipType, resolver.RelationshipType())
}

func TestAssetRelationshipEntityResolver_SourceEntity(t *testing.T) {
	ar := newMockAssetRelationshipEntity()
	resolver := NewAssetRelationshipEntityResolver(ar, nil)
	assert.Equal(t, ar.SourceEntity, resolver.SourceEntity().m)
}
