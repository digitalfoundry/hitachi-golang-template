package structs

import (
	"fmt"
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
)

// BestPractice represents the best practice object
type BestPractice struct {
	Throughput Throughput
	SeamType   SeamType
	KPICalculator
}

// NewBestPractice creates a new instance of the BestPractice struct
func NewBestPractice(rawDb insights.InfluxDBer, metaDB insights.ArangoDBer, throughput Throughput, seamType SeamType) *BestPractice {
	return &BestPractice{
		Throughput: throughput,
		SeamType:   seamType,
		KPICalculator: KPICalculator{
			ID:         "best-practice-output",
			RawDataDB:  rawDb,
			MetadataDB: metaDB,
		},
	}
}

// GetID gets the best practice ID
func (b *BestPractice) GetID() string {
	return b.ID
}

// Calculate is the best practice calculate function
func (b *BestPractice) Calculate(now time.Time) []*MeasurementEntity {
	metadata, err := b.getMetadataByID()
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error querying kpi metadata for best practice calculator")
		return nil
	}
	query := metadata.Query
	mainResponse, mainErr := b.RawDataDB.Query(query.Main.Template, "KPI")
	if mainErr != nil {
		insights.LogErrorWithInfo(mainErr, "", fmt.Sprintf("error querying %s for influx", b.ID))
		return nil
	}

	entity := &MeasurementEntity{
		AdditionalInfo: map[string]interface{}{},
	}
	bestPractice, parseErr := parseResponseFirstValue(mainResponse)
	if parseErr != nil {
		insights.LogErrorWithInfo(parseErr, "", fmt.Sprintf("error parsing influx response"))
		return nil
	}

	primaryOutput, err := parseFloat(bestPractice[1])
	if err != nil {
		insights.LogErrorWithInfo(err, "", fmt.Sprintf("error parsing primary output"))
		return nil
	}

	secondaryOutput, err := parseFloat(bestPractice[2])
	if err != nil {
		insights.LogErrorWithInfo(err, "", fmt.Sprintf("error parsing secondary output"))
		return nil
	}

	bpOutput := primaryOutput + secondaryOutput
	fields := map[string]interface{}{
		"value":                          bpOutput,
		"targetAsh":                      bestPractice[2],
		"primaryAsh":                     bestPractice[3],
		"secondaryAsh":                   bestPractice[4],
		"bestPracticeOutput":             bpOutput,
		"bestPracticePrimaryOutput":      bestPractice[0],
		"bestPracticeSecondaryOutput":    bestPractice[1],
		"CHPP_recInputFeed":              bestPractice[5],
		"Module1_recHeadPressure":        bestPractice[6],  // query does not match implementation
		"Module1_recSpecificGravity":     bestPractice[7],  // query does not match implementation
		"SecondStage_recHeadPressure":    bestPractice[8],  // query does not match implementation
		"SecondStage_recSpecificGravity": bestPractice[9],  // query does not match implementation
		"restrictionInForce":             bestPractice[10], // query does not match implementation
	}

	for name, query := range map[string]CronQuery{
		"CHPP_curInputFeed1":             query.CHPP_curInputFeed1,
		"CHPP_curInputFeed2":             query.CHPP_curInputFeed2,
		"Module1_curSpecificGravity":     query.Module1_curSpecificGravity,
		"SecondStage_curSpecificGravity": query.SecondStage_curSpecificGravity,
		"Module1_curHeadPressure":        query.Module1_curHeadPressure,
		"SecondStage_curHeadPressure":    query.SecondStage_curHeadPressure,
		"CHPP_primaryOutput":             query.CHPP_primaryOutput,
		"CHPP_secondaryOutput":           query.CHPP_secondaryOutput} {

		val, err := runBasicQuery(b.RawDataDB, query.Template)
		if err != nil {
			insights.LogErrorWithInfo(err, "", fmt.Sprintf("error querying %s for influx", b.ID))
		}

		fields[name] = val
	}

	tags := map[string]string{
		"assetId": "CHPP",
	}

	seamTypeProps := b.SeamType.Calculate(now)
	if len(seamTypeProps) > 0 {
		entity.AdditionalInfo[b.SeamType.GetID()] = seamTypeProps
	}

	throughputProps := b.Throughput.Calculate(now)
	if len(throughputProps) > 0 {
		entity.AdditionalInfo[b.Throughput.GetID()] = throughputProps
	}

	entity.Fields = fields
	entity.Tags = tags
	entity.Time = now
	entity.Measurement = b.ID

	return []*MeasurementEntity{entity}
}

// GetMeasurementsFromDatabase returns the processed influx data for best practice
func (b *BestPractice) GetMeasurementsFromDatabase(now time.Time) []*MeasurementEntity {
	return []*MeasurementEntity{}
}

// runBasicQuery returns first value as string for influx query
func runBasicQuery(db insights.InfluxDBer, query string) (string, error) {
	response, err := db.Query(query, "KPI")
	if err != nil {
		return "", err
	}

	vals, err := parseResponseFirstValue(response)
	if err != nil {
		return "", err
	}

	return vals[0].(string), nil
}
