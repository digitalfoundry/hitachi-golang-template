package structs

// Email Configuration is the email alert information including triggers and the email template
type EmailConfiguration struct {
	Enabled  bool                   `json:"enabled"`
	Triggers map[string]interface{} `json:"triggers"`
	Template EmailObject            `json:"template"`
}

// EmailConfigurationInput is the input struct for mutations on email configuration
type EmailConfigurationInput struct {
	Enabled  *bool                   `json:"enabled,omitempty"`
	Triggers *map[string]interface{} `json:"triggers,omitempty"`
	Template *EmailObjectInput       `json:"template,omitempty"`
}

// EmailObject is the email information including recipients, sender, and the message it self
type EmailObject struct {
	From    *string  `json:"from,omitempty"`
	To      []string `json:"to"`
	Subject string   `json:"subject"`
	Message string   `json:"message"`
}

// EmailObjectInput is the input struct for mutations on the email object
type EmailObjectInput struct {
	From    *string   `json:"from,omitempty"`
	To      *[]string `json:"to,omitempty"`
	Subject *string   `json:"subject,omitempty"`
	Message *string   `json:"message,omitempty"`
}

// EmailConfigurationResolver is the graphql resolver for the EmailConfiguration struct
type EmailConfigurationResolver struct {
	m EmailConfiguration
}

// Enabled is the graphql resolver function for the enabled variable
func (e *EmailConfigurationResolver) Enabled() bool {
	return e.m.Enabled
}

// Triggers is the graphql resolver function for the triggers object
func (e *EmailConfigurationResolver) Triggers() *GraphqlMap {
	return &GraphqlMap{Map: e.m.Triggers}
}

// Template is the graphql resolver function for the template object
func (e *EmailConfigurationResolver) Template() *EmailObjectResolver {
	return &EmailObjectResolver{m: e.m.Template}
}

// EmailObjectResolver is the graphql resolver object for the EmailObject struct
type EmailObjectResolver struct {
	m EmailObject
}

// From is the graphql resolver function for the From member variable
func (e *EmailObjectResolver) From() *string {
	return e.m.From
}

// To is the graphql resolver function for the To member variable
func (e *EmailObjectResolver) To() []string {
	return e.m.To
}

// Subject is the graphql resolver function for the Subject member variable
func (e *EmailObjectResolver) Subject() string {
	return e.m.Subject
}

// Message is the graphql resolver function for the Message member variable
func (e *EmailObjectResolver) Message() string {
	return e.m.Message
}
