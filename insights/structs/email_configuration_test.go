package structs

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func NewMockEmailConfiguration() EmailConfiguration {
	return EmailConfiguration{
		Triggers: map[string]interface{}{
			"bool":   false,
			"string": "string",
		},
		Template: NewMockEmailObject(),
	}
}

func NewMockEmailObject() EmailObject {
	tempFrom := "Subject"
	return EmailObject{
		From:    &tempFrom,
		To:      []string{"Recipient"},
		Subject: "Subject",
		Message: "Message",
	}
}

func TestEmailConfigurationResolver_Template(t *testing.T) {
	mockEmailConfig := NewMockEmailConfiguration()
	resolver := EmailConfigurationResolver{m: mockEmailConfig}
	template := resolver.Template()
	assert.Equal(t, mockEmailConfig.Template.To, template.m.To)
	assert.Equal(t, mockEmailConfig.Template.Message, template.m.Message)
	assert.Equal(t, mockEmailConfig.Template.Subject, template.m.Subject)
	assert.Equal(t, mockEmailConfig.Template.To[0], template.m.To[0])
}

func TestEmailConfigurationResolver_Triggers(t *testing.T) {
	mockEmailConfig := NewMockEmailConfiguration()
	resolver := EmailConfigurationResolver{m: mockEmailConfig}
	triggers := resolver.Triggers()
	boolVal, boolExists := triggers.Map["bool"]
	strVal, strExists := triggers.Map["string"]
	assert.Len(t, triggers.Map, 2)
	assert.True(t, boolExists)
	assert.True(t, strExists)
	assert.Equal(t, mockEmailConfig.Triggers["bool"], boolVal)
	assert.Equal(t, mockEmailConfig.Triggers["string"], strVal)
}

func TestEmailObjectResolver_From(t *testing.T) {
	eObj := NewMockEmailObject()
	resolver := EmailObjectResolver{m: eObj}
	assert.Equal(t, eObj.From, resolver.From())
}

func TestEmailObjectResolver_Message(t *testing.T) {
	eObj := NewMockEmailObject()
	resolver := EmailObjectResolver{m: eObj}
	assert.Equal(t, eObj.Message, resolver.Message())
}

func TestEmailObjectResolver_Subject(t *testing.T) {
	eObj := NewMockEmailObject()
	resolver := EmailObjectResolver{m: eObj}
	assert.Equal(t, eObj.Subject, resolver.Subject())
}

func TestEmailObjectResolver_To(t *testing.T) {
	eObj := NewMockEmailObject()
	resolver := EmailObjectResolver{m: eObj}
	to := resolver.To()
	assert.Len(t, to, 1)
	assert.Equal(t, eObj.To[0], to[0])
}
