package structs

import (
	"fmt"
	"math"
	"sort"
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"github.com/jinzhu/copier"
)

//ThresholdSeverity used to hold a threshold value and its severity level
type ThresholdSeverity struct {
	Threshold float64
	Severity  Severity
}

//Evaluator class to evaluate threshold and severity values
type Evaluator struct {
	EventConfiguration EventConfiguration
	hysteresis         map[Severity]Hysteresis
	thresholds         []ThresholdSeverity
	takeMin            bool
}

//Init initializers the Evaluator with the appropriate maps and array values
func (e *Evaluator) Init() {
	severities := e.EventConfiguration.Severities

	// sort severities in descending order
	sort.SliceStable(severities, func (i,j int) bool {
		severityI, err := severities[i].Severity.GetValue()
		if err != nil {
			insights.PanicErrorIfExists(err)
		}
		severityJ, err := severities[j].Severity.GetValue()
		if err != nil {
			insights.PanicErrorIfExists(err)
		}
		return severityI > severityJ
	})

	e.hysteresis = map[Severity]Hysteresis{}

	e.takeMin = severities[0].Threshold.MinValue > severities[len(severities)-1].Threshold.MinValue
	for i := range severities {
		hysteresisKey := *severities[i].Severity
		threshold := severities[i].Threshold
		thresholdValue := threshold.MaxValue
		hysteresisValue := threshold.Hysteresis

		if e.takeMin {
			thresholdValue = threshold.MinValue
		}

		e.thresholds = append(e.thresholds, ThresholdSeverity{
			Threshold: thresholdValue,
			Severity:  hysteresisKey,
		})

		h := Hysteresis{
			ThresholdToSet:  thresholdValue,
			IsSet:           false,
			CalculationType: *e.EventConfiguration.CalculationType,
		}

		if hysteresisValue != nil {
			h.ThresholdToReset = *hysteresisValue
		}

		e.hysteresis[hysteresisKey] = h
	}
}

func getValueFromEvent(event Event, field string) *float64 {
	value, valOk := event.Properties[field]
	if !valOk {
		fmt.Printf("Unable to calculate event severity. No value is found in field %s. return NONE. property map: %v",
			field, event.Properties)
		return nil
	}

	floatVal, ok := value.(float64)
	if !ok {
		fmt.Printf("Unable to convert value %v to float", value)
		return nil
	}

	return &floatVal
}

func (e *Evaluator) addDeviationToEventProperties(event Event, deviation *float64) {
	field := e.EventConfiguration.PreviousEventComparisonField

	if deviation != nil && field != nil {
		event.Properties[*field] = deviation
	}
}

func (e *Evaluator) shouldSuppress(value float64, severity Severity, propertyToCompare interface{}) bool {
	if hysteresisChecker, ok := e.hysteresis[severity]; ok {
		return hysteresisChecker.ShouldSuppress(value, propertyToCompare)
	}

	return false
}

//nolint:gocyglo
func (e *Evaluator) deviationCalculation(event Event, calculationType string, value float64) {
	baseField := e.EventConfiguration.CalculationBaseField
	baseValue := getValueFromEvent(event, *baseField)
	deviation := *new(*float64)

	if baseValue != nil {
		switch calculationType {
		case "ABSOLUTE_DEVIATION":
			thresholdValues := e.getThresholdStream()
			newValue := (value - *baseValue) / *baseValue
			deviation = &newValue
			var newMap = map[string]interface{}{}

			for i := range thresholdValues {
				threshold := thresholdValues[i]
				severity, err := GetSeverityString(threshold.Severity)
				tMap := map[string]float64{}
				if err != nil {
					insights.LogErrorWithInfo(err, "", "Unable to get string value to severity")
				}

				tMap["left"] = threshold.Threshold - *baseValue
				tMap["right"] = threshold.Threshold + *baseValue

				newMap[severity] = tMap
			}

			event.Thresholds = newMap
		case "ABSOLUTE_PERCENTAGE_DEVIATION":
			thresholdValues := e.getThresholdStream()
			newValue := (value - *baseValue) / *baseValue * 100
			deviation = &newValue
			var newMap = map[string]interface{}{}

			for i := range thresholdValues {
				threshold := thresholdValues[i]
				severity, err := GetSeverityString(threshold.Severity)
				tMap := map[string]float64{}
				if err != nil {
					insights.LogErrorWithInfo(err, "", "Unable to get string value to severity")
				}

				tMap["left"] = (1 - threshold.Threshold) * *baseValue
				tMap["right"] = (1 + threshold.Threshold) * *baseValue

				newMap[severity] = tMap
			}

			event.Thresholds = newMap
		default:
			thresholdValues := e.calculateActualThresholdValues(event)
			newValue := value - *baseValue
			deviation = &newValue

			newThresholdsMap := make(map[string]interface{})
			for i := range thresholdValues {
				threshold := thresholdValues[i]
				severity, err := GetSeverityString(threshold.Severity)
				if err != nil {
					insights.LogErrorWithInfo(err, "", "Unable to get string value to severity")
				}
				newThresholdsMap[severity] = threshold.Threshold
			}

			event.Thresholds = newThresholdsMap
		}
	}

	e.addDeviationToEventProperties(event, deviation)
}

//nolint:gocyglo //CalculateSeverity figures out what the current severity level is for an event
func (e *Evaluator) CalculateSeverity(event Event) Severity {
	calculationField := e.EventConfiguration.CalculationField
	var value float64
	if event.RiskScore != nil {
		value = *event.RiskScore
	}
	var v float64

	if calculationField != nil {
		value = *getValueFromEvent(event, *calculationField)
	}

	calculationType := e.EventConfiguration.CalculationType

	if calculationType == nil {
		none := "VALUE"
		calculationType = &none
	}

	defer e.deviationCalculation(event, *calculationType, value)

	if e.EventConfiguration.CalculationBaseField != nil {
		baseField := e.EventConfiguration.CalculationBaseField
		baseValue := event.Properties[*baseField].(float64)

		switch *calculationType {
		case "PERCENTAGE_DEVIATION":
		case "ABSOLUTE_PERCENTAGE_DEVIATION":
			v = (value - baseValue) / baseValue
		default:
			v = value
		}
	} else {
		v = value
	}

	for i := range e.thresholds {
		threshold := e.thresholds[i]
		if e.shouldSuppress(v, threshold.Severity, event.Properties["seam-type"]) {
			continue
		}

		switch *calculationType {
		case "ABSOLUTE_PERCENTAGE_DEVIATION":
		case "ABSOLUTE_DEVIATION":
			v = math.Abs(v)
		}

		if e.takeMin {
			if v >= threshold.Threshold {
				return threshold.Severity
			}
		} else {
			if v <= threshold.Threshold {
				return threshold.Severity
			}
		}
	}

	return None
}

//CalculateRiskScore calculates the risk score on an event
func (e *Evaluator) CalculateRiskScore(event Event) float64 {
	riskScore := event.Properties[*e.EventConfiguration.RiskScoreField]
	return *e.EventConfiguration.RiskScoreScalingFactor * riskScore.(float64)
}

//Returns all thresholdSeverities that aren't NONE
func (e *Evaluator) getThresholdStream() []ThresholdSeverity {
	var newThresholds []ThresholdSeverity
	for i := range e.thresholds {
		if e.thresholds[i].Severity != None {
			newThresholds = append(newThresholds, e.thresholds[i])
		}
	}

	return newThresholds
}

//nolint:gocyglo
func (e *Evaluator) calculateActualThresholdValues(event Event) []ThresholdSeverity {
	var thresholdValues []ThresholdSeverity
	calculationType := e.EventConfiguration.CalculationType

	if calculationType == nil || *calculationType == "VALUE" {
		thresholdValues = e.getThresholdStream()
		return thresholdValues
	}

	baseField := e.EventConfiguration.CalculationBaseField
	baseValue := event.Properties[*baseField].(float64)

	switch *calculationType {
	case "DEVIATION":
		for i := range e.thresholds {
			threshold := e.thresholds[i]
			threshold.Threshold = threshold.Threshold + baseValue
			thresholdValues = append(thresholdValues, threshold)
		}
	case "PERCENTAGE_DEVIATION":
		for i := range e.thresholds {
			threshold := e.thresholds[i]
			threshold.Threshold = (threshold.Threshold + 1) * baseValue
			thresholdValues = append(thresholdValues, threshold)
		}
	case "ABSOLUTE_DEVIATION":
		for i := range e.thresholds {
			origThreshold := e.thresholds[i]
			subThreshold := ThresholdSeverity{}
			subThreshold2 := ThresholdSeverity{}
			var copyErr error
			copyErr = copier.Copy(&origThreshold, &subThreshold)
			if copyErr != nil {
				insights.LogErrorWithInfo(copyErr, "", "Unable to deep copy threshold object")
			}

			copyErr = copier.Copy(&origThreshold, &subThreshold2)
			if copyErr != nil {
				insights.LogErrorWithInfo(copyErr, "", "Unable to deep copy threshold object")
			}

			subThreshold.Threshold = subThreshold.Threshold - baseValue
			severityValue, getErr := subThreshold.Severity.GetValue()
			if getErr != nil {
				fmt.Printf("Unable to get severity value")
			}

			newSeverity, _ := GetSeverity(severityValue - 1)
			subThreshold.Severity = newSeverity

			thresholdValues = append(thresholdValues, subThreshold)

			subThreshold2.Threshold = subThreshold2.Threshold + baseValue
			thresholdValues = append(thresholdValues, subThreshold2)
		}
	case "ABSOLUTE_PERCENTAGE_DEVIATION":
		for i := range e.thresholds {
			origThreshold := e.thresholds[i]
			subThreshold := ThresholdSeverity{}
			subThreshold2 := ThresholdSeverity{}
			var copyErr error
			copyErr = copier.Copy(&origThreshold, &subThreshold)
			if copyErr != nil {
				insights.LogErrorWithInfo(copyErr, "", "Unable to deep copy threshold object")
			}

			copyErr = copier.Copy(&origThreshold, &subThreshold2)
			if copyErr != nil {
				insights.LogErrorWithInfo(copyErr, "", "Unable to deep copy threshold object")
			}

			subThreshold.Threshold = (1 - subThreshold.Threshold) * baseValue
			severityValue, getErr := subThreshold.Severity.GetValue()
			if getErr != nil {
				fmt.Printf("Unable to get severity value")
			}

			newSeverity, _ := GetSeverity(severityValue - 1)
			subThreshold.Severity = newSeverity

			thresholdValues = append(thresholdValues, subThreshold)

			subThreshold2.Threshold = (1 + subThreshold2.Threshold) * baseValue
			thresholdValues = append(thresholdValues, subThreshold2)
		}
	default:
		fmt.Printf("calculationType is not supported: %s", *calculationType)
	}

	return thresholdValues
}

//IsHysteresisMode returns whether any of the hysteresis values has IsSet equal to true
func (e *Evaluator) IsHysteresisMode() bool {
	for _, value := range e.hysteresis {
		if value.IsSet {
			return true
		}
	}

	return false
}

// ShouldSuppress analyzes historical data for the asset type and event type combination to determine if the event shou
func (e *Evaluator) ShouldSuppress(assetType string, eventTypeID string, severity Severity, prevEvent *KPIEvent) bool {
	configuration := e.EventConfiguration

	if prevEvent != nil {

		severityVal, severityErr := severity.GetValue()
		if severityErr != nil {
			fmt.Printf("Unable to get severity value")
		}

		if prevEvent.Severity < severityVal {
			return false
		}

		if configuration.SuppressionTime == nil {
			fmt.Printf("Time suppression is null, continuing")
			return false
		}

		minsDiff := time.Now().Sub(prevEvent.CreatedDate).Minutes()
		if minsDiff < *configuration.SuppressionTime {
			fmt.Printf("Last event wihtin quiet window, ignoring")
			return true
		}

		return false
	}

	return false
}
