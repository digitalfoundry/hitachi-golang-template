package structs

import (
	"fmt"
	"time"

	"github.com/graph-gophers/graphql-go"
)

// NewEventResolver returns a new event resolver
func NewEventResolver(e *Event) *EventResolver {
	return &EventResolver{m: *e}
}

// EventResolver is the resolver for the event struct
type EventResolver struct {
	m Event
}

// Event represents the full event struct
type Event struct {
	ID           graphql.ID             `json:"id"` // omitempty
	Name         *string                `json:"name"`
	DisplayName  *string                `json:"displayName"`
	Description  *string                `json:"description"`
	LastUpdated  time.Time              `json:"lastUpdated"`
	CreatedDate  time.Time              `json:"createdDate"` // Required
	EventType    string                 `json:"eventType"`   // Required
	EventTypeID  string                 `json:"eventTypeId"` // Required
	RiskScore    *float64               `json:"riskScore"`
	Status       StatusWrapper          `json:"status"` // Required
	Priority     *Priority              `json:"priority"`
	Severity     SeverityWrapper        `json:"severity"` // Required
	AssetID      graphql.ID             `json:"assetId"`  // Required
	ResolvedDate time.Time              `json:"resolvedDate"`
	Properties   map[string]interface{} `json:"properties"`
	Thresholds   map[string]interface{} `json:"thresholds"`
}

// ID is the graphql resolver function for the id object on the EventResolver struct
func (a *EventResolver) ID() *graphql.ID {
	return &a.m.ID
}

// Name is the graphql resolver function for the name string on the EventResolver struct
func (a *EventResolver) Name() *string {
	return a.m.Name
}

// DisplayName is the graphql resolver function for the DisplayName string on the EventResolver struct
func (a *EventResolver) DisplayName() *string {
	return a.m.DisplayName
}

// Description is the graphql resolver function for the Description string on the EventResolver struct
func (a *EventResolver) Description() *string {
	return a.m.Description
}

// LastUpdated is the graphql resolver function for the LastUpdated time on the EventResolver struct
func (a *EventResolver) LastUpdated() *Date {
	return &Date{a.m.LastUpdated}
}

// CreatedDate is the graphql resolver function for the CreatedDate time on the EventResolver struct
func (a *EventResolver) CreatedDate() Date {
	return Date{a.m.CreatedDate}
}

// EventType is the graphql resolver function for the EventType string on the EventResolver struct
func (a *EventResolver) EventType() string {
	return a.m.EventType
}

// EventTypeID is the graphql resolver function for the EventTypeID string on the EventResolver struct
func (a *EventResolver) EventTypeID() string {
	return a.m.EventTypeID
}

// RiskScore is the graphql resolver function for the RiskScore string on the EventResolver struct
func (a *EventResolver) RiskScore() *float64 {
	return a.m.RiskScore
}

// Status is the graphql resolver function for the Status string on the EventResolver struct
func (a *EventResolver) Status() (Status, error) {
	result, err := a.m.Status.GetValue()
	if err != nil {
		return "", err
	}

	return result, nil
}

// Priority is the graphql resolver function for the Priority on the EventResolver struct
func (a *EventResolver) Priority() *Priority {
	return a.m.Priority
}

// Severity is the graphql resolver function for the Severity on the EventResolver struct
func (a *EventResolver) Severity() (Severity, error) {
	result, err := a.m.Severity.GetValue()
	if err != nil {
		return "", err
	}

	return result, nil
}

// AssetID is the graphql resolver function for the AssetID on the EventResolver struct
func (a *EventResolver) AssetID() graphql.ID {
	return a.m.AssetID
}

// ResolvedDate is the graphql resolver function for the ResolvedDate on the EventResolver struct
func (a *EventResolver) ResolvedDate() *Date {
	return &Date{a.m.ResolvedDate}
}

//Properties gets the Properties object on the EventResolver struct
func (a *EventResolver) Properties() *GraphqlMap {
	return &GraphqlMap{Map: a.m.Properties}
}

//nolint:underscore
// EventFilterOptions excusively uses AND, OR, or Field+condition/value to represent
// a query filter string
type EventFilterOptions struct {
	AND             *[]EventFilterOptions
	OR              *[]EventFilterOptions
	Field           *string
	String_is       *string
	String_not      *string
	String_in       *[]string
	String_not_in   *[]string
	String_contains *string
	Float_is        *float64
	Float_not       *float64
	Float_lt        *float64
	Float_lte       *float64
	Float_gt        *float64
	Float_gte       *float64
	Id_is           *graphql.ID
	Date_lt         *Date
	Date_lte        *Date
	Date_gt         *Date
	Date_gte        *Date
	Severity_is     *Severity
	Severity_not    *Severity
	Severity_in     *[]Severity
	Severity_not_in *[]Severity
	Status_is       *Status
	Status_not      *Status
	Status_in       *[]Status
	Status_not_in   *[]Status
	Priority_is     *Priority
	Priority_not    *Priority
	Priority_in     *[]Priority
	Priority_not_in *[]Priority
}

//nolint:gocyclo // GetWhereClause builds a filter clause for an AQL query
func (f *EventFilterOptions) GetAQLWhereClause(variableName string) (string, error) {
	operatorStr := ""

	arrayString := func(strArray []string) string {
		buildStr := ""

		for idx, stringN := range strArray {
			commaStr := ""

			if idx < len(strArray)-1 {
				commaStr = ","
			}

			buildStr += fmt.Sprintf(`"%s"%s`, stringN, commaStr)

		}

		return fmt.Sprintf("[%s]", buildStr)
	}

	intStr := func(intArray []int) string {
		buildStr := ""

		for idx, stringN := range intArray {
			commaStr := ""

			if idx < len(intArray)-1 {
				commaStr = ","
			}

			buildStr += fmt.Sprintf(`%d%s`, stringN, commaStr)

		}

		return fmt.Sprintf("[%s]", buildStr)
	}

	if f.String_is != nil {
		operatorStr = fmt.Sprintf(`== "%s"`, *f.String_is)
	} else if f.String_not != nil {
		operatorStr = fmt.Sprintf(`!= "%s"`, *f.String_not)
	} else if f.String_in != nil {
		operatorStr = fmt.Sprintf("IN %s", arrayString(*f.String_in))
	} else if f.String_not_in != nil {
		operatorStr = fmt.Sprintf("NOT IN %s", arrayString(*f.String_in))
	} else if f.String_contains != nil {
		return fmt.Sprintf(`CONTAINS(%s.%s,"%s")`, variableName, *f.Field, *f.String_contains), nil
	} else if f.Float_is != nil {
		operatorStr = fmt.Sprintf("== %f", *f.Float_is)
	} else if f.Float_not != nil {
		operatorStr = fmt.Sprintf("!= %f", *f.Float_not)
	} else if f.Float_lt != nil {
		operatorStr = fmt.Sprintf("< %f", *f.Float_lt)
	} else if f.Float_lte != nil {
		operatorStr = fmt.Sprintf("<= %f", *f.Float_lte)
	} else if f.Float_gt != nil {
		operatorStr = fmt.Sprintf("> %f", *f.Float_gt)
	} else if f.Float_gte != nil {
		operatorStr = fmt.Sprintf(">= %f", *f.Float_gte)
	} else if f.Id_is != nil {
		return fmt.Sprintf("%s._key == %s", variableName, string(*f.Id_is)), nil
	} else if f.Date_lt != nil {
		operatorStr = fmt.Sprintf(`< DATE_ISO8601("%s")`, f.Date_lt.Format(time.RFC3339))
	} else if f.Date_lte != nil {
		operatorStr = fmt.Sprintf(`<= DATE_ISO8601("%s")`, f.Date_lte.Format(time.RFC3339))
	} else if f.Date_gt != nil {
		operatorStr = fmt.Sprintf(`> DATE_ISO8601("%s")`, f.Date_gt.Format(time.RFC3339))
	} else if f.Date_gte != nil {
		operatorStr = fmt.Sprintf(`>= DATE_ISO8601("%s")`, f.Date_gte.Format(time.RFC3339))
	} else if f.Severity_is != nil {
		severity, err := f.Severity_is.GetValue()
		if err != nil {
			return "", err
		}
		operatorStr = fmt.Sprintf(`== %d`, severity)
	} else if f.Severity_not != nil {
		severity, err := f.Severity_not.GetValue()
		if err != nil {
			return "", err
		}
		operatorStr = fmt.Sprintf(`!= %d`, severity)
	} else if f.Severity_in != nil {
		intArr, err := SeverityArrayToIntArray(*f.Severity_in)
		if err != nil {
			return "", err
		}

		operatorStr = fmt.Sprintf("IN %s", intStr(intArr))
	} else if f.Severity_not_in != nil {
		intArr, err := SeverityArrayToIntArray(*f.Severity_not_in)
		if err != nil {
			return "", err
		}

		operatorStr = fmt.Sprintf("NOT IN %s", intStr(intArr))
	} else if f.Status_is != nil {
		status, err := f.Status_is.GetValue()
		if err != nil {
			return "", err
		}

		operatorStr = fmt.Sprintf(`== %d`, status)
	} else if f.Status_not != nil {
		status, err := f.Status_not.GetValue()
		if err != nil {
			return "", err
		}

		operatorStr = fmt.Sprintf(`!= %d`, status)
	} else if f.Status_in != nil {
		intArr, err := StatusArrayToIntArray(*f.Status_in)
		if err != nil {
			return "", err
		}

		operatorStr = fmt.Sprintf("IN %s", intStr(intArr))
	} else if f.Status_not_in != nil {
		intArr, err := StatusArrayToIntArray(*f.Status_not_in)
		if err != nil {
			return "", err
		}

		operatorStr = fmt.Sprintf("NOT IN %s", intStr(intArr))
	} else if f.Priority_is != nil {
		operatorStr = fmt.Sprintf(`== "%s"`, *f.Priority_is)
	} else if f.Priority_not != nil {
		operatorStr = fmt.Sprintf(`!= "%s"`, *f.Priority_not)
	} else if f.Priority_in != nil {
		strArr := make([]string, len(*f.Priority_in))
		for i, v := range *f.Priority_in {
			strArr[i] = string(v)
		}

		operatorStr = fmt.Sprintf("IN %s", arrayString(strArr))
	} else if f.Priority_not_in != nil {
		strArr := make([]string, len(*f.Priority_not_in))
		for i, v := range *f.Priority_not_in {
			strArr[i] = string(v)
		}

		operatorStr = fmt.Sprintf("NOT IN %s", arrayString(strArr))
	}

	resultStr := fmt.Sprintf("%s.%s %s", variableName, *f.Field, operatorStr)

	if f.AND != nil {
		for i := range *f.AND {
			whereStr, err := (*f.AND)[i].GetAQLWhereClause(variableName)
			if err != nil {
				return "", err
			}

			resultStr = fmt.Sprintf("%s && %s", resultStr, whereStr)
		}
	}

	if f.OR != nil {
		for i := range *f.OR {
			whereStr, err := (*f.OR)[i].GetAQLWhereClause(variableName)
			if err != nil {
				return "", err
			}
			resultStr = fmt.Sprintf("%s || %s", resultStr, whereStr)
		}
	}

	return resultStr, nil
}
