package structs

import (
	"context"

	"github.com/graph-gophers/graphql-go"
)

// NewEventConfigurationResolver returns a pointer to an resolver for the event configuration provided
func NewEventConfigurationResolver(ac *EventConfiguration) *EventConfigurationResolver {
	return &EventConfigurationResolver{m: *ac}
}

// EventConfiguration struct represents the EventConfiguration object
type EventConfiguration struct {
	Key                          string               `json:"_key,omitempty"`
	ID                           *graphql.ID          `json:"id,omitempty"`
	Name                         *string              `json:"name,omitempty"`
	DisplayName                  *string              `json:"displayName,omitempty"`
	Description                  string               `json:"description"`
	LastUpdated                  Date                 `json:"lastUpdated,omitempty"`
	AssetType                    string               `json:"assetType"`
	EventTypeID                  string               `json:"eventTypeId"`
	Severities                   []*SeverityThreshold `json:"severities"`
	EventNameFormat              *string              `json:"eventNameFormat,omitempty"`
	EventDescriptionFormat       []string             `json:"eventDescriptionFormat"`
	EventTypeDisplayName         *string              `json:"eventTypeDisplayName,omitempty"`
	RiskScoreField               *string              `json:"riskScoreField,omitempty"`
	RiskScoreScalingFactor       *float64             `json:"riskScoreScalingFactor,omitempty"`
	CalculationField             *string              `json:"calculationField,omitempty"`
	CalculationBaseField         *string              `json:"calculationBaseField,omitempty"`
	CalculationType              *string              `json:"calculationType,omitempty"`
	PreviousEventComparisonField *string              `json:"previousEventComparisonField,omitempty"`
	Email                        *EmailConfiguration  `json:"email,omitempty"`
	SuppressionTime              *float64             `json:"suppressionTime,omitempty"`
}

// EventConfigurationInput is the input for creating a new EventConfiguration
type EventConfigurationInput struct {
	Name        string
	DisplayName string
	Description string
	AssetType   string
	EventTypeID string
	Severities  []SeverityThresholdInput
}

// EventConfigurationInput is the input for creating a new EventConfiguration
type EventConfigurationUpdate struct {
	ID              graphql.ID
	Name            string
	DisplayName     string
	Description     string
	SuppressionTime *float64
	Severities      *[]SeverityThresholdInput
	Email           EmailConfigurationInput
}

// EventConfigurationResolver is the graphql resolver struct for the EventConfiguration model
type EventConfigurationResolver struct {
	m EventConfiguration
}

// NewEventTypeInfoResolver returns a pointer to a resolver for the event type info provided
func NewEventTypeInfoResolver(eti *EventTypeInfo) *EventTypeInfoResolver {
	return &EventTypeInfoResolver{m: *eti}
}

// EventTypeInfo struct represents the EventTypeInfo object
type EventTypeInfo struct {
	EventTypeID          *graphql.ID
	EventTypeDisplayName *string
}

// EventTypeInfoResolver is the graphql resolver struct for the EventTypeInfo model
type EventTypeInfoResolver struct {
	m EventTypeInfo
}

// ID is the graphql resolver function for the id object on the EventConfiguration struct
func (a EventConfigurationResolver) ID(ctx context.Context) *graphql.ID {
	return a.m.ID
}

// Name is the graphql resolver function for the Name object on the EventConfiguration struct
func (a EventConfigurationResolver) Name() *string {
	return a.m.Name
}

// DisplayName is the graphql resolver function for the DisplayName object on the EventConfiguration struct
func (a EventConfigurationResolver) DisplayName() *string {
	return a.m.DisplayName
}

// Description is the graphql resolver function for the Description object on the EventConfiguration struct
func (a EventConfigurationResolver) Description() *string {
	return &a.m.Description
}

// LastUpdated is the graphql resolver function for the LastUpdated object on the EventConfiguration struct
func (a EventConfigurationResolver) LastUpdated() *Date {
	return &a.m.LastUpdated
}

// AssetType is the graphql resolver function for the AssetType object on the EventConfiguration struct
func (a EventConfigurationResolver) AssetType() string {
	return a.m.AssetType
}

// EventTypeID is the graphql resolver function for the EventTypeID object on the EventConfiguration struct
func (a EventConfigurationResolver) EventTypeID() string {
	return a.m.EventTypeID
}

// Severities is the graphql resolver function for the Severities object on the EventConfiguration struct
func (a EventConfigurationResolver) Severities() []*SeverityThresholdResolver {
	var resolvers []*SeverityThresholdResolver
	for _, st := range a.m.Severities {
		resolvers = append(resolvers, &SeverityThresholdResolver{m: *st})
	}
	return resolvers
}

// EventNameFormat is the graphql resolver function for the EventNameFormat object on the EventConfiguration struct
func (a EventConfigurationResolver) EventNameFormat() *string {
	return a.m.EventNameFormat
}

// EventDescriptionFormat is the graphql resolver function for the EventDescriptionFormat object on the EventConfiguration struct
func (a EventConfigurationResolver) EventDescriptionFormat() []string {
	return a.m.EventDescriptionFormat
}

// EventTypeDisplayName is the graphql resolver function for the EventTypeDisplayName object on the EventConfiguration struct
func (a EventConfigurationResolver) EventTypeDisplayName() *string {
	return a.m.EventTypeDisplayName
}

// RiskScoreField is the graphql resolver function for the RiskScoreField object on the EventConfiguration struct
func (a EventConfigurationResolver) RiskScoreField() *string {
	return a.m.RiskScoreField
}

// RiskScoreScalingFactor is the graphql resolver function for the RiskScoreScalingFactor object on the EventConfiguration struct
func (a EventConfigurationResolver) RiskScoreScalingFactor() *float64 {
	return a.m.RiskScoreScalingFactor
}

// CalculationField is the graphql resolver function for the CalculationField object on the EventConfiguration struct
func (a EventConfigurationResolver) CalculationField() *string {
	return a.m.CalculationField
}

// CalculationBaseField is the graphql resolver function for the CalculationBaseField object on the EventConfiguration struct
func (a EventConfigurationResolver) CalculationBaseField() *string {
	return a.m.CalculationBaseField
}

// CalculationType is the graphql resolver function for the CalculationType object on the EventConfiguration struct
func (a EventConfigurationResolver) CalculationType() *string {
	return a.m.CalculationType
}

// PreviousEventComparisonField is the graphql resolver function for the PreviousEventComparisonField object on the EventConfiguration struct
func (a EventConfigurationResolver) PreviousEventComparisonField() *string {
	return a.m.PreviousEventComparisonField
}

// SuppressionTime is the graphql resolver function for the SuppressionTime object on the EventConfiguration struct
func (a *EventConfigurationResolver) SuppressionTime() *float64 {
	return a.m.SuppressionTime
}

// Email is the graphql resolver function for the Email object on the EventConfiguration struct
func (a EventConfigurationResolver) Email() *EmailConfigurationResolver {
	return &EmailConfigurationResolver{
		m: *a.m.Email,
	}
}

// EventTypeID is the graphql resolver function for the EventTypeID object on the EventTypeInfo struct
func (a EventTypeInfoResolver) EventTypeID(ctx context.Context) *graphql.ID {
	return a.m.EventTypeID
}

// EventTypeDisplayName is the graphql resolver function for the EventTypeDisplayName object on the EventTypeInfo struct
func (a EventTypeInfoResolver) EventTypeDisplayName() *string {
	return a.m.EventTypeDisplayName
}
