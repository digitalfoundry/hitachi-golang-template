package structs

import (
	"context"

	"github.com/arangodb/go-driver"
	"github.com/jinzhu/copier"
	"github.com/stretchr/testify/mock"
)

// MockArangoCursorEventConfiguration is the mock struct for the ArangoCursor interface
type MockArangoCursorEventConfiguration struct {
	mock.Mock
}

// ReadDocument is the mock function for the Arango go driver Cursor ReadDocument function
func (c *MockArangoCursorEventConfiguration) ReadDocument(ctx context.Context, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, result)
	if ac, ok := args[1].(*EventConfiguration); ok && ac != nil {
		copyErr := copier.Copy(result, ac)
		if copyErr != nil {
			return driver.DocumentMeta{}, copyErr
		}
	}

	return driver.DocumentMeta{}, args.Error(0)
}

// Close is the mock function for the Arango go driver Cursor Close function
func (c *MockArangoCursorEventConfiguration) Close() error {
	args := c.Called()
	return args.Error(0)
}

//MockArangoCursorTemplateClass is the mock struct for the ArangoCursor interface
type MockArangoCursorTemplateClass struct {
	mock.Mock
}

// ReadDocument is the mock function for the Arango go driver Cursor ReadDocument function
func (c *MockArangoCursorTemplateClass) ReadDocument(ctx context.Context, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, result)
	if ac, ok := args[1].(*TemplateClass); ok && ac != nil {
		copyErr := copier.Copy(result, ac)
		if copyErr != nil {
			return driver.DocumentMeta{}, copyErr
		}
	}

	return driver.DocumentMeta{}, args.Error(0)
}

// Close is the mock function for the Arango go driver Cursor Close function
func (c *MockArangoCursorTemplateClass) Close() error {
	args := c.Called()
	return args.Error(0)
}

// MockArangoCollectionEventConfiguration is the mock struct for the ArangoCollection interface
type MockArangoCollectionEventConfiguration struct {
	mock.Mock
}

// CreateDocument is the mock function for the Arango go driver Collection CreateDocument function
func (c *MockArangoCollectionEventConfiguration) CreateDocument(ctx context.Context, document interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, document)
	return driver.DocumentMeta{}, args.Error(0)
}

// ReadDocument is the mock function for the Arango go driver Collection ReadDocument function
func (c *MockArangoCollectionEventConfiguration) ReadDocument(ctx context.Context, key string, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, result)
	if ac, ok := args[1].(*EventConfiguration); ok && ac != nil {
		copyErr := copier.Copy(result, ac)
		if copyErr != nil {
			return driver.DocumentMeta{}, copyErr
		}
	}

	return driver.DocumentMeta{Key: key}, args.Error(0)
}

// UpdateDocument is the mock function for the Arango go driver Collection UpdateDocument function
func (c *MockArangoCollectionEventConfiguration) UpdateDocument(ctx context.Context, key string, update interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, key, update)
	return driver.DocumentMeta{}, args.Error(0)
}
