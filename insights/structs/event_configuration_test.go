package structs

import (
	"context"
	"testing"
	"time"

	"github.com/graph-gophers/graphql-go"
	"github.com/stretchr/testify/assert"
)

func NewMockEventConfiguration() EventConfiguration {
	tempFrom := "Person"
	id := graphql.ID("ID")
	name := "Name"
	displayName := "Display Name"
	eventNameFormat := "Event Name Format"
	eventTypeDisplayName := "Event Type Display Name"
	riskScoreField := "Risk Score Field"
	riskScoreScalingFactor := 0.0
	calculationField := "Calculation Field"
	calculationBaseField := "Calculation Base Field"
	calculationType := "Calculation Type"
	previousEventField := "Previous Event Comparison Field"
	lastUpdated := Date{time.Now()}

	return EventConfiguration{
		ID:              &id,
		Name:            &name,
		DisplayName:     &displayName,
		Description:     "Description",
		LastUpdated:     lastUpdated,
		AssetType:       "Asset Type",
		EventTypeID:     "Analytic Type",
		Severities:      []*SeverityThreshold{NewMockSeverityThreshold()},
		EventNameFormat: &eventNameFormat,
		EventDescriptionFormat: []string{
			"Event",
			"Description",
			"Format",
		},
		EventTypeDisplayName:         &eventTypeDisplayName,
		RiskScoreField:               &riskScoreField,
		RiskScoreScalingFactor:       &riskScoreScalingFactor,
		CalculationField:             &calculationField,
		CalculationBaseField:         &calculationBaseField,
		CalculationType:              &calculationType,
		PreviousEventComparisonField: &previousEventField,
		Email: &EmailConfiguration{
			Triggers: nil,
			Template: EmailObject{
				From:    &tempFrom,
				To:      []string{"Recipient"},
				Subject: "Subject",
				Message: "Message",
			}},
	}
}

func TestNewEventConfigurationResolver(t *testing.T) {
	mockAC := NewMockEventConfiguration()
	resolver := NewEventConfigurationResolver(&mockAC)
	assert.NotNil(t, resolver.m)
	assert.Equal(t, mockAC.ID, resolver.m.ID)
	assert.Equal(t, mockAC.Name, resolver.m.Name)
	assert.Equal(t, mockAC.DisplayName, resolver.m.DisplayName)
	assert.Equal(t, mockAC.Description, resolver.m.Description)
	assert.True(t, mockAC.LastUpdated.Equal(resolver.m.LastUpdated.Time))
	assert.Equal(t, mockAC.AssetType, resolver.m.AssetType)
	assert.Equal(t, mockAC.EventTypeID, resolver.m.EventTypeID)
	assert.Len(t, resolver.m.Severities, len(mockAC.Severities))
	for idx, severity := range resolver.m.Severities {
		assert.Equal(t, mockAC.Severities[idx].DisplayName, severity.DisplayName)
		assert.Equal(t, mockAC.Severities[idx].Threshold.Hysteresis, severity.Threshold.Hysteresis)
		assert.Equal(t, mockAC.Severities[idx].Threshold.MaxValue, severity.Threshold.MaxValue)
		assert.Equal(t, mockAC.Severities[idx].Threshold.MinValue, severity.Threshold.MinValue)
		assert.Equal(t, mockAC.Severities[idx].Severity, severity.Severity)
	}
	assert.Equal(t, mockAC.EventNameFormat, resolver.m.EventNameFormat)
	assert.Len(t, resolver.m.EventDescriptionFormat, len(mockAC.EventDescriptionFormat))
	for idx, format := range resolver.m.EventDescriptionFormat {
		assert.Equal(t, mockAC.EventDescriptionFormat[idx], format)
	}
	assert.Equal(t, mockAC.EventTypeDisplayName, resolver.m.EventTypeDisplayName)
	assert.Equal(t, mockAC.RiskScoreField, resolver.m.RiskScoreField)
	assert.Equal(t, mockAC.RiskScoreScalingFactor, resolver.m.RiskScoreScalingFactor)
	assert.Equal(t, mockAC.CalculationField, resolver.m.CalculationField)
	assert.Equal(t, mockAC.CalculationBaseField, resolver.m.CalculationBaseField)
	assert.Equal(t, mockAC.CalculationType, resolver.m.CalculationType)
	assert.Equal(t, mockAC.PreviousEventComparisonField, resolver.m.PreviousEventComparisonField)
	assert.Equal(t, mockAC.Email, resolver.m.Email)
}

func TestEventConfigurationResolver_EventTypeID(t *testing.T) {
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		EventTypeID: "Type 1",
	}}

	result := mockResolver.EventTypeID()
	assert.Equal(t, "Type 1", result)
}

func TestEventConfigurationResolver_AssetType(t *testing.T) {
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		AssetType: "Asset Type",
	}}

	result := mockResolver.AssetType()
	assert.Equal(t, "Asset Type", result)
}

func TestEventConfigurationResolver_CalculationBaseField(t *testing.T) {
	field := "Field"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		CalculationBaseField: &field,
	}}

	result := mockResolver.CalculationBaseField()
	assert.Equal(t, field, *result)
}

func TestEventConfigurationResolver_CalculationField(t *testing.T) {
	field := "Field"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		CalculationField: &field,
	}}

	result := mockResolver.CalculationField()
	assert.Equal(t, field, *result)
}

func TestEventConfigurationResolver_CalculationType(t *testing.T) {
	calcType := "Calc Type"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		CalculationType: &calcType,
	}}
	result := mockResolver.CalculationType()
	assert.Equal(t, calcType, *result)
}

func TestEventConfigurationResolver_Description(t *testing.T) {
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		Description: "Description",
	}}
	result := mockResolver.Description()
	assert.Equal(t, "Description", *result)
}

func TestEventConfigurationResolver_DisplayName(t *testing.T) {
	displayName := "DisplayName"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		DisplayName: &displayName,
	}}
	result := mockResolver.DisplayName()
	assert.Equal(t, displayName, *result)
}

func TestEventConfigurationResolver_Email(t *testing.T) {
	tempFrom := "Person"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		Email: &EmailConfiguration{
			Triggers: nil,
			Template: EmailObject{
				From: &tempFrom,
				To: []string{
					"Recipient 1",
					"Recipient 2",
				},
				Subject: "Subject",
				Message: "Message",
			},
		},
	}}

	result := mockResolver.Email()
	assert.Equal(t, "Person", *result.m.Template.From)
	assert.Equal(t, "Subject", result.m.Template.Subject)
	assert.Equal(t, "Message", result.m.Template.Message)
	assert.Len(t, result.m.Template.To, 2)
	assert.Equal(t, "Recipient 1", result.m.Template.To[0])
	assert.Equal(t, "Recipient 2", result.m.Template.To[1])
}

func TestEventConfigurationResolver_EventDescriptionFormat(t *testing.T) {
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		EventDescriptionFormat: []string{"Event", "Description", "Format"},
	}}

	result := mockResolver.EventDescriptionFormat()
	assert.Len(t, result, len(mockResolver.m.EventDescriptionFormat))
	assert.Equal(t, "Event", result[0])
	assert.Equal(t, "Description", result[1])
	assert.Equal(t, "Format", result[2])
}

func TestEventConfigurationResolver_EventNameFormat(t *testing.T) {
	eventNameFormat := "EventNameFormat"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		EventNameFormat: &eventNameFormat,
	}}
	result := mockResolver.EventNameFormat()
	assert.Equal(t, eventNameFormat, *result)
}

func TestEventConfigurationResolver_EventTypeDisplayName(t *testing.T) {
	eventTypeDisplayName := "EventTypeDisplayName"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		EventTypeDisplayName: &eventTypeDisplayName,
	}}
	result := mockResolver.EventTypeDisplayName()
	assert.Equal(t, eventTypeDisplayName, *result)
}

func TestEventConfigurationResolver_ID(t *testing.T) {
	id := graphql.ID("ID")
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		ID: &id,
	}}
	result := mockResolver.ID(context.Background())
	assert.Equal(t, id, *result)
}

func TestEventConfigurationResolver_LastUpdated(t *testing.T) {
	expectedTime := Date{time.Now()}
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		LastUpdated: expectedTime,
	}}
	result := mockResolver.LastUpdated()
	assert.True(t, result.Equal(expectedTime.Time))
}

func TestEventConfigurationResolver_Name(t *testing.T) {
	name := "Name"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		Name: &name,
	}}
	result := mockResolver.Name()
	assert.Equal(t, name, *result)
}

func TestEventConfigurationResolver_PreviousEventComparisonField(t *testing.T) {
	previousEventField := "PreviousEventComparisonField"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		PreviousEventComparisonField: &previousEventField,
	}}
	result := mockResolver.PreviousEventComparisonField()
	assert.Equal(t, previousEventField, *result)
}

func TestEventConfigurationResolver_RiskScoreField(t *testing.T) {
	riskScoreField := "RiskScoreField"
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		RiskScoreField: &riskScoreField,
	}}
	result := mockResolver.RiskScoreField()
	assert.Equal(t, riskScoreField, *result)
}

func TestEventConfigurationResolver_RiskScoreScalingFactor(t *testing.T) {
	riskScoreScalingFactor := 25.0
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		RiskScoreScalingFactor: &riskScoreScalingFactor,
	}}
	result := mockResolver.RiskScoreScalingFactor()
	assert.Equal(t, riskScoreScalingFactor, *result)
}

func TestEventConfigurationResolver_Severities(t *testing.T) {
	mockResolver := EventConfigurationResolver{m: EventConfiguration{
		Severities: []*SeverityThreshold{NewMockSeverityThreshold()},
	}}
	result := mockResolver.Severities()
	assert.Len(t, result, len(mockResolver.m.Severities))
	for idx, severity := range result {
		assert.Equal(t, mockResolver.m.Severities[idx].DisplayName, severity.m.DisplayName)
		assert.Equal(t, mockResolver.m.Severities[idx].Severity, severity.m.Severity)
		assert.Equal(t, mockResolver.m.Severities[idx].Threshold.MinValue, severity.m.Threshold.MinValue)
		assert.Equal(t, mockResolver.m.Severities[idx].Threshold.MaxValue, severity.m.Threshold.MaxValue)
		assert.Equal(t, mockResolver.m.Severities[idx].Threshold.Hysteresis, severity.m.Threshold.Hysteresis)
	}
}
