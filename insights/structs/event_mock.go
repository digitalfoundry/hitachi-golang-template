package structs

import (
	"context"

	"github.com/arangodb/go-driver"
	"github.com/jinzhu/copier"
	"github.com/stretchr/testify/mock"
)

// MockArangoCursorEvent is the mock for the event arango cursor
type MockArangoCursorEvent struct {
	mock.Mock
}

// ReadDocument is the mock function for the Arango go driver Cursor ReadDocument function
func (c *MockArangoCursorEvent) ReadDocument(ctx context.Context, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, result)
	if ac, ok := args[1].(*Event); ok && ac != nil {
		copyErr := copier.Copy(result, ac)
		if copyErr != nil {
			return driver.DocumentMeta{}, copyErr
		}
	}

	return driver.DocumentMeta{}, args.Error(0)
}

// Close is the mock function for the Arango go driver Cursor Close function
func (c *MockArangoCursorEvent) Close() error {
	args := c.Called()
	return args.Error(0)
}

// MockArangoCursorEventResult is the mock function for the Arango event result cursor
type MockArangoCursorEventResult struct {
	mock.Mock
}

// ReadDocument is the mock function for the Arango go driver Cursor ReadDocument function
func (c *MockArangoCursorEventResult) ReadDocument(ctx context.Context, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, result)
	if ac, ok := args[1].(*EventResult); ok && ac != nil {
		copyErr := copier.Copy(result, ac)
		if copyErr != nil {
			return driver.DocumentMeta{}, copyErr
		}
	}

	return driver.DocumentMeta{}, args.Error(0)
}

// Close is the mock function for the Arango go driver Cursor Close function
func (c *MockArangoCursorEventResult) Close() error {
	args := c.Called()
	return args.Error(0)
}

// MockArangoCollectionAssetEntity is the mock struct for the ArangoCollection interface
type MockArangoCollectionEvent struct {
	mock.Mock
}

// CreateDocument is the mock function for the Arango go driver Collection CreateDocument function
func (c *MockArangoCollectionEvent) CreateDocument(ctx context.Context, document interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, document)
	return driver.DocumentMeta{}, args.Error(0)
}

// ReadDocument is the mock function for the Arango go driver Collection ReadDocument function
func (c *MockArangoCollectionEvent) ReadDocument(ctx context.Context, key string, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, result)
	if ac, ok := args[1].(*Event); ok && ac != nil {
		copyErr := copier.Copy(result, ac)
		if copyErr != nil {
			return driver.DocumentMeta{}, copyErr
		}
	}

	return driver.DocumentMeta{Key: key}, args.Error(0)
}

// UpdateDocument is the mock function for the Arango go driver Collection UpdateDocument function
func (c *MockArangoCollectionEvent) UpdateDocument(ctx context.Context, key string, update interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, key, update)
	return driver.DocumentMeta{}, args.Error(0)
}

// Close is the mock function for the Arango go driver Cursor Close function
func (c *MockArangoCollectionEvent) Close() error {
	args := c.Called()
	return args.Error(0)
}
