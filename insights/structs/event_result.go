package structs

// EventResult is the struct for event result object
type EventResult struct {
	Edges      []EventEdge
	PageInfo   *PageInfo
	TotalCount int32
}

// EventResultResolver is the resolver for the event result struct
type EventResultResolver struct {
	m EventResult
}

// NewEventResultResolver returns a new event result resolver
func NewEventResultResolver(e *EventResult) *EventResultResolver {
	return &EventResultResolver{m: *e}
}

// Cursor is the graphql resolver function for the Cursor on the PageInfoResolver struct
func (a *EventResultResolver) Edges() []*EventEdgeResolver {
	resolvers := []*EventEdgeResolver{}
	for _, e := range a.m.Edges {
		resolvers = append(resolvers, &EventEdgeResolver{m: e})
	}
	return resolvers
}

// PageInfo returns the page info resolver for the event reuslt
func (a *EventResultResolver) PageInfo() *PageInfoResolver {
	return NewPageInfoResolver(a.m.PageInfo)
}

// TotalCount returns the event result total count
func (a *EventResultResolver) TotalCount() int32 {
	return a.m.TotalCount
}

// EventEdge represents nodes that are filtered and paginated together
type EventEdge struct {
	Cursor string
	Node   Event
}

// EventEdgeResolver wraps an EventEdge struct
type EventEdgeResolver struct {
	m EventEdge
}

// NewEventEdgeResolver returns a new EventEdgeResolver
func NewEventEdgeResolver(e *EventEdge) *EventEdgeResolver {
	return &EventEdgeResolver{m: *e}
}

// Cursor is the graphql resolver function for the Cursor on the EventEdgeResolver struct
func (a *EventEdgeResolver) Cursor() string {
	return a.m.Cursor
}

// Cursor is the graphql resolver function for the Cursor on the EventEdgeResolver struct
func (a *EventEdgeResolver) Node() *EventResolver {
	return NewEventResolver(&a.m.Node)
}

// PageInfo represents a page of events
type PageInfo struct {
	StartCursor *string
	EndCursor   *string
	HasNextPage bool
}

// PageInfoResolver provides access to properties of a PageInfo struct
type PageInfoResolver struct {
	m PageInfo
}

// NewPageInfoResolver gets a new PageInfoResolver
func NewPageInfoResolver(p *PageInfo) *PageInfoResolver {
	return &PageInfoResolver{m: *p}
}

// StartCursor is the graphql resolver function for the StartCursor on the PageInfoResolver struct
func (a *PageInfoResolver) StartCursor() *string {
	return a.m.StartCursor
}

// EndCursor is the graphql resolver function for the EndCursor on the PageInfoResolver struct
func (a *PageInfoResolver) EndCursor() *string {
	return a.m.EndCursor
}

// HasNextPage is the graphql resolver function for the HasNextPage on the PageInfoResolver struct
func (a *PageInfoResolver) HasNextPage() bool {
	return a.m.HasNextPage
}
