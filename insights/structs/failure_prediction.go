package structs

import (
	"context"
	"fmt"
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	"github.com/graph-gophers/graphql-go"
)

// FailurePrediction represents the failure prediction object
type FailurePrediction struct {
	KPICalculator
	AssetID  string
	SeamType SeamType
}

// NewFailurePrediction creates a new failure prediction
func NewFailurePrediction(configID string, assetID string, rawDb insights.InfluxDBer, metaDB insights.ArangoDBer, configService insights.Clienter, seamType SeamType) *FailurePrediction {
	return &FailurePrediction{
		KPICalculator: KPICalculator{
			ID:                  configID,
			RawDataDB:           rawDb,
			MetadataDB:          metaDB,
			ConfigServiceClient: configService,
		},
		AssetID:  assetID,
		SeamType: seamType,
	}
}

// GetID gets the failure prediction id
func (f *FailurePrediction) GetID() string {
	return f.ID
}

// GenerateEvents calculates raw influx data and evaluates the result against threshold data from event configuration to generate events
func (f *FailurePrediction) GenerateEvents(now time.Time) []Event {
	eventList := []Event{}
	eventTypeID := "Failure_Prediction"
	measurementEntities := f.Calculate(now)
	if len(measurementEntities) == 0 {
		insights.LogErrorWithInfo(fmt.Errorf("no data found; no measurement entities calculated"), "", "no events generated")
		return nil
	}
	// todo save measurement entities to measurementEntity influx
	configuration, getThresholdErr := f.getThresholdData(f.AssetID, eventTypeID)
	if getThresholdErr != nil {
		insights.LogErrorWithInfo(getThresholdErr, "", "Failed to get threshold data from config service.")
		return nil
	}

	if configuration == nil {
		fmt.Println("Failure Prediction - configuration not specified")
		return nil
	}

	status, err := NewStatusWrapper(Open)
	if err != nil {
		fmt.Println("Unknown status")
	}

	event := Event{
		EventType:   *configuration.EventTypeDisplayName,
		EventTypeID: eventTypeID,
		CreatedDate: now,
		LastUpdated: now,
		Status:      *status,
		Properties:  f.setEventProperties(measurementEntities),
	}

	evaluator := Evaluator{
		EventConfiguration: *configuration,
	}
	evaluator.Init()

	severity := evaluator.CalculateSeverity(event)
	severityWrapper, err := NewSeverityWrapper(severity)
	if err != nil {
		insights.LogErrorWithInfo(err, "", "could not get severity %s", severity)
		return nil
	}

	prevKPIEvent, err := f.getPreviousKPIEvent(f.AssetID, eventTypeID)
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error getting previous KPI event")
		return nil
	}

	if severityWrapper.IsNone() || evaluator.ShouldSuppress(f.AssetID, eventTypeID, severity, prevKPIEvent) {
		return eventList
	}

	description := f.getDescription(*configuration, event.Properties)
	event.Severity = *severityWrapper
	event.Description = &description
	event.Name = configuration.EventNameFormat
	event.DisplayName = configuration.EventNameFormat
	event.AssetID = graphql.ID(f.AssetID)

	fmt.Println("Failure Prediction - finished processing")

	saveErr := f.SaveKPIEvent(event)
	if saveErr != nil {
		insights.LogErrorWithInfo(saveErr, "", "error saving KPI event %s to arango", event.ID)
		return nil
	}

	eventList = append(eventList, event)
	return eventList
}

// Calculate is the failure prediction calculate function
func (f *FailurePrediction) Calculate(now time.Time) []*MeasurementEntity {
	metadata, err := f.getMetadataByID()
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error querying kpi metadata for failure prediction calculator")
		return nil
	}
	query := metadata.Query.Main.Template
	if query == "" {
		return []*MeasurementEntity{}
	}

	fpOutput, err := f.RawDataDB.Query(query, "KPI")
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error retrieving failure prediction raw data from influx")
		return nil
	}

	values, parseErr := parseResponseFirstValue(fpOutput)
	if parseErr != nil {
		insights.LogErrorWithInfo(err, "", "error parsing influx response for %s", f.ID)
		return nil
	}

	failureRisk, err := parseFloat(values[2])
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error converting failure prediction probability")
		return nil
	}
	failureRisk *= 100

	prediction := values[1]

	fields := map[string]interface{}{
		"value":      failureRisk,
		"prediction": prediction,
	}

	tags := map[string]string{
		"assetId": f.AssetID,
	}

	seamTypeProps := f.SeamType.Calculate(now)

	entity := MeasurementEntity{
		Measurement:    f.ID,
		Fields:         fields,
		Tags:           tags,
		AdditionalInfo: nil,
		Time:           now,
	}

	if len(seamTypeProps) > 0 {
		seam := seamTypeProps[0]
		seamInfo := map[string]interface{}{
			f.SeamType.ID: seam.Fields["json"],
		}
		entity.AdditionalInfo = seamInfo
	}

	returnList := []*MeasurementEntity{&entity}

	return returnList
}

// GetMeasurementsFromDatabase gets the failure prediction data from influx
func (f *FailurePrediction) GetMeasurementsFromDatabase(now time.Time) []*MeasurementEntity {
	return []*MeasurementEntity{}
}

// SaveKPIEvent saves the partial event payload to a local Arango database
func (f *FailurePrediction) SaveKPIEvent(event Event) error {
	col, colErr := f.MetadataDB.GetCollection("kpi-service", "KPI_Event")
	if colErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to get assetEntity collection from arango")
		return colErr
	}

	kpiEvent := &KPIEvent{
		AssetID:     event.AssetID,
		EventTypeID: event.EventTypeID,
		CreatedDate: event.CreatedDate,
	}

	meta, createErr := col.CreateDocument(context.Background(), kpiEvent)
	if createErr != nil {
		insights.LogErrorWithInfo(colErr, "", "Failed to create document in KPI_Event collection")
		return createErr
	}

	fmt.Printf("Document created with id (%s)\n", meta.Key)

	return nil
}
