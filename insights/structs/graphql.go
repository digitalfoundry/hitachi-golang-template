package structs

import (
	"encoding/json"
	"fmt"
	"time"
)

// GraphqlMap is the graphql map object used for representing the scalar type
type GraphqlMap struct {
	Map map[string]interface{}
}

// ImplementsGraphQLType method used to match object to graphql schema scalar name
func (GraphqlMap) ImplementsGraphQLType(name string) bool {
	return name == "Map"
}

// UnmarshalGraphQL is a custom unmarshaler for GraphqlMap into map[string]interface{}
func (g *GraphqlMap) UnmarshalGraphQL(input interface{}) error {
	switch input := input.(type) {
	case string:
		return json.Unmarshal([]byte(input), &g.Map)
	default:
		return fmt.Errorf("wrong type")
	}
}

// MarshalJSON is a custom marshaler for map[string]interface{} into use by graphqlmap
func (g GraphqlMap) MarshalJSON() ([]byte, error) {
	return json.Marshal(g.Map)
}

// Time is a custom GraphQL type to represent an instant in time. It corresponds to Date scalar in the graphql schema
type Date struct {
	time.Time
}

// ImplementsGraphQLType method used to match object to graphql schema scalar name
func (Date) ImplementsGraphQLType(name string) bool {
	return name == "Date"
}

// UnmarshalGraphQL is a custom unmarshaler for Date into time.Time
func (t *Date) UnmarshalGraphQL(input interface{}) error {
	switch input := input.(type) {
	case time.Time:
		t.Time = input
		return nil
	case string:
		var err error
		t.Time, err = time.Parse(time.RFC3339, input)
		return err
	case int:
		t.Time = time.Unix(int64(input), 0)
		return nil
	case float64:
		t.Time = time.Unix(int64(input), 0)
		return nil
	default:
		return fmt.Errorf("wrong type")
	}
}

// MarshalJSON is a custom marshaler for date into Time
func (t Date) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.Time)
}
