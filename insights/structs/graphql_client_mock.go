package structs

import (
	"context"
	"fmt"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"

	"github.com/jinzhu/copier"

	"github.com/stretchr/testify/mock"
)

// NewMockClienter returns a new mock graphql client
func NewMockClienter() *MockClient {
	return &MockClient{}
}

// MockClient is the mock graphql client struct
type MockClient struct {
	mock.Mock
}

// Run is the function for the graphql mock client
func (c *MockClient) Run(ctx context.Context, req *insights.Request, resp interface{}) error {
	args := c.Called(ctx, req, resp)
	if ae, ok := args[1].(*GetAssetsByNameLikeResponse); ok && ae != nil {
		copyErr := copier.Copy(resp, ae)
		if copyErr != nil {
			return copyErr
		}
	}
	if ae, ok := args[1].(*GetAssetByIDResponse); ok && ae != nil {
		copyErr := copier.Copy(resp, ae)
		if copyErr != nil {
			return copyErr
		}
	}
	if ae, ok := args[1].(*GetByAssetTypeAndEventTypeIDResponse); ok && ae != nil {
		copyErr := copier.Copy(resp, ae)
		if copyErr != nil {
			return copyErr
		}
	}

	return args.Error(0)
}

// NewMockRequester returns a new mock grpahql requester
func NewMockRequester() *MockRequester {
	return &MockRequester{}
}

// MockRequester is the mock graphql requester
type MockRequester struct {
	mock.Mock
}

// Var is the function for the graphql requester
func (r *MockRequester) Var(key string, value interface{}) {
	fmt.Println("mocked correctly?")
}
