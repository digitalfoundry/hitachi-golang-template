package structs

// GetAssetsByNameLikeResponse is the response struct for the getAssetsByNameLikeResponse query
type GetAssetsByNameLikeResponse struct {
	GetAssetsByName *[]AssetEntity `json:"getAssetsByNameLike"`
}

// GetAssetByIDResponse is the response struct for the getAssetByIDResponse query
type GetAssetByIDResponse struct {
	GetAssetByID AssetEntity `json:"getAssetById"`
}

// GetByAssetTypeAndEventTypeIDResponse is the response struct for the getByAssetTypeAndEventTypeIDResponse query
type GetByAssetTypeAndEventTypeIDResponse struct {
	GetByAssetTypeAndEventTypeID EventConfiguration `json:"getByAssetTypeAndEventTypeID"`
}
