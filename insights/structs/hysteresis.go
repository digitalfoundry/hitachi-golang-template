package structs

import (
	"bytes"
	"encoding/json"
	"math"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
)

// Hysteresis is the phenomenon in which the value of a physical property lags behind changes in the effect causing it, as for instance when magnetic induction lags behind the magnetizing force!
type Hysteresis struct {
	IsSet            bool
	ThresholdToSet   float64
	ThresholdToReset float64
	PreviousObj      []byte
	PreviousValue    float64
	CalculationType  string
}

// Update updates the hysteresis struct
func (h *Hysteresis) Update(thresholdToSet float64, thresholdToReset float64, calculationType string) {
	h.ThresholdToSet = thresholdToSet
	h.ThresholdToReset = thresholdToReset
	h.CalculationType = calculationType
}

//nolint:gocyglo
// ShouldSuppress calculates if hysteresis should be suppressed
func (h *Hysteresis) ShouldSuppress(value float64, obj interface{}) bool {
	objString, jsonErr := json.Marshal(obj)
	if jsonErr != nil {
		insights.LogErrorWithInfo(jsonErr, "", "Failed to marshall obj for shouldSuppress")
		return false
	}

	if !bytes.Equal(objString, h.PreviousObj) {
		h.PreviousObj = objString
		h.IsSet = false
	}

	if (value < 0 && h.PreviousValue > 0) || (value > 0 && h.PreviousValue < 0) {
		h.IsSet = false
	}

	h.PreviousValue = value
	switch h.CalculationType {
	case "ABSOLUTE_PERCENTAGE_DEVIATION":
	case "ABSOLUTE_DEVIATION":
		value = math.Abs(value)
	default:
	}

	if h.IsSet {
		if value <= h.ThresholdToReset {
			h.IsSet = false
		}
		return true
	} else {
		if value >= h.ThresholdToSet {
			h.IsSet = true
			return false
		}
		return true
	}
}
