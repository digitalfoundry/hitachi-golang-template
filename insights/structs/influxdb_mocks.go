package structs

import (
	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/stretchr/testify/mock"
)

// NewMockInfluxDBer returns a mock version of the InfluxDBer interface for usage in tests with the testify package
func NewMockInfluxDBer() *MockInfluxDBer {
	return &MockInfluxDBer{}
}

// MockInfluxDBer is the mock influxdb struct
type MockInfluxDBer struct {
	mock.Mock
}

// Connect is used to connect to a remote influx instance given an address, username, and password
func (i *MockInfluxDBer) Connect(addr string, username string, password string) error {
	return nil
}

// Query given a influxdb query it executes the query and returns a response
func (i *MockInfluxDBer) Query(query string, db string, parameters ...insights.Binder) (*client.Response, error) {
	args := i.Called(query)
	response := args[0].(*client.Response)
	return response, args.Error(1)
}

// Close closes the influx connection
func (i *MockInfluxDBer) Close() error {
	return nil
}
