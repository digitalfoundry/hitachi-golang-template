package structs

import (
	"github.com/graph-gophers/graphql-go"
)

// MetricEntity represents a unit value data pairing
type MetricEntity struct {
	Value float64
	Unit  *string
}

// MetricEntityResolver is the graphql resolver for the MetricEntity struct
type MetricEntityResolver struct {
	m *MetricEntity
}

// Value is the graphql resolver for the Value field of the MetricEntity Struct
func (r *MetricEntityResolver) Value() float64 {
	return r.m.Value
}

// Unit is the graphql resolver for the Unit field of the MetricEntity Struct
func (r *MetricEntityResolver) Unit() *string {
	return r.m.Unit
}

// NewKPIResolver returns a pointer to a new kpi resolver
func NewKPIResolver(kpi *KPI) *KPIResolver {
	return &KPIResolver{m: kpi}
}

// KPI is the struct that represents a systems key performance indicators
type KPI struct {
	ID                      graphql.ID
	Name                    *string
	DisplayName             *string
	Description             *string
	LastUpdated             Date // TODO this should be a pointer but mocking fails when passing this struct as an args so ill handle the nil case in the resolver below
	Indicator               *MetricEntity
	ComparisonIndicatorInfo *string
	ComparisonIndicator     *MetricEntity
}

// KPI resolver is the graphql resolver for the KPI struct
type KPIResolver struct {
	m *KPI
}

// ID is the graphql resolver function for the ID field of the KPI struct
func (r *KPIResolver) ID() *graphql.ID {
	return &r.m.ID
}

// Name is the graphql resolver function for the Name field of the KPI struct
func (r *KPIResolver) Name() *string {
	return r.m.Name
}

// DisplayName is the graphql resolver function for the DisplayName field of the KPI struct
func (r *KPIResolver) DisplayName() *string {
	return r.m.DisplayName
}

// Description is the graphql resolver function for the Description field of the KPI struct
func (r *KPIResolver) Description() *string {
	return r.m.Description
}

// LastUpdated is the graphql resolver function for the LastUpdated field of the KPI struct
func (r *KPIResolver) LastUpdated() *Date {
	return &r.m.LastUpdated
}

// Indicator is the graphql resolver function for the Indicator field of the KPI struct
func (r *KPIResolver) Indicator() *MetricEntityResolver {
	if r.m.Indicator != nil {
		return &MetricEntityResolver{m: r.m.Indicator}
	}

	return nil
}

// ComparisonIndicatorInfo is the graphql resolver function for the ComparisonIndicatorInfo field of the KPI struct
func (r *KPIResolver) ComparisonIndicatorInfo() *string {
	return r.m.ComparisonIndicatorInfo
}

// ComparisonIndicator is the graphql resolver function for the ComparisonIndicator field of the KPI struct
func (r *KPIResolver) ComparisonIndicator() *MetricEntityResolver {
	if r.m.ComparisonIndicator != nil {
		return &MetricEntityResolver{m: r.m.ComparisonIndicator}
	}

	return nil
}
