package structs

import (
	"context"
	"fmt"
	"time"

	"github.com/arangodb/go-driver"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
)

// KPICalculatorInterface is the interface for the KPI calculators
type KPICalculatorInterface interface {
	GetID() string
	GenerateEvents(now time.Time) []Event
	Calculate(now time.Time) []*MeasurementEntity
	GetMeasurementsFromDatabase(now time.Time) []*MeasurementEntity
}

// KPICalculator is the generic calculator with connections to the raw influx data and arango metadata
type KPICalculator struct {
	ID                  string
	RawDataDB           insights.InfluxDBer
	MetadataDB          insights.ArangoDBer
	ConfigServiceClient insights.Clienter
}

// getMetadataByID return the metadata for a kpi calculator from arango
func (k *KPICalculator) getMetadataByID() (*KPIMetadata, error) {
	col, err := k.MetadataDB.GetCollection("kpi-service", "KPI_Metadata")
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Failed to get KPI_Metadata collection")
		return nil, err
	}

	kpiMetadata := KPIMetadata{}
	_, err = col.ReadDocument(context.Background(), k.ID, &kpiMetadata)
	if err != nil {
		insights.LogErrorWithInfo(err, "", fmt.Sprintf("Failed to read KPI_Metadata collection for %s", k.ID))
		return nil, err
	}

	return &kpiMetadata, nil
}

func (k *KPICalculator) getPreviousKPIEvent(assetID string, eventTypeID string) (*KPIEvent, error) {
	query := insights.ArangoDBQuery{Query: "FOR e IN KPI_Event FILTER e.assetId == @assetID AND e.eventTypeId == @eventTypeID SORT e.CreatedDate DESC LIMIT 1 RETURN e "}
	assetIDParam := insights.Binder{
		Name:  "assetID",
		Value: assetID,
	}

	eventTypeIDParam := insights.Binder{
		Name:  "eventTypeID",
		Value: eventTypeID,
	}

	kpiEvent := KPIEvent{}
	cursor, queryErr := k.MetadataDB.Query("kpi-service", query, assetIDParam, eventTypeIDParam)
	if queryErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to query kpi-service on arango")
		return nil, queryErr
	}
	defer insights.DeferHandleError(cursor.Close)

	_, readErr := cursor.ReadDocument(context.Background(), &kpiEvent)
	if driver.IsNoMoreDocuments(readErr) {
		return &kpiEvent, nil
	} else if readErr != nil {
		insights.LogErrorWithInfo(queryErr, "", "Failed to read document on arango")
		return nil, readErr
	}

	return &kpiEvent, nil
}

func (k *KPICalculator) getThresholdData(assetTemplateID string, eventTypeID string) (*EventConfiguration, error) {
	graphqlRequester := insights.NewRequester(`
			query ($assetType: String!, $eventTypeId: String!) {
				getByAssetTypeAndEventTypeId(assetType: $assetType, eventTypeId: $eventTypeId) {
					assetType, eventTypeId, eventTypeDisplayName,
					riskScoreField, riskScoreScalingFactor, calculationField, calculationBaseField, calculationType,
					previousEventComparisonField, lastUpdated, suppressionTime,
	       			severities {
						severity,
						displayName,
						threshold {
							minValue,
							maxValue,
							hysteresis
						}
					},
					email {
						enabled,
						triggers,
						template {
							from,
							to,
							subject,
							message
						}
					}
				}
			}
		`)

	graphqlRequester.Var("assetType", assetTemplateID) // assetType maps to assetEntity.Template in the java code
	graphqlRequester.Var("eventTypeId", eventTypeID)

	getByAssetTypeAndEventTypeIDResponse := GetByAssetTypeAndEventTypeIDResponse{}

	err := k.ConfigServiceClient.Run(context.Background(), graphqlRequester, &getByAssetTypeAndEventTypeIDResponse)
	if err != nil {
		return nil, err
	}

	return &getByAssetTypeAndEventTypeIDResponse.GetByAssetTypeAndEventTypeID, nil
}

func (k *KPICalculator) setEventProperties(entities []*MeasurementEntity) map[string]interface{} {
	properties := map[string]interface{}{}

	properties["kpiType"] = k.ID
	for _, entity := range entities {
		for field, value := range entity.Fields {
			if field == "value" {
				properties[entity.Measurement] = value
				continue
			}

			properties[field] = value
		}

		for field, value := range entity.Tags {
			properties[field] = value
		}

		for field, value := range entity.AdditionalInfo {
			properties[field] = value
		}
	}

	return properties
}

func (k *KPICalculator) getDescription(configuration EventConfiguration, properties map[string]interface{}) string {
	descriptions := configuration.EventDescriptionFormat

	if len(descriptions) > 0 {
		//TODO: Add regex replace for descriptions from TemplateFormatter in common
		return descriptions[0]
	}

	return ""
}
