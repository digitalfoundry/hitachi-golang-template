package structs

import (
	"context"
	"github.com/arangodb/go-driver"
	"github.com/jinzhu/copier"
	"github.com/stretchr/testify/mock"
)

// MockArangoCollectionAssetEntity is the mock struct for the ArangoCollection interface
type MockArangoCollectionKPICalculator struct {
	mock.Mock
}

// CreateDocument is the mock function for the Arango go driver Collection CreateDocument function
func (c *MockArangoCollectionKPICalculator) CreateDocument(ctx context.Context, document interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, document)
	return driver.DocumentMeta{}, args.Error(0)
}

// ReadDocument is the mock function for the Arango go driver Collection ReadDocument function
func (c *MockArangoCollectionKPICalculator) ReadDocument(ctx context.Context, key string, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, result)
	if ac, ok := args[1].(*KPIMetadata); ok && ac != nil {
		copyErr := copier.Copy(result, ac)
		if copyErr != nil {
			return driver.DocumentMeta{}, copyErr
		}
	}

	return driver.DocumentMeta{Key: key}, args.Error(0)
}

// UpdateDocument is the mock function for the Arango go driver Collection UpdateDocument function
func (c *MockArangoCollectionKPICalculator) UpdateDocument(ctx context.Context, key string, update interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, key, update)
	return driver.DocumentMeta{}, args.Error(0)
}
