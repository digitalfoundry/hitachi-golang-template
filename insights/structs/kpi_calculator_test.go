package structs

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/arangodb/go-driver"
	"github.com/stretchr/testify/mock"
)

func Test_getMetadataByID(t *testing.T) {
	metadata := &KPIMetadata{
		ID:          "ID",
		Name:        "Name",
		DisplayName: "DisplayName",
		Description: "Description",
		Unit:        "Unit",
		ComparisonIndicatorInfoTemplate: ComparisonIndicatorInfoTemplate{},
		Query: CronQueries{},
	}

	mockDbSuccess := NewMockArangoDBer()
	mockCollectionSuccess := &MockArangoCollectionKPICalculator{}
	mockDbSuccess.On("GetCollection", mock.Anything, mock.Anything).Return(mockCollectionSuccess, nil).Once()
	mockCollectionSuccess.On("ReadDocument", mock.Anything, mock.Anything, mock.Anything).Return(nil, metadata).Once()

	mockDbNotFound := NewMockArangoDBer()
	mockCollectionNotFound := &MockArangoCollectionKPICalculator{}
	mockDbNotFound.On("GetCollection", mock.Anything, mock.Anything).Return(mockCollectionNotFound, nil).Once()
	mockCollectionNotFound.On("ReadDocument", mock.Anything, mock.Anything).Return(driver.NoMoreDocumentsError{}, nil).Once()

	mockDbCollectionFail := NewMockArangoDBer()
	mockCollectionGetCollectionFail := &MockArangoCollectionKPICalculator{}
	mockDbCollectionFail.On("GetCollection", mock.Anything, mock.Anything).Return(mockCollectionGetCollectionFail, fmt.Errorf("get collection error"))

	cases := map[string]struct {
		db                 *MockArangoDB
		collection         *MockArangoCollectionKPICalculator
		errMsg             string
		expectedResult     *KPIMetadata
		id                 string
		getCollectionCount int
		readDocCount       int
	}{
		"success": {
			db:                 mockDbSuccess,
			collection:         mockCollectionSuccess,
			errMsg:             "",
			expectedResult:     metadata,
			id:                 "ID",
			getCollectionCount: 1,
			readDocCount:       1,
		},
		"get_collection_fail": {
			db:                 mockDbCollectionFail,
			collection:         mockCollectionGetCollectionFail,
			errMsg:             "get collection error",
			expectedResult:     nil,
			id:                 "ID",
			getCollectionCount: 1,
			readDocCount:       0,
		},
		"not_found": {
			db:                 mockDbNotFound,
			collection:         mockCollectionNotFound,
			errMsg:             "no more documents",
			expectedResult:     nil,
			id:                 "No",
			getCollectionCount: 1,
			readDocCount:       1,
		},
	}

	for tc, c := range cases {
		t.Run(tc, func(t *testing.T) {
			calculator := &KPICalculator{
				ID:         c.id,
				MetadataDB: c.db,
			}
			meta, err := calculator.getMetadataByID()
			c.db.AssertNumberOfCalls(t, "GetCollection", c.getCollectionCount)
			c.collection.AssertNumberOfCalls(t, "ReadDocument", c.readDocCount)

			if err != nil {
				assert.Equal(t, c.errMsg, err.Error())
				assert.Nil(t, meta)
			} else {
				assert.Equal(t, c.errMsg, "")
				assert.NotNil(t, meta)
				assert.Equal(t, c.expectedResult.ID, meta.ID)
			}
		})
	}
}
