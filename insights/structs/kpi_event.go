package structs

import (
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
	graphql "github.com/graph-gophers/graphql-go"
)

// KPIEvent is the partial event object used for only cron service
type KPIEvent struct {
	AssetID     graphql.ID `json:"assetId"`
	EventTypeID string     `json:"eventTypeId"`
	Severity    int        `json:"severity"`
	CreatedDate time.Time  `json:"createdDate"`
}

// NewKPIEvent generates a new KPIEvent struct
func NewKPIEvent(event Event) KPIEvent {
	severity, err := event.Severity.GetValue()
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Unable to get severity from SeverityWrapper")
	}

	severityInt, err := severity.GetValue()
	if err != nil {
		insights.LogErrorWithInfo(err, "", "Unable to get severity from SeverityWrapper")
	}

	return KPIEvent{
		AssetID:     event.AssetID,
		EventTypeID: event.EventTypeID,
		Severity:    severityInt,
		CreatedDate: time.Time{},
	}
}
