package structs

// KPIMetadata is the struct representing the metadata for each KPI calculator from arango
type KPIMetadata struct {
	ID                              string                          `json:"id,omitempty"`
	Name                            string                          `json:"name,omitempty"`
	DisplayName                     string                          `json:"displayName,omitempty"`
	Description                     string                          `json:"description,omitempty"`
	Unit                            string                          `json:"unit,omitempty"`
	ComparisonIndicatorInfoTemplate ComparisonIndicatorInfoTemplate `json:"comparisonIndicatorInfoTemplate,omitempty"`
	Query                           CronQueries                     `json:"query,omitempty"`
	Properties                      Properties                      `json:"properties,omitempty"`
}

// CronQuery represents the cron service queries object
type CronQuery struct {
	Collection string `json:"collection,omitempty"`
	Template   string `json:"template,omitempty"`
}

// Properties is the properties object in the KPI metadata object
type Properties struct {
	ShiftTimeSpanInHours int `json:"shiftTimeSpanInHours"`
}

// ComparisonIndicatorInfoTemplate is a struct under KPI metadata
type ComparisonIndicatorInfoTemplate struct {
}

//nolint
// CronQueries holds the possible CronQuery objects of all KPI calculator metadata
type CronQueries struct {
	Value                          CronQuery `json:"value,omitempty"`
	Target                         CronQuery `json:"target,omitempty"`
	TargetValue                    CronQuery `json:"target_value,omitempty"`
	Diff                           CronQuery `json:"diff,omitempty"`
	Seam1                          CronQuery `json:"seam1,omitempty"`
	Seam2                          CronQuery `json:"seam2,omitempty"`
	Seam3                          CronQuery `json:"seam3,omitempty"`
	Main                           CronQuery `json:"main,omitempty"`
	RakeTorque                     CronQuery `json:"th701_rake-torque,omitempty"`
	BedMass                        CronQuery `json:"th701_bed-mass,omitempty"`
	SettlingTime                   CronQuery `json:"th701_settling-time,omitempty"`
	CHPP_curInputFeed1             CronQuery `json:"CHPP_curInputFeed1"`
	CHPP_curInputFeed2             CronQuery `json:"CHPP_curInputFeed2"`
	Module1_curSpecificGravity     CronQuery `json:"Module1_curSpecificGravity,omitempty"`
	SecondStage_curSpecificGravity CronQuery `json:"SecondStage_curSpecificGravity,omitempty"`
	Module1_curHeadPressure        CronQuery `json:"Module1_curHeadPerssure,omitempty"`
	SecondStage_curHeadPressure    CronQuery `json:"SecondStage_curHeadPerssure,omitempty"`
	CHPP_primaryOutput             CronQuery `json:"CHPP_primaryOutput,omitempty"`
	CHPP_secondaryOutput           CronQuery `json:"CHPP_secondaryOutput,omitempty"`
}

// AllSeamCronQueries returns the seam type cron queries
func (k *KPIMetadata) AllSeamCronQueries() []CronQuery {
	return []CronQuery{k.Query.Seam1, k.Query.Seam2, k.Query.Seam3}
}

// GetAllBestPracticeQueries returns the best practice cron queries
func (k *KPIMetadata) GetAllBestPracticeQueries() []CronQuery {
	return []CronQuery{}
}

// GetSetPointsQueries returns the set point cron queries
func (k *KPIMetadata) GetSetPointsQueries() (CronQuery, CronQuery) {
	return k.Query.Value, k.Query.Diff
}
