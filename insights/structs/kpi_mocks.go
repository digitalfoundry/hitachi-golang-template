package structs

import (
	"context"

	"github.com/arangodb/go-driver"
	"github.com/jinzhu/copier"
	"github.com/stretchr/testify/mock"
)

//MockArangoCursor is the mock struct for the ArangoCursor interface
type MockArangoCursorKPI struct {
	mock.Mock
}

// ReadDocument is the mock function for the Arango go driver Cursor ReadDocument function
func (c *MockArangoCursorKPI) ReadDocument(ctx context.Context, result interface{}) (driver.DocumentMeta, error) {
	args := c.Called(ctx, result)
	if kpi, ok := args[1].(*KPI); ok && kpi != nil {
		copyErr := copier.Copy(result, kpi)
		if copyErr != nil {
			return driver.DocumentMeta{}, copyErr
		}
	}

	return driver.DocumentMeta{}, args.Error(0)
}

// Close is the mock function for the Arango go driver Cursor Close function
func (c *MockArangoCursorKPI) Close() error {
	args := c.Called()
	return args.Error(0)
}
