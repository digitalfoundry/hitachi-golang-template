package structs

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func NewMockMetricEntity() *MetricEntity {
	tempUnit := "Unit"
	return &MetricEntity{
		Value: 42,
		Unit:  &tempUnit,
	}
}

func NewMockKPI() *KPI {
	tempName := "Name"
	tempDisplayName := "DisplayName"
	tempDescription := "Description"
	tempComparisonIndicator := "ComparisonIndicator"
	return &KPI{
		ID:                      "ID",
		Name:                    &tempName,
		DisplayName:             &tempDisplayName,
		Description:             &tempDescription,
		LastUpdated:             Date{time.Now()},
		Indicator:               NewMockMetricEntity(),
		ComparisonIndicatorInfo: &tempComparisonIndicator,
		ComparisonIndicator:     NewMockMetricEntity(),
	}
}

func TestKPIResolver_ComparisonIndicator(t *testing.T) {
	mKPI := NewMockKPI()
	resolver := KPIResolver{m: mKPI}
	assert.Equal(t, mKPI.ComparisonIndicator.Value, resolver.ComparisonIndicator().Value())
	assert.Equal(t, mKPI.ComparisonIndicator.Unit, resolver.ComparisonIndicator().Unit())
}

func TestKPIResolver_ComparisonIndicatorInfo(t *testing.T) {
	mKPI := NewMockKPI()
	resolver := KPIResolver{m: mKPI}
	assert.Equal(t, mKPI.ComparisonIndicatorInfo, resolver.ComparisonIndicatorInfo())
}

func TestKPIResolver_Description(t *testing.T) {
	mKPI := NewMockKPI()
	resolver := KPIResolver{m: mKPI}
	assert.Equal(t, mKPI.Description, resolver.Description())
}
func TestKPIResolver_DisplayName(t *testing.T) {
	mKPI := NewMockKPI()
	resolver := KPIResolver{m: mKPI}
	assert.Equal(t, mKPI.DisplayName, resolver.DisplayName())
}
func TestKPIResolver_ID(t *testing.T) {
	mKPI := NewMockKPI()
	resolver := KPIResolver{m: mKPI}
	assert.Equal(t, mKPI.ID, *resolver.ID())
}
func TestKPIResolver_Indicator(t *testing.T) {
	mKPI := NewMockKPI()
	resolver := KPIResolver{m: mKPI}
	assert.Equal(t, mKPI.Indicator, resolver.Indicator().m)
}
func TestKPIResolver_LastUpdated(t *testing.T) {
	mKPI := NewMockKPI()
	resolver := KPIResolver{m: mKPI}
	assert.Equal(t, mKPI.LastUpdated.Time, resolver.LastUpdated().Time)
}
func TestKPIResolver_Name(t *testing.T) {
	mKPI := NewMockKPI()
	resolver := KPIResolver{m: mKPI}
	assert.Equal(t, mKPI.Name, resolver.Name())
}
func TestNewKPIResolver(t *testing.T) {
	mKPI := NewMockKPI()
	resolver := NewKPIResolver(mKPI)

	assert.Equal(t, mKPI, resolver.m)
}
func TestMetricEntityResolver_Unit(t *testing.T) {
	unit := "pounds per septic baby koalas"
	metricEntity := &MetricEntity{
		Value: 4.44,
		Unit: &unit,
	}

	resolver := MetricEntityResolver{m: metricEntity}
	assert.Equal(t, metricEntity.Value, resolver.Value())
}
func TestMetricEntityResolver_Value(t *testing.T) {
	unit := "smoot per square friedmans"
	metricEntity := &MetricEntity{
		Value: 4.44,
		Unit: &unit,
	}

	resolver := MetricEntityResolver{m: metricEntity}
	assert.Equal(t, metricEntity.Unit, resolver.Unit())
}
