package structs

import (
	"context"

	"github.com/graph-gophers/graphql-go"
)

// NewLocationEntityResolver returns a pointer to a resolver for the location entity provided
func NewLocationEntityResolver(le *LocationEntity) *LocationEntityResolver {
	return &LocationEntityResolver{m: *le}
}

// LocationEntity struct represents the LocationEntity object
type LocationEntity struct {
	Description string     `json:"description,omitempty"`
	DisplayName string     `json:"displayName,omitempty"`
	ID          graphql.ID `json:"id,omitempty"`
	LastUpdated *Date      `json:"lastUpdated,omitempty"`
	Latitude    float64    `json:"latitude"`
	Longitude   float64    `json:"longitude"`
	Name        string     `json:"name"`
}

// LocationEntityInput is the input for mutating a LocationEntity
type LocationEntityInput struct {
	Description string
	DisplayName string
	LastUpdated Date
	Latitude    float64
	Longitude   float64
	Name        string
}

// LocationEntityResolver is the graphql resolver struct for the LocationEntity model
type LocationEntityResolver struct {
	m LocationEntity
}

// Description is the graphql resolver function for the description object on the EventConfiguration struct
func (le *LocationEntityResolver) Description() *string {
	return &le.m.Description
}

// DisplayName is the graphql resolver function for the display name object on the EventConfiguration struct
func (le *LocationEntityResolver) DisplayName() *string {
	return &le.m.DisplayName
}

// ID is the graphql resolver function for the id object on the EventConfiguration struct
func (le *LocationEntityResolver) ID(ctx context.Context) *graphql.ID {
	return &le.m.ID
}

// LastUpdated is the graphql resolver function for the last updated object on the EventConfiguration struct
func (le *LocationEntityResolver) LastUpdated() *Date {
	return le.m.LastUpdated
}

// Latitude is the graphql resolver function for the latitude object on the EventConfiguration struct
func (le *LocationEntityResolver) Latitude() *float64 {
	return &le.m.Latitude
}

// Longitude is the graphql resolver function for the longitude object on the EventConfiguration struct
func (le *LocationEntityResolver) Longitude() *float64 {
	return &le.m.Longitude
}

// Name is the graphql resolver function for the name object on the EventConfiguration struct
func (le *LocationEntityResolver) Name() *string {
	return &le.m.Name
}
