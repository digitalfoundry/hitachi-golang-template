package structs

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func newMockLocationEntity() *LocationEntity {
	t, err := time.Parse(time.RFC3339, "2019-08-27T13:22:53.108Z")
	if err != nil {
		return nil
	}

	return &LocationEntity{
		Description: "desc",
		DisplayName: "disp",
		ID:          "id-1",
		LastUpdated: &Date{Time: t},
		Latitude:    4.4,
		Longitude:   3.3,
		Name:        "uggs",
	}
}

func TestNewLocationEntityResolver(t *testing.T) {
	le := newMockLocationEntity()
	resolver := NewLocationEntityResolver(le)
	assert.NotNil(t, resolver)
}

func TestLocationEntityResolver_Description(t *testing.T) {
	le := newMockLocationEntity()
	resolver := NewLocationEntityResolver(le)
	assert.Equal(t, le.Description, *resolver.Description())
}

func TestLocationEntityResolver_DisplayName(t *testing.T) {
	le := newMockLocationEntity()
	resolver := NewLocationEntityResolver(le)
	assert.Equal(t, le.DisplayName, *resolver.DisplayName())
}

func TestLocationEntityResolver_ID(t *testing.T) {
	le := newMockLocationEntity()
	resolver := NewLocationEntityResolver(le)
	assert.Equal(t, le.ID, *resolver.ID(context.Background()))
}

func TestLocationEntityResolver_LastUpdated(t *testing.T) {
	le := newMockLocationEntity()
	resolver := NewLocationEntityResolver(le)
	assert.Equal(t, le.LastUpdated.Time, resolver.LastUpdated().Time)
}

func TestLocationEntityResolver_Latitude(t *testing.T) {
	le := newMockLocationEntity()
	resolver := NewLocationEntityResolver(le)
	assert.Equal(t, le.Latitude, *resolver.Latitude())
}

func TestLocationEntityResolver_Longitude(t *testing.T) {
	le := newMockLocationEntity()
	resolver := NewLocationEntityResolver(le)
	assert.Equal(t, le.Longitude, *resolver.Longitude())
}

func TestLocationEntityResolver_Name(t *testing.T) {
	le := newMockLocationEntity()
	resolver := NewLocationEntityResolver(le)
	assert.Equal(t, le.Name, *resolver.Name())
}
