package structs

import "time"

// MeasurementEntity is the struct for the processed influx db entry
type MeasurementEntity struct {
	Measurement    string
	Fields         map[string]interface{}
	Tags           map[string]string
	AdditionalInfo map[string]interface{}
	Time           time.Time
}
