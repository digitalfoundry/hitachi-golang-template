package structs

import (
	"context"

	"github.com/graph-gophers/graphql-go"
)

// Obj template object for graphql model
type Obj struct {
	ID          graphql.ID
	Name        string
	Description string
	Status      Status
}

// ObjResolver example graphql resolver for the Obj struct
type ObjResolver struct {
	M Obj
}

// ID example graphql function for the id object of Obj
func (o *ObjResolver) ID(ctx context.Context) *graphql.ID {
	return &o.M.ID
}

// Name example graphql function for the Name object of Obj
func (o *ObjResolver) Name(ctx context.Context) *string {
	return &o.M.Name
}

// Description example graphql function for the Description object of Obj
func (o *ObjResolver) Description(ctx context.Context) *string {
	return &o.M.Description
}

// Status example graphql function for the Status object of Obj
func (o *ObjResolver) Status(ctx context.Context) *Status {
	return &o.M.Status
}
