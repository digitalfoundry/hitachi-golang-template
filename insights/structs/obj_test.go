package structs

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func NewMockObj() Obj {
	return Obj{
		ID:          "ID",
		Name:        "Name",
		Description: "Description",
		Status:      "Status",
	}
}

func TestObjResolver_Description(t *testing.T) {
	obj := NewMockObj()
	resolver := ObjResolver{M: obj}
	assert.Equal(t, obj.Description, *resolver.Description(context.Background()))
}

func TestObjResolver_ID(t *testing.T) {
	obj := NewMockObj()
	resolver := ObjResolver{M: obj}
	assert.Equal(t, obj.ID, *resolver.ID(context.Background()))
}

func TestObjResolver_Name(t *testing.T) {
	obj := NewMockObj()
	resolver := ObjResolver{M: obj}
	assert.Equal(t, obj.Name, *resolver.Name(context.Background()))
}

func TestObjResolver_Status(t *testing.T) {
	obj := NewMockObj()
	resolver := ObjResolver{M: obj}
	assert.Equal(t, obj.Status, *resolver.Status(context.Background()))
}
