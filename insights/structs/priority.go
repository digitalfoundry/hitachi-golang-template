package structs

// Priority is priority level of an event
type Priority string

const (
	// PriorityCritical is the critical priority level
	PriorityCritical Priority = "CRITICAL"

	// PriorityHigh is the high priority level
	PriorityHigh     Priority = "HIGH"

	// PriorityMedium is the medium priority level
	PriorityMedium   Priority = "MEDIUM"

	// PriorityLow is the low priority level
	PriorityLow      Priority = "LOW"
)
