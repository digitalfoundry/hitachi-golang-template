package structs

import (
	"sort"

	"github.com/emirpasic/gods/maps/linkedhashmap"

	"fmt"
	"math"
	"strings"
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
)

// SeamType is the calculator for seam type
type SeamType struct {
	KPICalculator
	AssetID    string
	KPIValueDB insights.InfluxDBer
}

// SeamInfo is the struct for seam type info
type SeamInfo struct {
	CoalType string
	Ratio    float64
}

// NewSeamType creates a new seam type
func NewSeamType(rawDb insights.InfluxDBer, kpiValueDb insights.InfluxDBer, metaDB insights.ArangoDBer) *SeamType {
	return &SeamType{
		KPICalculator: KPICalculator{
			ID:         "seam-type",
			RawDataDB:  rawDb,
			MetadataDB: metaDB,
		},
		AssetID:    "CHPP",
		KPIValueDB: kpiValueDb,
	}
}

// GetID gets the seam type ID
func (s *SeamType) GetID() string {
	return s.ID
}

// Calculate is the calculate function for seam type
func (s *SeamType) Calculate(now time.Time) []*MeasurementEntity {
	seamTypes, err := s.getDataFromSource(now)
	if err != nil {
		insights.LogErrorWithInfo(err, "", fmt.Sprintf("error calculating %s", s.ID))
	}

	measurement := &MeasurementEntity{
		Measurement: s.ID,
		Fields: map[string]interface{}{
			"json": seamTypes,
		},
		Tags: map[string]string{
			"assetId": s.AssetID,
		},
		AdditionalInfo: nil,
		Time:           now,
	}

	return []*MeasurementEntity{measurement}
}

// GetMeasurementsFromDatabase gets the seam type data from influx
func (s *SeamType) GetMeasurementsFromDatabase(now time.Time) []*MeasurementEntity {
	// todo
	//findByMeasurementsAtTime(s.KPIValueDB, )

	return []*MeasurementEntity{}
}

func (s *SeamType) isValidSeam(info *SeamInfo) bool {
	return info != nil && strings.ToLower(info.CoalType) != "null" && info.Ratio > math.Pow10(-6)
}

func (s *SeamType) getDataFromSource(now time.Time) (*linkedhashmap.Map, error) {
	seamTypes := []*SeamInfo{}
	seams, err := s.getMetadataByID()
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error querying kpi metadata for seam type calculator")
		return nil, err
	}
	for _, query := range seams.AllSeamCronQueries() {
		binder := insights.Binder{
			Name:  "date",
			Value: now,
		}

		resp, err := s.RawDataDB.Query(query.Template, "KPI", binder)
		if err != nil {
			insights.LogErrorWithInfo(err, "", fmt.Sprintf("error querying %s for influx", s.ID))
			return nil, err
		}

		values, parseErr := parseResponseFirstValue(resp)
		if parseErr != nil {
			insights.LogErrorWithInfo(parseErr, "", fmt.Sprintf("error parsing influx response"))
		}

		coalType := fmt.Sprintf("%v", values[1])

		var ratio float64
		if values[2] != nil {
			ratio, err = parseFloat(values[2])
			if err != nil {
				return nil, err
			}
		}

		info := &SeamInfo{
			CoalType: coalType,
			Ratio:    ratio,
		}

		if !s.isValidSeam(info) {
			err := fmt.Errorf("invalid seam information: coalType: %s, ratio: %f", coalType, ratio)
			insights.LogErrorWithInfo(err, "", "")
			return nil, err
		}

		seamTypes = append(seamTypes, &SeamInfo{
			CoalType: coalType,
			Ratio:    ratio,
		})
	}

	if len(seamTypes) == 0 {
		err := fmt.Errorf("empty seamType")
		insights.LogErrorWithInfo(err, "", "")
		return nil, err
	}

	// todo validate sort order and direction
	sort.SliceStable(seamTypes, func(i, j int) bool {
		return seamTypes[i].Ratio > seamTypes[j].Ratio
	})

	sort.SliceStable(seamTypes, func(i, j int) bool {
		return seamTypes[i].CoalType > seamTypes[j].CoalType
	})

	// linkedhashmap to preserve order
	currentSeamType := linkedhashmap.New()
	for coalType, ratio := range seamTypes {
		currentSeamType.Put(coalType, ratio)
	}

	return currentSeamType, nil
}
