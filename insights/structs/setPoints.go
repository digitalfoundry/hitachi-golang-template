package structs

import (
	"fmt"
	"math"
	"regexp"
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
)

const shiftOne = 9
const shiftTwo = 21

// SetPoints is the struct for set points
type SetPoints struct {
	KPICalculator
}

// NewSetPoints creates a new set points struct
func NewSetPoints(rawDb insights.InfluxDBer, metadataDB insights.ArangoDBer) *SetPoints {
	return &SetPoints{
		KPICalculator: KPICalculator{
			ID:         "set-points",
			RawDataDB:  rawDb,
			MetadataDB: metadataDB,
		},
	}
}

// GetID gets the set point ID
func (s *SetPoints) GetID() string {
	return s.ID
}

// Calculate is the set point calculator function
func (s *SetPoints) Calculate(now time.Time) []*MeasurementEntity {
	entities, err := s.getDataFromSource(now)
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error calculating set points")
		return nil
	}

	return entities
}

// GetMeasurementsFromDatabase gets the set points data from influx
func (s *SetPoints) GetMeasurementsFromDatabase(now time.Time) []*MeasurementEntity {
	return []*MeasurementEntity{}
}

//nolint:gocyclo
func (s *SetPoints) getDataFromSource(now time.Time) ([]*MeasurementEntity, error) {
	metadata, err := s.getMetadataByID()
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error querying kpi metadata for %s calculator", s.ID)
		return nil, err
	}

	valueQuery, diffQuery := metadata.GetSetPointsQueries()

	shiftTimes := getShiftTime(now, metadata.Properties.ShiftTimeSpanInHours)

	values, getValuesErr := s.queryRawData(valueQuery.Template, now, shiftTimes)
	if getValuesErr != nil {
		return nil, getValuesErr
	}

	setPointValues, convertErr := convertQueryResults(values)
	if convertErr != nil {
		return nil, convertErr
	}

	setPointChanges := []map[time.Time]float64{}
	diffData, getDiffsErr := s.queryRawData(diffQuery.Template, now, shiftTimes)
	if getDiffsErr != nil {
		if getDiffsErr.Error() == "series nil or empty" {
			setPointChanges, err = getZeroDifference(values)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, getDiffsErr
		}
	} else {
		setPointChanges,convertErr = convertQueryResults(diffData)
		if convertErr != nil {
			return nil, convertErr
		}
	}

	deviations := calculateDeviations(setPointValues, setPointChanges)
	fmt.Println(deviations)

	columnNames := getColumnNames(diffQuery.Template)

	additionalInfo := map[string]interface{}{
		columnNames[1]: deviations[0],
		columnNames[0]: deviations[1],
	}

	tags := map[string]string{
		"assetId": "CHPP",
	}

	setPointsTags := map[string]string{}
	for k, v := range tags {
		setPointsTags[k] = v
	}

	for name, t := range shiftTimes {
		setPointsTags[name] = t.String()
	}

	setPoints := &MeasurementEntity{
		Measurement: s.ID,
		Time:        now,
		Fields: map[string]interface{}{
			"value": maxDeviation(deviations),
		},
		AdditionalInfo: additionalInfo,
		Tags:           setPointsTags,
	}

	entities := []*MeasurementEntity{}
	for _, value := range values {
		if value[1] != nil {
			val, err := parseFloat(value[1])
			if err != nil {
				return nil, err
			}

			t, err := time.Parse(time.RFC3339, value[0].(string))
			if err != nil {
				return nil, err
			}
			entity := createMeasurementEntity(columnNames[0], t, val, tags)
			entities = append(entities, entity)
		}

		if value[2] != nil {
			val, err := parseFloat(value[2])
			if err != nil {
				return nil, err
			}

			t, err := time.Parse(time.RFC3339, value[0].(string))
			if err != nil {
				return nil, err
			}
			entity := createMeasurementEntity(columnNames[1], t, val, tags)
			entities = append(entities, entity)
		}
	}

	entities = append(entities, setPoints)

	return entities, nil
}

func getZeroDifference(values [][]interface{}) ([]map[time.Time]float64, error) {
	diffs := []map[time.Time]float64{}
	for range [2]int{} {
		diffSet := map[time.Time]float64{}
		for _, val := range values {
			t, err := time.Parse(time.RFC3339, val[0].(string))
			if err != nil {
				return nil, err
			}

			diffSet[t] = 0
		}

		diffs = append(diffs, diffSet)
	}

	return diffs, nil
}

func createMeasurementEntity(measurement string, t time.Time, value float64, tags map[string]string) *MeasurementEntity {
	return &MeasurementEntity{
		Measurement: measurement,
		Fields: map[string]interface{}{
			"value": value,
		},
		Tags: tags,
		Time: t,
	}
}

func maxDeviation(devs []float64) float64 {
	max := 0.0
	for _, dev := range devs {
		if math.Abs(dev) > max {
			max = math.Abs(dev)
		}
	}

	return max
}

func getColumnNames(queryString string) []string {
	r, err := regexp.Compile(`"tag"='[^']+`)
	if err != nil {
		insights.PanicErrorIfExists(err)
	}

	cols := r.FindAllString(queryString, 2)
	for _, col := range cols {
		col = col[7:] + ".DIFF" // strip "tag"=' from regex (positive lookbehind not supported on golang
	}

	return cols
}

func calculateDeviations(values []map[time.Time]float64, diffs []map[time.Time]float64) []float64 {
	deviations := []float64{}
	for i := range values {
		max := 0.0
		for t := range values[i] {
			dev := diffs[i][t] / (values[i][t] - diffs[i][t])
			if math.Abs(dev) > max {
				max = math.Abs(dev)
			}
		}

		deviations = append(deviations, max)
	}

	return deviations
}

func convertQueryResults(results [][]interface{}) ([]map[time.Time]float64, error) {
	list1 := map[time.Time]float64{}
	list2 := map[time.Time]float64{}
	for _, data := range results {
		t, err := time.Parse(time.RFC3339, data[0].(string))
		if err != nil {
			return nil, err
		}
		value1, parseErr := parseFloat(data[1]) // value1
		if parseErr != nil {
			return nil, parseErr
		}
		value2, parseErr := parseFloat(data[2]) // value2
		if parseErr != nil {
			return nil, parseErr
		}

		list1[t] = value1
		list2[t] = value2
	}

	return []map[time.Time]float64{list1, list2}, nil
}

func (s *SetPoints) queryRawData(query string, now time.Time, shiftTimes map[string]time.Time) ([][]interface{}, error) {
	var binders []insights.Binder
	binders = append(binders, insights.Binder{
		Name:  "date",
		Value: now,
	})
	for name, val := range shiftTimes {
		binders = append(binders, insights.Binder{
			Name:  name,
			Value: val,
		})
	}

	resp, err := s.RawDataDB.Query(query, "KPI", binders...)
	if err != nil {
		return nil, err
	}

	values, parseErr := parseResponseFirstSeries(resp)
	if parseErr != nil {
		return nil, parseErr
	}

	return values, nil
}

func getShiftTime(now time.Time, shiftTimeSpan int) map[string]time.Time {
	var shiftTimes []time.Time
	shiftTimes = append(shiftTimes, time.Date(now.Year(), now.Month(), now.Day(), shiftOne, 0, 0, 0, time.UTC))
	shiftTimes = append(shiftTimes, time.Date(now.Year(), now.Month(), now.Day(), shiftTwo, 0, 0, 0, time.UTC))
	shiftTimes = append(shiftTimes, shiftTimes[0].AddDate(0, 0, 1))
	shiftTimes = append([]time.Time{shiftTimes[1].AddDate(0, 0, -1)}, shiftTimes...)

	var index int
	for i := range shiftTimes {
		if !now.After(shiftTimes[i]) {
			index = i
			break
		}
	}

	shiftStartTime := shiftTimes[index-1]
	shiftEndTime := shiftTimes[index]

	// shift time is the shift closest to the current time
	distanceFromStart := now.Sub(shiftStartTime)
	distanceFromEnd := shiftEndTime.Sub(now)

	var shiftTime time.Time
	if distanceFromStart > distanceFromEnd {
		shiftTime = shiftEndTime
	} else {
		shiftTime = shiftStartTime
	}

	return map[string]time.Time{
		"shiftTransitionStartTime": shiftTime.AddDate(0, 0, -shiftTimeSpan),
		"shiftTransitionEndTime":   shiftTime.AddDate(0, 0, shiftTimeSpan),
		"shiftTime":                shiftTime,
	}
}
