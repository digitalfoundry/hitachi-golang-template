package structs

import (
	"encoding/json"
	"errors"
	"strconv"
)

// Severity is a enum used to represent different levels of severities
type Severity string

const (
	// None is represents Level 1 severity
	None Severity = "NONE"
	// Negligible represents Level 2 severity
	Negligible Severity = "NEGLIGIBLE"
	// Marginal represents Level 3 severity
	Marginal Severity = "MARGINAL"
	// Critical represents Level 4 severity
	Critical Severity = "CRITICAL"
	// Catastrophic represents Level 5 severity
	Catastrophic Severity = "CATASTROPHIC"
)

// SeverityWrapper is a wrapper for the severity struct to be an int or Severity object
type SeverityWrapper struct {
	value *int
	Severity
}

// NewSeverityWrapper returns a new severity wrapper based on a Severity payload
func NewSeverityWrapper(severity Severity) (*SeverityWrapper, error) {
	intValue, err := severity.GetValue()
	if err != nil {
		return nil, err
	}
	return &SeverityWrapper{value: &intValue, Severity: severity}, nil
}

// UnmarshalJSON unmarshals data into the Severity object
func (s *SeverityWrapper) UnmarshalJSON(data []byte) error {
	if ordinal, err := strconv.Atoi(string(data)); err == nil {
		s.value = &ordinal
		s.Severity, err = s.GetValue()
		return err
	}

	return json.Unmarshal(data, &s.Severity)
}

// MarshalJSON returns a bytearray representation of the Severity value
func (s SeverityWrapper) MarshalJSON() ([]byte, error) {
	if s.value == nil {
		severityVal, err := s.Severity.GetValue()
		*s.value = severityVal
		if err != nil {
			return nil, err
		}
	}

	return json.Marshal(s.value)
}

// IsNone returns true if the severity wrapper value is 0
func (s *SeverityWrapper) IsNone() bool {
	return *s.value == 0
}

// GetValue returns the severity enum value if it doesn't exist
func (s *SeverityWrapper) GetValue() (Severity, error) {
	if s.Severity != "" {
		return s.Severity, nil
	}

	switch *s.value {
	case 0:
		return None, nil
	case 1:
		return Negligible, nil
	case 2:
		return Marginal, nil
	case 3:
		return Critical, nil
	case 4:
		return Catastrophic, nil
	default:
		return "", errors.New("unrecognized severity value")
	}
}

// GetValue returns the integer value of the severity enum
func (s Severity) GetValue() (int, error) {
	switch s {
	case None:
		return 0, nil
	case Negligible:
		return 1, nil
	case Marginal:
		return 2, nil
	case Critical:
		return 3, nil
	case Catastrophic:
		return 4, nil
	default:
		return 5, errors.New("unrecognized status value")
	}
}

// SeverityArrayToIntArray returns the integer values of an array of severity enums
func SeverityArrayToIntArray(sevArr []Severity) ([]int, error) {
	retArr := []int{}
	for _, sev := range sevArr {
		intVal, err := sev.GetValue()
		if err != nil {
			return []int{}, err
		}
		retArr = append(retArr, intVal)
	}

	return retArr, nil
}

// GetSeverity returns the severity enum from the integer value
func GetSeverity(value int) (Severity, error) {
	switch value {
	case 0:
		return None, nil
	case 1:
		return Negligible, nil
	case 2:
		return Marginal, nil
	case 3:
		return Critical, nil
	case 4:
		return Catastrophic, nil
	default:
		return None, errors.New("unrecognized severity value")
	}
}

// GetSeverityString returns the string value of the severity enum
func GetSeverityString(severity Severity) (string, error) {
	switch severity {
	case None:
		return "NONE", nil
	case Negligible:
		return "NEGLIGIBLE", nil
	case Marginal:
		return "MARGINAL", nil
	case Critical:
		return "CRITICAL", nil
	case Catastrophic:
		return "CATASTROPHIC", nil
	default:
		return "NONE", errors.New("unrecognized severity value")
	}
}
