package structs

import "github.com/graph-gophers/graphql-go"

// ShiftChangeSeries represents time data during shift change
type ShiftChangeSeries struct {
	ID               graphql.ID
	Timestamps       []Date
	VerticalBoundary [][]float64
}

// ShiftChangeSeriesResolver is the graphql resolver for the ShiftChangeSeries struct
type ShiftChangeSeriesResolver struct {
	m *ShiftChangeSeries
}

// ID is the graphql resolver function for the ID field of the ShiftChangeSeries struct
func (r *ShiftChangeSeriesResolver) ID() graphql.ID {
	return r.m.ID
}

// Timestamps is the graphql resolver function for the Timestamps field of the ShiftChangeSeries struct
func (r *ShiftChangeSeriesResolver) Timestamps() []Date {
	return r.m.Timestamps
}

// VerticalBoundary is the graphql resolver function for the VerticalBoundary field of the ShiftChangeSeries struct
func (r *ShiftChangeSeriesResolver) VerticalBoundary() [][]float64 {
	return r.m.VerticalBoundary
}
