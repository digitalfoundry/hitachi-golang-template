package structs

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func newMockShiftChange() *ShiftChangeSeries {
	return &ShiftChangeSeries{
		ID:               "shift-id",
		Timestamps:       []Date{
			{
				Time: time.Now(),
			},
		},
		VerticalBoundary: [][]float64{
			{
				4.44,
			},
			{
				44.44,
			},
		},
	}
}
func TestShiftChangeSeriesResolver_ID(t *testing.T) {
	shiftChangeSeries := newMockShiftChange()
	resolver := ShiftChangeSeriesResolver{m: shiftChangeSeries}

	assert.Equal(t, shiftChangeSeries.ID, resolver.ID())
}

func TestShiftChangeSeriesResolver_Timestamps(t *testing.T) {
	shiftChangeSeries := newMockShiftChange()
	resolver := ShiftChangeSeriesResolver{m: shiftChangeSeries}

	assert.Equal(t, shiftChangeSeries.Timestamps[0].Time, resolver.Timestamps()[0].Time)
}

func TestShiftChangeSeriesResolver_VerticalBoundary(t *testing.T) {
	shiftChangeSeries := newMockShiftChange()
	resolver := ShiftChangeSeriesResolver{m: shiftChangeSeries}

	assert.Equal(t, shiftChangeSeries.VerticalBoundary[0][0], resolver.VerticalBoundary()[0][0])
}
