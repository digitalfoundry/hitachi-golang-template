package structs

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
)

// SimpleKPICalculator is the simple KPI calculator struct
type SimpleKPICalculator struct {
	KPICalculator
}

// NewAvailability returns a new simple kpi calculator type availability
func NewAvailability(kpiID string, rawDb insights.InfluxDBer, metaDB insights.ArangoDBer) *SimpleKPICalculator {
	return &SimpleKPICalculator{
		KPICalculator: KPICalculator{
			ID:         kpiID,
			RawDataDB:  rawDb,
			MetadataDB: metaDB,
		},
	}
}

// GetID returns the simple kpi calculator id
func (a *SimpleKPICalculator) GetID() string {
	return a.ID
}

// Calculate is the generic calculate function for the simple kpi calculator
func (a *SimpleKPICalculator) Calculate(now time.Time) []*MeasurementEntity {
	target, value, err := a.getDataFromSource(now)
	if err != nil {
		insights.LogErrorWithInfo(err, "", fmt.Sprintf("error calculating %s", a.ID))
		return nil
	}

	actual := &MeasurementEntity{
		Measurement: a.ID,
		Fields: map[string]interface{}{
			"value": value,
		},
		Tags:           nil,
		AdditionalInfo: nil,
		Time:           now,
	}

	targetEntity := &MeasurementEntity{
		Measurement: a.ID + "_target",
		Fields: map[string]interface{}{
			"value": target,
		},
		Tags:           nil,
		AdditionalInfo: nil,
		Time:           now,
	}

	return []*MeasurementEntity{
		actual,
		targetEntity,
	}
}

// GetMeasurementsFromDatabase returns the simple kpi influx data
func (a *SimpleKPICalculator) GetMeasurementsFromDatabase(now time.Time) []*MeasurementEntity {
	return []*MeasurementEntity{}
}

func (a *SimpleKPICalculator) getDataFromSource(now time.Time) (float64, float64, error) {
	metadata, err := a.getMetadataByID()
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error querying kpi metadata for simple KPI calculator")
		return 0, 0, err
	}
	measurement := metadata.Query.TargetValue.Collection
	queryTemplate := metadata.Query.TargetValue.Template

	// TODO: Most updated metadata uses ${measurement}. The below string replacement will need to reflect this change once our
	// arango metadata reflects this change
	queryTemplate = strings.Replace(queryTemplate, `"${availabilityMeasurement}"`, measurement, 1)

	// TODO: Templates aren't currently designed to handle binding values. Need to find a solution to this problem
	resp, err := a.RawDataDB.Query(queryTemplate, "KPI")
	if err != nil {
		return 0, 0, err
	}

	values, parseErr := parseResponseFirstValue(resp)
	if parseErr != nil {
		return 0, 0, parseErr
	}

	var target float64
	if values[1] != nil {
		target, err = parseFloat(values[1])
		if err != nil {
			return 0, 0, err
		}
	}

	var value float64
	if values[2] != nil {
		value, err = parseFloat(values[2])
		if err != nil {
			return 0, 0, err
		}
	}

	return target, value, nil
}
