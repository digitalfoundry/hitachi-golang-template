package structs

import (
	"context"

	"github.com/graph-gophers/graphql-go"
)

// NewSimplePropertyCollectionEntityResolver return a new instance of SimplePropertyCollectionEntityResolver
func NewSimplePropertyCollectionEntityResolver(spc *SimplePropertyCollectionEntity) *SimplePropertyCollectionEntityResolver {
	return &SimplePropertyCollectionEntityResolver{m: *spc}
}

// NewSimplePropertyEntityResolver return a new instance of SimplePropertyEntityResolver
func NewSimplePropertyEntityResolver(sp *SimplePropertyEntity) *SimplePropertyEntityResolver {
	return &SimplePropertyEntityResolver{m: *sp}
}

// SimplePropertyCollectionEntity represents a collection of properties
type SimplePropertyCollectionEntity struct {
	Description string                  `json:"description,omitempty"`
	DisplayName string                  `json:"displayName,omitempty"`
	ID          graphql.ID              `json:"id,omitempty"`
	LastUpdated Date                    `json:"lastUpdated,omitempty"`
	Name        string                  `json:"name,omitempty"`
	Properties  []*SimplePropertyEntity `json:"properties,omitempty"`
}

// SimplePropertyEntity represents an a property
type SimplePropertyEntity struct {
	DataType    string     `json:"dataType,omitempty"`
	Description string     `json:"description,omitempty"`
	DisplayName string     `json:"displayName,omitempty"`
	ID          graphql.ID `json:"id,omitempty"`
	LastUpdated Date       `json:"lastUpdated,omitempty"`
	Name        string     `json:"name,omitempty"`
	Value       string     `json:"value,omitempty"`
}

// SimplePropertyEntityInput represents the fields that can be used by a user for a SimplePropertyEntity
type SimplePropertyEntityInput struct {
	DataType    string
	Description string
	DisplayName string
	LastUpdated Date
	Name        string
	Value       string
}

// SimplePropertyCollectionEntityInput represents the fields that can be used by a user for a SimplePropertyCollectionEntity
type SimplePropertyCollectionEntityInput struct {
	Properties []*SimplePropertyEntityInput
}

// SimplePropertyCollectionEntityResolver is the resolver struct for SimplePropertyCollectionEntity
type SimplePropertyCollectionEntityResolver struct {
	m SimplePropertyCollectionEntity
}

// SimplePropertyEntityResolver is the resolver struct for SimplePropertyEntity
type SimplePropertyEntityResolver struct {
	m SimplePropertyEntity
}

// Description returns a pointer to the description string
func (spc *SimplePropertyCollectionEntityResolver) Description() *string {
	return &spc.m.Description
}

// DisplayName returns a pointer to the display name string
func (spc *SimplePropertyCollectionEntityResolver) DisplayName() *string {
	return &spc.m.DisplayName
}

// ID returns a pointer to the ID graphql.ID
func (spc *SimplePropertyCollectionEntityResolver) ID(ctx context.Context) *graphql.ID {
	return &spc.m.ID
}

// LastUpdated returns a pointer to the last updated date
func (spc *SimplePropertyCollectionEntityResolver) LastUpdated() *Date {
	return &spc.m.LastUpdated
}

// Name returns a pointer to the name string
func (spc *SimplePropertyCollectionEntityResolver) Name() *string {
	return &spc.m.Name
}

// Properties returns an array of SimplePropertyEntityResolvers
func (spc *SimplePropertyCollectionEntityResolver) Properties() *[]*SimplePropertyEntityResolver {
	resolvers := []*SimplePropertyEntityResolver{}
	for _, prop := range spc.m.Properties {
		resolvers = append(resolvers, &SimplePropertyEntityResolver{m: *prop})
	}
	return &resolvers
}

// DataType returns a pointer to the data type string
func (spc *SimplePropertyEntityResolver) DataType() *string {
	return &spc.m.DataType
}

// Description returns a pointer to the description string
func (spc *SimplePropertyEntityResolver) Description() *string {
	return &spc.m.Description
}

// DisplayName returns a pointer to the display name string
func (spc *SimplePropertyEntityResolver) DisplayName() *string {
	return &spc.m.DisplayName
}

// ID returns a pointer to the ID graphql.ID
func (spc *SimplePropertyEntityResolver) ID(ctx context.Context) *graphql.ID {
	return &spc.m.ID
}

// LastUpdated returns a pointer to the last updated date
func (spc *SimplePropertyEntityResolver) LastUpdated() *Date {
	return &spc.m.LastUpdated
}

// Name returns a pointer to the name string
func (spc *SimplePropertyEntityResolver) Name() *string {
	return &spc.m.Name
}

// Value returns a pointer to the value string
func (spc *SimplePropertyEntityResolver) Value() *string {
	return &spc.m.Value
}
