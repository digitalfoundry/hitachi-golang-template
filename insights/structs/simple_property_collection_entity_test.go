package structs

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func newMockSimplePropertyEntity() *SimplePropertyEntity {
	t, err := time.Parse(time.RFC3339, "2019-08-27T13:22:53.108Z")
	if err != nil {
		return nil
	}

	return &SimplePropertyEntity{
		DataType:    "y",
		Description: "r",
		DisplayName: "a",
		ID:          "n",
		LastUpdated: Date{Time: t},
		Name:        "d",
		Value:       "y",
	}
}

func newMockSimplePropertyCollectionEntity() *SimplePropertyCollectionEntity {
	t, err := time.Parse(time.RFC3339, "2019-08-27T13:22:53.108Z")
	if err != nil {
		return nil
	}

	return &SimplePropertyCollectionEntity{
		Description: "r",
		DisplayName: "a",
		ID:          "n",
		LastUpdated: Date{Time: t},
		Name:        "d",
		Properties:  []*SimplePropertyEntity{newMockSimplePropertyEntity()},
	}
}

func TestNewSimplePropertyCollectionEntityResolver(t *testing.T) {
	spc := newMockSimplePropertyCollectionEntity()
	resolver := NewSimplePropertyCollectionEntityResolver(spc)
	assert.NotNil(t, resolver)
}

func TestNewSimplePropertyEntityResolver(t *testing.T) {
	sp := newMockSimplePropertyEntity()
	resolver := NewSimplePropertyEntityResolver(sp)
	assert.NotNil(t, resolver)
}

func TestSimplePropertyCollectionEntityResolver_Description(t *testing.T) {
	spc := newMockSimplePropertyCollectionEntity()
	resolver := NewSimplePropertyCollectionEntityResolver(spc)
	assert.Equal(t, spc.Description, *resolver.Description())
}

func TestSimplePropertyCollectionEntityResolver_DisplayName(t *testing.T) {
	spc := newMockSimplePropertyCollectionEntity()
	resolver := NewSimplePropertyCollectionEntityResolver(spc)
	assert.Equal(t, spc.DisplayName, *resolver.DisplayName())
}

func TestSimplePropertyCollectionEntityResolver_ID(t *testing.T) {
	spc := newMockSimplePropertyCollectionEntity()
	resolver := NewSimplePropertyCollectionEntityResolver(spc)
	assert.Equal(t, spc.ID, *resolver.ID(context.Background()))
}

func TestSimplePropertyCollectionEntityResolver_LastUpdated(t *testing.T) {
	spc := newMockSimplePropertyCollectionEntity()
	resolver := NewSimplePropertyCollectionEntityResolver(spc)
	assert.Equal(t, spc.LastUpdated.Time, resolver.LastUpdated().Time)
}

func TestSimplePropertyCollectionEntityResolver_Name(t *testing.T) {
	spc := newMockSimplePropertyCollectionEntity()
	resolver := NewSimplePropertyCollectionEntityResolver(spc)
	assert.Equal(t, spc.Name, *resolver.Name())
}

func TestSimplePropertyCollectionEntityResolver_Properties(t *testing.T) {
	spc := newMockSimplePropertyCollectionEntity()
	resolver := NewSimplePropertyCollectionEntityResolver(spc)
	props := *resolver.Properties()

	assert.Equal(t, spc.Properties[0], &props[0].m)
}

func TestSimplePropertyEntityResolver_DataType(t *testing.T) {
	sp := newMockSimplePropertyEntity()
	resolver := NewSimplePropertyEntityResolver(sp)
	assert.Equal(t, sp.DataType, *resolver.DataType())
}

func TestSimplePropertyEntityResolver_Description(t *testing.T) {
	sp := newMockSimplePropertyEntity()
	resolver := NewSimplePropertyEntityResolver(sp)
	assert.Equal(t, sp.Description, *resolver.Description())
}

func TestSimplePropertyEntityResolver_DisplayName(t *testing.T) {
	sp := newMockSimplePropertyEntity()
	resolver := NewSimplePropertyEntityResolver(sp)
	assert.Equal(t, sp.DisplayName, *resolver.DisplayName())
}

func TestSimplePropertyEntityResolver_ID(t *testing.T) {
	sp := newMockSimplePropertyEntity()
	resolver := NewSimplePropertyEntityResolver(sp)
	assert.Equal(t, sp.ID, *resolver.ID(context.Background()))
}

func TestSimplePropertyEntityResolver_LastUpdated(t *testing.T) {
	sp := newMockSimplePropertyEntity()
	resolver := NewSimplePropertyEntityResolver(sp)
	assert.Equal(t, sp.LastUpdated.Time, resolver.LastUpdated().Time)
}

func TestSimplePropertyEntityResolver_Name(t *testing.T) {
	sp := newMockSimplePropertyEntity()
	resolver := NewSimplePropertyEntityResolver(sp)
	assert.Equal(t, sp.Name, *resolver.Name())
}

func TestSimplePropertyEntityResolver_Value(t *testing.T) {
	sp := newMockSimplePropertyEntity()
	resolver := NewSimplePropertyEntityResolver(sp)
	assert.Equal(t, sp.Value, *resolver.Value())
}
