package structs

// SortOrder enum determines how a query is sorted
type SortOrder string

const (
	// Ascending is ascending sort order
	Ascending  SortOrder = "ASCENDING"

	// Descending is descending sort order
	Descending SortOrder = "DESCENDING"

	// Unosrted is unsorted
	Unsorted   SortOrder = "UNSORTED"
)

// SortFieldType represents the type of field
type SortFieldType string

const (
	// String is string field type to sort on
	String SortFieldType = "STRING"

	// Number is number field type to sort on
	Number SortFieldType = "NUMBER"
)

// SortOptions represents options for sorting a query
type SortOptions struct {
	Field     *string
	Order     *SortOrder
	FieldType *SortFieldType
}
