package structs

import (
	"encoding/json"
	"errors"
	"strconv"
)

// Status is the status enum can be re purposed to a real status when needed
type Status string

const (
	// Open represents the open status
	Open Status = "OPEN"
	// Closed represents the closed status
	Closed Status = "CLOSED"
	// Pending represents the pending status
	Pending Status = "PENDING"
	// Dismissed represents the dismissed status
	Dismissed Status = "DISMISSED"
)

// StatusWrapper is the struct to wrap the status in an int or status object value
type StatusWrapper struct {
	value *int
	Status
}

// NewStatusWrapper returns a new status wrapper with the int and Status value populated
func NewStatusWrapper(status Status) (*StatusWrapper, error) {
	intValue, err := status.GetValue()
	if err != nil {
		return nil, err
	}
	return &StatusWrapper{value: &intValue, Status: status}, nil
}

// UnmarshalJSON unmarshals a byte array into a status wrapper struct
func (s *StatusWrapper) UnmarshalJSON(data []byte) error {
	if ordinal, err := strconv.Atoi(string(data)); err == nil {
		s.value = &ordinal
		s.Status, err = s.GetValue()
		return err
	}

	return json.Unmarshal(data, &s.Status)
}

// MarshalJSON marshals the StatusWrapper struct into a byte array
func (s StatusWrapper) MarshalJSON() ([]byte, error) {
	if s.value == nil {
		statusVal, err := s.Status.GetValue() // maybe put this inline?
		*s.value = statusVal
		if err != nil {
			return nil, err
		}
	}

	return json.Marshal(s.value)
}

// GetValue returns the Status enum value of the status wrapper
func (s *StatusWrapper) GetValue() (Status, error) {
	if s.Status != "" {
		return s.Status, nil
	}

	switch *s.value {
	case 0:
		return Open, nil
	case 1:
		return Closed, nil
	case 2:
		return Pending, nil
	case 3:
		return Dismissed, nil
	default:
		return "", errors.New("unrecognized status value")
	}
}

// GetValue returns the integer value of the Status struct
func (s Status) GetValue() (int, error) {
	switch s {
	case Open:
		return 0, nil
	case Closed:
		return 1, nil
	case Pending:
		return 2, nil
	case Dismissed:
		return 3, nil
	default:
		return 4, errors.New("unrecognized status value")
	}
}

// StatusArrayToIntArray returns an array of statuses to an array of their corresponding integer values
func StatusArrayToIntArray(statArr []Status) ([]int, error) {
	retArr := []int{}
	for _, stat := range statArr {
		intVal, err := stat.GetValue()
		if err != nil {
			return []int{}, err
		}
		retArr = append(retArr, intVal)
	}

	return retArr, nil
}
