package structs

// TemplateClass is the struct of
type TemplateClass struct {
	Name          string
	Type          string
	BaseType      string
	Attributes    []TemplateAttribute
	Props         []TemplateProperty
	Relationships []TemplateRelation
}

// TemplateAttribute is the struct of
type TemplateAttribute struct {
	Name         string
	DataType     string
	DefaultValue string
	IsSystem     bool
	IsRequired   bool
	Cardinality  string
}

// TemplateProperty is the struct of
type TemplateProperty struct {
	Name         string
	DataType     string
	DefaultValue string
	IsSystem     bool
	IsRequired   bool
	Cardinality  string
}

// TemplateRelation is the struct of
type TemplateRelation struct {
	Name        string
	DataType    string
	Type        string
	IsSystem    bool
	IsRequired  bool
	Cardinality string
}

// TemplateAttributeInput is the struct of
type TemplateAttributeInput struct {
	Name         string
	DataType     string
	DefaultValue string
	IsSystem     bool
	IsRequired   bool
	Cardinality  string
}

// TemplatePropertyInput is the struct of
type TemplatePropertyInput struct {
	Name         string
	DataType     string
	DefaultValue string
	IsSystem     bool
	IsRequired   bool
	Cardinality  string
}

// TemplateRelationInput is the struct of
type TemplateRelationInput struct {
	Name        string
	DataType    string
	Type        string
	IsSystem    bool
	IsRequired  bool
	Cardinality string
}

// NewTemplateClassResolver returns a new TemplateClassResolver given a Template Class
func NewTemplateClassResolver(tc *TemplateClass) *TemplateClassResolver {
	return &TemplateClassResolver{m: *tc}
}

// Primary Resolvers

// TemplateClassResolver is the graphql resolver struct for the TemplateClass
type TemplateClassResolver struct {
	m TemplateClass
}

// Name is the graphql resolver function for the Name object on the TemplateClass struct
func (t *TemplateClassResolver) Name() *string {
	return &t.m.Name
}

// Type is the graphql resolver function for the Type object on the TemplateClass struct
func (t *TemplateClassResolver) Type() *string {
	return &t.m.Type
}

// BaseType is the graphql resolver function for the BaseType object on the TemplateClass struct
func (t *TemplateClassResolver) BaseType() *string {
	return &t.m.BaseType
}

// Attributes is the graphql resolver function for the Attributes object on the TemplateClass struct
func (t *TemplateClassResolver) Attributes() *[]*TemplateAttributeResolver {
	attrResolver := []*TemplateAttributeResolver{}
	for _, attr := range t.m.Attributes {
		attrResolver = append(attrResolver, &TemplateAttributeResolver{m: attr})
	}

	return &attrResolver
}

// Props is the graphql resolver function for the Props object on the TemplateClass struct
func (t *TemplateClassResolver) Props() *[]*TemplatePropertyResolver {
	attrResolver := []*TemplatePropertyResolver{}
	for _, attr := range t.m.Props {
		attrResolver = append(attrResolver, &TemplatePropertyResolver{m: attr})
	}

	return &attrResolver
}

// Relationships is the graphql resolver function for the Relationships object on the TemplateClass struct
func (t *TemplateClassResolver) Relationships() *[]*TemplateRelationResolver {
	attrResolver := []*TemplateRelationResolver{}
	for _, attr := range t.m.Relationships {
		attrResolver = append(attrResolver, &TemplateRelationResolver{m: attr})
	}

	return &attrResolver
}

// Child Resolvers
// TemplateAttributeResolver is the graphql resolver struct for the TemplateAttribute struct
type TemplateAttributeResolver struct {
	m TemplateAttribute
}

// Name is the graphql resolver function for the Name object on the TemplateAttribute struct
func (t *TemplateAttributeResolver) Name() *string {
	return &t.m.Name
}

// DataType is the graphql resolver function for the DataType object on the TemplateAttribute struct
func (t *TemplateAttributeResolver) DataType() *string {
	return &t.m.DataType
}

// DefaultValue is the graphql resolver function for the DefaultValue object on the TemplateAttribute struct
func (t *TemplateAttributeResolver) DefaultValue() *string {
	return &t.m.DefaultValue
}

// IsSystem is the graphql resolver function for the IsSystem object on the TemplateAttribute struct
func (t *TemplateAttributeResolver) IsSystem() *bool {
	return &t.m.IsSystem
}

// IsRequired is the graphql resolver function for the IsRequired object on the TemplateAttribute struct
func (t *TemplateAttributeResolver) IsRequired() *bool {
	return &t.m.IsRequired
}

// Cardinality is the graphql resolver function for the Cardinality object on the TemplateAttribute struct
func (t *TemplateAttributeResolver) Cardinality() *string {
	return &t.m.Cardinality
}

// TemplatePropertyResolver is the graphql resolver struct for the TemplateProperty struct
type TemplatePropertyResolver struct {
	m TemplateProperty
}

// Name is the graphql resolver function for the Name object of the TemplateProperty struct
func (t *TemplatePropertyResolver) Name() *string {
	return &t.m.Name
}

// DataType is the graphql resolver function for the DataType object of the TemplateProperty struct
func (t *TemplatePropertyResolver) DataType() *string {
	return &t.m.DataType
}

// DefaultValue is the graphql resolver function for the DefaultValue object of the TemplateProperty struct
func (t *TemplatePropertyResolver) DefaultValue() *string {
	return &t.m.DefaultValue
}

// IsSystem is the graphql resolver function for the IsSystem object of the TemplateProperty struct
func (t *TemplatePropertyResolver) IsSystem() *bool {
	return &t.m.IsSystem
}

// IsRequired is the graphql resolver function for the IsRequired object of the TemplateProperty struct
func (t *TemplatePropertyResolver) IsRequired() *bool {
	return &t.m.IsRequired
}

// Cardinality is the graphql resolver function for the Cardinality object of the TemplateProperty struct
func (t *TemplatePropertyResolver) Cardinality() *string {
	return &t.m.Cardinality
}

// TemplateRelationResolver is the graphql resolver struct for the the TemplateRelation struct
type TemplateRelationResolver struct {
	m TemplateRelation
}

// Name is the graphql resolver function for the Name object of the TemplateProperty struct
func (t *TemplateRelationResolver) Name() *string {
	return &t.m.Name
}

// DataType is the graphql resolver function for the DataType object of the TemplateProperty struct
func (t *TemplateRelationResolver) DataType() *string {
	return &t.m.DataType
}

// Type is the graphql resolver function for the Type object of the TemplateProperty struct
func (t *TemplateRelationResolver) Type() *string {
	return &t.m.Type
}

// IsSystem is the graphql resolver function for the IsSystem object of the TemplateProperty struct
func (t *TemplateRelationResolver) IsSystem() *bool {
	return &t.m.IsSystem
}

// IsRequired is the graphql resolver function for the IsRequired object of the TemplateProperty struct
func (t *TemplateRelationResolver) IsRequired() *bool {
	return &t.m.IsRequired
}

// Cardinality is the graphql resolver function for the Name Cardinality of the TemplateProperty struct
func (t *TemplateRelationResolver) Cardinality() *string {
	return &t.m.Cardinality
}
