package structs

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func NewMockTemplateAttribute() TemplateAttribute {
	return TemplateAttribute{
		Name:        "Attribute Name",
		DataType:    "Attribute DataType",
		IsSystem:    false,
		IsRequired:  true,
		Cardinality: "Attribute Cardinality",
	}
}

func NewMockTemplateProperty() TemplateProperty {
	return TemplateProperty{
		Name:         "Property Name",
		DataType:     "Property DataType",
		DefaultValue: "Property DefaultValue",
		IsSystem:     true,
		IsRequired:   false,
		Cardinality:  "Property Cardinality",
	}
}

func NewMockTemplateRelation() TemplateRelation {
	return TemplateRelation{
		Name:        "Relation Name",
		DataType:    "Relation DataType",
		Type:        "Relation Type",
		IsSystem:    true,
		IsRequired:  false,
		Cardinality: "Relation Cardinality",
	}
}

func NewMockTemplateClass() TemplateClass {
	return TemplateClass{
		Name:          "Class Name",
		Type:          "Class Type",
		BaseType:      "Class BaseType",
		Attributes:    []TemplateAttribute{NewMockTemplateAttribute()},
		Props:         []TemplateProperty{NewMockTemplateProperty()},
		Relationships: []TemplateRelation{NewMockTemplateRelation()},
	}
}

func TestNewTemplateClassResolver(t *testing.T) {
	mockTemplateClass := NewMockTemplateClass()
	resolver := NewTemplateClassResolver(&mockTemplateClass)
	assert.Equal(t, mockTemplateClass.Name, resolver.m.Name)
	assert.Equal(t, mockTemplateClass.Type, resolver.m.Type)
	assert.Equal(t, mockTemplateClass.BaseType, resolver.m.BaseType)
	assert.Len(t, resolver.m.Attributes, 1)
	assert.Equal(t, mockTemplateClass.Attributes[0].Name, resolver.m.Attributes[0].Name)
	assert.Equal(t, mockTemplateClass.Attributes[0].DataType, resolver.m.Attributes[0].DataType)
	assert.Equal(t, mockTemplateClass.Attributes[0].Cardinality, resolver.m.Attributes[0].Cardinality)
	assert.Equal(t, mockTemplateClass.Attributes[0].IsRequired, resolver.m.Attributes[0].IsRequired)
	assert.Equal(t, mockTemplateClass.Attributes[0].IsSystem, resolver.m.Attributes[0].IsSystem)
	assert.Len(t, resolver.m.Props, 1)
	assert.Equal(t, mockTemplateClass.Props[0].Name, resolver.m.Props[0].Name)
	assert.Equal(t, mockTemplateClass.Props[0].DataType, resolver.m.Props[0].DataType)
	assert.Equal(t, mockTemplateClass.Props[0].Cardinality, resolver.m.Props[0].Cardinality)
	assert.Equal(t, mockTemplateClass.Props[0].IsRequired, resolver.m.Props[0].IsRequired)
	assert.Equal(t, mockTemplateClass.Props[0].IsSystem, resolver.m.Props[0].IsSystem)
	assert.Equal(t, mockTemplateClass.Props[0].DefaultValue, resolver.m.Props[0].DefaultValue)
	assert.Len(t, resolver.m.Relationships, 1)
	assert.Equal(t, mockTemplateClass.Relationships[0].Name, resolver.m.Relationships[0].Name)
	assert.Equal(t, mockTemplateClass.Relationships[0].DataType, resolver.m.Relationships[0].DataType)
	assert.Equal(t, mockTemplateClass.Relationships[0].Cardinality, resolver.m.Relationships[0].Cardinality)
	assert.Equal(t, mockTemplateClass.Relationships[0].IsRequired, resolver.m.Relationships[0].IsRequired)
	assert.Equal(t, mockTemplateClass.Relationships[0].IsSystem, resolver.m.Relationships[0].IsSystem)
	assert.Equal(t, mockTemplateClass.Relationships[0].Type, resolver.m.Relationships[0].Type)
}

func TestTemplateAttributeResolver_Cardinality(t *testing.T) {
	att := NewMockTemplateAttribute()
	resolver := TemplateAttributeResolver{m: att}
	assert.Equal(t, att.Cardinality, *resolver.Cardinality())
}

func TestTemplateAttributeResolver_DataType(t *testing.T) {
	att := NewMockTemplateAttribute()
	resolver := TemplateAttributeResolver{m: att}
	assert.Equal(t, att.DataType, *resolver.DataType())
}

func TestTemplateAttributeResolver_IsRequired(t *testing.T) {
	att := NewMockTemplateAttribute()
	resolver := TemplateAttributeResolver{m: att}
	assert.Equal(t, att.IsRequired, *resolver.IsRequired())
}

func TestTemplateAttributeResolver_IsSystem(t *testing.T) {
	att := NewMockTemplateAttribute()
	resolver := TemplateAttributeResolver{m: att}
	assert.Equal(t, att.IsSystem, *resolver.IsSystem())
}

func TestTemplateAttributeResolver_Name(t *testing.T) {
	att := NewMockTemplateAttribute()
	resolver := TemplateAttributeResolver{m: att}
	assert.Equal(t, att.Name, *resolver.Name())
}

func TestTemplateClassResolver_Attributes(t *testing.T) {
	class := NewMockTemplateClass()
	resolver := TemplateClassResolver{m: class}

	tempAtts := resolver.Attributes()
	atts := *tempAtts
	assert.Len(t, atts, 1)
	assert.Equal(t, class.Attributes[0].Cardinality, *atts[0].Cardinality())
	assert.Equal(t, class.Attributes[0].IsSystem, *atts[0].IsSystem())
	assert.Equal(t, class.Attributes[0].IsRequired, *atts[0].IsRequired())
	assert.Equal(t, class.Attributes[0].Name, *atts[0].Name())
	assert.Equal(t, class.Attributes[0].DataType, *atts[0].DataType())
}

func TestTemplateClassResolver_BaseType(t *testing.T) {
	class := NewMockTemplateClass()
	resolver := TemplateClassResolver{m: class}
	assert.Equal(t, class.BaseType, *resolver.BaseType())
}

func TestTemplateClassResolver_Name(t *testing.T) {
	class := NewMockTemplateClass()
	resolver := TemplateClassResolver{m: class}
	assert.Equal(t, class.Name, *resolver.Name())
}

func TestTemplateClassResolver_Props(t *testing.T) {
	class := NewMockTemplateClass()
	resolver := TemplateClassResolver{m: class}

	tempProps := resolver.Props()
	props := *tempProps
	assert.Len(t, props, 1)
	assert.Equal(t, class.Props[0].Cardinality, *props[0].Cardinality())
	assert.Equal(t, class.Props[0].IsSystem, *props[0].IsSystem())
	assert.Equal(t, class.Props[0].IsRequired, *props[0].IsRequired())
	assert.Equal(t, class.Props[0].Name, *props[0].Name())
	assert.Equal(t, class.Props[0].DataType, *props[0].DataType())
	assert.Equal(t, class.Props[0].DefaultValue, *props[0].DefaultValue())
}

func TestTemplateClassResolver_Relationships(t *testing.T) {
	class := NewMockTemplateClass()
	resolver := TemplateClassResolver{m: class}

	tempRelationships := resolver.Relationships()
	relationships := *tempRelationships
	assert.Len(t, relationships, 1)
	assert.Equal(t, class.Relationships[0].Cardinality, *relationships[0].Cardinality())
	assert.Equal(t, class.Relationships[0].IsSystem, *relationships[0].IsSystem())
	assert.Equal(t, class.Relationships[0].IsRequired, *relationships[0].IsRequired())
	assert.Equal(t, class.Relationships[0].Name, *relationships[0].Name())
	assert.Equal(t, class.Relationships[0].DataType, *relationships[0].DataType())
	assert.Equal(t, class.Relationships[0].Type, *relationships[0].Type())
}

func TestTemplateClassResolver_Type(t *testing.T) {
	class := NewMockTemplateClass()
	resolver := TemplateClassResolver{m: class}
	assert.Equal(t, class.Type, *resolver.Type())
}

func TestTemplatePropertyResolver_Cardinality(t *testing.T) {
	prop := NewMockTemplateProperty()
	resolver := TemplatePropertyResolver{m: prop}
	assert.Equal(t, prop.Cardinality, *resolver.Cardinality())
}

func TestTemplatePropertyResolver_DataType(t *testing.T) {
	prop := NewMockTemplateProperty()
	resolver := TemplatePropertyResolver{m: prop}
	assert.Equal(t, prop.DataType, *resolver.DataType())
}

func TestTemplatePropertyResolver_DefaultValue(t *testing.T) {
	prop := NewMockTemplateProperty()
	resolver := TemplatePropertyResolver{m: prop}
	assert.Equal(t, prop.DefaultValue, *resolver.DefaultValue())
}

func TestTemplatePropertyResolver_IsRequired(t *testing.T) {
	prop := NewMockTemplateProperty()
	resolver := TemplatePropertyResolver{m: prop}
	assert.Equal(t, prop.IsRequired, *resolver.IsRequired())
}

func TestTemplatePropertyResolver_IsSystem(t *testing.T) {
	prop := NewMockTemplateProperty()
	resolver := TemplatePropertyResolver{m: prop}
	assert.Equal(t, prop.IsSystem, *resolver.IsSystem())
}

func TestTemplatePropertyResolver_Name(t *testing.T) {
	prop := NewMockTemplateProperty()
	resolver := TemplatePropertyResolver{m: prop}
	assert.Equal(t, prop.Name, *resolver.Name())
}

func TestTemplateRelationResolver_Cardinality(t *testing.T) {
	relation := NewMockTemplateRelation()
	resolver := TemplateRelationResolver{m: relation}
	assert.Equal(t, relation.Cardinality, *resolver.Cardinality())
}

func TestTemplateRelationResolver_DataType(t *testing.T) {
	relation := NewMockTemplateRelation()
	resolver := TemplateRelationResolver{m: relation}
	assert.Equal(t, relation.DataType, *resolver.DataType())
}

func TestTemplateRelationResolver_IsRequired(t *testing.T) {
	relation := NewMockTemplateRelation()
	resolver := TemplateRelationResolver{m: relation}
	assert.Equal(t, relation.IsRequired, *resolver.IsRequired())
}

func TestTemplateRelationResolver_IsSystem(t *testing.T) {
	relation := NewMockTemplateRelation()
	resolver := TemplateRelationResolver{m: relation}
	assert.Equal(t, relation.IsSystem, *resolver.IsSystem())
}

func TestTemplateRelationResolver_Name(t *testing.T) {
	relation := NewMockTemplateRelation()
	resolver := TemplateRelationResolver{m: relation}
	assert.Equal(t, relation.Name, *resolver.Name())
}

func TestTemplateRelationResolver_Type(t *testing.T) {
	relation := NewMockTemplateRelation()
	resolver := TemplateRelationResolver{m: relation}
	assert.Equal(t, relation.Type, *resolver.Type())
}
