package structs

import (
	"context"
)

// Threshold represents a configured threshold
type Threshold struct {
	MinValue   float64
	MaxValue   float64
	Hysteresis *float64
}

// SeverityThreshold links a threshold to a named severity
type SeverityThreshold struct {
	Severity    *Severity
	DisplayName *string
	Threshold   *Threshold
}

// ThresholdResolver is the graphql resolver for the Threshold struct
type ThresholdResolver struct {
	m Threshold
}

// MinValue is the resolver function for MinValue on the Threshold struct
func (t *ThresholdResolver) MinValue(ctx context.Context) float64 {
	return t.m.MinValue
}

// MaxValue is the resolver function for MaxValue on the Threshold struct
func (t *ThresholdResolver) MaxValue(ctx context.Context) float64 {
	return t.m.MaxValue
}

// Hysteresis is the resolver function for Hysteresis on the Threshold struct returns a pointer because Hysteresis is optional
func (t *ThresholdResolver) Hysteresis(ctx context.Context) *float64 {
	return t.m.Hysteresis
}

// SeverityThresholdResolver is the graphql resolver for the SeverityThreshold struct
type SeverityThresholdResolver struct {
	m SeverityThreshold
}

// Severity is the graphql resolve function for Severity on the SeverityThreshold struct
func (s *SeverityThresholdResolver) Severity(ctx context.Context) *Severity {
	return s.m.Severity
}

// DisplayName is the graphql resolver function for DisplayName on the SeverityThreshold struct
func (s *SeverityThresholdResolver) DisplayName(ctx context.Context) *string {
	return s.m.DisplayName
}

// Threshold is the graphql resolver function for the Threshold object on the Severity Threshold struct
func (s *SeverityThresholdResolver) Threshold(ctx context.Context) *ThresholdResolver {
	return &ThresholdResolver{m: *s.m.Threshold}
}

// SeverityThresholdInput is the input object for changes to severity threshold in the graphql resolvers
type SeverityThresholdInput struct {
	Severity    *Severity
	DisplayName *string
	Threshold   *ThresholdInput
}

// ThresholdInput is the input object for changes to threshold in the graphql resolvers
type ThresholdInput struct {
	MinValue   float64
	MaxValue   float64
	Hysteresis *float64
}
