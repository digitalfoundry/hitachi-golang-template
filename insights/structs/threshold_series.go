package structs

import "github.com/graph-gophers/graphql-go"

// ThresholdSeries represents a collection of threshold time series for a given id
type ThresholdSeries struct {
	ID         graphql.ID
	Thresholds []*TimeSeries
}

// ThresholdSeriesResolver is the graphql resolver for the ThresholdSeries struct
type ThresholdSeriesResolver struct {
	m *ThresholdSeries
}

// ID is the graphql resolver function for the ID field of the ThresholdSeries struct
func (r *ThresholdSeriesResolver) ID() graphql.ID {
	return r.m.ID
}

// Thresholds is the graphql resolver function for the Thresholds field of the ThresholdSeries struct
func (r *ThresholdSeriesResolver) Thresholds() []*TimeSeriesResolver {
	resolvers := []*TimeSeriesResolver{}
	for _, thresh := range r.m.Thresholds {
		resolvers = append(resolvers, &TimeSeriesResolver{m: *thresh})
	}

	return resolvers
}
