package structs

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func newMockThresholdSeriesResolver() *ThresholdSeries {
	return &ThresholdSeries{
		ID:         "thresh-1",
		Thresholds: []*TimeSeries{
			{
				ID: "randy",
				Timestamps: nil,
				Values: nil,
			},
		},
	}
}

func TestThresholdSeriesResolver_ID(t *testing.T) {
	threshold := newMockThresholdSeriesResolver()
	resolver := ThresholdSeriesResolver{m: threshold}

	assert.Equal(t, threshold.ID, resolver.ID())
}

func TestThresholdSeriesResolver_Thresholds(t *testing.T) {
	threshold := newMockThresholdSeriesResolver()
	resolver := ThresholdSeriesResolver{m: threshold}

	assert.Equal(t, *threshold.Thresholds[0], resolver.Thresholds()[0].m)
}
