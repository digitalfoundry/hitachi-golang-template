package structs

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func NewMockThreshold() *Threshold {
	tempHysteresis := 30.0
	return &Threshold{
		MinValue:   10,
		MaxValue:   20,
		Hysteresis: &tempHysteresis,
	}
}

func NewMockSeverityThreshold() *SeverityThreshold {
	tempDisplayName := "Display Name"
	tempSeverity := Catastrophic
	return &SeverityThreshold{
		Severity:    &tempSeverity,
		DisplayName: &tempDisplayName,
		Threshold:   NewMockThreshold(),
	}
}

func NewMockThresholdResolver(threshold *Threshold) ThresholdResolver {
	return ThresholdResolver{m: *threshold}
}

func TestSeverityThresholdResolver_DisplayName(t *testing.T) {
	mockST := NewMockSeverityThreshold()
	mockResolver := SeverityThresholdResolver{m: *mockST}
	assert.Equal(t, mockST.DisplayName, mockResolver.DisplayName(context.Background()))
}

func TestSeverityThresholdResolver_Severity(t *testing.T) {
	mockST := NewMockSeverityThreshold()
	mockResolver := SeverityThresholdResolver{m: *mockST}
	assert.Equal(t, mockST.Severity, mockResolver.Severity(context.Background()))
}

func TestSeverityThresholdResolver_Threshold(t *testing.T) {
	mockST := NewMockSeverityThreshold()
	mockResolver := SeverityThresholdResolver{m: *mockST}
	assert.Equal(t, mockST.Threshold.Hysteresis, mockResolver.Threshold(context.Background()).m.Hysteresis)
	assert.Equal(t, mockST.Threshold.MaxValue, mockResolver.Threshold(context.Background()).m.MaxValue)
	assert.Equal(t, mockST.Threshold.MinValue, mockResolver.Threshold(context.Background()).m.MinValue)
}

func TestThresholdResolver_Hysteresis(t *testing.T) {
	mockThresh := NewMockThreshold()
	mockResolver := ThresholdResolver{m: *mockThresh}
	assert.Equal(t, mockThresh.Hysteresis, mockResolver.Hysteresis(context.Background()))
}

func TestThresholdResolver_MaxValue(t *testing.T) {
	mockThresh := NewMockThreshold()
	mockResolver := ThresholdResolver{m: *mockThresh}
	assert.Equal(t, mockThresh.MaxValue, mockResolver.MaxValue(context.Background()))
}

func TestThresholdResolver_MinValue(t *testing.T) {
	mockThresh := NewMockThreshold()
	mockResolver := ThresholdResolver{m: *mockThresh}
	assert.Equal(t, mockThresh.MinValue, mockResolver.MinValue(context.Background()))
}
