package structs

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/digitalfoundry/hitachi-golang-template/insights"
)

// Throughput is the throughput struct
type Throughput struct {
	KPICalculator
	MeasurementDB insights.InfluxDBer
}

// NewThroughput creates a new throughput struct
func NewThroughput(rawDb insights.InfluxDBer, measurementDb insights.InfluxDBer, metaDB insights.ArangoDBer) *Throughput {
	return &Throughput{
		KPICalculator: KPICalculator{
			ID:         "throughput",
			RawDataDB:  rawDb,
			MetadataDB: metaDB,
		},
		MeasurementDB: measurementDb,
	}
}

// GetID returns the throughput ID
func (t *Throughput) GetID() string {
	return t.ID
}

// Calculate is the throughput calculate function
func (t *Throughput) Calculate(now time.Time) []*MeasurementEntity {
	metadata, err := t.getMetadataByID()
	if err != nil {
		insights.LogErrorWithInfo(err, "", "error querying kpi metadata for throughput calculator")
	}
	value, target, getDataErr := t.getDataFromSource(now, *metadata)
	if getDataErr != nil {
		insights.LogErrorWithInfo(getDataErr, "", fmt.Sprintf("error calculating %s", t.ID))
		return nil
	}

	fields, getMoreErr := t.getMoreFields(*metadata)
	if getMoreErr != nil {
		insights.LogErrorWithInfo(getMoreErr, "", fmt.Sprintf("error calculating %s", t.ID))
		return nil
	}

	fields["value"] = value

	tags := map[string]string{
		"assetId": "CHPP",
	}

	valueEntity := &MeasurementEntity{
		Measurement:    t.ID,
		Fields:         fields,
		Tags:           tags,
		AdditionalInfo: nil,
		Time:           now,
	}

	fields = map[string]interface{}{
		"value": target,
	}

	targetEntity := &MeasurementEntity{
		Measurement:    t.ID + "_target",
		Fields:         fields,
		Tags:           tags,
		AdditionalInfo: nil,
		Time:           now,
	}

	return []*MeasurementEntity{valueEntity, targetEntity}
}

// GetMeasurementsFromDatabase gets the throughput data from influx
func (t *Throughput) GetMeasurementsFromDatabase(now time.Time) []*MeasurementEntity {
	query := "SELECT last(value) AS value FROM \"${measurement}\", \"${measurement}_target\" WHERE \"assetId\"=$assetId AND time <= $now"
	query = strings.Replace(query, "${measurement}", t.ID, 1)

	binders := []insights.Binder{
		{
			Name:  "assetId",
			Value: "CHPP",
		},
		{
			Name:  "date",
			Value: now,
		},
	}

	response, err := t.MeasurementDB.Query(query, "TODO database", binders...)
	if err != nil {
		insights.LogErrorWithInfo(err, "", fmt.Sprintf("error querying %s for influx", t.ID))
		return nil
	}

	entities := []*MeasurementEntity{}
	for _, result := range response.Results {
		for _, series := range result.Series {
			for i, value := range series.Values {
				entity := &MeasurementEntity{
					Measurement:    series.Name,
					Fields:         nil,
					Tags:           nil,
					AdditionalInfo: nil,
					Time:           time.Time{},
				}

				col := series.Columns[i]
				if col == "time" {
					entity.Time, err = time.Parse(time.RFC3339, value[i].(string))
					if err != nil {
						insights.LogErrorWithInfo(err, "", fmt.Sprintf("error parsing throughput time value"))
					}
				} else {
					entity.Fields[col] = value
				}

				entity.Tags = map[string]string{
					"assetId": "CHPP",
				}
				entities = append(entities, entity)
			}
		}
	}

	return entities
}

func (t *Throughput) getDataFromSource(now time.Time, metadata KPIMetadata) (float64, float64, error) {
	//metadata, err := t.getMetadataByID()
	//if err != nil {
	//	insights.LogErrorWithInfo(err, "", "error querying kpi metadata for throughput calculator")
	//	return 0, 0, err
	//}

	throughputMeasurement := metadata.Query.Value.Collection
	targetMeasurement := metadata.Query.Target.Collection

	throughputQueryTemplate := metadata.Query.Value.Template
	targetQueryTemplate := metadata.Query.Target.Template

	throughputQueryTemplate = strings.Replace(throughputQueryTemplate, `"${throughputMeasurement}"`, throughputMeasurement, 1)
	throughputQueryTemplate = strings.Replace(throughputQueryTemplate, `"${targetMeasurement}"`, targetMeasurement, 1)
	throughputQueryTemplate = strings.Replace(throughputQueryTemplate, `"${date}"`, now.String(), 1)

	targetQueryTemplate = strings.Replace(targetQueryTemplate, `"${throughputMeasurement}"`, throughputMeasurement, 1)
	targetQueryTemplate = strings.Replace(targetQueryTemplate, `"${targetMeasurement}"`, targetMeasurement, 1)
	targetQueryTemplate = strings.Replace(targetQueryTemplate, `"${date}"`, now.String(), 1)

	binder := insights.Binder{
		Name:  "date",
		Value: now,
	}

	throughputResult, resultErr := t.RawDataDB.Query(throughputQueryTemplate, "KPI", binder)
	if resultErr != nil {
		insights.LogErrorWithInfo(resultErr, "", fmt.Sprintf("error querying %s for influx", t.ID))
		return 0, 0, resultErr
	}

	// todo multiple bound $date params not binding
	targetQueryTemplate = strings.Replace(targetQueryTemplate, "$date", now.Format("20060102150405"), 2)
	targetResult, targetErr := t.RawDataDB.Query(targetQueryTemplate, "KPI", binder)
	if targetErr != nil {
		insights.LogErrorWithInfo(targetErr, "", fmt.Sprintf("error querying %s for influx", t.ID))
		return 0, 0, targetErr
	}

	kpiValue, parseErr := parseResponseFirstValue(throughputResult)
	if parseErr != nil {
		return 0, 0, parseErr
	}
	kpiTarget, parseErr := parseResponseFirstValue(targetResult)
	if parseErr != nil {
		return 0, 0, parseErr
	}

	var value float64
	var err error
	if kpiValue[0] != nil {
		value, err = parseFloat(kpiValue[1])
		if err != nil {
			return 0, 0, err
		}
	}

	var target float64
	if kpiTarget[1] != nil {
		target, err = parseFloat(kpiTarget[1])
		if err != nil {
			return 0, 0, err
		}
	}

	return value, target, nil
}

// note: Java implementation uses CompletableFuture (async) in this function
func (t *Throughput) getMoreFields(metadata KPIMetadata) (map[string]interface{}, error) {
	moreFields := map[string]interface{}{}

	query := metadata.Query

	feed1 := query.CHPP_curInputFeed1
	feedResponse1, feedErr1 := t.RawDataDB.Query(feed1.Template, "KPI")
	if feedErr1 != nil {
		insights.LogErrorWithInfo(feedErr1, "", fmt.Sprintf("error querying %s for influx", t.ID))
		return nil, feedErr1
	}

	feed2 := query.CHPP_curInputFeed2
	feedResponse2, feedErr2 := t.RawDataDB.Query(feed2.Template, "KPI")
	if feedErr2 != nil {
		insights.LogErrorWithInfo(feedErr2, "", fmt.Sprintf("error querying %s for influx", t.ID))
		return nil, feedErr2
	}

	feedResult1, parseErr := parseResponseFirstValue(feedResponse1)
	if parseErr != nil {
		return nil, parseErr
	}

	feedResult2, parseErr := parseResponseFirstValue(feedResponse2)
	if parseErr != nil {
		return nil, parseErr
	}

	feedValue1 := feedResult1[0]
	moreFields["CHPP_curInputFeed1"] = feedValue1

	feedValue2 := feedResult2[0]
	moreFields["CHPP_curInputFeed2"] = feedValue2

	return moreFields, nil
}
