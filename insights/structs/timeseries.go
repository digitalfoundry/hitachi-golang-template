package structs

import (
	"context"

	"github.com/graph-gophers/graphql-go"
)

// NewTimeSeriesResolver returns a new TimeSeriesResolver given a TimeSeries
func NewTimeSeriesResolver(ts *TimeSeries) *TimeSeriesResolver {
	return &TimeSeriesResolver{m: *ts}
}

// TimeSeries represents a series of values with timestamps
type TimeSeries struct {
	ID         graphql.ID
	Timestamps []Date
	Values     *[]float64
}

// TimeSeriesResolver wraps a TimeSeries
type TimeSeriesResolver struct {
	m TimeSeries
}

// ID is the graphql resolver function of the ID filed of the TimeSeries struct
func (r *TimeSeriesResolver) ID() graphql.ID {
	return r.m.ID
}

// Timestamps is the graphql resolver function of the Timestamps filed of the TimeSeries struct
func (r *TimeSeriesResolver) Timestamps() []Date {
	return r.m.Timestamps
}

// Values is the graphql resolver function of the Values filed of the TimeSeries struct
func (r *TimeSeriesResolver) Values() *[]float64 {
	return r.m.Values
}

// EventTraceTimeSeries wraps a time series and threshold
type EventTraceTimeSeries struct {
	ID        graphql.ID
	Values    *TimeSeries
	Threshold []TimeSeries
}

// EventTraceTimeSeriesResolver wraps a EventTraceTimeSeries
type EventTraceTimeSeriesResolver struct {
	m EventTraceTimeSeries
}

// NewEventTraceTimeSeriesResolver returns a new event trace time series resolver
func NewEventTraceTimeSeriesResolver(t *EventTraceTimeSeries) *EventTraceTimeSeriesResolver {
	return &EventTraceTimeSeriesResolver{m: *t}
}

// ID returns the ID from a EventTraceTimeSeriesResolver
func (a *EventTraceTimeSeriesResolver) ID(ctx context.Context) graphql.ID {
	return a.m.ID
}

// Values returns the Values from a EventTraceTimeSeriesResolver
func (a *EventTraceTimeSeriesResolver) Values(ctx context.Context) *TimeSeriesResolver {
	return &TimeSeriesResolver{m: *a.m.Values}
}

// Threshold returns the Threshold from a EventTraceTimeSeriesResolver
func (a *EventTraceTimeSeriesResolver) Threshold(ctx context.Context) []*TimeSeriesResolver {
	resolvers := []*TimeSeriesResolver{}
	for _, t := range a.m.Threshold {
		resolvers = append(resolvers, &TimeSeriesResolver{m: t})
	}
	return resolvers
}
