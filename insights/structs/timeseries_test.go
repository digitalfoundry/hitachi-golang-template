package structs

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func newMockTimeSeries() *TimeSeries {
	return &TimeSeries{
		ID:         "ts-1",
		Timestamps: []Date{
			{
				Time: time.Now(),
			},
		},
		Values:     &[]float64{4.44, 4.44, 44.44, 4, 44},
	}
}
func TestNewTimeSeriesResolver(t *testing.T) {
	ts := newMockTimeSeries()
	resolver := NewTimeSeriesResolver(ts)

	assert.Equal(t, *ts, resolver.m)
}

func TestTimeSeriesResolver_ID(t *testing.T) {
	ts := newMockTimeSeries()
	resolver := NewTimeSeriesResolver(ts)

	assert.Equal(t, ts.ID, resolver.ID())
}

func TestTimeSeriesResolver_Timestamps(t *testing.T) {
	ts := newMockTimeSeries()
	resolver := NewTimeSeriesResolver(ts)

	assert.Equal(t, ts.Timestamps[0].Time, (resolver.Timestamps())[0].Time)
}

func TestTimeSeriesResolver_Values(t *testing.T) {
	ts := newMockTimeSeries()
	resolver := NewTimeSeriesResolver(ts)

	assert.Equal(t, (*ts.Values)[4], (*resolver.Values())[4])
}
