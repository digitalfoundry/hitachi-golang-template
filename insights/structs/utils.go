package structs

import (
	"encoding/json"
	"fmt"

	client "github.com/influxdata/influxdb1-client/v2"
)

func parseFloat(val interface{}) (float64, error) {
	json, ok := val.(json.Number)
	if !ok {
		return 0, fmt.Errorf("value nil or not a number")
	}

	value, err := json.Float64()
	if err != nil {
		return 0, err
	}

	return value, nil
}

func parseResponseFirstValue(resp *client.Response) ([]interface{}, error) {
	values, err := parseResponseFirstSeries(resp)
	if err != nil {
		return nil, err
	}

	return values[0], nil
}

func parseResponseFirstSeries(resp *client.Response) ([][]interface{}, error) {
	if resp.Results == nil || len(resp.Results) == 0 {
		return nil, fmt.Errorf("results nil or empty")
	}

	if resp.Results[0].Series == nil || len(resp.Results[0].Series) == 0 {
		return nil, fmt.Errorf("series nil or empty")
	}

	if resp.Results[0].Series[0].Values == nil || len(resp.Results[0].Series[0].Values) == 0 {
		return nil, fmt.Errorf("values nil or empty")
	}

	return resp.Results[0].Series[0].Values, nil
}
