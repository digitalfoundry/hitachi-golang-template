# Hitachi Golang Server Template Repository

## Starting the Service
In order to start the service, install docker and run the following command from the root of the repository.  The server is then hosted on localhost:8080
```bash
docker-compose up
```

## Starting the Service with Go Remote Debugging Enabled
In order to start the service and expose a go remote debugging port(2345) run the following command, then attach with a debugger.

```bash
docker-compose -f docker-compose.yml -f docker/debug-service-dc.yml up --build
```

## Setting up GOPATH
The GOPATH environment variable must be set so go understands where to install packages. (https://github.com/golang/go/wiki/GOPATH) The following can be added to a .profile or .bash_profile to setup gopath
```bash
export GOPATH=/path/to/go/directory
export PATH=$PATH:$GOPATH/bin
```

## Running golangci-lint
golangci-lint is used to run a full suite of linters over the golang code to ensure the code is in the correct style and follows defined golang patterns.(https://github.com/golangci/golangci-lint)
- Install golangci-lint:
```bash
go get -u github.com/golangci/golangci-lint/cmd/golangci-lint
```
- Run linter
```bash
golangci-lint run ./...
```

## Vendoring a Package
When adding a 3rd party dependency govendor can be used to add the dependency to the vendor directory
- Install govendor:
```bash
go get -u github.com/kardianos/govendor
```
- Add a remote dependency:
```bash
govendor fetch github.com/arangodb/go-driver
```
- Add a package from your local GOPATH
```bash
govendor add bitbucket.org/digitalfoundry/my-other-repo
```
- Update a package from your local GOPATH
```bash
govendor update bitbucket.org/digitalfoundry/my-other-repo
```

## Notes for development
- A workaround to attaching the Authorization header is to comment out the KeyCloak 'VerifyRequest' call under setupGraphqlEndpoint for the service.
- In docker-compose, the 'SSL_SERVER_CERT_PATH' and 'SSL_SERVER_KEY_PATH' configs are commented out for each service container to disable SSL due to issues with self signed certificates. Alternatively they can be excluded from the Postman call.
- In docker-compose, the directory mapping for Volumes maps each subdirectory individually to exclude the vendor directory in order to allow Go packages to be updated on build.
- In the Dockerfile of each container, govendor is installed and run to sync down the project dependencies for each container.

## Seeding InfluxDB-raw
- influxdb-raw-seeder/rawInflux_seed.txt contains seed data for the raw influx database
- When the seed file is changed, run:
```$xslt
docker-compose up influxdb-seeder
```
- You may need to restart any services that utilize the raw influx database

## Seeding ArangoDB
- If new databases, collections, or data is added to the seed files update the version at ./arangodb-seeder/dbs/dbs.json
- After updating the database version, rerun the arangodb-seeder service to update the database

